<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
	<title>Scheduled Maintenance</title>
	<!--[if lte IE 8]>
	<script type="text/javascript" charset="utf-8" src="selectivizr-min.js"></script>
	<![endif]-->
	<link rel='stylesheet' type='text/css' href='main.css' />
	<base href="" />
</head>
<style>
	.back_col {
		background-color: #c3d3e5;
	}
</style>

<body class='back_col'>
<div class="main_cont">
	<div class="main_head">
		<img src="logo.png" style='float:left'>
		<div class="main_title_padding main_title_1">Curriculum Mapping System
			<span style="color:black;">
				
			</span>
		</div>
		<div class="main_title_2 back_col round" style="width:50%">
		</div>
	</div>
	<div class="clear"></div>
	<div class="main_menu round">
	</div>

	<div class="main_pagetitle top_round back_col">
		<p>Scheduled Maintenance</p>
	</div>


	<div class="main_body">
		<p>The site is currently offline for scheduled maintenance. Please try again later.</p>
		<p>&nbsp;</p>
	</div>

	<div class="main_foot round">
		&copy; Copyright 2012
	</div>
</div>
</body>
</html>

		
