<?php

$rc = new RandContent();
for ($i = 0 ; $i < 100 ; $i++)
{
	echo $rc->paragraph() . "<br>";
}

class RandContent
{
	var $nextwords = array();
	var $words = array();
	var $wcount = 0;
	var $nextcounts = array();
	
	function __construct()
	{
		$data = explode(" ",file_get_contents("source3.txt"));
		$this->data = array();
		
		for($i = 0 ; $i < count($data)-1 ; $i++)
		{
			$word = $data[$i];
			$nextword = $data[$i+1];
			
			$this->words[] = $word;
			
			if (!array_key_exists($word,$this->nextwords))
			{
				$this->nextwords[$word] = array();
			}	
			
			$this->nextwords[$word][] = $nextword;
		}	
		
		$this->wcount = count($this->words);
		foreach ($this->nextwords as $word => &$data)
		{
			$this->nextcounts[$word] = count($data);	
		}
	}
	
	function string($count)
	{
		global $table;
		
		$words = array();
		//$word = array_rand($this->data);
		
		$word = $this->words[mt_rand(0,$this->wcount - 1)];
		$words[] = strtoupper(substr($word,0,1)) . substr($word,1);
	
		for ($i = 1 ; $i < $count ; $i++)
		{
			$word = $this->nextwords[$word][mt_rand(0,$this->nextcounts[$word]-1)];
			$words[] = $word;
		}
	
		return implode(" ",$words);
	}
	
	function sentance()
	{
		global $table;
		$count = mt_rand(4,20);
		
		$words = array();
		//$word = array_rand($this->data);
		
		$word = $this->words[mt_rand(0,$this->wcount - 1)];
		$words[] = strtoupper(substr($word,0,1)) . substr($word,1);
		
		for ($i = 1 ; $i < $count ; $i++)
		{
			$word = $this->nextwords[$word][mt_rand(0,$this->nextcounts[$word]-1)];
			$words[] = $word;
		}
		
		return implode(" ",$words) . ".";
	}
	
	function paragraph()
	{
		$output = array();
		$count = mt_rand(4,20);
		for ($i = 0 ; $i < $count ; $i++)
		{
			$output[] = $this->sentance();	
		}	
		return implode(" ",$output);
	}
}
/*
for ($i = 0 ; $i < 100 ; $i++)
{
	echo sentance() . "<br>";
}

function sentance()
{
	global $table;
	$count = mt_rand(4,20);
	
	$words = array();
	$word = array_rand($table);
	
	$words[] = strtoupper(substr($word,0,1)) . substr($word,1);
	
	for ($i = 1 ; $i < $count ; $i++)
	{
		$wcount = count($table[$word]);
		$rand = mt_rand(0,$wcount-1);
		$word = $table[$word][$rand];
		$words[] = $word;
	}
	
	return implode(" ",$words) . ". ";
}
}*/