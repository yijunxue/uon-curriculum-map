
var order_img_base;

$(document).ready(function () {
    var enableGroupSort = function() {
        $('.relations_items').slideUp();
        $('.relations_group').sortable({
            axis: 'y',
            placeholder: 'sort-highlight',
            update: function(event, ui) {
                var groupOrder = $(this).sortable('toArray', { attribute: 'data-rowid' });

                $.each(groupOrder, function(index, value) {
                    $('#group_order_' + value).val(index);
                });
            }
        });
    }

    var disableGroupSort = function() {
        $('.relations_items').slideDown();
        $('.relations_group').sortable('disable');
    }

    order_img_base = $('#order_img_base').text();
    sortArrows();

    $('.item_up').click(function (ev) {
        ev.preventDefault();
        itemChange(this, true);
    });
    $('.item_down').click(function (ev) {
        ev.preventDefault();
        itemChange(this, false);
    });
    $('.group_up').click(function (ev) {
        ev.preventDefault();
        groupChange(this, true);
    });
    $('.group_down').click(function (ev) {
        ev.preventDefault();
        groupChange(this, false);
    });

    $('#ordermode').change(function() {
        if ($(this).is(':checked')) {
            enableGroupSort();
        } else {
            disableGroupSort();
        }
    })

    $('.relations_items').sortable({
        axis: 'y',
        placeholder: 'sort-highlight',
        update: function(event, ui) {
            var outcomeOrder = $(this).sortable('toArray', { attribute: 'data-rowid' });

            $.each(outcomeOrder, function(index, value) {
                $('#order_' + value).val(index);
            });
        }
    });

});

function swapNodes(a, b) {
    var aparent = a.parentNode;
    var asibling = a.nextSibling === b ? a : a.nextSibling;
    b.parentNode.insertBefore(a, b);
    aparent.insertBefore(b, asibling);
}

function itemChange(el, isup) {
    // find current order number
    var id = $(el).attr('id');
    var groupid = id.split('_')[1];
    var itemid = id.split('_')[2];

    var input = $('#order_' + itemid);

    // find prev or next element
    var tr = getParentTr(el);

    var swaprow;
    if (isup) {
        swaprow = tr.prev();
    } else {
        swaprow = tr.next();
    }

    if (!swaprow)
        return;

    // swap values
    var swapinput = swaprow.find('input');
    var swapinputval = swapinput.val();
    swapinput.val(input.val());
    input.val(swapinputval);

    // swap rows
    swapNodes(tr[0], swaprow[0]);

    sortArrows();
}

function groupChange(el, isup) {
    // find current order number
    var id = $(el).attr('id');
    var groupid = id.split('_')[1];

    var input = $('#group_order_' + groupid);

    // find prev or next element
    var tr = getParentTr(el);
    var tr_items = tr.next();

    var swaprow;
    var swaprow_items;
    if (isup) {
        swaprow_items = tr.prev();
        if (swaprow_items)
            swaprow = swaprow_items.prev();
    } else {
        swaprow = tr_items.next();
        if (swaprow)
            swaprow_items = swaprow.next();
    }

    if (!swaprow)
        return;

    // swap values
    var swapinput = swaprow.find('.groupinput');
    var swapinputval = swapinput.val();
    swapinput.val(input.val());
    input.val(swapinputval);

    // swap rows
    swapNodes(tr[0], swaprow[0]);
    swapNodes(tr_items[0], swaprow_items[0]);

    sortArrows();
}

function getParentTr(el) {
    el = $(el);
    while (el.get(0).tagName != "TR") {
        el = el.parent();
    }

    return el;
}


function sortArrows() {
    // get array of groups
    var groups = $('.group');
    //alert(groups);

    // for all groups, sort inner items
    for (var i = 0; i < groups.length; i++) {
        var group = $(groups[i]);

        if (groups.length == 1) {
            setImages(group, true, true);
        } else if (i == 0) {
            setImages(group, true, false);
        } else if (i == groups.length - 1) {
            setImages(group, false, true);
        } else {
            setImages(group, false, false);
        }

        var id = group.attr('id').split("_")[1];
        var contents = $("#group_" + id + "_contents");

        var items = contents.find('.item');

        for (var k = 1; k < items.length - 1; k++) {
            setImages(items[k], false, false);
        }

        // set first to have no up
        setImages(items[0], true, false);

        // set last to have no down
        setImages(items[items.length - 1], false, true);

        if (items.length == 1)
            setImages(items[0], true, true);
    }

}

function setImages(el, upgrey, downgrey) {
    var el = $(el);
    var image = el.find('.up img');
    if (upgrey) {
        image.attr('src', order_img_base + "up_g.png");
    } else {
        image.attr('src', order_img_base + "up.png");
    }

    image = el.find('.down img');
    if (downgrey) {
        image.attr('src', order_img_base + "down_g.png");
    } else {
        image.attr('src', order_img_base + "down.png");
    }

}