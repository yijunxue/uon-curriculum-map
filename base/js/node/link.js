/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

var link_image_base = '';
var weight_image_base = '';

var link_add = new Object();
var link_add_count = 0;

var link_remove = new Object();
var link_remove_count = 0;

var weight_change = new Object();
var weight_change_count = 0;

if (!Object.keys) {
    Object.keys = function (obj) {
        var keys = [],
            k;
        for (k in obj) {
            if (Object.prototype.hasOwnProperty.call(obj, k)) {
                keys.push(k);
            }
        }
        return keys;
    };
}

function ToggleLink(nodeid) {

    // check if in to add,
    
    if (nodeid in link_add)
    {
        // remove from add list
        delete link_add[nodeid];
        link_add_count--;
        // set image to original
        $('img.link_img_' + nodeid).attr('src', link_image_base + 'dounlink.png');

        // remove node from to add list
        $('#list_node_' + nodeid).remove();

        // hide weights
        ShowHideWeight(nodeid, false);

        if (link_add_count == 0)
            $('#add_list_empty').show();
    }

    // check if in to remove
    else if (nodeid in link_remove) {
        // remove from remove list
        delete link_remove[nodeid];
        link_remove_count--;
        // set image to original
        $('img.link_img_' + nodeid).attr('src', link_image_base + 'dolink.png');

        // remove node from to remove list
        $('#list_node_' + nodeid).remove();

        // show weights
        ShowHideWeight(nodeid, true);

        if (link_add_count == 0)
            $('#remove_list_empty').show();
    }

    // check if already linked
    else if (nodeid in link_current)
    {
        // add to remove list
        // set image to dounlink
        $('img.link_img_' + nodeid).attr('src', link_image_base + 'dounlink.png');
        link_remove[nodeid] = 1;
        link_remove_count++;

        // add node to remove list
        AddNodeToList(nodeid, 'remove_list', 'dounlink');

        // hide weights
        ShowHideWeight(nodeid, false);

        $('#remove_list_empty').hide();

    }

    // not linked
    else {
        // add to add list
        // set image to dolink
        $('img.link_img_' + nodeid).attr('src', link_image_base + 'dolink.png');
        link_add[nodeid] = 1;
        link_add_count++;

        // add node to add list
        AddNodeToList(nodeid, 'add_list', 'dolink');

        // show weights
        ShowHideWeight(nodeid, true);

        $('#add_list_empty').hide();
    }

    // update text areas with list of elements
    UpdateTextAreas();

}

var q_temp;

function UpdateTextAreas() {
    $('#link_add').val(JSON.stringify(link_add));
    $('#link_remove').val(JSON.stringify(link_remove));
    $('#weight_change').val(JSON.stringify(weight_change));


    if (using_weight) {
        // set up display of weight listing if there are any weight changes
        if (Object.keys(weight_change).length > 0) {
            $('#weight_list_title').show();

            for (var i in weight_change) {

                var key = '#weight_node_' + i;
                if ($(key).length == 0) {
                    //alert("Adding " + i);
                    var url = $('#link_title_url').text().replace("XXXX", i);
                    $('#weight_list').append("<div id='weight_node_" + i + "' class='weight_nodes' style='clear:both;' w='" + weight_change[i] + "'><div style='float:right'><img src='" + weight_image_base + weight_change[i] + ".png'></div><span id='weight_node_title_" + i + "'></span></div>");
                    $('#weight_node_title_' + i).load(url);
                } else {
                    // exists, so check that the weight is still correct, if not update it
                    var curw = $(key).attr('w');

                    // different, update element
                    if (curw != weight_change[i]) {
                        //alert("updating " + i);
                        $(key).attr('w', weight_change[i])
                        $(key + " div img").attr('src', weight_image_base + weight_change[i] + ".png");
                    }
                }

            }

            // loop through all elements in the div, and make sure they are all in the weight_change array. If not remove the html
            $('#weight_list').children().each(function () {
                var id = $(this).attr('id').split('_')[2];

                if (id in weight_change) {
                    // ok
                } else {
                    $(this).remove();
                }

            });

            $('#weight_list').show();
        } else {
            $('#weight_list_title').hide();
            $('#weight_list').hide();
        }

        // update any weights that are in the to add list
        if (Object.keys(link_add).length > 0) {

            for (var i in link_add) {

                var key = '#list_node_' + i;
                if ($(key).length > 0) {
                    // exists, so check that the weight is still correct, if not update it
                    var curw = $(key).attr('w');

                    // different, update element
                    if (curw != link_add[i]) {
                        $(key).attr('w', link_add[i])
                        $(key + " div img.wi").attr('src', weight_image_base + link_add[i] + ".png");
                    }
                }
            }
        }
    }

    if (link_add_count == 0 && link_remove_count == 0 && Object.keys(weight_change).length == 0) {
        $('#save_link').attr('disabled', 'disabled');
    } else {
        $('#save_link').removeAttr('disabled');
    }
}

function AddNodeToList(nodeid, list, img) {
    var url = $('#link_title_url').text().replace("XXXX", nodeid);
    $.get(url, function (data) {
        var html = "<div id='list_node_" + nodeid + "' style='clear:both;'><div style='float:right'>";
        if (using_weight)
            html += "<img class='wi' id='added_weight_" + nodeid + "' style='padding-right:3px' src='" + weight_image_base + "100.png'>";
        html += "<img src='" + link_image_base + img + ".png'></div>" + data + "</div>";
        $('#' + list).append(html);
    });
}

function UpdateLinkImages() {
    //alert("UpdateLinkImages");

    // reset all images (no longer needed)
    //

    for (itemid in link_current) {
        $('img.link_img_' + itemid).attr('src', link_image_base + 'dolink.png');
    }

    for (itemid in link_add) {
        $('img.link_img_' + itemid).attr('src', link_image_base + 'dolink.png');
    }
    for (itemid in link_remove) {
        $('img.link_img_' + itemid).attr('src', link_image_base + 'dounlink.png');
    }
}

$(document).ready(function () {

    link_image_base = $('#link_img_src').attr('src').replace('blank.png', '');
    weight_image_base = link_image_base.replace("link", "weight");

    UpdateLinkImages();

    // modify ajax filter to call updatelinkimages after every ajax call (so images are updated when we have new data loaded
    $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
        jqXHR.complete(function () {
            UpdateLinkImages();
        });
    });

});

// weight code

var using_weight = 0;

$(document).ready(function () {
    if ($('.wp_popup').length > 0) // only init if weighting in use
    {
        using_weight = 1;

        $('.wp_popup').appendTo("body");
        $('.wp_popup').hide();

        $('.wp_close').click(function (ev) {
            $('.wp_popup').hide();

        });
    }
});

var weight_node = null;

function SetWeight(weight) {
    // return if no weights in use
    if ($('.wp_popup').length == 0)
        return;

    // build element queries
    var img = '#link_weight_' + weight_node + ' img';
    var div = '#link_weight_' + weight_node;

    // get and modify the image source
    $(img).attr('src', weight_image_base + weight + ".png");

    // hide the popup
    $('.wp_popup').hide();

    // change tooltip text
    $(div).attr('title', weight + "% weight<br>Click to change");

    // add weight change to changes tab
    if (weight_node in link_add) {
        link_add[weight_node] = weight;
    } else {
        weight_change[weight_node] = weight;
    }

    UpdateTextAreas();
}

function ShowHideWeight(nodeid, should_show) {
    // return if no weights in use
    if ($('.wp_popup').length == 0)
        return;

    if (should_show) {
        $('div.link_weight_' + nodeid).show();
        weight_node = nodeid;
        SetWeight(100);
    } else {
        $('div.link_weight_' + nodeid).hide();

        // remove the item from the weight_change array
        delete weight_change[nodeid];
    }
}

function WeightLink(elem) {
    weight_node = $(elem).attr('id').split('_')[2];
    $('.wp_popup').show();
    $('.wp_popup').css('left', $(elem).offset().left - 189 + 'px');
    $('.wp_popup').css('top', $(elem).offset().top - 12 + 'px');
}