/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

// generic node expand code
function LoadSubNodes(nodeid, placement, path) {

    var el = $("#subrels_" + placement);
    if (el.html() == "") {
        var url = ajax_base_url;
        url = url.replace("XXTYPEXX", "related");
        url = url.replace("XXIDXX", nodeid);
        url = url + "?path=" + path + "," + nodeid;
        el.css('display', 'block');
        //el.html("Please Wait...");// loading " + url);
        $.get(url, function (data) {
            el.html(data);
        });

        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('add', 'delete'));
    } else if (el.css('display') == 'block') {
        el.css('display', 'none');
        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('delete', 'add'));
    } else {
        el.css('display', 'block');
        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('add', 'delete'));
    }

    return false;
}
