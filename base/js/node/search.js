/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

var current_type = 0;
var current_search = '';
var search_no = 0;

$(document).ready(function () {
    setTimeout("update_search()", 1);

    $('#search_box').change(function () {
        update_search();
    });
    $('#node_type').change(function () {
        update_search();
    });
    $('#search_box').keyup(function () {
        update_search();
    });
});

function update_search() {
    var new_search = $('#search_box').val();
    var new_type = $('#node_type').val();

    if (new_search == "") {
        $('#search_results').html("");
    }

    if ((new_search == current_search && new_type == current_type) || new_search == "")
        return;

    current_search = new_search;
    current_type = new_type;

    search_no++;
    //$('#search_results').html("Searcing - " + current_type + " - " + current_search  + " - " + search_no + "<br>" + new_search);

    var url = ajax_url_search;
    url = url.replace("XXTYPEXX", "search");
    url = url.replace("XXSEARCHXX", encodeURIComponent(current_search));
    url = url.replace("XXTYPEIDXX", encodeURIComponent(current_type));

    $('#search_results').load(url);
}