// create and edit javascript

// Handle the KVPair stuff when editing or creating a node
$(document).ready(function () {
    $('.kv_pair_select').each(function () {
        ChangeKVPair(this);
    });
    $('.kv_pair_select').change(function (ev) {
        ChangeKVPair(this);
    });

    $('input:checkbox.confirm').click(cbConfirm);
});

function ChangeKVPair(elem) {
    var id = $(elem).attr('id');

    if ($(elem).val() == "") { // selected "New Group"
        $('#' + id + '_exgroup').hide();
        $('#' + id + '_newgroup').show();
    } else { // selected anything else
        $('#' + id + '_exgroup').show();
        $('#' + id + '_newgroup').hide();

        var desc = decodeURIComponent(kvpairs[id][$(elem).val()]);
        //var desc = decval.split("~")[1];
        if (desc.trim() == "") {
            $('#' + id + '_description').html("<i>There is no description for this group</i>");
        } else {
            $('#' + id + '_description').html(desc);
        }
    }
}

function cbConfirm(ev) {
    if ($(this).is(':checked')) {
        if(!confirm("Are you sure?")) {
            ev.preventDefault();
            return;
        }
    }
}