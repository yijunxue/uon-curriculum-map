/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

function ExpandCloneChildren(nodeid, placement) {

    var el = $("#subrels_" + placement);
    if (el.html() == "") {
        var url = base_url + "clonenode/relnodes/" + nodeid;
        el.css('display', 'block');
        el.html("Please Wait... loading " + url);
        $.get(url, function(data) {
            el.html(data);
            setupTooltips();
        });

        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('add', 'delete'));
    } else if (el.css('display') == 'block') {
        el.css('display', 'none');
        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('delete', 'add'));
    } else {
        el.css('display', 'block');
        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('add', 'delete'));
    }

    return false;
}

var title_edits = new Object();
var node_exclude = new Object();

$(document).ready(function() {
    setupTooltips();
    setupChangedTooltips();
    setupHelpTooltips();

    $('#dataset').change(function() {
        if ($('#dataset').val() == current_dataset) {
            ChooseNode($('#orig_parentid').val());
        } else {
            $('#parentid').val("");
            $('#node_title').html("Please Select a new Parent Node");
        }
    });

    $('input[name=skip_parent]').change(function() {
        if ($('#dataset').val() == current_dataset && !$(this).is(':checked')) {
            ChooseNode($('#orig_parentid').val());
        } else {
            $('#parentid').val("");
            $('#node_title').html("Please Select a new Parent Node");
        }
    });

    $('#clone_form').submit(function(ev) {
        var ok = true;

        if ($('#parentid').val() == "") {
            alert("Please select a parent node");
            ok = false;
        }

        if (!ok) {
            ev.preventDefault();
        }
    });
});

function setupTooltips() {
    $('.hasTooltip').qtip({
        position: {
            my: 'top right',
            at: 'bottom left',
            adjust: {
                x: -5,
                y: -18
            }
        },
        style: {
            classes: 'ui-tooltip-rounded ui-tooltip-shadow'
        },
    });
}

function setupChangedTooltips() {
    $('.hasTooltipChanged').qtip({
        position: {
            my: 'middle left',
            at: 'middle right',
        },
        style: {
            classes: 'ui-tooltip-rounded ui-tooltip-shadow ui-tooltip-pad'
        },
    });
}

function setupHelpTooltips() {
    $('.hasTooltipHelp').qtip({
        position: {
            my: 'middle left',
            at: 'middle right',
        },
        style: {
            classes: 'ui-tooltip-rounded ui-tooltip-shadow ui-tooltip-pad'
        },
    });
}

function editNode(nodeid) {
    var div = $('#node_title_' + nodeid);
    var span = $('#node_title_' + nodeid + ' span.clone_node_title');
    if (span.is(":hidden")) {
        closeEditNode(nodeid)
    } else {
        var curtitle = span.text();
        span.hide();
        div.append("<input value='" + $.trim(curtitle) + "' class='clone_edit_box' id='node_input_" + nodeid + "'>");
        var input = $('#node_input_' + nodeid);
        var changed = $('#node_changed_' + nodeid);
        changed.hide();
        input.blur(function() {
            closeEditNode(nodeid);
        });
        input.focus();

    }
}

function closeEditNode(nodeid) {
    var div = $('#node_title_' + nodeid);
    var span = $('#node_title_' + nodeid + ' span.clone_node_title');
    var input = $('#node_input_' + nodeid);
    if (!input) return;

    var changed = $('#node_changed_' + nodeid);
    var text = $.trim(input.val());
    //if (text.length() == 0) return;
    if ($.trim(span.text()) != text) {
        changed.attr('haschanged', 1);
        setupChangedTooltips();
        span.text(text);
        addChangedText(nodeid, text);
    }

    if (changed.attr('haschanged') > 0) changed.show();

    input.remove();
    span.show();
}

function getnodeid(nodeid) {
    return $('#node_id_' + nodeid).text();
}

function addChangedText(nodeid, text) {
    nodeid = getnodeid(nodeid);
    // store text change for posting to use when we clone the nodes
    title_edits[nodeid] = text;

    $('#title_edits').val(JSON.stringify(title_edits));
}

function updateNodeState(nodeid, newstate) {
    nodeid = getnodeid(nodeid);

    if (newstate == 0) {
        delete node_exclude[nodeid];
    } else {
        node_exclude[nodeid] = newstate;
    }

    $('#node_exclude').val(JSON.stringify(node_exclude));
}

function dump_title_edits() {
    var text = "";
    title_edits.dump();
}


// TODO: Need to skip the partial node option if there are no children of the node in question

function toggleNode(nodeid) {
    var image = $('#image_toggle_' + nodeid);
    var hidden = image.attr('excluded');
    if (hidden == 1) {
        excludeNode(nodeid);
    } else if (hidden == 2) {
        includeNode(nodeid);
    } else {
        partialNode(nodeid);
    }
}

function setimage(el, image) {
    var src = el.attr('src');
    src = src.substr(0, src.lastIndexOf("/"));
    src += "/" + image + ".png";
    el.attr('src', src);
}

function excludeNode(nodeid) {
    var image = $('#image_toggle_' + nodeid);
    image.attr('excluded', '2');
    setimage(image, "cross");
    $('#node_expand_' + nodeid).hide();
    $('#image_edit_' + nodeid).hide();
    $('#subrels_' + nodeid).hide();

    updateNodeState(nodeid, 2);
}

function partialNode(nodeid) {
    var image = $('#image_toggle_' + nodeid);
    image.attr('excluded', '1');
    setimage(image, "partial");
    $('#node_expand_' + nodeid).hide();
    $('#image_edit_' + nodeid).show();
    $('#subrels_' + nodeid).hide();

    updateNodeState(nodeid, 1);
}

function includeNode(nodeid) {
    var image = $('#image_toggle_' + nodeid);
    image.attr('excluded', '0');
    setimage(image, "tick");
    $('#node_expand_' + nodeid).show();
    $('#image_edit_' + nodeid).show();
    $('#subrels_' + nodeid).show();

    updateNodeState(nodeid, 0);
}

function toggleNodeType(target) {
    var image = $('#type_toggle_' + target);
    var hidden = image.attr('excluded');
    if (hidden == 1) {
        image.attr('excluded', '2');
        $('.section_row_' + target).show();
        setimage(image, "cross");

        // need to find all node ids from within this section and add them to the ignore list
        $('.section_row_' + target).each(function() {
            var nodeid = $(this).attr('id').split('_')[1];
            excludeNode(nodeid);
        });
    } else if (hidden == 2) {
        image.attr('excluded', '0');
        $('.section_row_' + target).show();
        setimage(image, "tick");

        // need to find all node ids from within this section and add them to the ignore list
        $('.section_row_' + target).each(function() {
            var nodeid = $(this).attr('id').split('_')[1];
            includeNode(nodeid);
        });
    } else {
        image.attr('excluded', '1');
        $('.section_row_' + target).show();
        setimage(image, "partial");
        // need to find all node ids from within this section and add remove them from the ignore list
        $('.section_row_' + target).each(function() {
            var nodeid = $(this).attr('id').split('_')[1];
            partialNode(nodeid);
        });
    }
}


$(document).ready(function() {
    $('.choose_node').click(function(ev) {
        ev.preventDefault();
        var source = $('#parentid');
        var current = source.val();
        var dataset = $('#dataset').val();
        // need to do a popup window with the following url:
        if (current_dataset == dataset) {
            var url = base_url + 'select/index/';
            if (!$('input[name=skip_parent]').is(':checked')) {
                url += current;
            } else {
                url += base_nodeid;
            }
            url += '/parentid';
        } else {
            var url = root_url + dataset + '/select/type/' + node_type;
            if (!$('input[name=skip_parent]').is(':checked')) {
                url += '/parent';
            }
        }
        TINY.box.show({
            iframe: url,
            width: 800,
            height: 600
        });
    });
});


function ChooseNode(nodeid) {
    var dataset = $('#dataset').val();
    $('#node_title').html('');
    $('#parentid').val(nodeid);
    var url = root_url + dataset + '/select/title/' + nodeid;
    $('#node_title').load(url, function(response, status, xhr) {
        if (status == "error" || response == '') {
            $('#node_title').html(url);
        }
    });
    $('#node_title').effect("highlight", {}, 2000);
}