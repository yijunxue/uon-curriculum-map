// view node js code

// section tree expand code
function TreeExpand(rand, nodeid, placement) {

    var el = $("#subrels_" + placement);
    var html = String(el.html());
    if (html.trim() == "") {
        var url_div_name = 'tree_url_' + rand;
        var url = $('#' + url_div_name).text();
        url = url.replace("XXIDXX", nodeid);
        el.css('display', 'block');
        //el.html("Please Wait... " + url + "<br>");
        $.get(url, function (data) {
            el.html(data);
            setupMainTooltips();
        });

        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('add', 'delete'));
    } else if (el.css('display') == 'block') {
        el.css('display', 'none');
        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('delete', 'add'));
    } else {
        el.css('display', 'block');
        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('add', 'delete'));
    }

    

    return false;
}

// toggle node for treerev
function ToggleElement(elid) {
    var el = $("#subrels_" + elid);
    if (el.css('display') == 'block') {
        el.css('display', 'none');
        var img = $('#image_' + elid);
        img.attr('src', img.attr('src').replace('delete', 'add'));
    } else {
        el.css('display', 'block');
        var img = $('#image_' + elid);
        img.attr('src', img.attr('src').replace('add', 'delete'));
    }
}

// nodetree expand code

function ExpandNodeTree(rand, nodeid, placement) {
    var el = $("#subrels_" + placement);
    if (el.html() == "") {
        var url_div_name = "nodetree_url_" + rand;
        var url = $('#' + url_div_name).text();
        url = url.replace("XXIDXX", nodeid);
        el.css('display', 'block');
        //el.html("Please Wait... " + url);
        $.get(url, function (data) {
            el.html(data);
        });

        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('add', 'delete'));
    } else if (el.css('display') == 'block') {
        el.css('display', 'none');
        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('delete', 'add'));
    } else {
        el.css('display', 'block');
        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('add', 'delete'));
    }

    return false;
}



// long tree search nodes and pagination

var cursearch = new Array();
var curbase = new Array();

function setOffset(section, offset, search, base) {
    var url = ajax_base_url;
    url = url.replace("XXTYPEXX", "longtree_page");
    url = url.replace("XXIDXX", section);
    url = url + "/" + offset;
    if (search)
        url = url + "/" + search;
    else if (cursearch[section])
        url = url + "/" + cursearch[section];
    else
        url = url + "/-";

    var resultdiv = $('#longtree_' + section);

    $.get(url, function (data) {
        resultdiv.html(data);
    });
}

function searchUpdate(section) {
    var text = $('#search_' + section).val();
    cursearch[section] = text;
    setOffset(section, 1, text);
}

function searchReset(section) {
    $('#search_' + section).val("");
    cursearch[section] = "";
    setOffset(section, 1);
}
