
$(document).ready(function () {
    $('#areyousure').click(function (ev) {
        validateForm();
    });
});

function validateForm() {
    var ok = 1;

    if (!$('#areyousure').is(':checked'))
        ok = 0;

    if (ok == 1) {
        $('#do_delete').removeAttr('disabled');
    } else {
        $('#do_delete').attr('disabled', 'disabled');
    }
}
