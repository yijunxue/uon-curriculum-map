/* SORT TOOLTIPS AND REMOVE THIS FILE */


$(document).ready(function () {
    setupTooltips();
    setupChangedTooltips();
    setupHelpTooltips();
});

function setupTooltips() {
    $('.hasTooltip').qtip({
        position: {
            my: 'top right', 
            at: 'bottom left',
            adjust: {
                x: -5,
                y: -18
            }
        },
        style: {
            classes: 'ui-tooltip-rounded ui-tooltip-shadow ui-tooltip-pad'
        },
    });
}

function setupChangedTooltips()
{
    $('.hasTooltipChanged').qtip({
        position: {
            my: 'middle left', 
            at: 'middle right',
        },
        style: {
            classes: 'ui-tooltip-rounded ui-tooltip-shadow ui-tooltip-pad'
        },
    });
}

function setupHelpTooltips()
{
    $('.hasTooltipHelp').qtip({
        position: {
            my: 'middle left', 
            at: 'middle right',
        },
        style: {
            classes: 'ui-tooltip-rounded ui-tooltip-shadow ui-tooltip-pad'
        },
    });

    $('.hasTooltipAbove').qtip({
        position: {
            my: 'bottom middle', 
            at: 'top middle'
			,
            adjust: {
                x: 0,
                y: -5
            }
        },
        style: {
            classes: 'ui-tooltip-rounded ui-tooltip-shadow ui-tooltip-pad'
        },
    });
}

