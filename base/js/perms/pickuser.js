// js for the select user and group for permissions

// expand out the group pick list
function ExpandGroup(nodeid, placement, path) {

    var el = $("#subrels_" + placement);
    if (el.html() == "") {
        var url = base_url + "perms/ajax/groups/" + nodeid;
        url = url + "?path=" + path + "," + nodeid;

        el.css('display', 'block');
        //el.html("Please Wait...");// loading " + url);
        $.get(url, function (data) {
            el.html(data);
        });

        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('add', 'delete'));
    } else if (el.css('display') == 'block') {
        el.css('display', 'none');
        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('delete', 'add'));
    } else {
        el.css('display', 'block');
        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('add', 'delete'));
    }

    return false;
}

// select the user
function Popup_ChooseGroup(nodeid) {
    window.parent.TINY.box.hide();
    window.parent.ChooseGroup(nodeid);
    return false;
}

// User search javascript
var cursearch = new Array();
var curbase = new Array();

$(document).ready(function () {

    $('#user_search').change(function () {
        searchUsers();
    });

    $('#user_search').keyup(function () {
        searchUsers();
    });
});

function offsetUsers(offset, search) {
    var url = base_url + "perms/ajax/user_find/";
    url = url + $('#user_group').val(); // add groupid
    url = url + "/" + offset; // add page
    if (search) // add search term if any
        url = url + "/" + search;
    else if (usersearch)
        url = url + "/" + usersearch;
    else
        url = url + "/-";

    var resultdiv = $('#user_search_results');

    //resultdiv.html(url);
    $.get(url, function (data) {
        resultdiv.html(data);
    });
}

function searchUsers() {
    var text = $('#user_search').val();
    usersearch = text;
    offsetUsers(1, text);
}

function resetUsers() {
    $('#user_search').val("");
    usersearch = "";
    offsetUsers(1);
}

