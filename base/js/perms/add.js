var node_exclude = new Object();
var cal_from;
var cal_to;

$(document).ready(function () {
    // please wait on submit
    $('#permadd_form').submit(function () {
        $('#main_table').hide();
        $('.please_wait').show();
        $('button').attr('disabled', 'disabled');
    });

    // event for showing and hiding child selection
    $('input[name=target]').change(function () {
        ShowHideChildren();
    });
    ShowHideChildren();

    // set up pick user/group popup
    $('.choose_group').click(function (ev) {
        ev.preventDefault();
        var source = $('#groupid');
        var current = source.val();
        // need to do a popup window with the following url:
        var url = base_url + 'perms/pickgroup/' + current;
        TINY.box.show({ iframe: url, width: 800, height: 600 });
    });

    // calendar init
    cal_from = new dhtmlXCalendarObject(['date_from']);
    cal_from.setSkin('omega');
    cal_from.setDateFormat('%Y-%m-%d %H:%i');
    cal_from.setPosition('right');

    cal_to = new dhtmlXCalendarObject(['date_to']);
    cal_to.setSkin('omega');
    cal_to.setDateFormat('%Y-%m-%d %H:%i');
    cal_to.setPosition('right');

    $('input[name=date]').change(function (ev) {
        sortDateFieldDisplay();
    });
    sortDateFieldDisplay();

});

// decide if to show child list or not
function ShowHideChildren() {
    var current = $('input[name=target]:checked').val();
    if (current == "node") {
        $('#children_disp').hide();
    } else {
        $('#children_disp').show();
    }
}

// set all permissions to a specific value
function setAll(value) {
    for (perm in perms) {
        perm = perms[perm];
        $('#p' + value + '_' + perm).attr('checked', 'checked');
    }
}

// a group has been chosen, so reload its permissions
function ChooseGroup(nodeid) {
    var origGroupId = $('#origgroupid').val();


    var url = base_url + 'perms/groupinfo/' + base_nodeid + '/' + nodeid;

    $('#groupid').val(nodeid);

    $.get(url, function (data) {
        //alert(data);
        var result = JSON.parse(data);

        $('#node_title').html(result.title);

        // Don't reset the permissions if we're reassigning
        if (typeof origGroupId == 'undefined' || origGroupId == '') {
            for (perm in perms) {
                perm = perms[perm];
                $('#curperm_' + perm).html(result['perms'][perm]);
            }

            if (result['perms']['date'] != "") {
                $('#date_to').val(result['perms']['to_date']);
                $('#date_from').val(result['perms']['from_date']);
                $('#date_opt_' + result['perms']['date']).attr('checked', 'checked');

                sortDateFieldDisplay();
            }
        }

        $('#select_warn').hide();
        $('#add_save').removeAttr('disabled');
    });
}

// expand list of child nodes
function ExpandAddChildren(nodeid, placement) {

    var el = $("#subrels_" + placement);
    if (el.html() == "") {
        var url = base_url + "perms/ajax/add_children/" + nodeid;
        el.css('display', 'block');
        //el.html("Please Wait... loading " + url);
        $.get(url, function (data) {
            el.html(data);
            setupTooltips();
        });

        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('add', 'delete'));
    } else if (el.css('display') == 'block') {
        el.css('display', 'none');
        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('delete', 'add'));
    } else {
        el.css('display', 'block');
        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('add', 'delete'));
    }

    return false;
}


// show and hide date fields
function sortDateFieldDisplay() {
    var current = $('input[name=date]:checked').val();
    if (current == "any") {
        $('.from_date').hide();
        $('.to_date').hide();
    } else if (current == "from") {
        $('.from_date').show();
        $('.to_date').hide();
    } else if (current == "to") {
        $('.from_date').hide();
        $('.to_date').show();
    } else if (current == "range") {
        $('.from_date').show();
        $('.to_date').show();
    }
}

// sort allowed date range on from date
function clickFrom() {
    var value = $('input[name=date]:checked').val();

    // range minimum is always today, max is determined by to date
    if (value == "range" && $('#date_from').val() != "") {
        cal_from.setSensitiveRange(new Date().format("yyyy-mm-dd HH:MM"), $('#date_from').val());
    } else {
        cal_from.setSensitiveRange(new Date().format("yyyy-mm-dd HH:MM"), null);
    }
}

// sort out allowed date range on to date
function clickTo() {
    var value = $('input[name=date]:checked').val();


    if (value == "range" && $('#date_from').val() != "") {
        cal_to.setSensitiveRange($('#date_from').val(), null);
    } else {
        cal_to.setSensitiveRange(new Date().format("yyyy-mm-dd HH:MM"), null);
    }
}

// code to handle toggle inclusion of nodes

function getnodeid(nodeid) {
    return $('#node_id_' + nodeid).text();
}

function updateNodeState(nodeid, newstate) {
    nodeid = getnodeid(nodeid);

    if (newstate == 0) {
        delete node_exclude[nodeid];
    } else {
        node_exclude[nodeid] = newstate;
    }

    $('#node_exclude').val(JSON.stringify(node_exclude));
}

function toggleNode(nodeid) {
    var image = $('#image_toggle_' + nodeid);
    var hidden = image.attr('excluded');
    if (hidden == 1) {
        excludeNode(nodeid);
    } else if (hidden == 2) {
        includeNode(nodeid);
    } else {
        partialNode(nodeid);
    }
}

function setimage(el, image) {
    var src = el.attr('src');
    src = src.substr(0, src.lastIndexOf("/"));
    src += "/" + image + ".png";
    el.attr('src', src);
}

function excludeNode(nodeid) {
    var image = $('#image_toggle_' + nodeid);
    image.attr('excluded', '2');
    setimage(image, "cross");
    $('#node_expand_' + nodeid).hide();
    $('#subrels_' + nodeid).hide();

    updateNodeState(nodeid, 2);
}

function partialNode(nodeid) {
    var image = $('#image_toggle_' + nodeid);
    image.attr('excluded', '1');
    setimage(image, "partial");
    $('#node_expand_' + nodeid).hide();
    $('#subrels_' + nodeid).hide();

    updateNodeState(nodeid, 1);
}

function includeNode(nodeid) {
    var image = $('#image_toggle_' + nodeid);
    image.attr('excluded', '0');
    setimage(image, "tick");
    $('#node_expand_' + nodeid).show();
    $('#subrels_' + nodeid).show();

    updateNodeState(nodeid, 0);
}

function toggleNodeType(target) {
    var image = $('#type_toggle_' + target);
    var hidden = image.attr('excluded');
    if (hidden == 1) {
        image.attr('excluded', '2');
        $('.section_row_' + target).hide();
        setimage(image, "cross");

        // need to find all node ids from within this section and add them to the ignore list
        $('.section_row_' + target).each(function () {
            var nodeid = $(this).attr('id').split('_')[1];
            excludeNode(nodeid);
        });
    } else if (hidden == 2) {
        image.attr('excluded', '0');
        $('.section_row_' + target).show();
        setimage(image, "tick");

        // need to find all node ids from within this section and add them to the ignore list
        $('.section_row_' + target).each(function () {
            var nodeid = $(this).attr('id').split('_')[1];
            includeNode(nodeid);
        });
    } else {
        image.attr('excluded', '1');
        $('.section_row_' + target).show();
        setimage(image, "partial");
        // need to find all node ids from within this section and add remove them from the ignore list
        $('.section_row_' + target).each(function () {
            var nodeid = $(this).attr('id').split('_')[1];
            partialNode(nodeid);
        });
    }
}
