// js for listing permissions on a node

function ExpandListChildren(nodeid, placement) {

    var el = $("#subrels_" + placement);
    if (el.html() == "") {
        var url = base_url + "perms/ajax/list_children/" + nodeid;
        el.css('display', 'block');
        //el.html("Please Wait... loading " + url);
        $.get(url, function (data) {
            el.html(data);
            setupTooltips();
        });

        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('add', 'delete'));
    } else if (el.css('display') == 'block') {
        el.css('display', 'none');
        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('delete', 'add'));
    } else {
        el.css('display', 'block');
        var img = $('#image_' + placement);
        img.attr('src', img.attr('src').replace('add', 'delete'));
    }

    return false;
}
