// js for removing permission from a group

$(document).ready(function (ev) {

    // show please wait on form submit
    $('#permdel_form').submit(function () {
        $('#main_table').hide();
        $('.please_wait').show();
        $('button').attr('disabled', 'disabled');
    });
});
