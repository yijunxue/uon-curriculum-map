/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

// add trim for IE
if(typeof String.prototype.trim !== 'function') {
  String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, ''); 
  }
}


$(document).ready(function () {

    // handle any links with class a_popup
    $('a.a_popup').click(function (ev) {
        ev.preventDefault();
        var url = $(this).attr('href');
        var width = 800;
        var height = 600;

        var rel = $(this).attr('rel');
        if (rel) {
            width = rel.split('x')[0];
            height = rel.split('x')[1];
        }
        TINY.box.show({ iframe: url, width: width, height: height });
    });

    // node popup menu
    $('.main_nodemenu').click(function(event) {
        showPopupMenu(event);
    });

    setupMainTooltips();

    // modify ajax filter to call updatelinkimages after every ajax call so highlights are updated when we have new data loaded
    $.ajaxPrefilter(function (options, originalOptions, jqXHR) {
        jqXHR.complete(function () {
            SetupHighlights();
        });
    });
    SetupHighlights();

    $('.collremoveconfirm').click( function (ev)
    {
        if (!confirm("Are you sure you want to remove this item from the collection?"))
            ev.preventDefault();
    });
});

function ChangeRole()
{
    var role = $('#role').val();
    var url = base_url + "auth/role/" + role + "/" + Base64.encode(document.URL);
    window.location = url;
}

// show popup menu
function showPopupMenu(event) {
    event.stopPropagation();

    var pos = $('.main_nodemenu').offset();

    var w1 = $('.main_nodemenu').outerWidth(true);
    var w2 = $('.main_nodemenu_popupcont').outerWidth(true);
    //alert(pos);
    $('.main_nodemenu_popupcont').css('left', (pos.left - 1) + 'px');
    $('.main_nodemenu_popupcont').css('top', (pos.top + 1) + 'px');
    $('.main_nodemenu_popupcont').show();
    $('body').click(hidePopupMenu);
}

function hidePopupMenu() {
    $('.main_nodemenu_popupcont').hide();
    $('body').unbind('click');
}

function setupMainTooltips() {
    $('.hasTooltip_wide').qtip({
        style: {
            classes: 'ui-tooltip-rounded ui-tooltip-shadow ui-tooltip-wide ui-tooltip-pad'
        }
    });
    $('.hasTooltip_bl').qtip({
        position: {
            my: 'top right', 
            at: 'bottom left',
            adjust: {
                x: -5,
                y: -18
            }
        },
        style: {
            classes: 'ui-tooltip-rounded ui-tooltip-shadow ui-tooltip-pad'
        }
    });
    
    $('.hasTooltip').qtip({
        style: {
            classes: 'ui-tooltip-rounded ui-tooltip-shadow ui-tooltip-pad'
        }
    });
}

// setup highlights for row mouseovers
function SetupHighlights()
{
    $('.relations_title').mouseover( function (ev) {
        $(this).css('background-color','#ddd');
    });
    $('.relations_title').mouseout( function (ev) {
        $(this).css('background-color','');
    });
}