// dev tools js

$(document).ready( function () 
{
    // toggle debug
	$('#debug_stats').click(function (ev) {
		ev.preventDefault();
		var url = base_url + 'auth/toggle_debug';
		$('#debug_stats').html("Please Wait!");
		$.get(url, function (data) {
			location.reload(true);
		});
	});
    // toggle template
	$('#debug_template').click(function (ev) {
		ev.preventDefault();
		var url = base_url + 'auth/toggle_templates';
		$('#debug_template').html("Please Wait!");
		$.get(url, function (data) {
			location.reload(true);
		});
	});
});