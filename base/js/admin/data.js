// admin data tools js

// handle creating of fake cm data
$(document).ready( function () {
	$('#fake_start').click( function (ev) {
		ev.preventDefault();
		
		var url = base_url + 'admindata/fake/yes';
		$('#fake_link').css('display', 'none');
		$('#fake_inprogress').css('display', 'block');
		$('#fake_complete').css('display', 'none');
		
		$.get(url, function (data) {
			$('#fake_inprogress').css('display', 'none');
			$('#fake_link').css('display', 'block');
			$('#fake_result').html(data);
			$('#fake_complete').css('display', 'block');
		});
	});
});

// refresh the dataset stats
function loadstats() {
    var url = base_url + 'admindata/stats';
    $.get(url, function (data) {
        $('#dataset_stats').html(data);
    });
}

// refresh the node stats
function loadnodestats() {
    var url = base_url + 'admindata/nodestats';
    $.get(url, function (data) {
        $('#node_stats').html(data);
    });
}

// reset database handler
$(document).ready( function () {
	$('#reset_data_confirm').click(function (ev) {
		ev.preventDefault();
		var url = base_url + 'admindata/reset/yes';
		$.get(url, function(data) {
			$('#clear_complete').css('display', 'block');
			setTimeout("hide_Complete_reset()",10000);
		});
	});
});

// hide the reset complete display after a bit
function hide_Complete_reset()
{
	$('#clear_complete').css('display', 'none');
}
