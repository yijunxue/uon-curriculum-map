/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

function Stack_Dump_ShowHide(id) {
    var el = $('#stack_' + id + '_body');
    if (el.css('display') == "block") {
        el.css("display", "none");
    } else {
        el.css("display", "block");
    }
}

function Debug_Show_SQL() {
    $('.debug_nodes').css('display', 'none');
    $('.debug_session').css('display', 'none');
    $('.debug_memory').css('display', 'none');

    var sqlel = $('.debug_sql');
    if (sqlel.css('display') == "block") {
        sqlel.css("display", "none");
    } else {
        sqlel.css("display", "block");
    }
}

function Debug_Show_Nodes() {
    $('.debug_sql').css('display', 'none');
    $('.debug_session').css('display', 'none');
    $('.debug_memory').css('display', 'none');

    var dumpel = $('.debug_nodes');
    if (dumpel.css('display') == "block") {
        dumpel.css("display", "none");
    } else {
        dumpel.css("display", "block");
    }
}

function Debug_Show_Session() {
    $('.debug_nodes').css('display', 'none');
    $('.debug_sql').css('display', 'none');
    $('.debug_memory').css('display', 'none');

    var dumpel = $('.debug_session');
    if (dumpel.css('display') == "block") {
        dumpel.css("display", "none");
    } else {
        dumpel.css("display", "block");
    }
}

function Debug_Show_Memory() {
    $('.debug_nodes').css('display', 'none');
    $('.debug_sql').css('display', 'none');
    $('.debug_session').css('display', 'none');

    var dumpel = $('.debug_memory');
    if (dumpel.css('display') == "block") {
        dumpel.css("display", "none");
    } else {
        dumpel.css("display", "block");
    }
}

function Debug_Show_Node(nid) {
    var el = $('.node_dump_row_' + nid);
    if (el.css('display') == "table-row") {
        el.css("display", "none");
    } else {
        el.css("display", "table-row");
    }
}

function Debug_Show_Relations(nid) {
    var el1 = $('.node_dump_relh_' + nid);
    var el2 = $('.node_dump_rel_' + nid);
    el1.css('display', 'none');
    el2.css('display', 'table-row');
} 

function Debug_Hide_Relations(nid) {
    var el1 = $('.node_dump_relh_' + nid);
    var el2 = $('.node_dump_rel_' + nid);
    el1.css('display', 'table-row');
    el2.css('display', 'none');
}

function Debug_Show_Attributes(nid) {
    var el1 = $('.node_dump_attribh_' + nid);
    var el2 = $('.node_dump_attrib_' + nid);
    el1.css('display', 'none');
    el2.css('display', 'table-row');
}

function Debug_Hide_Attributes(nid) {
    var el1 = $('.node_dump_attribh_' + nid);
    var el2 = $('.node_dump_attrib_' + nid);
    el1.css('display', 'table-row');
    el2.css('display', 'none');
}

function Debug_Show_Hist(nid) {
    var el1 = $('.node_dump_histh_' + nid);
    var el2 = $('.node_dump_hist_' + nid);
    el1.css('display', 'none');
    el2.css('display', 'table-row');
}

function Debug_Hide_Hist(nid) {
    var el1 = $('.node_dump_histh_' + nid);
    var el2 = $('.node_dump_hist_' + nid);
    el1.css('display', 'table-row');
    el2.css('display', 'none');
}

function dump(arr, level) {
    var dumped_text = "";
    if (!level) level = 0;

    //The padding given at the beginning of the line.
    var level_padding = "";
    for (var j = 0; j < level + 1; j++) level_padding += "    ";

    if (typeof (arr) == 'object') { //Array/Hashes/Objects 
        for (var item in arr) {
            var value = arr[item];

            if (typeof (value) == 'object') { //If it is an array,
                dumped_text += level_padding + "'" + item + "' ...\n";
                dumped_text += dump(value, level + 1);
            } else {
                dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
            }
        }
    } else { //Stings/Chars/Numbers etc.
        dumped_text = "===>" + arr + "<===(" + typeof (arr) + ")";
    }
    return dumped_text;
}
