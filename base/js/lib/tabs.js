/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

$(document).ready(function () {

    $("dl.fsjtabs > dt").click(function (e) {
        // find and close current tab
        $(this).parent().find("dt.open").each(function () {
            $(this).removeClass('open');
            $(this).addClass('closed');
            var id = $(this).attr('id');
            $('#tab_' + id).css('display', 'none');
        });
        $(this).removeClass('closed');
        $(this).addClass('open');
        var id = $(this).attr('id');
        $('#tab_' + id).css('display', 'block');

        var parent = $(this).parent();
        var parid = $(parent).attr('id');
        // open new tab

        TabStore.storeTab(parid, id);

        return false;
    });

    TabStore.init();
});

String.prototype.hashCode = function () {
    var hash = 0;
    if (this.length == 0) return hash;
    for (i = 0; i < this.length; i++) {
        char = this.charCodeAt(i);
        hash = ((hash << 5) - hash) + char;
        hash = hash & hash; // Convert to 32bit integer
    }
    return hash;
}

TabStore = {
    storeTab: function (tabset, newtab) {
        var urlhash = window.location.href.hashCode();
        if (TabStore.s[urlhash]) {
            var item = TabStore.s[urlhash];
        } else {
            var item = new Object();
            item.tabs = new Object();
            item.url = window.location.href;
        }

        item.timestamp = new Date().getTime();
        item.tabs[tabset] = newtab;

        TabStore.s[urlhash] = item;

        TabStore.trimStore();

        TabStore.store.set('tabhistory', JSON.stringify(TabStore.s));
    },

    trimStore: function () {
        var max = 10;

        var count = TabStore.count();

        while (count > max) {
            TabStore.removeOldest();

            count = TabStore.count();
        }
    },

    count: function () {
        var count = 0;
        for (hash in TabStore.s)
            count++;

        return count;
    },

    removeOldest: function () {
        var oldest = 0;
        var oldtime = new Date().getTime();
        for (hash in TabStore.s) {
            if (TabStore.s[hash].timestamp < oldtime) {
                oldtime = TabStore.s[hash].timestamp;
                oldest = hash;
            }
        }

        delete TabStore.s[oldest];
    },

    loadTabs: function () {
        var urlhash = window.location.href.hashCode();

        if (!TabStore.s[urlhash]) return;

        var item = TabStore.s[urlhash];

        for (var tab in item.tabs) {
            var selected = item.tabs[tab];
            TabStore.selectTab(tab, selected);
        }
    },

    selectTab: function (set, tab) {
        var tab = $('dl#' + set + ' dt#' + tab);

        $(tab).parent().find("dt.open").each(function () {
            $(this).removeClass('open');
            $(this).addClass('closed');
            var id = $(this).attr('id');
            $('#tab_' + id).css('display', 'none');
        });
        $(tab).removeClass('closed');
        $(tab).addClass('open');
        var id = $(tab).attr('id');
        $('#tab_' + id).css('display', 'block');
    },

    init: function () {
        // create new persistent store
        TabStore.store = new Persist.Store('tabstore', {
            swf_path: '../persist.swf'
        });

        TabStore.s = new Object();

        TabStore.store.get('tabhistory', function (ok, val) {
            if (ok && val != null) {
                TabStore.s = JSON.parse(val);
            }
        });

        TabStore.loadTabs();
    }
};
