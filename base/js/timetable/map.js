/**
 * This script listens for changes to selects on the page.
 * It ensures that an existing session is only ever selected a single time.
 */

/**
 * Sets up the event listener.
 */
$(document).ready(function () {
    // Load the listener.
    $('body').change(session_changed);
});

/**
 * Listens for changes to .session_select selects
 * @param {type} event
 * @returns {void}
 */
function session_changed(event) {
    var changed = $(event.target);
    if (!changed.is('select') && !changed.hasClass('session_select')) {
        return;
    }
    // Get all the session_select nodes, other than the one that triggered the change.
    $('select.session_select:not([id=' + event.target.id + '])').each(function(index, element) {
        if (element.value !== '' && element.value === event.target.value) {
            element.value = '';
        }
    });
}
