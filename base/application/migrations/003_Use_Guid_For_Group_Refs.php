<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Use_guid_for_group_refs extends CI_Migration {

	public function up()
	{
		$CI =& get_instance();
		$graph_db = $CI->graph_db;

    $datasets = array();
    foreach ($graph_db->datasets as $dataset) {
      $new_gdb = new Graph_DB();
      $new_gdb->setDataset($dataset->table, false);
      $datasets[] = $new_gdb;
    }

    foreach ($datasets as $ds) {
      $graph_db = $ds;

      // Set up mapping of node ID/group title to group node ID
      $group_map = array();

      // Get all module outcomes with group attribute
      $mod_outs = $graph_db->findNodeFromAttr(8, 'group', '%', false, 100000);
      if ($mod_outs != null)
      {
        foreach ($mod_outs as $mod_out)
        {
          $moid = $mod_out->getID();
          $group_name = $this->strip_desc($mod_out->getAttributeValue('group'));

          // See if there are local outcomes that match
          $found = $this->find_module_group_id('module', $group_name, $mod_out, $group_map);

          if (!$found) {
            // See if there are programme outcomes that match
            $found = $this->find_module_group_id('module/course', $group_name, $mod_out, $group_map);
          }

          if (!$found) {
            // See if there are body outcomes that match
            $this->find_module_group_id('module/course/body', $group_name, $mod_out, $group_map);
          }
        }
      }


      // Get all session outcomes with group attribute
      $sess_outs = $graph_db->findNodeFromAttr(9, 'group', '%', false, 100000);
      if ($sess_outs != null)
      {
        foreach ($sess_outs as $sess_out)
        {
          $group_name = $this->strip_desc($sess_out->getAttributeValue('group'));

          // See if there are local outcomes that match
          $found = $this->find_module_group_id('session', $group_name, $sess_out, $group_map);

          if (!$found) {
            // See if there are programme outcomes that match
            $found = $this->find_module_group_id('session/module/course', $group_name, $sess_out, $group_map);
          }

          if (!$found) {
            // See if there are body outcomes that match
            $found = $this->find_module_group_id('session/module/course/body', $group_name, $sess_out, $group_map);
          }

          if (!$found) {
            // See if there are module outcomes that match
            $this->find_module_group_id('session/module', $group_name, $sess_out, $group_map);
          }
        }
      }

      // Get all learning activity outcomes with group attribute
      $la_outs = $graph_db->findNodeFromAttr(13, 'group', '%', false, 100000);
      if ($la_outs != null)
      {
        foreach ($la_outs as $la_out)
        {
          $group_name = $this->strip_desc($la_out->getAttributeValue('group'));

          // See if there are local outcomes that match
          $found = $this->find_module_group_id('learning_act', $group_name, $la_out, $group_map);

          if (!$found) {
            // See if there are programme outcomes that match
            $found = $this->find_module_group_id('learning_act/module/course', $group_name, $la_out, $group_map);
          }

          if (!$found) {
            // See if there are body outcomes that match
            $found = $this->find_module_group_id('learning_act/module/course/body', $group_name, $la_out, $group_map);
          }

          if (!$found) {
            // See if there are module outcomes that match
            $this->find_module_group_id('learning_act/module', $group_name, $la_out, $group_map);
          }
        }
      }

      // Get all topic outcomes with group attribute
      $t_outs = $graph_db->findNodeFromAttr(16, 'group', '%', false, 100000);
      if ($t_outs != null)
      {
        foreach ($t_outs as $t_out)
        {
          $group_name = $this->strip_desc($t_out->getAttributeValue('group'));

          // See if there are local outcomes that match
          $found = $this->find_module_group_id('topic', $group_name, $t_out, $group_map);

          if (!$found) {
            // See if there are programme outcomes that match
            $found = $this->find_module_group_id('topic/module/course', $group_name, $t_out, $group_map);
          }

          if (!$found) {
            // See if there are body outcomes that match
            $found = $this->find_module_group_id('topic/module/course/body', $group_name, $t_out, $group_map);
          }

          if (!$found) {
            // See if there are module outcomes that match
            $this->find_module_group_id('topic/module', $group_name, $t_out, $group_map);
          }
        }
      }

      // Get all programme outcomes with group attribute
      $prog_outs = $graph_db->findNodeFromAttr(7, 'group', '%', false, 100000);
      if ($prog_outs != null)
      {
        foreach ($prog_outs as $prog_out)
        {
          $group_name = $this->strip_desc($prog_out->getAttributeValue('group'));

          // See if there are local outcomes that match
          $found = $this->find_module_group_id('course', $group_name, $prog_out, $group_map);

          if (!$found) {
            // See if there are body outcomes that match
            $this->find_module_group_id('course/body', $group_name, $prog_out, $group_map);
          }
        }
      }

      // Get all programme outcomes with group attribute
      $body_outs = $graph_db->findNodeFromAttr(6, 'group', '%', false, 100000);
      if ($body_outs != null)
      {
        foreach ($body_outs as $body_out)
        {
          $group_name = $this->strip_desc($body_out->getAttributeValue('group'));

          // See if there are local outcomes that match
          $this->find_module_group_id('body', $group_name, $body_out, $group_map);
        }
      }
    }
	}

	public function down()
	{
		$CI =& get_instance();
		$graph_db = $CI->graph_db;

    $datasets = array();
    foreach ($graph_db->datasets as $dataset) {
      $new_gdb = new Graph_DB();
      $new_gdb->setDataset($dataset->table, false);
      $datasets[] = $new_gdb;
    }

    foreach ($datasets as $ds) {
      $graph_db = $ds;

      // Get all module outcomes with group attribute
      $mod_outs = $graph_db->findNodeFromAttr(8, 'group', '%', false, 100000);
      if ($mod_outs != null)
      {
        foreach ($mod_outs as $mod_out)
        {
          $guid = $mod_out->getAttributeValue('group');

          $group_node = $graph_db->loadNodeByGuid($guid);
          if ($group_node != null and $group_node !== false) {
          	$new_group = $group_node->getTitle();
          	$desc = $group_node->getAttributeValue('desc');
          	if ($desc != '') {
          		$new_group .= '~' . $desc;
          	}
          	$mod_out->setAttribute('group', $new_group);
            $mod_out->save();
          }
        }
      }

      // Get all session outcomes with group attribute
      $sess_outs = $graph_db->findNodeFromAttr(9, 'group', '%', false, 100000);
      if ($sess_outs != null)
      {
        foreach ($sess_outs as $sess_out)
        {
          $guid = $sess_out->getAttributeValue('group');

          $group_node = $graph_db->loadNodeByGuid($guid);
          if ($group_node != null and $group_node !== false) {
          	$new_group = $group_node->getTitle();
          	$desc = $group_node->getAttributeValue('desc');
          	if ($desc != '') {
          		$new_group .= '~' . $desc;
          	}
          	$sess_out->setAttribute('group', $new_group);
            $sess_out->save();
          }
        }
      }

	    // Get all learning activity outcomes with group attribute
	    $la_outs = $graph_db->findNodeFromAttr(13, 'group', '%', false, 100000);
      if ($la_outs != null)
      {
        foreach ($la_outs as $la_out)
        {
          $guid = $la_out->getAttributeValue('group');

          $group_node = $graph_db->loadNodeByGuid($guid);
          if ($group_node != null and $group_node !== false) {
          	$new_group = $group_node->getTitle();
          	$desc = $group_node->getAttributeValue('desc');
          	if ($desc != '') {
          		$new_group .= '~' . $desc;
          	}
          	$la_out->setAttribute('group', $new_group);
            $la_out->save();
          }
        }
      }

      // Get all topic outcomes with group attribute
      $t_outs = $graph_db->findNodeFromAttr(16, 'group', '%', false, 100000);
      if ($t_outs != null)
      {
        foreach ($t_outs as $t_out)
        {
          $guid = $t_out->getAttributeValue('group');

          $group_node = $graph_db->loadNodeByGuid($guid);
          if ($group_node != null and $group_node !== false) {
          	$new_group = $group_node->getTitle();
          	$desc = $group_node->getAttributeValue('desc');
          	if ($desc != '') {
          		$new_group .= '~' . $desc;
          	}
          	$t_out->setAttribute('group', $new_group);
            $t_out->save();
          }
        }
      }

      // Get all programme outcomes with group attribute
      $prog_outs = $graph_db->findNodeFromAttr(7, 'group', '%', false, 100000);
      if ($prog_outs != null)
      {
        foreach ($prog_outs as $prog_out)
        {
          $guid = $prog_out->getAttributeValue('group');

          $group_node = $graph_db->loadNodeByGuid($guid);
          if ($group_node != null and $group_node !== false) {
          	$new_group = $group_node->getTitle();
          	$desc = $group_node->getAttributeValue('desc');
          	if ($desc != '') {
          		$new_group .= '~' . $desc;
          	}
          	$prog_out->setAttribute('group', $new_group);
            $prog_out->save();
          }
        }
      }

      // Get all body outcomes with group attribute
      $body_outs = $graph_db->findNodeFromAttr(6, 'group', '%', false, 100000);
      if ($body_outs != null)
      {
        foreach ($body_outs as $body_out)
        {
          $guid = $body_out->getAttributeValue('group');

          $group_node = $graph_db->loadNodeByGuid($guid);
          if ($group_node != null and $group_node !== false) {
          	$new_group = $group_node->getTitle();
          	$desc = $group_node->getAttributeValue('desc');
          	if ($desc != '') {
          		$new_group .= '~' . $desc;
          	}
          	$body_out->setAttribute('group', $new_group);
            $body_out->save();
          }
        }
      }
    }
	}

	private function find_module_group_id($path, $group_name, $dest_out, &$group_map)
	{
    $source_nodes = $dest_out->nodesAlongPath($path);

		foreach ($source_nodes as $source_node)
		{
      $s_id = $source_node->getID();
      $g_id = -1;

      if (isset($group_map[$s_id][$group_name]))
      {
      	$g_id = $group_map[$s_id][$group_name];
      } else {
	      $c_groups = $source_node->getRelations('kv_pair');

	      foreach ($c_groups as $c_group)
	      {
	        if ($c_group->getTitle() == $group_name)
	        {
	        	$g_id = $c_group->getGuid();
	        	$group_map[$s_id][$group_name] = $g_id;
            break;
	        }
	      }
      }

      if ($g_id != -1)
      {
      	$dest_out->setAttribute('group', $g_id);
      	$dest_out->save();
      	return true;
      }
		}

		return false;
	}

  private function strip_desc($string)
  {
    $tilde_pos = strpos($string, '~');
    if ($tilde_pos === false)
    {
      return $string;
    } else {
      return substr($string, 0, $tilde_pos);
    }
  }
}