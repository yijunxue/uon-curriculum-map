<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Remove_refs_attribute extends CI_Migration {

  public function up()
  {
    $CI =& get_instance();
    $graph_db = $CI->graph_db;

    $sql = "DELETE FROM gdb_%d_graph_attr WHERE attrib = 'refs'";
    foreach (array_keys($graph_db->datasets) as $setid)
    {
      $this->db->simple_query(sprintf($sql, $setid));
    }
  }

  public function down()
  {
    // There's no going back from this one
  }
}