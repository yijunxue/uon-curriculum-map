<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Change_Default_Year extends CI_Migration {

	public function up()
	{
		$this->db->simple_query('UPDATE datasets SET isdefault=0');
		$this->db->simple_query("UPDATE datasets SET isdefault=1 WHERE datasets.table=2013");
	}

	public function down()
	{
		$this->db->simple_query('UPDATE datasets SET isdefault=0');
		$this->db->simple_query("UPDATE datasets SET isdefault=1 WHERE datasets.table='2012'");
	}
}