<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Guid_To_Node extends CI_Migration {

	public function up()
	{
		$fields = array(
			'guid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'null' => true));
		$this->dbforge->add_column('gdb_2012_graph_node', $fields);

		$this->db->simple_query('UPDATE gdb_2012_graph_node SET guid=UUID()');
		$fields = array(
			'guid' => array(
				'type' => 'CHAR',
				'constraint' => 36,
				'after' => 'id'));
		$this->dbforge->modify_column('gdb_2012_graph_node', $fields);
	}

	public function down()
	{
		$this->dbforge->drop_column('gdb_2012_graph_node', 'guid');
	}
}