<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Migration class for version 5.  This will alter the default values of table columns.
 */
class Migration_Alter_Table_Defaults extends CI_Migration {

	public $gdb;

	/**
	 * Method for altering default values of table columns
	 */
	public function up() {
		$CI = & get_instance();
		$this->gdb = $CI->load->database('graph', TRUE);
		$graph_db = $CI->graph_db;

		// Set defaults for the ci_sessions table.
		$sql = "ALTER TABLE `" . $this->gdb->dbprefix . "ci_sessions`
            MODIFY COLUMN `user_agent` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL DEFAULT ''
            ";
		$this->db->simple_query($sql);

		// Set defaults for the datasets table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "datasets
            MODIFY COLUMN `description` TEXT NULL ,
            MODIFY COLUMN `adminonly` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `isdefault` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `nodes` TEXT NULL,
            MODIFY COLUMN `table` VARCHAR(20) NOT NULL DEFAULT '',
            MODIFY COLUMN `name` VARCHAR(250) NULL DEFAULT '',
            MODIFY COLUMN `color` VARCHAR(10) NULL DEFAULT '',
            MODIFY COLUMN `nodedef` VARCHAR(20) NULL DEFAULT ''
            ";
		$this->db->simple_query($sql);

		// Set defaults for the gdb_####_graph_attr tables.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "gdb_%d_graph_attr
            MODIFY COLUMN `node_id` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `node_type` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `attrib` VARCHAR(50) NOT NULL DEFAULT '',
            MODIFY COLUMN `type` INT(11) NOT NULL DEFAULT 0
            ";

		foreach (array_keys($graph_db->datasets) as $setid)
		{
			$this->db->simple_query(sprintf($sql, $setid));
		}

		// Set defaults for the gdb_####_graph_hist tables.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "gdb_%d_graph_hist
            MODIFY COLUMN `node_id` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `action` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `user` INT(11) NOT NULL DEFAULT 0
            ";

		foreach (array_keys($graph_db->datasets) as $setid)
		{
			$this->db->simple_query(sprintf($sql, $setid));
		}

		// Set defaults for the gdb_####_graph_node tables.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "gdb_%d_graph_node
            MODIFY COLUMN `type` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `revised` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `orig_author` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `last_author` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `deleted` TINYINT(4) NOT NULL DEFAULT 0,
            MODIFY COLUMN `perms` TEXT NULL ,
            MODIFY COLUMN `title` VARCHAR(250) NOT NULL DEFAULT '',
            MODIFY COLUMN `guid` CHAR(36) NOT NULL DEFAULT '',
            MODIFY COLUMN `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
            MODIFY COLUMN `modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'
            ";

		foreach (array_keys($graph_db->datasets) as $setid)
		{
			$this->db->simple_query(sprintf($sql, $setid));
		}

		// Set defaults for the gdb_####_graph_rel tables.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "gdb_%d_graph_rel
            MODIFY COLUMN `source_id` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `dest_id` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `type` INT(11) NOT NULL DEFAULT 0
            ";

		foreach (array_keys($graph_db->datasets) as $setid)
		{
			$this->db->simple_query(sprintf($sql, $setid));
		}

		// Set defaults for the login_attempts table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "login_attempts
            MODIFY COLUMN `ip_address` VARCHAR(40) NOT NULL DEFAULT ''
            ";
		$this->db->simple_query($sql);

		// Set defaults for the lookups table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "lookups
            MODIFY COLUMN `user_id` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `node_type` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `field` VARCHAR(50) NOT NULL DEFAULT '',
            MODIFY COLUMN `value` VARCHAR(200) NOT NULL DEFAULT ''
            ";
		$this->db->simple_query($sql);

		// Set defaults for the migrations table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "`migrations`
            MODIFY COLUMN `version` INT(3) NOT NULL DEFAULT 0
            ";
		$this->db->simple_query($sql);

		// Set defaults for the permissions table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "permissions
            MODIFY COLUMN `role_id` INT(11) NOT NULL DEFAULT 0
            ";
		$this->db->simple_query($sql);

		// Set defaults for the role_node table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "`role_node`
            MODIFY COLUMN `role` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `dataset` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `nodeid` INT(11) NOT NULL DEFAULT 0
            ";
		$this->db->simple_query($sql);

		// Set defaults for the roles table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "roles
            MODIFY COLUMN `name` VARCHAR(30) NOT NULL DEFAULT '',
            MODIFY COLUMN `description` VARCHAR(250) NOT NULL DEFAULT ''
            ";
		$this->db->simple_query($sql);

		// Set defaults for the user_autologin table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "user_autologin
            MODIFY COLUMN `user_agent` VARCHAR(150) NOT NULL DEFAULT '',
            MODIFY COLUMN `last_ip` VARCHAR(40) NOT NULL DEFAULT ''
            ";
		$this->db->simple_query($sql);

		// Set defaults for the user_node table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "user_node
            MODIFY COLUMN `userid` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `dataset` INT(11) NOT NULL DEFAULT 0,
            MODIFY COLUMN `nodeid` INT(11) NOT NULL DEFAULT 0
            ";
		$this->db->simple_query($sql);

		// Set defaults for the user_profile table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "user_profile
            MODIFY COLUMN `user_id` INT(11) NOT NULL DEFAULT 0
            ";
		$this->db->simple_query($sql);

		// Set defaults for the user_temp table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "user_temp
            MODIFY COLUMN `username` VARCHAR(255) NOT NULL DEFAULT '',
            MODIFY COLUMN `password` VARCHAR(34) NOT NULL DEFAULT '',
            MODIFY COLUMN `email` VARCHAR(100) NOT NULL DEFAULT '',
            MODIFY COLUMN `activation_key` VARCHAR(50) NOT NULL DEFAULT '',
            MODIFY COLUMN `last_ip` VARCHAR(40) NOT NULL DEFAULT '0'
            ";
		$this->db->simple_query($sql);

		// Set defaults for the users table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "users
            MODIFY COLUMN `username` VARCHAR(25) NOT NULL DEFAULT '',
            MODIFY COLUMN `password` VARCHAR(34) NOT NULL DEFAULT '',
            MODIFY COLUMN `email` VARCHAR(100) NOT NULL DEFAULT '',
            MODIFY COLUMN `role_id` INT(11) NOT NULL DEFAULT '4',
            MODIFY COLUMN `last_ip` VARCHAR(40) NOT NULL DEFAULT '0'
            ";
		$this->db->simple_query($sql);
	}

	public function down() {
		$CI = & get_instance();
		$graph_db = $CI->graph_db;
		$this->gdb = $CI->load->database('graph', TRUE);

		// Set defaults for the ci_sessions table.
		$sql = "ALTER TABLE `" . $this->gdb->dbprefix . "ci_sessions`
            MODIFY COLUMN `user_agent` VARCHAR(150) CHARACTER SET 'utf8' COLLATE 'utf8_bin' NOT NULL
            ";
		$this->db->simple_query($sql);

		// Set defaults for the datasets table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "datasets
            MODIFY COLUMN `description` TEXT NOT NULL,
            MODIFY COLUMN `adminonly` INT(11) NOT NULL,
            MODIFY COLUMN `isdefault` INT(11) NOT NULL,
            MODIFY COLUMN `nodes` TEXT NULL,
            MODIFY COLUMN `table` VARCHAR(20) NOT NULL,
            MODIFY COLUMN `name` VARCHAR(250) NULL,
            MODIFY COLUMN `color` VARCHAR(10) NULL,
            MODIFY COLUMN `nodedef` VARCHAR(20) NULL
            ";
		$this->db->simple_query($sql);

		// Set defaults for the gdb_####_graph_attr tables.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "gdb_%d_graph_attr
            MODIFY COLUMN `node_id` INT(11) NOT NULL,
            MODIFY COLUMN `node_type` INT(11) NOT NULL,
            MODIFY COLUMN `attrib` VARCHAR(50) NOT NULL,
            MODIFY COLUMN `type` INT(11) NOT NULL,
            MODIFY COLUMN `value_datetime` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'
            ";

		foreach (array_keys($graph_db->datasets) as $setid)
		{
			$this->db->simple_query(sprintf($sql, $setid));
		}

		// Set defaults for the gdb_####_graph_hist tables.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "gdb_%d_graph_hist
            MODIFY COLUMN `node_id` INT(11) NOT NULL,
            MODIFY COLUMN `action` INT(11) NOT NULL,
            MODIFY COLUMN `user` INT(11) NOT NULL
            ";

		foreach (array_keys($graph_db->datasets) as $setid)
		{
			$this->db->simple_query(sprintf($sql, $setid));
		}

		// Set defaults for the gdb_####_graph_node tables.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "gdb_%d_graph_node
            MODIFY COLUMN `type` INT(11) NOT NULL,
            MODIFY COLUMN `revised` INT(11) NOT NULL,
            MODIFY COLUMN `orig_author` INT(11) NOT NULL,
            MODIFY COLUMN `last_author` INT(11) NOT NULL,
            MODIFY COLUMN `deleted` TINYINT(4) NOT NULL DEFAULT 0,
            MODIFY COLUMN `perms` TEXT NULL ,
            MODIFY COLUMN `title` VARCHAR(250) NOT NULL,
            MODIFY COLUMN `guid` CHAR(36) NOT NULL,
            MODIFY COLUMN `created` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
            MODIFY COLUMN `modified` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00'
            ";

		foreach (array_keys($graph_db->datasets) as $setid)
		{
			$this->db->simple_query(sprintf($sql, $setid));
		}

		// Set defaults for the gdb_####_graph_rel tables.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "gdb_%d_graph_rel
            MODIFY COLUMN `source_id` INT(11) NOT NULL,
            MODIFY COLUMN `dest_id` INT(11) NOT NULL,
            MODIFY COLUMN `type` INT(11) NOT NULL
            ";

		foreach (array_keys($graph_db->datasets) as $setid)
		{
			$this->db->simple_query(sprintf($sql, $setid));
		}

		// Set defaults for the login_attempts table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "login_attempts
            MODIFY COLUMN `ip_address` VARCHAR(40) NOT NULL
            ";
		$this->db->simple_query($sql);

		// Set defaults for the lookups table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "lookups
            MODIFY COLUMN `user_id` INT(11) NOT NULL,
            MODIFY COLUMN `node_type` INT(11) NOT NULL,
            MODIFY COLUMN `field` VARCHAR(50) NOT NULL,
            MODIFY COLUMN `value` VARCHAR(200) NOT NULL
            ";
		$this->db->simple_query($sql);

		// Set defaults for the migrations table.
		$sql = "ALTER TABLE `" . $this->gdb->dbprefix . "migrations`
            MODIFY COLUMN `version` INT(3) NOT NULL
            ";
		$this->db->simple_query($sql);

		// Set defaults for the permissions table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "permissions
            MODIFY COLUMN `role_id` INT(11) NOT NULL
            ";
		$this->db->simple_query($sql);

		// Set defaults for the role_node table.
		$sql = "ALTER TABLE `" . $this->gdb->dbprefix . "role_node`
            MODIFY COLUMN `role` INT(11) NOT NULL,
            MODIFY COLUMN `dataset` INT(11) NOT NULL,
            MODIFY COLUMN `nodeid` INT(11) NOT NULL
            ";
		$this->db->simple_query($sql);

		// Set defaults for the roles table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "roles
            MODIFY COLUMN `name` VARCHAR(30) NOT NULL,
            MODIFY COLUMN `description` VARCHAR(250) NOT NULL
            ";
		$this->db->simple_query($sql);

		// Set defaults for the user_autologin table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "user_autologin
            MODIFY COLUMN `user_agent` VARCHAR(150) NOT NULL,
            MODIFY COLUMN `last_ip` VARCHAR(40) NOT NULL
            ";
		$this->db->simple_query($sql);

		// Set defaults for the user_node table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "user_node
            MODIFY COLUMN `userid` INT(11) NOT NULL,
            MODIFY COLUMN `dataset` INT(11) NOT NULL,
            MODIFY COLUMN `nodeid` INT(11) NOT NULL
            ";
		$this->db->simple_query($sql);

		// Set defaults for the user_profile table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "user_profile
            MODIFY COLUMN `user_id` INT(11) NOT NULL
            ";
		$this->db->simple_query($sql);

		// Set defaults for the user_temp table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "user_temp
            MODIFY COLUMN `username` VARCHAR(255) NOT NULL,
            MODIFY COLUMN `password` VARCHAR(34) NOT NULL,
            MODIFY COLUMN `email` VARCHAR(100) NOT NULL,
            MODIFY COLUMN `activation_key` VARCHAR(50) NOT NULL,
            MODIFY COLUMN `last_ip` VARCHAR(40) NOT NULL
            ";
		$this->db->simple_query($sql);

		// Set defaults for the users table.
		$sql = "ALTER TABLE " . $this->gdb->dbprefix . "users
            MODIFY COLUMN `username` VARCHAR(25) NOT NULL,
            MODIFY COLUMN `password` VARCHAR(34) NOT NULL,
            MODIFY COLUMN `email` VARCHAR(100) NOT NULL,
            MODIFY COLUMN `role_id` INT(11) NOT NULL,
            MODIFY COLUMN `last_ip` VARCHAR(40) NOT NULL
            ";
		$this->db->simple_query($sql);
	}

}
