<?php

/* NEEDS CLEANUP */
/* NEEDS COMMENTS */
/**
 * Auth view controller
 *
 * Implements login and out etc
 */
class Auth extends CI_Controller
{
	// Used for registering and changing password form validation
	var $min_username = 2;
	var $max_username = 20;
	var $min_password = 2;
	var $max_password = 20;

	function __construct()
	{
		parent::__construct();

		$this->load->library('Form_validation');
		//$this->load->library('DX_Auth');

		$this->load->helper('url');
		$this->load->helper('form');

		setup_main_menu($this->menu);
		$this->setup_sub_menu();

		$this->gdb = $this->graph_db->gdb;
	}

	function setup_sub_menu()
	{
		$this->menu->AddSubMenuItem("Account:");
		$this->menu->AddSubMenuItem("My Account",'auth');
		$this->menu->AddSubMenuItem("Logout",'auth/logout');
		$this->menu->AddSubMenuItem("Change Password",'auth/change_password');
	}

	function index()
	{
    if ($this->dx_auth->is_logged_in()) // user is logged in, reload their node and redirect to it
    {
      $CI =& get_instance();
      redirect("/view/" . $CI->session->userdata('my_node'));
    } else {
      redirect("/");
    }
	}

  function dataset()
  {
    if ($this->dx_auth->is_logged_in()) // user is logged in, reload their node and redirect to it
    {
      $this->loadMyNode();
      redirect("/");
    } else { // not logged in, just goto the year page
      redirect("main/year");
    }
  }

	/* Callback function */

	function username_check($username)
	{
		$result = $this->dx_auth->is_username_available($username);
		if ( ! $result)
		{
			$this->form_validation->set_message('username_check', 'Username already exist. Please choose another username.');
		}

		return $result;
	}

	function email_check($email)
	{
		$result = $this->dx_auth->is_email_available($email);
		if ( ! $result)
		{
			$this->form_validation->set_message('email_check', 'Email is already used by another user. Please choose another email address.');
		}

		return $result;
	}

	function captcha_check($code)
	{
		$result = TRUE;

		if ($this->dx_auth->is_captcha_expired())
		{
			// Will replace this error msg with $lang
			$this->form_validation->set_message('captcha_check', 'Your confirmation code has expired. Please try again.');
			$result = FALSE;
		}
		elseif ( ! $this->dx_auth->is_captcha_match($code))
		{
			$this->form_validation->set_message('captcha_check', 'Your confirmation code does not match the one in the image. Try again.');
			$result = FALSE;
		}

		return $result;
	}

	function recaptcha_check()
	{
		$result = $this->dx_auth->is_recaptcha_match();
		if ( ! $result)
		{
			$this->form_validation->set_message('recaptcha_check', 'Your confirmation code does not match the one in the image. Try again.');
		}

		return $result;
	}

	/* End of Callback function */


	function login($redirect = "")
	{
		$popup = $this->input->get('popup');
		if ($popup > 0) $popup = "/popup";

		// store redirect in session for when we have a successful login
		if ($redirect)
		{
			$redirect = base64_decode($redirect);
			if (strpos($redirect,"logout") > 0)
					$redirect = "";
					$this->session->set_userdata('redirect', $redirect);
		}


		if ( ! $this->dx_auth->is_logged_in())
		{
			$val = $this->form_validation;

			// Set form validation rules
			$val->set_rules('username', 'Username', 'trim|required|xss_clean');
			$val->set_rules('password', 'Password', 'trim|required');
			$val->set_rules('remember', 'Remember me', 'integer');

			// Set captcha rules if login attempts exceed max attempts in config
			if ($this->dx_auth->is_max_login_attempts_exceeded())
			{
				$val->set_rules('captcha', 'Confirmation Code', 'trim|required|xss_clean|callback_captcha_check');
			}

			if ($val->run() AND $this->dx_auth->login($val->set_value('username'), $val->set_value('password'), $val->set_value('remember')))
			{
				// need to refresh parent window
				$nodeid = $this->loadMyNode();

				if ($this->input->post('gohome') == 1)
				{
          $this->session->set_userdata('redirect', false);
					$this->login_redirect($popup, 'view/' . $nodeid);
				} else {
					$redir_path = '';
					if (!$popup and $this->session->userdata('redirect'))
					{
						$redir_path = $this->session->userdata('redirect');
					}
					$this->session->set_userdata('redirect', false);
					$this->login_redirect($popup, $redir_path);
				}
			}
			else
			{
				// Check if the user is failed logged in because user is banned user or not
				if ($this->dx_auth->is_banned())
				{
					// Redirect to banned uri
					$this->dx_auth->deny_access('banned');
				}
				else
				{
					// Default is we don't show captcha until max login attempts eceeded
					$data['show_captcha'] = FALSE;

					// Show captcha if login attempts exceed max attempts in config
					if ($this->dx_auth->is_max_login_attempts_exceeded())
					{
						// Create catpcha
						$this->dx_auth->captcha();

						// Set view data to show captcha on view file
						$data['show_captcha'] = TRUE;
					}

					// Load login page view
					$data['title'] = "Please log in";

					// add form open to top of popup
					$attributes = array('id' => 'login_form');

					$url = $this->uri->uri_string();
					if ($popup)
							$url .= "?popup=1";
					$this->head->AddLeading(form_open($url, $attributes));

					//add form close to bottom of popup
					$this->head->AddTrailing(form_close());

					$CI =& get_instance();

					if ($CI->config->item('UONCM_CanResetPass'))
						$this->head->AddFooter("<div style='float:left;'><a href='" . site_url('auth/forgot_password') . "'>Forgot Password</a></div>");

					// add save button to footer
					$this->head->AddFooter("<button id=\"edit_save\" class=\"footer_button\">Login</button>");

					// add cancel button to footer
					$cancel_js = ($popup) ? 'window.parent.TINY.box.hide();' : 'history.back(-1);';
					$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='{$cancel_js}return false;'>Cancel</button>");

					$this->load->view("templates$popup/header", $data);
					$this->load->view($this->dx_auth->login_view, $data);
					$this->load->view("templates$popup/footer");
				}
			}
		}
		else
		{
			// already logged in, refresh the parent window
			$this->login_redirect($popup);
//			$this->head->ParentRefresh();
		}
	}

	public function loadMyNode()
	{
		// clear my node, WHY IS THIS COMMENTED OUT?
		//$this->dx_auth->set_my_node("");

		// if user is an admin, then set the default role to course admin
		if ($this->dx_auth->get_current_role() == "admin")
			$this->dx_auth->set_current_role("courseadmin");

    // NEED TO PRETEND TO BE ADMIN AT THIS POINT OR SOMETIME THIS WILL FAIL DUE TO PERMISSIONS
    $CI =& get_instance();
    $oldrole = $CI->session->userdata('DX_role_name');
    $CI->session->set_userdata('DX_role_name',"admin");

    $node = $this->getMyNode();

    // SWITCH BACK TO NORMAL USER
    $CI->session->set_userdata('DX_role_name',$oldrole);


		// store login
		$node->setAttribute("logedin",date("Y-m-d H:i:s"));
		$node->setAttribute("ip",$_SERVER['REMOTE_ADDR']);
		$node->save();

		// load a list of groups we belong to
		$rels = $node->getRelations("perm_group");

		$groups = array();

		foreach ($rels as &$rel)
			$groups[$rel->getId()] = $rel->getId();

		$CI->session->set_userdata("my_node",$node->getId());
		$CI->session->set_userdata('groups',$groups);

		return $node->getId();
	}

	private function getMyNode()
	{
		$userid = $this->dx_auth->get_user_id();
		$datasetid = $this->graph_db->active_dataset->id;

    $qry = "SELECT nodeid FROM user_node WHERE userid = ? AND dataset = ?";
		$query = $this->db->query($qry, array($userid, $datasetid));
		foreach ($query->result() as $row)
		{
			$node = $this->graph_db->loadNode($row->nodeid);
			if ($node)
			{
				$this->dx_auth->set_my_node($row->nodeid);

				return $node;
			}
			// if we cant load the current node, make a new one! (this should only happen if the node is deleted, or permission broken somehow
		}

		$this->load->model('dx_auth/users', 'users');

    $usernode = $this->users->create_user_node($this->dx_auth->get_username(), $this->graph_db->active_dataset->nodes->all_users, $userid, $datasetid);

		$this->dx_auth->set_my_node($usernode->getId());

		$this->users->set_base_user_perms($usernode);

		//print_p($usernode->perms);
		$usernode->save();

    // Not currently attempting to automatically create users
//		$this->lookup_user_info($usernode);

		return $usernode;
	}

  // LOGGING
  function write_login_log($file, $msg) {
    if ( ! file_exists($file))
    {
      $msg = "<"."?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed'); ?".">\n\n" . $msg;
    }

    if ($fp = @fopen($file, FOPEN_WRITE_CREATE))
    {
      flock($fp, LOCK_EX);
      fwrite($fp, $msg);
      flock($fp, LOCK_UN);
      fclose($fp);

      @chmod($file, FILE_WRITE_MODE);
    }
  }
  // LOGGING

	function lookup_user_info(&$usernode)
	{
		// get attributes from xml server

		// NEED TO CHANGE THIS TO BE GOT FROM A CONFIG FILE!
		$CI =& get_instance();

		$url = $CI->config->item('UONCM_InfoURL');
		$url = str_replace("$$",urlencode($this->dx_auth->get_username()),$url);
		$xml = file_get_contents($url);

		$xml = simplexml_load_string($xml);
		if (!$xml)
			return;

		if ($xml->StudentID)
			$usernode->setAttribute("studentid",(string)$xml->StudentID);

		$name = array();
		if ($xml->Title && trim($xml->Title) != "")
			$name[] = trim($xml->Title);
		if ($xml->Forename && trim($xml->Forename) != "")
			$name[] = trim($xml->Forename);
		if ($xml->Surname && trim($xml->Surname) != "")
			$name[] = trim($xml->Surname);

		if (count($name) > 0)
			$usernode->setAttribute("fullname", implode(" ",$name));

		if ($xml->Gender)
			$usernode->setAttribute("gender",(string)$xml->Gender);

		if ($xml->YearofStudy)
			$usernode->setAttribute("yearofstudy",(string)$xml->YearofStudy);


		if ($xml->Email)
		{
			// check to see if we have an email or not, if not then set it
			$email = (string)$xml->Email;

			$curemail = $this->dx_auth->get_email();

			if ($curemail == "")
			{
				$this->dx_auth->set_email($email);
			}
		}

		foreach($xml->CourseCode as $code)
		{
			$cc = (string)$code;
			//echo "Course Code : $cc<br>";

			// try to find course

			$nodes = $this->graph_db->findNodeFromAttr("course","code",$cc);
			if (count($nodes) > 0)
			{
				foreach ($nodes as &$node)
				{
					$usernode->addRelate($node,GDB_Rel::Relation);
				}
			}
		}
		$usernode->save();
	}

	function logout()
	{
		$this->dx_auth->logout();

		$this->head->AddMessage('You have been logged out.');
		$this->session->set_userdata('redirect', false);

		redirect("");
	}

	function forgot_password()
	{
		$CI =& get_instance();

		if (!$CI->config->item('UONCM_CanResetPass'))
			return;

		$val = $this->form_validation;

		// Set form validation rules
		$val->set_rules('login', 'Username or Email address', 'trim|required|xss_clean');

		// Validate rules and call forgot password function
		if ($val->run() AND $this->dx_auth->forgot_password($val->set_value('login')))
		{
			$data['auth_message'] = 'An email has been sent to your email with instructions with how to activate your new password.';

			$data['title'] = "Password has been reset";
			//add form close to bottom of popup

			// add cancel button to footer
			$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.parent.TINY.box.hide();'>Close</button>");

			$this->load->view("templates/popup/header", $data);
			$this->load->view($this->dx_auth->forgot_password_success_view, $data);
			$this->load->view("templates/popup/footer");
		}
		else
		{
			$this->head->AddLeading(form_open($this->uri->uri_string()));
			$this->head->AddTrailing(form_close());

			$data['title'] = "Reset Password";
			//add form close to bottom of popup

			// add save button to footer
			$this->head->AddFooter("<button id=\"edit_save\" class=\"footer_button\">Reset Now</button>");

			// add cancel button to footer
			$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.parent.TINY.box.hide();'>Cancel</button>");

			$this->load->view("templates/popup/header", $data);
			$this->load->view($this->dx_auth->forgot_password_view, $data);
			$this->load->view("templates/popup/footer");

			//$this->load->view($this->dx_auth->forgot_password_view);
		}
	}

	function reset_password()
	{
		// Get username and key
		$username = $this->uri->segment(3);
		$key = $this->uri->segment(4);

		// Reset password
		if ($this->dx_auth->reset_password($username, $key))
		{
			$data['auth_message'] = 'You have successfully reset you password, <a class="a_popup" rel="400x258" href="'. site_url("auth/login/?popup=1") .'">Login</a>';
			$data['title'] = "Password Reset";
			$this->load->view('templates/header', $data);
			$this->load->view($this->dx_auth->reset_password_success_view, $data);
			$this->load->view('templates/footer');
		}
		else
		{
			$data['auth_message'] = 'Reset failed. Your username and key are incorrect. Please check your email again and follow the instructions.';
			$data['title'] = "Password Reset";
			$this->load->view('templates/header', $data);
			$this->load->view($this->dx_auth->reset_password_failed_view, $data);
			$this->load->view('templates/footer');
		}
	}

	function change_password()
	{
		// Check if user logged in or not
		if ($this->dx_auth->is_logged_in())
		{
			$val = $this->form_validation;

			// Set form validation
			$val->set_rules('old_password', 'Old Password', 'trim|required|min_length['.$this->min_password.']|max_length['.$this->max_password.']');
			$val->set_rules('new_password', 'New Password', 'trim|required|min_length['.$this->min_password.']|max_length['.$this->max_password.']|matches[confirm_new_password]');
			$val->set_rules('confirm_new_password', 'Confirm new Password', 'trim|required');

			// Validate rules and change password
			if ($val->run() AND $this->dx_auth->change_password($val->set_value('old_password'), $val->set_value('new_password')))
			{
				$data['auth_message'] = 'Your password has successfully been changed.';
				$data['title'] = "Password Reset";
				$this->load->view('templates/header', $data);
				$this->load->view($this->dx_auth->change_password_success_view, $data);
				$this->load->view('templates/footer');
			}
			else
			{
				$data['title'] = "Password Reset";
				$this->load->view('templates/header', $data);
				$this->load->view($this->dx_auth->change_password_view);
				$this->load->view('templates/footer');
			}
		}
		else
		{
			// Redirect to login page
			$this->dx_auth->deny_access('login');
		}
	}

	// used to set the current role being used for an admin user
	// needs some sort of security adding to this, so cant set a role the user doesnt have - Added in dx_auth class
	function role($newrole, $redirect)
	{
		//echo $newrole."<br>";
		$redirect = base64_decode($redirect);
		//echo "New URL : $redirect<br>";

		$this->dx_auth->set_current_role($newrole);

		redirect($redirect);
	}

	// turn debug stuff on and off
	function toggle_debug()
	{
		// only allow for admin users
		if (!$this->dx_auth->is_role("admin"))
			return;

		if ($this->dx_auth->showdebug())
		{
			$this->dx_auth->set_debug(false);
			echo "Debug OFF";
		} else {
			$this->dx_auth->set_debug(true);
			echo "Debug ON";
		}
	}

	function toggle_templates()
	{
		// only allow for admin users
		if (!$this->dx_auth->is_role("admin"))
			return;

		if ($this->dx_auth->hidetemplates())
		{
			$this->dx_auth->set_templates(false);
			echo "Dont Use Templates";
		} else {
			$this->dx_auth->set_templates(true);
			echo "Use Templates";
		}
	}


	function create()
	{
		if (!$this->dx_auth->is_admin())
			return;

		$val = $this->form_validation;

		// Set form validation rules
		$val->set_rules('username', 'Username', 'trim|required|xss_clean|min_length['.$this->min_username.']|max_length['.$this->max_username.']|callback_username_check|alpha_dash');
		$val->set_rules('password', 'Password', 'trim|required|min_length['.$this->min_password.']|max_length['.$this->max_password.']|matches[confirm_password]');
		$val->set_rules('confirm_password', 'Confirm Password', 'trim|required');
		$val->set_rules('email', 'Email', 'trim|required|xss_clean|valid_email|callback_email_check');
		$val->set_rules('role', 'Role', 'required');

		$this->dx_auth->email_activation = false;
		// Run form validation and register user if it's pass the validation
		if ($val->run())
		{
			$new_user = $this->dx_auth->register($val->set_value('username'), $val->set_value('password'), $val->set_value('email'));

			if ($new_user) {
				$this->load->model('dx_auth/users', 'users');

				$role = $val->set_value('role');

				// Create the user's node
		    $usernode = $this->users->create_user_node($new_user['username'], $this->graph_db->active_dataset->nodes->all_users, $new_user['id'], $this->graph_db->active_dataset->id, false);
				$usernode->save();
				$this->users->set_base_user_perms($usernode);

				// Change user role
				$role_data = $this->dx_auth->_get_role_data($role);
				$this->users->change_user_role($role_data, $new_user['id'], $role);

				$this->head->AddMessage("User created");
				redirect("adminusers/users");
			}
		}

		$qry = "SELECT * FROM roles WHERE id != 0 ORDER BY id DESC";

		$CI =& get_instance();

		$result = $CI->db->query($qry);

		$data['roles'] = array();

		foreach ($result->result() as $row)
		{
			$data['roles'][$row->id] = $row->description;
		}


		// Load registration page
		$this->head->AddLeading(form_open($this->uri->uri_string()));

		//add form close to bottom of popup
		$this->head->AddTrailing(form_close());

		$data['title'] = 'Create User';

		$this->head->AddFooter("<button id=\"edit_save\" class=\"footer_button\">Create User</button>");

		// add cancel button to footer
		$this->head->AddFooter("<button id='cancel_delete' class='footer_button' onclick='window.location=\"".site_url("adminusers/users") ."\";return false;'>Cancel</button>");

		$this->head->AddFooter("<button id='bulk_import' class='footer_button' onclick='window.location=\"".site_url("auth/import/") ."\";return false;'>Bulk Import</button>");

		$this->load->view('templates/header', $data);
		$this->load->view($this->dx_auth->admin_create_view);
		$this->load->view('templates/footer');


	}

	function import($group_id = -1)
	{
		if (!$this->dx_auth->is_admin())
			return;

		// do a bulk import from csv file

		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['title'] = "Bulk import users";

		$group = false;
		if ($group_id != -1)
		{
			$group = $this->graph_db->loadNode($group_id);
			$data['title'] .= ' to group ' . $group->getTitle();			
		}

		$this->head->AddLeading(form_open_multipart($this->uri->uri_string(), array('id' => 'import_form')));

		//add form close to bottom of popup
		$this->head->AddTrailing(form_close());


		//$this->head->AddFooter("<div style='float:left'><span class='req_field'>*</span> - Required Field</div>");

		// output header

		// if the form fails to validate (ie, not submitted or submitted with errors, then display the create template

		$result = $this->do_import($group);

		$this->load->view('templates/header', $data);

		$url = ($group_id != -1) ? site_url('view/' . $group_id) : site_url('adminusers/');

		if (!$result)
		{
			// add save button to footer
			$this->head->AddFooter("<button id=\"edit_save\" class=\"footer_button\">Import</button>");

			// add cancel button to footer
			$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.location=\"" . $url ."\";return false;'>Cancel</button>");

			$this->load->view('auth/import', $data);
		}
		else // form submitted sucessfully, so create the node
		{
			$data['title'] = "Import users completed";
			$data['results'] = $result;
			$data['for_group'] = ($group_id != -1);
			$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.location=\"". $url ."\";return false;'>Done</button>");
			$this->load->view('auth/import_done', $data);
		}
		$this->load->view('templates/footer');
	}

	private function do_import($group_node = false)
	{
		$this->load->helper('csv');
		$this->load->model('dx_auth/users', 'users');

		if (!array_key_exists("csvfile", $_FILES))
			return false;

		$file = $_FILES['csvfile'];

		$data = load_csv_file($file['tmp_name']);

		if (count($data) < 1)
		{
			$this->head->AddMessage("Unable to load file");
			return false;
		}

		if (!array_key_exists("username", $data[0]) or !array_key_exists("email", $data[0]))
		{
			$this->head->AddMessage("Missing required attribute in csv file");
			return false;
		}

		$imported = array('success' => array(), 'failure' => array());

		foreach ($data as &$row)
		{
			if (!array_key_exists("username", $row)) continue;
			if (!array_key_exists("email", $row)) continue;

			$username = $row['username'];
			$email = $row['email'];
			$password = (array_key_exists('password', $row)) ? $row['password'] : uniqid();

			$create_node = true;

			if ($query = $this->users->get_user_by_username($username) AND $query->num_rows() == 0) {
				$new_user = $this->dx_auth->register($username, $password, $email);

				if ($new_user)
				{
					$imported['success'][] = $username;

					$userid = $new_user['id'];
					// Lecturer by default
					$role = 3;
				} else {
					$imported['failure'][] = $username;
					$create_node = false;
				}
			} else {
				$imported['failure'][] = $username;

				$userid = $query->row()->id;
				$role = $query->row()->role_id;
			}

			if ($create_node)
			{
				$datasetid = $this->graph_db->active_dataset->id;

				$usernode = false;

		    $qry = "SELECT nodeid FROM user_node WHERE userid = ? AND dataset = ?";
				$node_query = $this->db->query($qry, array($userid, $datasetid));
				foreach ($node_query->result() as $row)
				{
					$usernode = $this->graph_db->loadNode($row->nodeid);
					if ($usernode)
					{
						continue;
					}
					// if we cant load the current node, make a new one! (this should only happen if the node is deleted, or permission broken somehow
				}

				// No user node found, create it
				if (!$usernode)
				{
					// user has been created, set the role into the record
					$this->load->model('dx_auth/users', 'users');

					// Create the user's node
			    $usernode = $this->users->create_user_node($username, $this->graph_db->active_dataset->nodes->all_users, $userid, $this->graph_db->active_dataset->id, false);
					$usernode->save();
					$this->users->set_base_user_perms($usernode);

					// Change user role
					$role_data = $this->dx_auth->_get_role_data($role);
					$this->users->change_user_role($role_data, $userid, $role);
				}

				if ($group_node) {
					// Add relationship between user node and the group node
					$group_node->addRelate($usernode, GDB_Rel::Relation, 1);
					$group_node->save();
				}

				unset($usernode);
			}
		}
		return $imported;
	}

	private function login_redirect($popup, $path = '') {
		if ($popup == '/popup') {
			if ($path == '') {
				$this->head->ParentRefresh();
			} else {
				$this->head->ParentRefresh($path);
			}
		} else {
			redirect($path);
		}
	}
}
?>