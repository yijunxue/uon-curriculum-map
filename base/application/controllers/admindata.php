<?php

/**
 * Data import and test data class
 *
 * Used to import or create sample data, and clear the database.
 *
 * Only will be used for the development of the system
 *
 */
class AdminData extends CI_Controller {

  public function __construct() {
    parent::__construct();
    $this->load->library('tabs');
    $this->load->helper('url');
    $this->load->helper('clone');

    setup_main_menu($this->menu);
    setup_admin_menu($this->menu);

    $this->session->set_userdata('redirect', uri_string());
    $this->dx_auth->check_uri_permissions();
  }

  /**
   * PAGE: Main data function index page
   */
  public function index() {
    $this->data['title'] = 'Data Functions';

    $this->data['stats'] = $this->graph_db->getStats();
    $this->data['nodestats'] = $this->graph_db->getNodeStats();
    $this->data['currentversion'] = $this->db->get('migrations')->row()->version;
    $this->load->model('migrationversions', '', true);
    $this->data['versions'] = $this->migrationversions->get_versions();

    $this->head->AddJS("js/admin/data.js");

    $this->load->view('templates/header', $this->data);
    $this->load->view('admin/data/index', $this->data);
    $this->load->view('templates/footer');
  }

 	/**
	 * Migrate to the version specified in the base/application/config/migration.php config file.
	 * @param string $confirm
	 * @param int $toversion Version to migrate to
	 * @return void
	 */
	public function migrate($confirm = '', $toversion = '') {
		if ($confirm == 'yes')
		{
			// Switch to a DB user with greater privileges so that structural changes can be made
			$this->load->database('admin', FALSE, TRUE);
			$this->load->library('migration');
			$this->load->model('migrationversions');
			$CI = & get_instance();
			$currentversion = $this->migrationversions->get_current_version();
			$versions = $this->migrationversions->get_versions();

			if (array_key_exists($toversion, $versions)) {
				$this->data['version'] = $toversion;
			} else {
				$this->data['error'] = 'It is not possible to upgrade to the specified version number.';
			}

			if ($currentversion == $this->data['version']) {
				$this->data['error'] = "The current migration is already on version " . $currentversion;
			}

			if ($this->migration->version($this->data['version']) === FALSE) {
				$this->data['error'] = 'Migration failed due to a coding error';
			}

			if (array_key_exists('error', $this->data)) {
				$this->data['title'] = 'Migration failed';
				$this->data['fromversion'] = $currentversion;
				$this->load->view('admin/data/migrate_failure', $this->data);
				return;
			}

			$this->data['title'] = 'Migration';
			$this->data['fromversion'] = $currentversion;
			$this->load->view('admin/data/migrate_success', $this->data);
		}
	}
  /**
   * AJAX: Show the dataset statistics
   */
  public function stats() {
    $this->data['stats'] = $this->graph_db->getStats();
    $this->load->view('admin/dataset/dataset_size', $this->data);
  }

  /**
   * AJAX: Show node counts for the current dataset
   */
  public function nodestats() {
    $this->data['nodestats'] = $this->graph_db->getNodeStats();
    $this->load->view('admin/dataset/nodedetails', $this->data);
  }

  /**
   * AJAX: Confirm the database wipe
   */
  public function reset($confirm = '') {
    if ($confirm == 'yes')
    {
      $this->graph_db->wipe_db();
    }
  }

  /**
   * Page to edit datasets
   * @param int $table The dataset folder e.g. 2015
   * @return void
   */
  public function editdataset($table) {

    $this->load->model('Dataset_model', '', TRUE);
    $dataset = $this->Dataset_model->get_dataset($table);

    $this->load->helper('form');
    $this->load->library('form_validation');

    $this->data['title'] = 'Edit dataset';

    $this->data['nodedef'] = 'cm';
    $this->data['table'] = $dataset->table;
    $this->data['name'] = $dataset->name;
    $this->data['description'] = $dataset->description;
    $this->data['color'] = $dataset->color;
    $this->data['adminonly'] = $dataset->adminonly;
    $this->data['isdefault'] = $dataset->isdefault;

    $this->head->AddJS("js/admin/data.js");
    $this->head->AddJS("js/spectrum.js");
    $this->head->AddCSS("css/spectrum.css");

    $this->head->AddLeading(form_open("admindata/editdataset/$table", array('id' => 'dataset_form')));

    //add form close to bottom of popup
    $this->head->AddTrailing(form_close());

    $this->head->AddFooter("<div style='float:left'><span class='req_field'>*</span> - Required Field</div>");

    // add save button to footer
    $this->head->AddFooter("<button id=\"edit_save\" class=\"footer_button\">Save</button>");
    $this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.location=\"" . site_url('admindata') . "\";return false;'>Cancel</button>");

    $this->form_validation->set_rules('table', 'Table prefix', 'required');
    $this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('table', 'Table prefix', 'callback_is_integer');
    $this->form_validation->set_message('is_integer', 'The table prefix must be an integer');
    $this->form_validation->set_rules('description', 'Description', 'required');
    $this->form_validation->set_rules('colour', 'Colour', 'required');
    $this->form_validation->set_rules('adminonly', 'Admin only?', '');
    $this->form_validation->set_rules('isdefault', 'Is Default?', '');

    $this->load->view('templates/header', $this->data);

    // if the form fails to validate (ie, not submitted or submitted with errors, then display the create tempalte
    if ($this->form_validation->run() === FALSE) {
      $this->load->view('admin/dataset/editdataset', $this->data);
    } else {
      // Form submitted sucessfully, so edit the dataset
      $data = array(
        'id' => $dataset->id,
        'table' => $this->input->post('table'),
        'name' => $this->input->post('name'),
        'description' => $this->input->post('description'),
        'color' => $this->input->post('colour'),
        'adminonly' => $this->input->post('adminonly'),
        'isdefault' => $this->input->post('isdefault')
      );

      if ((int)($this->input->post('table') != $this->input->post('table')) && $this->input->post('table') > 0) {
        $this->load->view('admin/dataset/dataset_int_error');
        return;
      }

      $this->Dataset_model->update_dataset($data);
			redirect('admindata');

    }
    $this->load->view('templates/footer');
  }

  /**
   * PAGE: Create new dataset form page
   */
  public function createdataset() {
    global $current_data_set_folder;

    $this->load->helper('form');
    $this->load->library('form_validation');

    $this->data['title'] = 'Create dataset';

    $ds_list = array('' => 'empty dataset', $current_data_set_folder => $current_data_set_folder);

    $this->data['datasets'] = $ds_list;
    $this->data['nodedef'] = 'cm';

    $this->head->AddJS("js/admin/data.js");
    $this->head->AddJS("js/spectrum.js");
    $this->head->AddCSS("css/spectrum.css");

    $this->head->AddLeading(form_open("admindata/createdataset", array('id' => 'dataset_form')));

    // Add form close to bottom of popup.
    $this->head->AddTrailing(form_close());


    $this->head->AddFooter("<div style='float:left'><span class='req_field'>*</span> - Required Field</div>");

    // Add save button to footer.
    $this->head->AddFooter("<button id=\"edit_save\" class=\"footer_button\">Save</button>");
    $this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.location=\"" . site_url('admindata') . "\";return false;'>Cancel</button>");

    $this->form_validation->set_rules('source_ds', 'Source dataset', '');
    $this->form_validation->set_rules('table', 'Table prefix', 'required');
    $this->form_validation->set_rules('table', 'Table prefix', 'callback_is_integer');
    $this->form_validation->set_message('is_integer', 'The table prefix must be an integer');
    $this->form_validation->set_rules('name', 'Name', 'required');
    $this->form_validation->set_rules('description', 'Description', 'required');
    $this->form_validation->set_rules('colour', 'Colour', 'required');
    $this->form_validation->set_rules('nodedef', 'Node defnition', 'required');
    $this->form_validation->set_rules('adminonly', 'Admin only?', '');
    $this->form_validation->set_rules('isdefault', 'Is Default?', '');

    $this->load->view('templates/header', $this->data);


    // If the form fails to validate (ie, not submitted or submitted with errors, then display the create tempalte.
    if ($this->form_validation->run() === FALSE)
    {
      $this->load->view('admin/dataset/newdataset', $this->data);
    } else // form submitted sucessfully, so create the dataset
    {
      // Switch to a DB user with greater privileges so that structural changes can be made.
      $this->load->database('admin', FALSE, TRUE);

      $this->load->model('Dataset_model', '', TRUE);
      $ds_data = array(
        'table' => $this->input->post('table'),
        'name' => $this->input->post('name'),
        'description' => $this->input->post('description'),
        'color' => $this->input->post('colour'),
        'nodedef' => $this->input->post('nodedef'),
        'adminonly' => $this->input->post('adminonly'),
        'isdefault' => $this->input->post('isdefault'),
        'nodes' => 'O:8:"stdClass":5:{s:4:"year";i:2;s:16:"users_and_groups";i:3;s:9:"all_users";i:4;s:11:"role_groups";i:5;s:10:"role_group";O:8:"stdClass":4:{s:1:"1";i:6;s:1:"2";i:7;s:1:"3";i:8;s:1:"4";i:9;}}'
      );

      if ((int)($this->input->post('table') != $this->input->post('table')) && $this->input->post('table') > 0) {
        $this->load->view('admin/dataset/dataset_int_error');
        return;
      }
      $table = $this->input->post('table');
      $newdatasetfolder = BASEPATH . "../../$table";
      $newdatasetindex = "$newdatasetfolder/index.php";
      $template_path = BASEPATH . '../../install/config/index.php';

      // Open the file.
      $indexfile = file_get_contents($template_path);

      if (!file_exists($newdatasetindex)) {
        mkdir($newdatasetfolder);

        // Write the new dataset index.php file.
        $handle = fopen($newdatasetindex, 'w+');

        // Verify file permissions.
        if (is_writable($newdatasetindex)) {
          // Write the file.
          try {
            fwrite($handle, $indexfile);
            chmod($newdatasetindex, 0444);
            $this->Dataset_model->add_new($ds_data);
            if ($this->input->post('source_ds') != '') {
              $this->Dataset_model->clone_nodes_db($this->input->post('source_ds'));
            }
            $table = $this->input->post('table');
            header("Location: " . base_url() . "/../../$table/admindata/");
          } catch (Exception $e) {
            $this->load->view('admin/dataset/error');
            return;
          }
        } else {
          redirect('admindata');
        }
      } else {
          $this->load->view('admin/dataset/alreadyexists');
          $this->load->view('admin/dataset/newdataset', $this->data);
      }
    }
    $this->load->view('templates/footer');
  }

  /**
   * Checks that form field is an integer.
   * 
   * @param string $value The table prefix
   * @param string $fieldname The name of the field that is being validated. (Unused, required by the callback)
   * @return boolean
   */
  public function is_integer($value, $fieldname) {
    if ($value === (string)(int)$value) {
      // If the value casts to an integer and back to a string,
      // while not changing we can be certain it is an integer.
      $return = true;
    } else {
      $return = false;
    }
    return $return;
  }
}
