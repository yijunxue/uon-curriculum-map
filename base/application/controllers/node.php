<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

/**
 * General node controller class
 *
 * Handles lots of node functions such as create and edit
 *
 * View, Linking, and several other functions which require larger ammounts
 * of code have been separated into their own controllers
 *
 */

class Node extends CI_Controller {

	/**
	 * This is method __constructreate a new instance of the node controller
	 */
	public function __construct()
	{
		// call parent constructor
		parent::__construct();

		// setup the main menu items for the page (same for all controllers)
		setup_main_menu($this->menu);

		$this->ci =& get_instance();
	}

	/**********************************************************/
	/**
	 * ****************************************
	 * Main entry points into the controller
	 * **************************************
	 **/

	/**
	 * Redirects to the view node page
	 *
	 * @param int $nodeid ID of the node to display
	 *
	 */
	public function index($nodeid = -1)
	{
		if ($nodeid == -1)
		{
			$CI =& get_instance();
			$nodeid = $CI->session->userdata('my_node');
			//echo "Session Node ID : $nodeid<br>";
			//exit;
			if (!$nodeid)
				$nodeid = 1;
		}
		// redirect to view node
		redirect('view/'.$nodeid);
	}

	/**
	 * Create a new node, will display the form and process its result
	 *
	 * @param int $baseid Id of the parent node this is being created for
	 * @param int $type Node type id
	 *
	 */
	function create($baseid = -1, $type = 0, $nochange = 1)
	{
		$CI =& get_instance();

		// load helpers
		$this->load->helper('form');
		$this->load->library('form_validation');

		// set up page title
		$data['title'] = "Create New";

		$data['nochange'] = $nochange;

		// if we have a base node specified, then load it into $basenode
		if ($baseid > -1)
		{
			$data['basenode'] = $this->graph_db->loadNode($baseid);

			if (!$data['basenode'])
			{
				show_error("node/create Unable to find node $baseid");
			}

			if (!$data['basenode']->can_create())
				return redirect("view/" . $data['basenode']->getId());

		} else {
			$data['basenode'] = null;
		}

		// load a list of nodes that can be created
		// this will need to be restricted later based on $baseid as each node will only
		// allow certain sub node types to be created
		if ($data['basenode'])
		{
			$data['types'] = $data['basenode']->getCreateTypesForDropdown();
		} else {
			$data['types'] = $this->node_types->GetTypesForForm(false);
		}

		if ($type > 0)
		{
			$nodetype = $CI->node_types->GetType((int)$type);
			$data['title'] .= " - " . $nodetype->name;
		} else {
			foreach ($data['types'] as $id => $type)
			{
				$data['title'] .= " - " . $type;
				break;
			}
			//print_p($data['types']);
		}

		// if no type has been specified, then get the first one from the list provided
		if (!$type)
		{
			foreach ($data['types'] as $type_id => &$type_data)
			{
				if ($type_id == 0) continue;
				$type = $type_id;
				break;
			}
		}

		// node type dropdown js
		$data['type_js'] = 'onChange="type_changed(this)"';

		// make node type and base node id available to template
		$data['type'] = $type;
		$data['baseid'] = $baseid;


		$this->head->AddJS("js/lib/datepick.js");
		$this->head->AddCSS("css/lib/datepick.css");
		$this->head->AddCSS("css/lib/datepick_omega.css");
		$this->head->AddJS("js/tiny_mce/jquery.tinymce.js");
		$this->head->AddJS("js/node/edit.js");

		$this->head->AddJS("js/jquery/jquery-ui-1.8.19.custom.min.js");
		$this->head->AddCSS("css/ui-lightness/jquery-ui-1.8.19.custom.css");

		$this->head->AddLeading(form_open("node/create/$baseid/$type", array('id' => 'create_form')));

		//add form close to bottom of popup
		$this->head->AddTrailing(form_close());


		$this->head->AddFooter("<div style='float:left'><span class='req_field'>*</span> - Required Field</div>");

		// add save button to footer
		$this->head->AddFooter("<button id=\"edit_save\" class=\"footer_button\">Save</button>");

		// add save button to footer
		if ($type != 100)
			$this->head->AddFooter("<button id=\"edit_save_new\" class=\"footer_button\" onclick=\"$('#what').val('add');\">Save + New</button>");

		// add cancel button to footer
		//$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.parent.TINY.box.hide();return false;'>Cancel</button>");
		$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.location=\"".site_url("view/" . $baseid) ."\";return false;'>Cancel</button>");

		$this->head->AddFooter("<button id='bulk_import' class='footer_button' onclick='window.location=\"".site_url("node/import/" . $baseid . "/" . $type) ."\";return false;'>Bulk Import</button>");

		// we have a type specified (should always be the case)
		if ($type)
		{
			// type selected, so do proper form handling
			$typeobj = $this->node_types->getType($type);

			if (!$typeobj->no_title_req)
			{
				$this->form_validation->set_rules('title', 'Node Title', 'required');
			} else {
				$this->form_validation->set_rules('title', 'Node Title');
			}

			// load the type object for the selected node type
			$data['type_obj'] = $this->node_types->GetType($type);

			// load available attributes for the node type
			$attrs = $data['type_obj']->GetAttributes();

			if ($type == 100 && array_key_exists('type', $_GET) && $_GET['type'] != "")
			{
				foreach ($attrs as $attr)
				{
					if ($attr->id != "type") continue;

					foreach ($attr->values as $id => $name)
					{
						if ($id == $_GET['type'])
						{
							$data['title'] = "Create New - " . $name;
						}
					}
				}
			}

			// setup the validation for the attributes
			if (count($attrs) > 0)
			{
				foreach($attrs as $attr)
				{
					$attr->SetValidation($this->form_validation);
				}
			}
		}

		// output header
		$this->load->view('templates/header', $data);

		// if the form fails to validate (ie, not submitted or submitted with errors, then display the create tempalte
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('node/create', $data);
		}
		else // form submitted sucessfully, so create the node
		{
			$this->create_node($data, $baseid, $type);
		}
		$this->load->view('templates/footer');
	}

	/**
	 * Node edit main call
	 *
	 * @param mixed $nodeid This is a description
	 * @return mixed This is the return value description
	 *
	 */
	function edit($nodeid)
	{
		$this->load->helper('form');

		$data['node'] = $this->graph_db->loadNode($nodeid);

		if (!$data['node'])
		{
			show_error("node/edit Unable to find node $nodeid");
		}

		if (!$data['node']->can_edit())
			return redirect("view/" . $data['node']->getId());

		$data['title'] = "Edit Node - " . $data['node']->getTitleDisp();
		$data['nodeid'] = $data['node']->GetID();
		$this->load->library('form_validation');

		$typeobj = $data['node']->getTypeObj();

		if (!$typeobj->no_title_req)
		{
			$this->form_validation->set_rules('title', 'Node Title', 'required');
		} else {
			$this->form_validation->set_rules('title', 'Node Title');
		}

		$data['type_obj'] = $this->node_types->GetType($data['node']->GetType());
		$attrs = $data['type_obj']->GetAttributes();
		if (count($attrs) > 0)
		{
			foreach($attrs as $attr)
			{
				$attr->SetValidation($this->form_validation);
			}

		}

		$this->head->AddJS("js/lib/datepick.js");
		$this->head->AddCSS("css/lib/datepick.css");
		$this->head->AddCSS("css/lib/datepick_omega.css");

		$this->head->AddJS("js/tiny_mce/jquery.tinymce.js");

		$this->head->AddJS("js/jquery/jquery-ui-1.8.19.custom.min.js");
		$this->head->AddCSS("css/ui-lightness/jquery-ui-1.8.19.custom.css");

		$this->head->AddJS("js/node/edit.js");

		// add form open to top of popup
		$attributes = array('id' => 'edit_form');
		$this->head->AddLeading(form_open("node/edit/$nodeid", $attributes));

		//add form close to bottom of popup
		$this->head->AddTrailing(form_close());

		$this->head->AddFooter("<div style='float:left'><span class='req_field'>*</span> - Required Field</div>");

		// add save button to footer
		$this->head->AddFooter("<button id=\"edit_save\" class=\"footer_button\">Save</button>");

		// add cancel button to footer
		$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.location=\"".site_url(set_value('return', get_referer())) ."\";return false;'>Cancel</button>");

		$this->load->view('templates/header', $data);
		if ($this->form_validation->run() == FALSE)
		{
			$this->load->view('node/edit', $data);
		}
		else
		{
			$this->save_node($data);
		}

		$this->load->view('templates/footer');
	}

	/**
	 * This is method history
	 *
	 * @param mixed $nodeid This is a description
	 * @return mixed This is the return value description
	 *
	 */
	function history($nodeid = 1)
	{

		$data['node'] = $this->graph_db->loadNode($nodeid);

		if (!$data['node'])
		{
			show_error("node/history Unable to find node $nodeid");
		}

		if (!$data['node']->can_history())
			return redirect("view/" . $data['node']->getId());

		$data['history'] = $data['node']->GetHistory();
		$data['title'] = "History - " . $data['node']->getTitleDisp();

		// add cancel button to footer
		$this->head->AddFooter("<button id='hist_close' class='footer_button' onclick='window.parent.TINY.box.hide();return false;'>Close</button>");
		$this->head->AddJS("js/node/history.js");

		$this->load->view('templates/popup/header', $data);
		$this->load->view('node/history', $data);
		$this->load->view('templates/popup/footer');
	}

	/**
	 * Not sure if this is still required here or not
	 *
	 */
	function unlink($nodeid, $destid)
	{
		$node = $this->graph_db->loadNode($nodeid);
		$noderel = $this->graph_db->loadNode($destid);
		// Check that the user has permission to remove the link
		if ($node->can_link()) { // User does have permission to remove the link.
			$node->removeRelate($noderel);
			$node->save();

			$this->head->AddMessage("The relationship has been removed");
		} else { // User does not have permission to remove the link.
			$this->head->AddMessage("You do not have permission to remove the relationship");
		}
		redirect("view/" . $nodeid);
	}

	function find()
	{
		$this->load->helper('form');

		$data['types'] = $this->node_types->GetTypesForForm();
		$data['type'] = 0;

		$this->head->AddJS("js/node/search.js");
		$this->head->AddJS("js/node/subnodes.js");
		$this->head->AddRawScript('var ajax_base_url = "' . site_url('node/ajax/XXTYPEXX/0/XXIDXX') . '";');
		$this->head->AddRawScript('var ajax_url_search = "' . site_url('node/ajax/XXTYPEXX/0/XXSEARCHXX/XXTYPEIDXX') . '";');

		$data['title'] = "Find Node";

		$this->load->view('templates/header', $data);
		$this->load->view('general/search', $data);
		$this->load->view('templates/footer');
	}

	function delete($nodeid)
	{
		$this->load->helper('form');

		$data['title'] = "Delete Node";
		$data['node'] = $this->graph_db->loadNode($nodeid);

		if (!$data['node'])
		{
			show_error("node/link Unable to find node $nodeid");
		}

		if (!$data['node']->can_delete())
			return redirect("view/" . $data['node']->getId());

		// add form open to top of popup
		$attributes = array('id' => 'delete_form');
		$this->head->AddLeading(form_open("node/procdel/$nodeid", $attributes));

		//add form close to bottom of popup
		$this->head->AddTrailing(form_close());

		//$this->head->AddFooter("<div style='float:left'><span class='req_field'>Please Note: This feature is currently unfinished. It is currently disabled.</span></div>");

		// add save button to footer
		$this->head->AddFooter("<button id='do_delete' class='footer_button' disabled='disabled'>Delete</button>");

		// add cancel button to footer
		$this->head->AddFooter("<button id='cancel_delete' class='footer_button' onclick='window.location=\"".site_url(set_value('return', get_referer())) ."\";return false;'>Cancel</button>");

		$this->head->AddJS("js/node/delete.js");
		$this->head->AddJS("js/node/perms.js");

		// load children
		$rels = $data['node']->getRelations_GroupByRelType_GroupByNodeType();

		if (!array_key_exists(GDB_Rel::ChildOf,$rels))
			$rels[GDB_Rel::ChildOf] = array();

		$data['children'] =& $rels[GDB_Rel::ChildOf];
		$data['relcounts'] = $this->graph_db->GetRelCounts($data['node']->getRelationIDs(GDB_Rel::ChildOf), GDB_Rel::ChildOf);

		$data['has_children'] = count($data['children']) > 0 ? 1 : 0;


		$this->load->view('templates/header', $data);
		$this->load->view('node/delete', $data);
		$this->load->view('templates/footer');
	}


	public function procdel($nodeid)
	{
		//echo "Delete  $nodeid<br>";
		//print_p($_POST);

		$node = $this->graph_db->loadNode($nodeid);
		$parent = $node->getParent();

		$this->dodelete($nodeid);

		$this->head->AddMessage("Node deleted");

		/*if (array_key_exists('return', $_POST) && $_POST['return'] != "")
		{
			//$this->head->ParentRefresh($referer);
			redirect($_POST['return']);
		} else {*/
			redirect("view/" . $parent->getID());
		//}
	}

	private function dodelete($nodeid)
	{
		$node = $this->graph_db->loadNode($nodeid);

		if (!$node->can_delete())
			return;

		$node->delete();
		$node->save();

		$rels = $node->getRelations_OfType(GDB_Rel::ChildOf);
		foreach ($rels as &$rel)
		{
			$this->dodelete($rel->getId());
		}
	}


	/**
	 * Generic ajax call. Will call a function based on $type passed
	 */
	public function ajax($type, $param1 = "", $param2 = "", $param3 = "", $param4 = "")
	{
		$function = "ajax_$type";
		if (method_exists($this,$function))
		{

			$debug = array_key_exists('debug', $_GET);
			$data =& $this->$function($param1, $param2, $param3, $param4);
			if ($debug == 1)
			{
				$this->load->view('templates/header', $data);
				if ($data)
					$this->load->view("node/ajax/$type", $data);
				$this->load->view('templates/footer');
			} else {
				if ($data)
					$this->load->view("node/ajax/$type", $data);
			}
		} else {
			echo json_encode(array('error' => "Invalid AJAX request, 'Node::ajax_$type()' does not exist"));
		}
	}

	/**********************************************************/
	/**
	 * *****************************************
	 * Functions used by the main entry points
	 * *****************************************
	 **/

	/**
	 * Perform the actual node creation, including link back to its parent node
	 *
	 * @param mixed $data Array of data from create
	 *
	 */
	private function create_node(&$data, $baseid = -1, $type = 0)
	{
		// called with a data array containing several things from the create function
		// shouldnt be called directly really
		// used to be inline above, but as separate function makes the code nicer

		// store node
		$node_type = $data['type'];
		$node_title = set_value('title');

		// create the node
		$node = $this->graph_db->createNode($node_title, $node_type, $data['basenode']);
		$node->save();

    $type_obj = $node->GetTypeObj();

		$reset_order = false;

    // set up its attributes based on the node type
		foreach($data['type_obj']->GetAttributes() as $attr)
		{
			$Value = $attr->GetPostValue($node);
			$Value = html_entity_decode($Value);
			$node->setAttribute($attr->id, $Value, $attr->type);

			// If we're setting a group, also set an order within the group
			if ($attr->id == 'group') {
				$reset_order = $Value;
			}

			if ($attr->autocomplete && $Value != "")
			{
				$qry = "REPLACE INTO lookups (user_id, node_type, field, value) VALUES (?, ?, ?, ?)";
				$user_id = $this->dx_auth->get_user_id();
				$data = array($user_id, $type_obj->gdbid, $attr->id, $Value);
				$this->db->query($qry, $data);
			}
		}

		// We've set a group for the node so make sure that the order is correct within the group
		if ($reset_order !== false) {
			$group_order = $this->get_group_order($node, $type_obj->id, $reset_order);
			$node->setAttribute('order', $group_order);
		}

		// if a base node has been passed, then we should link to it as a child
		//if (array_key_exists('basenode',$data) && $data['basenode'])
		//	$node->addRelate($data['basenode'], GDB_Rel::ChildOf);
		// save the node being created
		$node->save();

    $this->head->AddMessage($type_obj->name . ": " . $node->getTitle() . " created" . " (" . $node->getID() .")");


    $this->do_postedit_hooks('create', $node, $type_obj);

    //exit;

		if ($this->input->post('what') == "add")
		{
			redirect('node/create/' . $baseid . '/' . $type . '/1' );
		}

		if (array_key_exists('return', $_POST) && $_POST['return'] != "")
		{
			//$this->head->ParentRefresh($referer);
			redirect($_POST['return']);
		} else if ($data['baseid'])
		// if we have been created from an existing node, go back to it
		{
			//$this->head->ParentRefresh('view/' . $data['baseid']);
			redirect('view/' . $data['baseid']);
		} else {
			// redirect to node display page
			//$this->head->ParentRefresh('view/' . $node->GetID());
			redirect('view/' . $node->GetID());
		}
	}

	/**
	 * This is method save_node
	 *
	 * @param mixed $data This is a description
	 * @return mixed This is the return value description
	 *
	 */
	private function save_node(&$data)
	{
		//echo "SAVE NODE<br>";
		$node = $this->graph_db->loadNode($data['nodeid']);

		//print_p($_POST);

		$newtitle = set_value('title');
		if ($newtitle != $node->getTitle())
		{
			//echo "Set Title $newtitle<br>";
			$node->setTitle($newtitle);
		}

		$type_obj = $node->getTypeObj();

		$reset_order = false;
		foreach($data['type_obj']->GetAttributes() as $attr)
		{
			if ($attr->noedit) continue;
			$newValue = $attr->GetPostValue($node);
			$newValue = html_entity_decode($newValue);
			//echo "Setting {$attr->id} ($attr->type) to " . htmlentities($newValue) . "<br>";

			// If we're changing the group, reset the order field
			if ($attr->id == 'group') {
				$currValue = $node->getAttribute($attr->id);
				if ($currValue != $newValue) {
					$reset_order = $newValue;
				}
			}

			$node->setAttribute($attr->id, $newValue, $attr->type);

			if ($attr->autocomplete && $newValue)
			{
				$qry = "REPLACE INTO lookups (user_id, node_type, field, value) VALUES (?, ?, ?, ?)";
				$user_id = $this->dx_auth->get_user_id();
				$data = array($user_id, $type_obj->gdbid, $attr->id, $newValue);
				$this->db->query($qry, $data);
			}
		}

		// Special handling for changed groups. If new group, add to end of node list for the group.
		// If unset group, remove the existing order
		if ($reset_order !== false) {
			$group_order = $this->get_group_order($node, $type_obj->id, $reset_order);
			$node->setAttribute('order', $group_order);
		}

		$node->save();

		$this->do_postedit_hooks('edit', $node, $type_obj);

		// redirect to node display page
		$this->head->AddMessage("Changes saved!");

		//exit;
		//$this->head->ParentRefresh();
		if (array_key_exists('return', $_POST) && $_POST['return'] != "")
		{
			//$this->head->ParentRefresh($referer);
			redirect($_POST['return']);
		} else {
			redirect('view/' . $node->GetID());
		}

	}

	/**********************************************************/
	/**
	 * ******************************
	 * AJAX sub entry points
	 * ******************************
	 **/

	/**
	 * This is method ajax_related
	 *
	 * @param mixed $baseid This is a description
	 * @param mixed $nodeid This is a description
	 * @return mixed This is the return value description
	 *
	 */
	private function ajax_related($baseid, $nodeid)
	{
		$node = $this->graph_db->loadNode($nodeid);
		$data['subdata']['rels'] = $node->getRelations_GroupByNodeType();
		$data['subdata']['node'] =	$this->graph_db->loadNode($baseid);
		$data['subdata']['path'] = $this->input->get('path');
		$data['subdata']['baseid'] = $baseid;
		return $data;
	}

	/**
	 * This is method ajax_add_relate
	 *
	 * @param mixed $baseid This is a description
	 * @param mixed $nodeid This is a description
	 * @return mixed This is the return value description
	 *
	 */
	private function ajax_add_relate($baseid, $nodeid)
	{
		$targetnode = $this->graph_db->loadNode($nodeid);
		$basenode = $this->graph_db->loadNode($baseid);
		if (!$basenode->isRelatedTo($targetnode))
		{
			$basenode->addRelate($targetnode);
			$basenode->save();
		}

		if (count(ob_list_handlers()) > 0) {
			ob_clean();
		}
		echo json_encode(array('state' => 'yes'));

		return false;
	}

	/**
	 * This is method ajax_remove_relate
	 *
	 * @param mixed $baseid This is a description
	 * @param mixed $nodeid This is a description
	 * @return mixed This is the return value description
	 *
	 */
	private function ajax_remove_relate($baseid, $nodeid)
	{
		$targetnode = $this->graph_db->loadNode($nodeid);
		$basenode = $this->graph_db->loadNode($baseid);
		if ($basenode->isRelatedTo($targetnode))
		{
			$basenode->removeRelate($targetnode);
			$basenode->save();
		}

		if (count(ob_list_handlers()) > 0) {
			ob_clean();
		}
		echo json_encode(array('state' => 'no'));

		return false;
	}

	/**
	 * Search for nodes
	 *
	 * @param mixed $baseid This is a description
	 * @param mixed $string This is a description
	 * @return mixed This is the return value description
	 *
	 */
	private function ajax_search($baseid, $string, $type = 0)
	{
		$type = urldecode($type);

		//echo "Search : $type<br>";

		$basenode = $this->graph_db->loadNode($baseid);

		$search_nodes = $this->graph_db->findNodes($string, $type, 50);

		if (array_key_exists('error',$search_nodes))
		{
			echo $this->head->MessageText($search_nodes['error']);
			return null;
		}

		$have_nodes = array();
		foreach ($search_nodes as &$node)
			$have_nodes[$node->getID()] = 1;

		// if we have a type specified, check for any search attributes, and find nodes based on the attributes too
		if ($type > 0 && strpos($type, "|") < 1)
		{
			$node_type = $this->node_types->getType($type);
			$attribs = $node_type->GetAttributes();
			//print_p($attribs);
			foreach ($attribs as $attrib)
			{
				if (!$attrib->search) continue;

				$attr_res = $this->graph_db->findNodeFromAttr($type,$attrib->id, $string, false);

				if (is_array($attr_res))
				{
					foreach ($attr_res as &$node)
					{
						if (!array_key_exists($node->GetID(),$have_nodes))
						{
							$have_nodes[$node->getID()] = 1;
							$search_nodes[] = $node;
						}
					}
				}
			}
		}

		if (count($search_nodes) == 0)
		{
			echo $this->head->MessageText("No results found");
			return null;
		}

		$data['subdata']['rels'] = $this->graph_db->groupNodes_Type($search_nodes);
		$data['subdata']['node'] =	$this->graph_db->loadNode($baseid);
		$data['subdata']['baseid'] = $baseid;
		$data['subdata']['path'] = 0;

		return $data;
	}

	public function reporting($nodeid = -1)
	{
		$CI =& get_instance();
		$this->load->helper('form');

		// load helpers
		// set up page title
		$data['title'] = "API / Reporting : ";

		// if we have a base node specified, then load it into $basenode
		$data['node'] = $this->graph_db->loadNode($nodeid);
		$data['nodeid'] = $nodeid;

		if (!$data['node'])
		{
			show_error("node/create Unable to find node $nodeid");
		}

		$folder = $data['node']->GetTypeObj()->id;

		$basedir = $CI->node_def;
		$basepath = "nodedef/$basedir/api/$folder/";

		$files = get_filenames($basepath);

		$data['items'] = array();

		//echo "API Path: $basepath<br>";
		//print_p($files);

		if (count($files) > 0 && is_array($files))
		{
			foreach ($files as &$file)
			{
				if (stripos($file, ".xml") < 1) continue;
				$xml = @simplexml_load_file($basepath.$file);
				if (!$xml) continue;

				$item = new stdClass();
				$item->file = $file;
				$item->id = str_ireplace(".xml","",$file);
				$item->title = (string)$xml->title;
				$item->description = (string)$xml->description;

				$data['items'][] = $item;
			}
		}

		$basepath = "nodedef/$basedir/reports/$folder/";

		$files = get_filenames($basepath);

		$data['reports'] = array();

		//echo "Report Path: $basepath<br>";

		if (count($files) > 0 && is_array($files))
		{
			foreach ($files as &$file)
			{
				if (stripos($file, ".xml") < 1) continue;
				$xml = @simplexml_load_file($basepath.$file);
				if (!$xml) continue;

				if (isset($xml->show) && $xml->show == 'false') continue;

				$item = new stdClass();
				$item->file = $file;
				$item->id = str_ireplace(".xml","",$file);
				$item->title = (string)$xml->title;
				$item->description = (string)$xml->description;

				if (property_exists($xml, 'csv_type') && $xml->csv_type == 'csv') {
					$item->csv = true;
				}

				$data['reports'][] = $item;
			}
		}

		$data['title'] .= $data['node']->GetTitleDisp();
		// setup node sub menu

		$this->head->AddFooter("<button id='cancel_delete' class='footer_button' onclick='window.location=\"".site_url("view/" .$nodeid) ."\";return false;'>Cancel</button>");

		$this->data = &$data;
		// output header
		$this->load->view('templates/header', $data);
		$this->load->view('node/report', $data);
		$this->load->view('templates/footer');
	}

	function lookups($nodeid, $attrib)
	{
		$term = $_GET['term'];

		$user_id = $this->dx_auth->get_user_id();
		$node = $this->graph_db->loadNode($nodeid);
		$nodetype = $node->getTypeObj();

		$values = array();

		/*if ($user_id > 0)
		{
			echo "Lookup : $nodeid, $attrib, $user_id<br>";

			$qry = "SELECT value FROM lookups WHERE user_id = ? AND node_type = ? AND field = ?";
			$data = array($user_id, $nodetype->gdbid, $attrib);

			$query = $this->db->query($qry, $data);
			foreach ($query->result() as $row)
			{
				$values[$row->value] = $row->value;
			}
		}*/

		// sibling lookip
		$parent = $node->getParent();

		//echo "Node Type : {$nodetype->id}<br>";

		$siblings = $parent->getRelations($nodetype->id);

		foreach ($siblings as &$node)
		{
			$value = $node->getAttributeValue($attrib);
			if ($value == "") continue;

			$values[$value] = $value;
			//$node->dump();
		}

		$result = array();

		foreach ($values as $value)
		{
			if (stripos($value, $term) !== false)
				$result[] = $value;
		}
		sort($result);

		//print_p($values);

		echo json_encode(array_slice($result,0,20));
	}

	function import($baseid, $type)
	{
		// do a bulk import from csv file

		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['nodetype'] = $this->ci->node_types->getType($type);
		$data['node'] = $this->graph_db->loadNode($baseid);

		$data['title'] = "Bulk import '{$data['nodetype']->name}' for '{$data['node']->getTitle()}'";

		$this->head->AddLeading(form_open_multipart($this->uri->uri_string(), array('id' => 'import_form')));

		//add form close to bottom of popup
		$this->head->AddTrailing(form_close());


		//$this->head->AddFooter("<div style='float:left'><span class='req_field'>*</span> - Required Field</div>");

		// output header

		// if the form fails to validate (ie, not submitted or submitted with errors, then display the create tempalte

		$result = $this->do_import($baseid, $type);

		$this->load->view('templates/header', $data);

		if (!$result)
		{

			// add save button to footer
			$this->head->AddFooter("<button id='edit_save' class='footer_button'>Import</button>");

			// add cancel button to footer
			$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.location=\"".site_url("view/" . $baseid) ."\";return false;'>Cancel</button>");

			$this->load->view('node/import', $data);
		}
		else // form submitted sucessfully, so create the node
		{
			$data['title'] = "Import '{$data['nodetype']->name}' for '{$data['node']->getTitle()}' completed";
			$data['nodes'] = $result;
			$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.location=\"".site_url("view/" . $baseid) ."\";return false;'>Done</button>");
			$this->load->view('node/import_done', $data);
		}
		$this->load->view('templates/footer');
	}

	function do_import($baseid, $type)
	{
		$this->load->helper('csv');


		if (!array_key_exists("csvfile", $_FILES))
			return false;

		$file = $_FILES['csvfile'];
		$nodetype = $this->ci->node_types->getType($type);

		$data = load_csv_file($file['tmp_name']);

		if (count($data) < 1)
		{
			$this->head->AddMessage("Unable to load file");
			return false;
		}

		if (!array_key_exists("title", $data[0]))
		{
			$this->head->AddMessage("Missing title attribute in csv file");
			return false;
		}

		$parnode = $this->graph_db->loadNode($baseid);

		$imported = array();

		foreach ($data as &$row)
		{
			if (!array_key_exists("title", $row)) contine;

			// Stop the import of blank lines. Issue #42
			if (empty($row['title']))
			{
				if ($type == 6 || $type == 7 || $type == 8 || $type == 9 || $type == 13 || $type == 16)
				{
					// It is one of the outcome nodes.
					if (!array_key_exists("desc", $row))
					{
						// Does not have a description or a title.
						continue;
					}
					if (empty($row['desc']))
					{
						// Both the title and description fields are empty.
						continue;
					}
				}
				else
				{
					// Skip import if no title is set and not a node that works without one.
					continue;
				}
			}

			$title = $row['title'];

			$node = $this->graph_db->createNode($title, $type, $parnode->getId());

			$group = false;
			foreach ($nodetype->attrs as &$attr)
			{
				$value = '';

				if (array_key_exists($attr->id, $row))
				{
					// attribute present, try to import it!
					$value = $row[$attr->id];
					if ($attr->type == GDB_Attrib_Type::_Date || $attr->type == GDB_Attrib_Type::_DateTime || $attr->type == GDB_Attrib_Type::_Time)
					{
						//echo "Converted time $value to ";
						$value = date("Y-m-d H:i:s",strtotime($value));
						//echo "$value <br>";
					}
					$node->setAttribute($attr->id, $value, $attr->type);
				}

				if ($attr->id == 'group') {
					$group = $value;
				}
			}

			//$node->dump();

			$node->save();

			// All nodes with a group attribute should now have a group order, if not a group
			if ($group !== false) {
				$group_order = $this->get_group_order($node, $nodetype->id, $group);
				$node->setAttribute('order', $group_order);
			}

			$node->save();

			$imported[] = $node;

			//echo "Imported " . $node->getID() . "<br>";
		}
		return $imported;
	}

  /**
   * Run any postedit hooks defined for this node
   * @param $edit_type
   * @param $node
   * @param $type_obj
   */
  private function do_postedit_hooks($edit_type, $node, $type_obj) {
		$CI =& get_instance();
		$basedir = $CI->node_def;

		if ($type_obj->hasHooks and isset($type_obj->hooks['postedit'])) {
			foreach ($type_obj->hooks['postedit'] as $hook) {
				if ($edit_type == 'edit' and isset($hook['noedit']) and $hook['noedit'] == '1') continue;
				if ($edit_type == 'create' and isset($hook['nocreate']) and $hook['nocreate'] == '1') continue;
				$filename = strtolower($hook['class']);
				$file = "nodedef/{$basedir}/hooks/{$filename}.php";
				if (file_exists($file)) {
					require_once($file);
					$hook_inst = new $hook['class']();
					$hook_inst->execute($node);
				}
			}
		}
  }

	private function get_group_order($node, $type, $group_text) {
		$group_o = '';
		$max_group_o = '';
		$item_o = '';
		$this->graph_db->clearLoadedNodes();
		$parent = $node->getParent();
		$rels = $parent->getRelations($type);

		foreach ($rels as $outcome) {
			$order_a = $outcome->getAttribute('order')->getValue();
			$order_parts = explode('|', $order_a);
			if ($max_group_o == '' or intval($order_parts[0]) > intval($max_group_o)) {
				$max_group_o = $order_parts[0];
			}
			if ($outcome->getID() != $node->getID()) {
				$group = $outcome->getAttribute('group')->getValue();
				if ($group == $group_text) {
					if ($order_a != '') {
						$group_o = $order_parts[0];
						if ($item_o == '' or intval($order_parts[1]) > intval($item_o)) {
							$item_o = $order_parts[1];
						}
					}
				}
			}
		}

		if ($item_o != '') {
			$item_no = intval($item_o) + 1;
			$item_o = str_pad(strval($item_no), 3, '0', STR_PAD_LEFT);
			$item_o = $group_o . '|' . $item_o;
		} else {
			if ($max_group_o == '') {
				$max_group_o = '000';
			} else {
				$max_group_o = intval($max_group_o) + 1;
				$max_group_o = str_pad(strval($max_group_o), 3, '0', STR_PAD_LEFT);
			}
			$item_o = $max_group_o . '|000';
		}

		return $item_o;
	}
}
