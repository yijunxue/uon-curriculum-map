<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

class Perms extends CI_Controller {

	public function __construct()
	{
		// call parent constructor
		parent::__construct();	
		
		// setup the main menu items for the page (same for all controllers)
		setup_main_menu($this->menu);	
	}

	// main permissions index page
	public function index($nodeid = '')
	{
		// show display the users initial node here
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		// if we have a page passed
		$this->data['title'] = "Node Permissions"; // Capitalize the first letter
		$this->data['node'] = $this->graph_db->loadNode($nodeid);
		$this->data['type_obj'] = $this->data['node']->getTypeObj();
		
		if (!$this->data['node']->can_perms())
			return redirect("view/" . $this->data['node']->getId());
	
		$this->data['perms'] = $this->data['node']->getPerms();

		// Check for incorrect premissions object (can happen after cloning)
		$this->check_perm_node_types($this->data['perms']);

		//$this->head->AddFooter("<div style='float:left'><span class='req_field'>Please Note: This feature is currently unfinished. It is currently disabled.</span></div>");	
		$this->head->AddJS("js/node/perms.js");
		
		// add cancel button to footer
		$this->head->AddFooter("<button id='add_perms' class='footer_button' onclick='window.location=\"".site_url("perms/add/" .$nodeid) ."\";return false;'>New Permission</button>");

		// add cancel button to footer
		$this->head->AddFooter("<button id='add_perms' class='footer_button' onclick='window.location=\"".site_url("perms/add/" .$nodeid . "/base") ."\";return false;'>Set Base Permission</button>");

		// add cancel button to footer
		$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.location=\"".site_url("view/" .$nodeid) ."\";return false;'>Close</button>");

		$this->head->AddJS("js/perms/list.js");

		// load children		
		$rels = $this->data['node']->getRelations_GroupByRelType_GroupByNodeType();

		if (!array_key_exists(GDB_Rel::ChildOf,$rels))
			$rels[GDB_Rel::ChildOf] = array();
		
		if (!array_key_exists(GDB_Rel::ParentOf,$rels))
			$rels[GDB_Rel::ParentOf] = array();
		
		$this->data['children'] =& $rels[GDB_Rel::ChildOf];
		$this->data['parents'] =& $rels[GDB_Rel::ParentOf];
		$this->data['relcounts'] = $this->graph_db->GetRelCounts($this->data['node']->getRelationIDs(GDB_Rel::ChildOf), GDB_Rel::ChildOf);
		
		// load parents

		$this->load->view('templates/header', $this->data);
		$this->load->view('perms/index', $this->data);
		$this->load->view('templates/footer', $this->data);			
	}
	
	private function check_perm_node_types(&$perms) {
		foreach ($perms as $perm)
		{
			if ($perm->nodeid != 1) {
				// Not base permissions
				
				if ($perm->groupid != '' && $perm->groupid > 0)
				{
					$node = $this->graph_db->loadNode($perm->groupid);
					if (empty($node)) {
						$perm->doesnotexist = true;
						continue;
					}
					if (!in_array($node->getTypeId(), array('perm_group', 'perm_user')))
					{
						$perm->typeerror = true;
					}
				}
			}
		}

	}

	// add/edit permissions page
	public function add($nodeid = '', $groupid = -1, $reassign = false)
	{
		// show display the users initial node here
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		// if we have a page passed
		if ($groupid > 0)
		{
			$this->data['title'] = "Node Permissions - Edit Permission Set";
			$this->data['is_add'] = 0;
		}  else {
			$this->data['title'] = "Node Permissions - Create Permission Set";
			$this->data['is_add'] = 1;
		}
		$this->data['node'] = $this->graph_db->loadNode($nodeid);
		
		if (!$this->data['node'])
			redirect("/");
		
		$this->data['nodeid'] = $this->data['node']->getID();
		$this->data['type_obj'] = $this->data['node']->getTypeObj();
		$this->data['groupid'] = $groupid;
		$this->data['perms'] = $this->data['node']->getPerms();
		$this->data['is_reassign'] = 0;
		
		if (!$this->data['node']->can_perms())
			return redirect("view/" . $this->data['node']->getId());
	
		//$this->head->AddFooter("<div style='float:left'><span class='req_field'>Please Note: This feature is currently unfinished. It is currently disabled.</span></div>");	
		
		if ($groupid > 0)
		{
			$groupnode = $this->graph_db->loadNode($groupid);
			if (!$groupnode)
				show_error("Invalid Group or User");	
			
			if ($groupnode->getType() != 501 && $groupnode->getType() != 502 && !$reassign)
				show_error("Invalid Group or User");	
			
			$this->data['groupnode'] = $groupnode;
			
			if ($reassign) {
				$this->data['is_reassign'] = 1;
				$this->data['origgroupnode'] = $groupnode;
			}
		}
		
		$this->head->AddJS("js/node/perms.js");
		$this->head->AddJS("js/lib/datepick.js");
		$this->head->AddJS("js/lib/dateformat.js");
		$this->head->AddCSS("css/lib/datepick.css");
		$this->head->AddCSS("css/lib/datepick_omega.css");
		$this->head->AddJS("js/perms/add.js");

		$hidden = array('nodeid' => $nodeid);
		$this->head->AddLeading(form_open("perms/processadd", array('id' => 'permadd_form'/*, 'target' => 'form_result'*/), $hidden));					
					
		//add form close to bottom of popup
		$this->head->AddTrailing(form_close());					

		if ($groupid == -1)
		{
			$this->head->AddFooter("<div style='float:left'><span class='req_field' id='select_warn'>Please select a group or user.</span></div>");	
		} /*elseif ($groupid == -1) {
			$this->head->AddFooter("<div style='float:left'><span class='req_field' id='select_warn'>Warning: Updating the base permissions on child nodes will overwrite any existing permissions on the nodes.</span></div>");		
		}*/

		
		// add cancel button to footer
		$dis = "";
		if ($groupid == -1)
			$dis = "disabled='disabled'";
		$this->head->AddFooter("<button id='add_save' class='footer_button' $dis>Save</button>");

		// add cancel button to footer
		$this->head->AddFooter("<button id='add_cancel' class='footer_button' onclick='window.location=\"".site_url("perms/index/" .$nodeid) ."\";return false;'>Cancel</button>");
	
		$this->data['relations'] = $this->data['node']->getRelations_GroupByNodeType_GroupByRelType();
		$this->data['relcounts'] = $this->graph_db->GetRelCounts($this->data['node']->getRelationIDs(GDB_Rel::ChildOf), GDB_Rel::ChildOf);
		$this->data['child_count'] = count($this->data['node']->getRelationIDs(GDB_Rel::ChildOf));
		
		$this->load->view('templates/header', $this->data);
		if ($groupid != 0)
		{
			$this->load->view('perms/add', $this->data);
		} else {

			$this->data['base_perms'] = $this->data['node']->getBasePerms();
			
			$this->load->view('perms/base', $this->data);
		}
		$this->load->view('templates/footer', $this->data);				
	}	
	
	public function reassign($nodeid = '', $groupid = -1) {
		$this->add($nodeid, $groupid, true);
	}

	// select user or group page
	public function pickgroup($sourceid = 0)
	{
		$this->data['title'] = "Select User or Group";
		
		$this->data['types'][0] = 'User And Groups';
		$this->data['types'][501] = 'Users';
		$this->data['types'][502] = 'Groups';
		$this->data['type'] = 0;
			
		// find the year node off the root node
		$rootnode = $this->graph_db->loadNode(1);
		$yearnodes = $rootnode->getRelations("year");
		
		$this->data['year_node'] = &$yearnodes[0];
				
		// need to build a group list to display in a select box
		$this->data['grouplist'] = $this->GetGroupListing();
				
		$this->data['pick_xml'] = $this->MakeXMLForUserList();
			
		$this->load->helper('form');
		$this->load->library('form_validation');
	
		$this->head->AddJS("js/perms/pickuser.js");

		// add cancel button to footer
		$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.parent.TINY.box.hide();return false;'>Cancel</button>");
		$this->head->AddRawScript('var ajax_url_search = "' . site_url('perms/ajax/search/XXSEARCHXX/XXTYPEIDXX') . '";');
		
		//var url = base_url + "perms/ajax/search/" + encodeURIComponent(current_search) + "/" + encodeURIComponent(current_type);
    
		$this->data['popupheight'] = 601;

		$this->load->view('templates/popup/header', $this->data);
		$this->load->view('perms/pick', $this->data);
		$this->load->view('templates/popup/footer');
	}
	
	function MakeXMLForUserList($groupid = 'all_users')
	{
		$xml = '<section id="users" type="longtree">
					<start>'.$groupid.'</start>
					<layer path="perm_user" />
				</section>';

		return $xml;
	}
	
	function GetGroupListing()
	{
		// return a list of groups available
		
	}	
		
	/**
	 * This is method ajax
	 *
	 * @param mixed $type This is a description
	 * @param mixed $param1 This is a description
	 * @param mixed $param2 This is a description
	 * @param mixed $param3 This is a description
	 * @param mixed $param4 This is a description
	 * @return mixed This is the return value description
	 *
	 */
	function ajax($type, $param1 = "", $param2 = "", $param3 = "", $param4 = "")
	{
		//echo "ajax($type, $param1, $param2, $param3, $param4)<br>";
		$function = "ajax_$type";
		if (method_exists($this,$function))
		{
			
			$debug = array_key_exists('debug', $_GET);
			$data =& $this->$function($param1, $param2, $param3, $param4);	
			if ($debug == 1)
			{
				$this->load->view('templates/header', $data);
				if ($data)
					$this->load->view("perms/ajax/$type", $data);
				$this->load->view('templates/footer');	
			} else {
				if ($data)
					$this->load->view("perms/ajax/$type", $data);
			}
		} else {
			echo json_encode(array('error' => "Invalid AJAX request, 'Node::ajax_$type()' does not exist"));		
		}
	}
	
	
	/**
	 * When picking a user or group, handle the ajax group expand 
	 */
	private function ajax_groups($baseid)
	{
		//echo "ajax_related($baseid)<br>";
		$node = $this->graph_db->loadNode($baseid);
		$data['subdata']['nodes'] = $node->getRelations('perm_group');
		$data['subdata']['node'] =	$node;
		$data['subdata']['path'] = $this->input->get('path');
		$data['subdata']['baseid'] = $baseid;
		return $data;
	}
	
	// handle the expand on the main permissions page when listing child nodes
	private function ajax_list_children($baseid)
	{
		//echo "ajax_related($baseid)<br>";
		$node = $this->graph_db->loadNode($baseid);
		$rels = $node->getRelations_GroupByRelType_GroupByNodeType();
		if (!array_key_exists(GDB_Rel::ChildOf,$rels))
			$rels[GDB_Rel::ChildOf] = array();
		
		$data['subdata']['rels'] =& $rels[GDB_Rel::ChildOf];
		$data['subdata']['node'] =	$node;
		$data['subdata']['path'] = $this->input->get('path');
		$data['subdata']['baseid'] = $baseid;
		$data['subdata']['relcounts'] = $this->graph_db->GetRelCounts($node->getRelationIDs(GDB_Rel::ChildOf), GDB_Rel::ChildOf);
		
		return $data;
	}
	
	// lists the children for add/edit page (Should this be ajax_???)
	public function ajax_add_children($nodeid)
	{
		$data['subdata']['node'] = $this->graph_db->loadNode($nodeid);
		$data['subdata']['relations'] = $data['subdata']['node']->getRelations_GroupByNodeType_GroupByRelType();
		$data['subdata']['relcounts'] = $this->graph_db->GetRelCounts($data['subdata']['node']->getRelationIDs(GDB_Rel::ChildOf), GDB_Rel::ChildOf);
	
		return $data;
	}
	
	public function ajax_user_find($groupid, $page, $search)
	{
		//echo "ajax_user_find($groupid, $page, $search)<br>";
		
		// search for user nodes with $search of type 502
		
		$user_nodes = $this->graph_db->findNodes($search, 502, 50, false);
		
		$nodeids = array();
		
		foreach ($user_nodes as &$node)
		{
			if (!is_object($node)) continue;
			$nodeids[$node->getID()] = 1;
		}
		
		if (strlen($search) > 1)
		{
			$user_names = $this->graph_db->findNodeFromAttr(502, "fullname", $search, false);
			if (is_array($user_names))
			{		
				foreach ($user_names as &$node)
				{
					if (!array_key_exists($node->getID(),$nodeids))
						$user_nodes[] = $node;
		//			$node->dump();
				}
			}
		}
		
		$data['users'] = &$user_nodes;
		
		return $data;
	}

	// get information for new user or group when one has been picked on the permissions add/ edit screen
	public function groupinfo($nodeid, $groupid)
	{
		$node = $this->graph_db->loadNode($nodeid);
		$group = $this->graph_db->loadNode($groupid);
		$type_obj = $group->GetTypeObj();
		$result = new stdClass();
		$result->title = trim($type_obj->name,"s") . " - " . $group->getTitleDisp();
		
		$baseperms = $node->getBasePerms();
		$perms = $node->getPerms();
		
		//print_p($perms);
		$result->perms = new Perm();
		$result->perms->date = "";
		$result->perms->to_date = "";
		$result->perms->from_date = "";
		
		foreach ($perms as $permgroup => &$perm)
		{
			if ($groupid != $permgroup) continue;
			$result->perms = $perm;	
			
			if ($perm->from_date > 0 && $perm->to_date > 0)
			{
				$result->perms->date = "range";
			} else if ($perm->from_date > 0)
			{
				$result->perms->date = "from";
			} else if ($perm->to_date > 0)
			{
				$result->perms->date = "to";
			} else {
				$result->perms->date = "any";
			}
			
			if ($perm->from_date > 0)
			{
				$perm->from_date = date("Y-m-d H:i",$perm->from_date);
			} else {
				$perm->from_date = "";
			}
			
			if ($perm->to_date > 0)
			{
				$perm->to_date = date("Y-m-d H:i",$perm->to_date);
			} else {
				$perm->to_date = "";
			}
			
			break;
		}
		
		//print_p($result);
		//print_p($baseperms);
		
		$pl = new PermList();
		
		foreach ($pl->perms as $name => &$data)
		{
			if ($result->perms->$name == -1)
			{
				$result->perms->$name = "<img src='" .  asset_url() . "image/clone/cross.png'>";
			} else if ($result->perms->$name == 1)
			{
				$result->perms->$name = "<img src='" .  asset_url() . "image/clone/tick.png'>";
			} else if ($baseperms->$name == 1)
			{
				$result->perms->$name = "<img src='" .  asset_url() . "image/clone/tick_grey.png'>";
			} else {
				$result->perms->$name = "<img src='" .  asset_url() . "image/clone/cross_grey.png'>";
			}
		}
		//print_p($result);
		
		if (count(ob_list_handlers()) > 0) {
				ob_clean();
		}
		echo json_encode($result);
		
		exit;		
	}
	
	// process the add permission
	public function processadd()
	{
		$permobj = new Perm();
		$permobj->from_post($this->input);
		
		$this->node_exclude = json_decode($this->input->post('node_exclude'));
		
		$node = $this->graph_db->loadNode($this->input->post('nodeid'));		
		
		if (!$node->can_perms())
			return redirect("view/" . $node->getId());
	
		$this->graph_db->flushLoadedNodes();
		
		// call clonenode on main node, this will copy all the children nodes
		$this->applypermissions($permobj->nodeid, $permobj, true);
		
		//sleep(5);
		
		$node = $this->graph_db->loadNode($this->input->post('nodeid'));		

		if ($this->input->post('groupid') == 0)
		{
			$this->head->AddMessage("Base permissions for node '" . $node->getTitleDisp() . "' updated");
		} else {
			$groupnode = $this->graph_db->loadNode($this->input->post('groupid'));
			$this->head->AddMessage("Permissions for " . strtolower(trim($groupnode->getTypeObj()->name,"s")) . " '" . $groupnode->getTitleDisp() . "' on node '" . $node->getTitleDisp() . "' updated");
		}
		$this->head->ParentRefresh('perms/index/' . $this->input->post('nodeid'));
	}
	
	// apply the permission set to a node id, and its subnodes recursivly
	private function applypermissions($nodeid, &$permobj, $isfirst = false)
	{
		if (!empty($this->node_exclude->$nodeid) && $this->node_exclude->$nodeid == 2) 
		{
			//echo "Skipping $nodeid<br>";
			return;
		}
		
		$node = $this->graph_db->loadNode($nodeid);
		
		if (!$node->can_perms())
			return;
		
		$apply = false;
		
		if ($isfirst) //special case for the initial node
		{
			// determine if self or children are included
			if ($permobj->target != "children")
			{
				$apply = true;
			}				
		} else {
			$apply = true;	
		}

		if ($permobj->origgroupid != 0 && $permobj->groupid != $permobj->origgroupid)
		{
			$curr_perms = $node->getPerms();

			if (array_key_exists($permobj->origgroupid, $curr_perms))
			{
				$permobj_new = $curr_perms[$permobj->origgroupid];
				$permobj_new->groupid = $permobj->groupid;
				$permobj_new->origgroupid = $permobj->origgroupid;

				// Reassigning - remove the permissions for the old group
				$node->removePermSet($permobj->origgroupid);

				$permobj = $permobj_new;
			} else {
				// If current node doesn't have permissions for the original group then skip the applicaiton of new perms
				$apply = false;
			}
		}
		
		if ($apply) {
			$node->addPermSet($permobj);
			$node->save();
		}
		//echo "set permission on node $nodeid<br>";
	
		if ($isfirst && $permobj->target == 'node')
		{
			//echo "Skipping children on main node<br>";
			return;
		}
			
		if (!empty($this->node_exclude->$nodeid) && $this->node_exclude->$nodeid == 1) 
		{
			//echo "Skipping children of $nodeid, excluded in selection<br>";
			return;
		}
		
		$rels = $node->getRelations_OfType(GDB_Rel::ChildOf);
		foreach ($rels as &$rel)
		{
			$this->applypermissions($rel->getId(), $permobj);
		}
	}
	
	// remove a permission set page
	public function delete($nodeid, $groupid)
	{
			// show display the users initial node here
		$this->load->helper('form');
		$this->load->library('form_validation');
		
		// if we have a page passed
		$this->data['title'] = "Node Permissions - Delete Permission Set"; // Capitalize the first letter
		$this->data['node'] = $this->graph_db->loadNode($nodeid);
		$this->data['group'] = $this->graph_db->loadNode($groupid);
		
		$hidden = array('nodeid' => $nodeid, 'groupid' => $groupid);
		$this->head->AddLeading(form_open("perms/processdel", array('id' => 'permdel_form', 'target' => 'delete_result'), $hidden));					
					
		//add form close to bottom of popup
		$this->head->AddTrailing(form_close());					


		$this->head->AddFooter("<button id='add_save' class='footer_button'>Remove Permissions</button>");

		// add cancel button to footer
		$this->head->AddFooter("<button id='add_cancel' class='footer_button' onclick='window.parent.TINY.box.hide();return false;'>Cancel</button>");
	
		$this->head->AddJS("js/perms/delete.js");

		$this->load->view('templates/popup/header', $this->data);
		$this->load->view('perms/delete', $this->data);
		$this->load->view('templates/popup/footer', $this->data);					
	}
	
	// process the permission set delete
	public function processdel()
	{
		$nodeid = $this->input->post('nodeid');
		$groupid = $this->input->post('groupid');
		$target = $this->input->post('target');
		
		//sleep(10);
		
		if ($target == "both")
		{
			$this->removepermissions($nodeid, $groupid);
		} else {
			$node = $this->graph_db->loadNode($nodeid);
			$node->removePermSet($groupid);
			$node->save();
		}
		
		$groupnode = $this->graph_db->loadNode($groupid);
		$node = $this->graph_db->loadNode($nodeid);		
		$this->head->AddMessage("Permissions for " . strtolower(trim($groupnode->getTypeObj()->name,"s")) . " '" . $groupnode->getTitleDisp() . "' on node '" . $node->getTitleDisp() . "' removed");
?>
<script>
window.parent.parent.location.reload(true);
</script>
<?php
		//$this->head->ParentRefresh();	
	}
	
	// remove the permission set from a node and its children recusivly
	private function removepermissions($nodeid, $groupid)
	{
		//echo "Removing set $groupid from $nodeid<br>";
		
		$node = $this->graph_db->loadNode($nodeid);
		$node->removePermSet($groupid);
		$node->save();
		
		$rels = $node->getRelations_OfType(GDB_Rel::ChildOf);
		foreach ($rels as &$rel)
		{
			$this->removepermissions($rel->getId(), $groupid);
		}
	}
	
}
