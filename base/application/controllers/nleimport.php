<?php

/**
 * Controller class for importing sessions and outcomes from the NLE using the API provided
 *
 */

class NleImport extends CI_Controller {

  private $basenode;
  private $nle_ws = 'http://www.nle.nottingham.ac.uk/webServices/NLERestAPI2.php';
//  private $nle_ws = 'http://nle.rji.ac.uk/webServices/NLERestAPI2.php';

  /**
    * This is method __construct. Create a new instance of the NleImport controller
    */
   public function __construct()
   {
    // call parent constructor
    parent::__construct();

    // setup the main menu items for the page (same for all controllers)
    setup_main_menu($this->menu);

    $this->load->library('curl');
    $this->load->helper('curl');
    $this->load->helper('node_types');
    $this->load->helper('node');
  }

  /**********************************************************/
  /**
   * ****************************************
   * Main entry points into the controller
   * **************************************
   **/

  /**
   * Redirects to the view node page
   *
   * @param int $nodeid ID of the node to display
   *
   */
  public function index($nodeid = -1)
  {
    if ($nodeid == -1)
      {
        $CI =& get_instance();
        $nodeid = $CI->session->userdata('my_node');
        //echo "Session Node ID : $nodeid<br>";
        //exit;
        if (!$nodeid)
          $nodeid = 1;
      }
      // redirect to view node
      redirect('view/'.$nodeid);
  }

  /**
   * @param $baseid
   */
  public function import($target='', $baseid = -1, $topic_level = -1, $toc_start = -1, $toc_end = -1) {
    // If not passed a node, return
    if ($baseid == -1) {
      return redirect("view/");
    }

    if ($target == '') {
      show_error("Import type not defined");
    }

    // Get node and type
    $this->basenode = $this->graph_db->loadNode($baseid);
    $data['basenode'] = $this->basenode;
    if (!$data['basenode'])
    {
      show_error("node/edit Unable to find node $baseid");
    }

    $cal_year = '2012/13';

    switch ($target) {
      case 'module':
        $type = $data['basenode']->getType();
        if ($type != NTypes::MODULE) {
          show_error("Node does not match import type");
        }

        $module_id = $data['basenode']->getAttribute('code');
        $module_id = $module_id->getValue();

        if (!isset($module_id) or $module_id == '') {
          show_error("Module code not found");
        }

        $mod_outcomes_created = 0;
        $tt_sessions_created = 0;
        $outcomes_created = 0;
        $mod_sess_relations = 0;
        $mod_td_relations = 0;

        $res_tt = $this->import_tt_sessions($baseid, $module_id, $cal_year, $tt_sessions_created);
        $this->basenode->setAttribute('ttsync', '1');
        if ($tt_sessions_created > 0) {
          $this->basenode->setAttribute('ttsynced', '1');
        }
        $this->basenode->save();

        $res = $this->import_sessions($baseid, $module_id, '2011/12', $outcomes_created);

        $res_mod = $this->import_module_objectives($baseid, $module_id, '2011/12', 'session', $mod_outcomes_created, $mod_sess_relations, $mod_td_relations);

        $data['title'] = 'NLE Import for module ' . $module_id;
        $data['json_raw'] = $this->indent($res_tt . $res . $res_mod);
        $data['no_mod_outcomes'] = $mod_outcomes_created;
        $data['no_sessions'] = $tt_sessions_created;
        $data['no_outcomes'] = $outcomes_created;
        $data['no_mod_session'] = $mod_sess_relations;
        $data['no_mod_td'] = $mod_td_relations;
        break;
      case 'modulentt':
        $type = $data['basenode']->getType();
        if ($type != NTypes::MODULE) {
          show_error("Node does not match import type");
        }

        $module_id = $data['basenode']->getAttribute('code');
        $module_id = $module_id->getValue();

       if (!isset($module_id) or $module_id == '') {
          show_error("Module code not found");
        }

        $mod_outcomes_created = 0;
        $sessions_created = 0;
        $ntt_sessions_created = 0;
        $outcomes_created = 0;
        $tt_outcomes_created = 0;
        $mod_sess_relations = 0;
        $mod_td_relations = 0;

        $res = $this->import_nle_sessions($baseid, $module_id, $cal_year, $sessions_created, $tt_outcomes_created);

        $res_ntt = $this->import_ntt_sessions($baseid, $module_id, $cal_year, $topic_level, $ntt_sessions_created, $outcomes_created, $toc_start, $toc_end);

        $res_mod = $this->import_module_objectives($baseid, $module_id, $cal_year, 'both', $mod_outcomes_created, $mod_sess_relations, $mod_td_relations);

        $data['title'] = 'NLE Import for module ' . $module_id;
        $data['json_raw'] = $this->indent($res . $res_ntt . $res_mod);
        $data['no_mod_outcomes'] = $mod_outcomes_created;
        $data['no_sessions'] = $sessions_created;
        $data['no_sess_outcomes'] = $tt_outcomes_created;
        $data['no_learning_acts'] = $ntt_sessions_created;
        $data['no_act_outcomes'] = $outcomes_created;
        $data['no_mod_session'] = $mod_sess_relations;
        $data['no_mod_td'] = $mod_td_relations;
        break;
      case 'degree':
        $type = $data['basenode']->getType();
        if ($type != NTypes::PROGRAMME) {
          show_error("Node does not match import type");
        }

        $degree_code = $data['basenode']->getAttribute('code');

        if (!isset($degree_code) or $degree_code == '') {
          show_error("Programme code not found");
        }

        $deg_outcomes_created = 0;

        $res_deg = $this->import_degree_objectives($baseid, $degree_code, $cal_year, $deg_outcomes_created);

        $data['title'] = 'NLE Import for programme ' . $degree_code;
        $data['json_raw'] = $this->indent($res_deg);
        $data['no_deg_outcomes'] = $deg_outcomes_created;
        break;
      case 'body':
        $type = $data['basenode']->getType();
        if ($type != NTypes::ACCREDITING_BODY) {
          show_error("Node does not match import type");
        }

        $body_outcomes_created = 0;

        $res_body = $this->import_body_objectives($baseid, '2011/12', $body_outcomes_created);

        $data['title'] = 'NLE Import for Tomorrows Doctors';
        $data['json_raw'] = $this->indent($res_body);
        $data['no_body_outcomes'] = $body_outcomes_created;
        break;
      case 'fixshared':
        $type = $data['basenode']->getType();
        if ($type != NTypes::MODULE) {
          show_error("Node does not match import type");
        }

        $module_id = $data['basenode']->getAttribute('code');
        $module_id = $module_id->getValue();

        if (!isset($module_id) or $module_id == '') {
          show_error("Module code not found");
        }

        $tt_shared_updated = 0;
        $session_list = array();
        $res_fix = $this->update_tt_shared($baseid, $module_id, $cal_year, $session_list);

        $mod_outcomes_created = 0;
        $outcomes_created = 0;
        $mod_sess_relations = 0;
        $mod_td_relations = 0;

        if (count($session_list) > 0)
        {
          $res = $this->import_sessions($baseid, $module_id, '2011/12', $outcomes_created, $session_list);

          $res_mod = $this->import_module_objectives($baseid, $module_id, '2011/12', 'session', $mod_outcomes_created, $mod_sess_relations, $mod_td_relations, true);
        }

        $data['title'] = 'Update sessions for ' . $module_id;
        $data['json_raw'] = $this->indent($res_fix);
        $data['no_sess_updated'] = count($session_list);
        $data['no_outcomes'] = $outcomes_created;
        $data['no_mod_session'] = $mod_sess_relations;
        break;
      default:
        show_error('Unknown import type');
        break;
    }

    $data['type'] = $target;
    $this->load->view('templates/header', $data);
    $this->load->view('nleimport/index');
    $this->load->view('templates/footer');
  }

  /**********************************************************/
  /*
   * *****************************************
   * Functions used by the main entry points
   * *****************************************
   */

  private function import_module_objectives($baseid, $module_id, $academic_year, $type, &$mod_outcomes_created, &$mod_sess_relations, &$mod_td_relations, $link_session_only=false) {
    // Call the NLE web service
    setup_curl($this, "{$this->nle_ws}?url=getModuleObjectives/{$module_id}/{$academic_year}");
    curl_login($this);

    $res = $this->curl->execute();

    if ($res === false) {
      show_error("Error fetching module objective data from NLE");
    }

    $map_data = json_decode($res, true);

    $programme = $this->basenode->getParent();

    if (isset($map_data[$module_id]['objectives']) and count($map_data[$module_id]['objectives']) > 0) {
      $new_groups = array();
      $old_groups_p = $programme->getRelations("kv_pair");
      $old_groups_m = $this->basenode->getRelations("kv_pair");
      $old_groups = array_merge($old_groups_p, $old_groups_m);
      foreach($map_data[$module_id]['objectives'] as $obj_id => $outcome) {
        if (isset($mod_id)) unset($mod_id);
        if (isset($mod_node)) unset($mod_node);

        if ($link_session_only) {
          // Just get the node which should already exist
          $target = $this->graph_db->findNodeFromAttr(NTypes::MODULE_OUTCOME, 'nleid', $outcome['id'], $exact = true, $limit = 1);
          if (count($target) > 0) {
            $mod_node = $target[0];
          }
        } else {
          // Create a new node with group if necessary
          if (count($new_groups) == 0 and count($old_groups) == 0) {
            $new_groups[$outcome['category']] = $this->create_group($outcome['category'], $baseid);
          } else {
            if (count($new_groups) == 0 or !array_key_exists($outcome['category'], $new_groups)) {
              $found = false;
              foreach ($old_groups as $kvnode) {
                if ($kvnode->getTitle() == $outcome['category']) {
                  $found = true;
                  break;
                }
              }
              if (!$found) {
                $new_groups[$outcome['category']] = $this->create_group($outcome['category'], $baseid);
              }
            }
          }

          $new_data = array();
          $new_data['type_obj'] = $this->node_types->GetType(NTypes::MODULE_OUTCOME);
          $new_data['type'] = NTypes::MODULE_OUTCOME;
          $new_data['title'] = ''; //$obj_id;
          $new_data['desc'] = $outcome['content'];
          $new_data['group'] = $outcome['category'];
          $new_data['nleid'] = $outcome['id'];
          $new_data['order'] = '000|' . str_pad($outcome['order'], 3, '0', STR_PAD_LEFT);

          $mod_id = create_node($new_data, $baseid);
          $mod_outcomes_created++;
        }

        // Map module to session objectives
        if (isset($outcome['sess_mappings']) and count($outcome['sess_mappings']) > 0) {
          if (isset($mod_id)) {
            $mod_node = $this->graph_db->loadNode($mod_id);
          }

          if (isset($mod_node)) {
            foreach ($outcome['sess_mappings'] as $mapped_session) {
              if ($type == 'learning_act') {
                $nodes = $this->graph_db->findAttribute($mapped_session, NTypes::LEARNING_ACT_OUTCOME, 'nleid');
              } elseif ($type == 'learning_act') {
                $nodes = $this->graph_db->findAttribute($mapped_session, NTypes::SESSION_OUTCOME, 'nleid');
              } else {
                // Find either
                $nodes = $this->graph_db->findAttribute($mapped_session, NTypes::LEARNING_ACT_OUTCOME, 'nleid');
                if (count($nodes) == 0) {
                  $nodes = $this->graph_db->findAttribute($mapped_session, NTypes::SESSION_OUTCOME, 'nleid');
                }
              }
              if (count($nodes) > 0) {
                $sess_node = reset($nodes);
                $mod_node->addRelate($sess_node, 3);
                $mod_sess_relations++;
                $mod_node->save();
              }
            }
          }
        }

        if (!$link_session_only) {
          // Map module to Tomorrow's Doctors objectives
          if (isset($outcome['td_mappings']) and count($outcome['td_mappings']) > 0) {
            $mod_node = $this->graph_db->loadNode($mod_id);
            foreach ($outcome['td_mappings'] as $mapped_outcome) {
              $nodes = $this->graph_db->findAttribute($mapped_outcome, NTypes::BODY_OUTCOME, 'nleid');
              if (count($nodes) > 0) {
                $td_node = reset($nodes);
                $mod_node->addRelate($td_node, 3);
                $mod_td_relations++;
                $mod_node->save();
              }
            }
          }
        }
      }
    }

    return $res;
  }

  private function import_degree_objectives($baseid, $degree_code, $academic_year, &$deg_outcomes_created) {
    // Call the NLE web service
    $degree_code = $degree_code->getValue();
    setup_curl($this, "{$this->nle_ws}?url=getDegreeObjectives/{$degree_code}/{$academic_year}");
    curl_login($this);

    $res = $this->curl->execute();

    if ($res === false) {
      show_error("Error fetching degree objective data from NLE");
    }

    $map_data = json_decode($res, true);

    if (isset($map_data[$degree_code]['objectives']) and count($map_data[$degree_code]['objectives']) > 0) {
      $new_groups = array();
      $old_groups = $this->basenode->getRelations("kv_pair");
      foreach($map_data[$degree_code]['objectives'] as $obj_id => $outcome){
        if (count($new_groups) == 0 and count($old_groups) == 0) {
          $new_groups[$outcome['category']] = $this->create_group($outcome['category'], $baseid);
        } else {
          if (count($new_groups) == 0 or !array_key_exists($outcome['category'], $new_groups)) {
            $found = false;
            foreach ($old_groups as $kvnode) {
              if ($kvnode->getTitle() == $outcome['category']) {
                $found = true;
                break;
              }
            }
            if (!$found) {
              $new_groups[$outcome['category']] = $this->create_group($outcome['category'], $baseid);
            }
          }
        }

        $new_data = array();
        $new_data['type_obj'] = $this->node_types->GetType(NTypes::PROGRAMME_OUTCOME);
        $new_data['type'] = NTypes::PROGRAMME_OUTCOME;
        $new_data['title'] = ''; //$obj_id;
        $new_data['desc'] = $outcome['content'];
        $new_data['nleid'] = $outcome['id'];
        $new_data['group'] = $outcome['category'];
        $new_data['order'] = '000|' . str_pad($outcome['order'], 3, '0', STR_PAD_LEFT);

        $deg_id = create_node($new_data, $baseid);
        $deg_outcomes_created++;
      }
    }

    return $res;
  }

  private function import_body_objectives($baseid, $academic_year, &$body_outcomes_created) {
    // Call the NLE web service
    $url = "{$this->nle_ws}?url=getTomorrowsDoctors/{$academic_year}";
    setup_curl($this, $url);
    curl_login($this);

    $res = $this->curl->execute();

    if ($res === false) {
      show_error("Error fetching Tomorrow's Doctors data from NLE");
    }

    $map_data = json_decode($res, true);

    if (isset($map_data['objectives']) and count($map_data['objectives']) > 0) {
      foreach($map_data['objectives'] as $obj_id => $outcome){
        $new_data = array();
        $new_data['type_obj'] = $this->node_types->GetType(NTypes::BODY_OUTCOME);
        $new_data['type'] = NTypes::BODY_OUTCOME;
        $new_data['title'] = $outcome['title']; //$obj_id;
        $new_data['desc'] = $outcome['content'];
        $new_data['nleid'] = $outcome['id'];
        $new_data['order'] = '000|' . str_pad($outcome['order'], 3, '0', STR_PAD_LEFT);
//        $new_data['group'] = $outcome['category'];

        $deg_id = create_node($new_data, $baseid);
        $body_outcomes_created++;
      }
    }

    return $res;
  }

  /**
   * Import sessions from the timetabling system
   * @param $baseid
   * @param $module_id
   * @param $academic_year
   * @param $tt_sessions_created
   * @return string JSON encoded result of query to Timetabling system
   */
  private function import_tt_sessions($baseid, $module_id, $academic_year, &$tt_sessions_created) {
    // Call the IMAT web service
    $year = substr($academic_year, 0, strpos($academic_year, '/'));
    
    $apikey = $this->config->item('UONCM_TimetableAPIKey');
    $url = $this->config->item('UONCM_TimetableURL') . "/$year/activities/module/$module_id/?apiKey=$apikey/";

    setup_curl($this, $url);

    $res = $this->curl->execute();

    if ($res === false) {
      show_error("Error fetching session data from timetable for  {$url}: " . $this->curl->error_string);
    }

    $map_data = json_decode($res, true);

    // Be really sure we have some data
    if ($map_data != ''
      and isset($map_data['Activities'])
        and isset($map_data['Activities']['Module'])
          and isset($map_data['Activities']['Module']['Activity'])
            and count($map_data['Activities']['Module']['Activity']) > 0) {
      foreach ($map_data['Activities']['Module']['Activity'] as $t_session) {
        $lecturer = '';
        if (is_array($t_session['Staff']) and is_array($t_session['Staff']['Person'])) {
          if (isset($t_session['Staff']['Person']['Name'])) {
            $lecturer .= $t_session['Staff']['Person']['Name'];
          } else {
            foreach ($t_session['Staff']['Person'] as $person) {
              $lecturer .= $person['Name'] . ' / ';
            }
            $lecturer = rtrim($lecturer, " /");
          }
        }

        $new_data = array();
        $new_data['type_obj'] = $this->node_types->GetType(NTypes::SESSION);
        $new_data['type'] = NTypes::SESSION;
        $new_data['title'] = $t_session['Description'];
        $new_data['start'] = $t_session['Date']['Year'] . '-' . str_pad($t_session['Date']['Month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($t_session['Date']['Day'], 2, '0', STR_PAD_LEFT) . ' ' . str_pad($t_session['StartTime']['Hours'], 2, '0', STR_PAD_LEFT) . ':' . str_pad($t_session['StartTime']['Minutes'], 2, '0', STR_PAD_LEFT) . ':00';
        $new_data['end'] = $t_session['Date']['Year'] . '-' . str_pad($t_session['Date']['Month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($t_session['Date']['Day'], 2, '0', STR_PAD_LEFT) . ' ' . str_pad($t_session['EndTime']['Hours'], 2, '0', STR_PAD_LEFT) . ':' . str_pad($t_session['EndTime']['Minutes'], 2, '0', STR_PAD_LEFT) . ':00';
        $new_data['code'] = $t_session['Code'];
        $new_data['staff'] = $lecturer;
        $new_data['ttguid'] = $t_session['Guid'];

        $t_sess_id = create_node($new_data, $baseid);
        $tt_sessions_created++;
      }
    }

    return $res;
  }

  /**
   * Import non-timetables sessions from the NLE
   * @param $baseid
   * @param $module_id
   * @param $academic_year
   * @param $ntt_sessions_created
   * @param $outcomes_created
   * @return string JSON encoded result of query to Timetabling system
   */
  private function import_ntt_sessions($baseid, $module_id, $academic_year, $topic_level, &$ntt_sessions_created, &$outcomes_created, $toc_start = -1, $toc_end = -1) {
    setup_curl($this, "{$this->nle_ws}?url=getNTObjectives/{$module_id}/{$academic_year}/{$topic_level}/{$toc_start}/{$toc_end}");
    curl_login($this);

    $res = $this->curl->execute();

    if ($res === false) {
      show_error("Error fetching session data from NLE");
    }

    $map_data = json_decode($res, true);
    $curr_book = '';
    $book_level = 9999;
    $type = '';

    if (isset($map_data[$module_id]) and count($map_data[$module_id]) > 0) {
      $i = 0;
      $j = 0;
      foreach ($map_data[$module_id] as $identifier => $t_session) {
        // TODO: themes
        if ($t_session['type'] == 'book') {
          if ($t_session['level'] == $topic_level) {
            $book_level = $t_session['level'];
            $book_data = array();
            $book_data['type_obj'] = $this->node_types->GetType(NTypes::TOPIC);
            $book_data['type'] = NTypes::TOPIC;
            $book_data['title'] = $t_session['title'];
            $book_data['order'] = '000|' . str_pad($j++, 3, '0', STR_PAD_LEFT);

            $curr_book_id = create_node($book_data, $baseid);
            $curr_book = $this->graph_db->loadNode($curr_book_id);

            $type = '';
          } else {
            $type = preg_replace('/s$/', '', $t_session['title']);
          }
        } else {
          $new_data = array();
          $new_data['type_obj'] = $this->node_types->GetType(NTypes::LEARNING_ACT);
          $new_data['type'] = NTypes::LEARNING_ACT;
          // if ($type != '') {
          //   $new_data['act_type'] = $type;
          // }
          $new_data['title'] = $t_session['title'];
          $new_data['order'] = '000|' . str_pad($i++, 3, '0', STR_PAD_LEFT);

          $t_sess_id = create_node($new_data, $baseid);
          $ntt_sessions_created++;

          $target = $this->graph_db->loadNode($t_sess_id);

          if (isset($t_session['objectives']) and count($t_session['objectives']) > 0) {
            $k = 0;
            foreach($t_session['objectives'] as $obj_id => $outcome){
              $new_data = array();
              $new_data['type_obj'] = $this->node_types->GetType(NTypes::LEARNING_ACT_OUTCOME);
              $new_data['type'] = NTypes::LEARNING_ACT_OUTCOME;
              $new_data['title'] = ''; //$obj_id;
              $new_data['desc'] = $outcome['content'];
              $new_data['nleid'] = $outcome['id'];
              $new_data['order'] = '000|' . str_pad($k++, 3, '0', STR_PAD_LEFT);

              create_node($new_data, $t_sess_id);
              $outcomes_created++;
            }
          }

          if ($curr_book != '' and $t_session['level'] > $book_level) {
            $curr_book->addRelate($target);
            $curr_book->save();
          }
        }
      }
    }

    return $res;
  }

  /**
   * Get sessions and their outcomes from the NLE.  Don't create sessions from this but try to link them up with Timetable
   * sessions by GUID
   * @param $baseid
   * @param $module_id
   * @param $academic_year
   * @param $outcomes_created
   * @return string JSON encoded result of query to NLE
   */
  private function import_sessions($baseid, $module_id, $academic_year, &$outcomes_created, $session_list=null) {
    // Call the NLE web service
    setup_curl($this, "{$this->nle_ws}?url=getObjectives/{$module_id}/{$academic_year}");
    curl_login($this);

    $res = $this->curl->execute();

    if ($res === false) {
      show_error("Error fetching session data from NLE");
    }

    $map_data = json_decode($res, true);

    if (isset($map_data[$module_id]) and count($map_data[$module_id]) > 0) {
      foreach ($map_data[$module_id] as $identifier => $t_session) {
        if (isset($t_session['GUID']) and (!isset($session_list) or (is_array($session_list) and in_array($t_session['GUID'], $session_list)))) {
          $target = $this->graph_db->findNodeFromAttr(NTypes::SESSION, 'ttguid', $t_session['GUID'], $exact = true, $limit = 1);

          if (count($target) > 0) {
            $target = $target[0];
            $target_id = $target->getId();

            if (isset($t_session['objectives']) and count($t_session['objectives']) > 0) {
              $i = 0;
              foreach($t_session['objectives'] as $obj_id => $outcome){
                $new_data = array();
                $new_data['type_obj'] = $this->node_types->GetType(NTypes::SESSION_OUTCOME);
                $new_data['type'] = NTypes::SESSION_OUTCOME;
                $new_data['title'] = ''; //$obj_id;
                $new_data['desc'] = $outcome['content'];
                $new_data['nleid'] = $outcome['id'];
                $new_data['order'] = '000|' . str_pad($i++, 3, '0', STR_PAD_LEFT);

                create_node($new_data, $target_id);
                $outcomes_created++;
              }
            }
          }
        }
      }
    }

    return $res;
  }

  private function import_nle_sessions($baseid, $module_id, $academic_year, &$sessions_created, &$outcomes_created) {
    // Call the NLE web service
    setup_curl($this, "{$this->nle_ws}?url=getObjectives/{$module_id}/{$academic_year}");
    curl_login($this);

    $res = $this->curl->execute();

    if ($res === false) {
      show_error("Error fetching session data from NLE");
    }

    $map_data = json_decode($res, true);

    $i = 0;
    if (isset($map_data[$module_id]) and count($map_data[$module_id]) > 0) {
      foreach ($map_data[$module_id] as $identifier => $t_session) {
        if ($t_session['class_code'] != '') {
          $new_data = array();
          $new_data['type_obj'] = $this->node_types->GetType(NTypes::SESSION);
          $new_data['type'] = NTypes::SESSION;
          $new_data['title'] = $t_session['title'];
          $new_data['start'] = '20' . preg_replace('/(\d{2})\/(\d{2})\/(\d{2})/i', '\3-\2-\1', $t_session['occurrance']) . ':00';
          $new_data['end'] = date('Y-m-d H:i:s', strtotime($new_data['start'] . ' +' . $t_session['duration'] . 'minutes'));
          $new_data['code'] = $t_session['class_code'];
          $new_data['staff'] = $t_session['staff'];
          $new_data['ttguid'] = $t_session['GUID'];

          $t_sess_id = create_node($new_data, $baseid);
          $sessions_created++;

          if (isset($t_session['objectives']) and count($t_session['objectives']) > 0) {
            foreach($t_session['objectives'] as $obj_id => $outcome){
              $new_data = array();
              $new_data['type_obj'] = $this->node_types->GetType(NTypes::SESSION_OUTCOME);
              $new_data['type'] = NTypes::SESSION_OUTCOME;
              $new_data['title'] = ''; //$obj_id;
              $new_data['desc'] = $outcome['content'];
              $new_data['nleid'] = $outcome['id'];
              $new_data['order'] = '000|' . str_pad($i++, 3, '0', STR_PAD_LEFT);

              create_node($new_data, $t_sess_id);
              $outcomes_created++;
            }
          }
        }
      }
    }

    return $res;
  }

  /**
   * Import sessions from the timetabling system
   * @param $baseid
   * @param $module_id
   * @param $academic_year
   * @param $tt_sessions_created
   * @return string JSON encoded result of query to Timetabling system
   */
  private function update_tt_shared($baseid, $module_id, $academic_year, &$session_list) {
    // Call the IMAT web service
    $year = substr($academic_year, 0, strpos($academic_year, '/'));
    $apikey = $this->config->item('UONCM_TimetableAPIKey');
    $url = $this->config->item('UONCM_TimetableURL') . "/$year/activities/module/$module_id/?apiKey=$apikey/";
    setup_curl($this, $url);

    $res = $this->curl->execute();

    if ($res === false) {
      show_error("Error fetching session data from timetable for  {$url}: " . $this->curl->error_string);
    }

    $map_data = json_decode($res, true);

    // Be really sure we have some data
    if ($map_data != ''
      and isset($map_data['Activities'])
        and isset($map_data['Activities']['Module'])
          and isset($map_data['Activities']['Module']['Activity'])
            and count($map_data['Activities']['Module']['Activity']) > 0) {
      foreach ($map_data['Activities']['Module']['Activity'] as $t_session) {
        if ($t_session['Guid'] != '' and is_array($t_session['Shared'])) {
          if (isset($t_session['Shared']['School'])) {
            if (is_array($t_session['Shared']['School'])) {
              foreach ($t_session['Shared']['School'] as $school) {
                if (isset($school['Module']) and is_array($school['Module']) and isset($school['Module']['Code'])) {
                  if ($school['Module']['Code'] == $module_id) {
                    if (isset($school['Module']['Activity']) and is_array($school['Module']['Activity'])) {
                      $new_sess_code = $school['Module']['Activity']['Code'];
                      $new_sess_desc = $school['Module']['Activity']['Description'];
                      $new_sess_guid = $school['Module']['Activity']['Guid'];
                      $session_list[] = $new_sess_guid;
                    }
                  }
                }
              }
            }
          }

          if (isset($new_sess_code)) {
            $target = $this->graph_db->findNodeFromAttr(NTypes::SESSION, 'ttguid', $t_session['Guid'], $exact = true, $limit = 1);

            // Update session if we have one
            if (count($target) > 0) {
              $target[0]->setAttribute('code', $new_sess_code);
              $target[0]->setTitle($new_sess_desc);
              $target[0]->setAttribute('ttguid', $new_sess_guid);
              $target[0]->save();
            }
          }

          if (isset($new_sess_code)) {
            unset($new_sess_code);
            unset($new_sess_desc);
            unset($new_sess_guid);
          }
        }
      }
    }

    return $res;
  }


  /**
   * Indents a flat JSON string to make it more human-readable.
   *
   * @param string $json The original JSON string to process.
   *
   * @return string Indented version of the original JSON string.
   */
  private function indent($json) {

    $result      = '';
    $pos         = 0;
    $strLen      = strlen($json);
    $indentStr   = '  ';
    $newLine     = "\n";
    $prevChar    = '';
    $outOfQuotes = true;

    for ($i=0; $i<=$strLen; $i++) {

      // Grab the next character in the string.
      $char = substr($json, $i, 1);

      // Are we inside a quoted string?
      if ($char == '"' && $prevChar != '\\') {
        $outOfQuotes = !$outOfQuotes;

        // If this character is the end of an element,
        // output a new line and indent the next line.
      } else if(($char == '}' || $char == ']') && $outOfQuotes) {
        $result .= $newLine;
        $pos --;
        for ($j=0; $j<$pos; $j++) {
          $result .= $indentStr;
        }
      }

      // Add the character to the result string.
      $result .= $char;

      // If the last character was the beginning of an element,
      // output a new line and indent the next line.
      if (($char == ',' || $char == '{' || $char == '[') && $outOfQuotes) {
        $result .= $newLine;
        if ($char == '{' || $char == '[') {
          $pos ++;
        }

        for ($j = 0; $j < $pos; $j++) {
          $result .= $indentStr;
        }
      }

      $prevChar = $char;
    }

    return $result;
  }

  private function create_group($title, $baseid) {
    $g_data = array();
    $g_data['type_obj'] = $this->node_types->GetType(NTypes::KV_PAIR);
    $g_data['type'] = NTypes::KV_PAIR;
    $g_data['title'] = $title;

    $group = $this->graph_db->loadNode(create_node($g_data, $baseid));
    $group->setAttribute('type', 'outcome');
    $group->save();
    return $group;
  }
}