<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

/**
 * Main general controller
 *
 * Implements stuff like dataset selecting and viewing static pages
 */
class Main extends CI_Controller {

	/**
	 * Main entry point to the site.
	 *
	 * Redirects to the users own node if logged in, otherwise redirects to the root node. The root
	 * node will then redirect to the appropriate node based on its XML config
	 */
	public function index($page = '')
	{
		global $current_data_set_folder;

		// if we have a page passed
		if ($page) {
			return $this->page($page);
		}

		$CI =& get_instance();

		$datasets = $this->graph_db->datasets;

		if (empty($datasets)) {
			// There are no datasets in the database so redirect to the install page.
			redirect("../install/");
		}

		$nodes = $this->graph_db->firstNodes(1,1);
		$nodeid = $nodes[0]->getId();

		$defaultdataset = false;

		$redirecttoyear = false;

		// Loop through all datasets and auto create dataset folders.  Keep a record of the default dataset.
		foreach ($datasets as $datasetname => $dataset) {
			if (isset($dataset->isdefault)) {
				$defaultdataset = ("../$datasetname/view/$nodeid");
			}

			if ($datasetname == $current_data_set_folder) {
				$redirecttoyear = ("../$datasetname/view/$nodeid");
			}

			$datasetfolder = "../$datasetname";
			$datasetindex = "$datasetfolder/index.php";

				if (!file_exists($datasetfolder)) {
					mkdir($datasetfolder);
				}

				if (!file_exists($datasetindex)) {
					$template_path = '../install/config/index.php';
					$indexfile = file_get_contents($template_path);
					$handle = fopen($datasetindex, 'w+');
					@chmod($datasetfolder, 0777);

					if (is_writable($datasetfolder)) {
						if (fwrite($handle, $indexfile)) {
							continue;
						} else {
							show_error("The $datasetfolder dataset index file could not be created");
						}

					} else {
						show_error("The $datasetfolder dataset folder is not writable so the dataset index file could not be created");
					}
				}
		}

		if ($redirecttoyear) {
			redirect($redirecttoyear);
		}
		if ($defaultdataset) {
			// Redirect to the default dataset URL.
			redirect($defaultdataset);
		}
		show_error("You have existing datasets but no default has been set.  Please edit the datasets table to define the default dataset.");
	}

	/**
	 * Redirect to the year node
	 *
	 * This really shouldnt be in the main code, but somehow within the node definitions
	 */
	function year()
	{
		$yearnodes = $this->graph_db->firstNodes(1, 1);
		if (count($yearnodes) == 0)
		{
			// no year node, we have a problem
		} else {
			redirect("view/" . $yearnodes[0]->getId());
		}
	}

	function page($page = "")
	{
		setup_main_menu($this->menu);

		if ( ! file_exists('application/views/pages/'.$page.'.php'))
		{
			// Whoops, we don't have a page for that!
			show_error("pages/view Unable to find view $page");
		}

		$data['title'] = ucfirst($page); // Capitalize the first letter

		$this->load->view('templates/header', $data);
		$this->load->view('pages/'.$page, $data);
		$this->load->view('templates/footer', $data);
	}

	function dataset($newset)
	{
		$this->change_dataset($newset);
		$referer = $this->session->userdata('referrer', current_url());
		redirect($referer);
	}

	function change_dataset($newset)
	{
		$newdata = array(
			'dataset'  => $newset
			);

		// validate that the dataset exists
		if (!array_key_exists($newset, $this->graph_db->datasets))
		{
			$data['title'] = "Dataset $newset not found";
			// invalide dataset
			$this->load->view('templates/header', $data);
			$this->output->append_output("Dataset $newset not found");
			$this->load->view('templates/footer');
			return;
		}

		$this->session->set_userdata($newdata);

		include "application/controllers/auth.php";

		$a = new Auth();
		$a->loadMyNode();
	}

	function dataset_popup($newset)
	{
		$this->change_dataset($newset);
		$this->head->ParentRefresh('/');
	}

	function listdatasets()
	{
		$data['title'] = "Select a year";

		// add cancel button to footer
		$this->head->AddFooter("<button id='hist_close' class='footer_button' onclick='window.parent.TINY.box.hide();return false;'>Close</button>");

		$this->load->view('templates/popup/header', $data);
		$this->load->view('main/listdatasets', $data);
		$this->load->view('templates/popup/footer', $data);
	}
}