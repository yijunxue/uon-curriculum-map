<?php

/**
 * Display a node based on a view passed in
 */
class View extends CI_Controller {

	var $xml_base = "views";
	var $vn = 'view';
	var $subtemplate = '';

	/**
	 * This is method __construct creates a new instance of the node controller
	 */
	public function __construct()
	{
		// call parent constructor
		parent::__construct();

		$this->CI =& get_instance();
		$this->load->library('tabs');
		$this->load->helper('pagination');
		$this->load->helper('view');

		$this->head->AddJS("js/node/view.js");
		$this->head->AddJS("js/node/subnodes.js");

		setup_main_menu($this->menu);
	}

	// will list nodes from $nodeid along $path
	// should only ever be called as ajax

	public function index($nodeid = 1, $path = "")
	{
		$this->node($nodeid, $path);
	}

	public function kv($nodeid = 1, $type = '')
	{
		$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.location=\"".site_url("view/" .$nodeid) ."\";return false;'>Done</button>");
		//$node = $this->graph_db->loadNode($nodeid);

		$template = 'kvpair_edit';
		if ($type != '')
			$template .= "_" . $type;
		$this->node($nodeid, '', $template);
	}

	public function order($nodeid = 1, $nodetype)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->head->AddLeading(form_open("view/saveorder/$nodeid/$nodetype", array('id' => 'create_form')));
		$this->head->AddTrailing(form_close());
		$this->head->AddFooter("<button id='edit_save' class='footer_button' onclick='if (confirm(\"Are you sure you want to reset the order?\")) window.location=\"".site_url("view/resetorder/" .$nodeid . "/$nodetype") ."\";return false;'>Reset Order</button>");
		$this->head->AddFooter("<button id='edit_save' class='footer_button' onclick=''>Save</button>");
		$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.location=\"".site_url("view/" .$nodeid) ."\";return false;'>Cancel</button>");
		$this->head->AddJS("js/jquery/jquery-ui-1.8.19.custom.min.js");
		$this->head->AddJS("js/node/order.js");
		$this->node($nodeid, '', 'order_edit_' . $nodetype);
	}

	public function saveorder($nodeid, $nodetype)
	{
		$parnodeid = $nodeid;
		$groups = array();
		$nodes = array();
		$nodeids = array();

		foreach ( $_POST as $key => $value )
		{
			if ($key != 'ordermode') {
				$post[$key] = $this->input->post($key);

				$bits = explode("_",$key);
				if ($bits[0] == "group")
				{
					$groups[$bits[2]] = $this->input->post($key);
				} else {
					$nodes[$bits[1]] = $this->input->post($key);
					$nodeids[] = $bits[1];
				}
			}
		}

		//print_p($_POST);
		$nogroup_no = sprintf("%03d", count($groups));

		$this->graph_db->batchLoadNodes($nodeids);

		foreach ($nodes as $nodeid => $neworder)
		{
			$node = $this->graph_db->loadNode($nodeid);
			$group = $node->getAttributeValue("group");

			// find group order
			// $bits = explode("~",$group);
			// $groupname = $bits[0];
			// $gkey = abs(crc32($group));
			//echo "Group : $groupname, Key : $gkey<br>";

			if ($group != '') {
				$neworder = sprintf("%03d|%03d", $groups[$group], $neworder);
			} else {
				$neworder = sprintf($nogroup_no . "|%03d", $neworder);
			}
			//echo "New Order : $neworder<br>";

			$node->setAttribute("order", $neworder);
			$node->save();
		}

		$this->head->AddMessage("Order has been saved");
		redirect("view/" . $parnodeid);
	}

	public function resetorder($nodeid, $nodetype)
	{

		$node = $this->graph_db->loadNode($nodeid);
		$relnodes = $node->getRelations($nodetype);

		// First blank all orders
		foreach ($relnodes as &$relnode)
		{
			$relnode->setAttribute("order","");
			$relnode->save();
		}

		// Then create a new order based on the title
		$groups = array();
		$items = array();
		$max_group_o = -1;

		$relnodes = $node->getRelations($nodetype);

		foreach ($relnodes as &$relnode)
		{
			$group = (string)$relnode->getAttribute('group');

			if (!array_key_exists($group, $groups)) {
				$max_group_o++;
				$groups[$group] = $max_group_o;
				$items[$group] = 0;
			} else {
				$items[$group] = $items[$group] + 1;
			}

			$order = str_pad($groups[$group], 3, '0', STR_PAD_LEFT) . '|' . str_pad($items[$group], 3, '0', STR_PAD_LEFT);;
			$relnode->setAttribute("order", $order);
			$relnode->save();
		}

		$this->head->AddMessage("Order has been reset");
		redirect("view/" . $nodeid);
	}

	public function node($nodeid = 1, $extra = '', $file = '')
	{
//		print_p($this->graph_db->active_dataset->nodes);
		//echo "node($nodeid = 1, $extra = '', $file = '')<br>";
		//$CI =& get_instance();
		//print_p($CI->dx_auth->get_group_ids());
		//exit;

		$this->getNode($nodeid);

		if (!$this->data['node'])
			return redirect("/");

		if (!$this->data['node']->can_read())
		{
			return show_error("You do not have permission to view this item");
			//return redirect("/");
		}

		$this->head->AddRawScript('var ajax_base_url = "' . site_url($this->vn . '/ajax/XXTYPEXX/'.$this->data['node']->getID().'/XXIDXX') . '";');

		$filename = "";
		if ($extra)
			$xml = $this->getXMLFile($this->data['node'], $filename, true, '', $file);
		else
			$xml = $this->getXMLFile($this->data['node'], $filename, false, '', $file);

		//echo "<pre>".htmlentities($xml->asXML()) . "</pre>";
		$this->ParseGlobals($xml);
		//echo "<pre>".htmlentities($xml->asXML()) . "</pre>";

		// if permissions, need to add menu items for the current node
		$this->add_node_menu_items($this->data['node']);

		foreach ($xml->section as $section)
		{
			$this->outputSection($section);
		}

		$this->data['filename'] = $filename;

		if ($xml->title)
		{
			$this->data['title'] = $this->data['node']->ParseTitle((string)$xml->title);
		} else {
			$this->data['title'] = $this->data['node']->getTitleDisp();
		}

		$output = $this->output->get_output();
		$this->output->set_output("");
		$this->load->view('templates/header', $this->data);
		$this->output->append_output($output);
		if ($this->dx_auth->showdebug())
			$this->_include('_misc/debug', $this->data);
		$this->load->view('templates/footer');

	}

	function ParseGlobals($xml)
	{
		//echo "ParseGlobals<br>";
		if (empty($this->global_xml))
		{
			$temp_xml = $this->xml_file;
			$this->xml_file = null;
			$filename = "";
			$global_xml = $this->getXMLFile($this->data['node'],$filename, false, '', "_global");
			$this->xml_file = $temp_xml;
			//echo "Loaded Globals $filename<br>";

			$this->global_xml = array();

			foreach ($global_xml->global as $sect)
			{
				$id = (string)$sect->attributes()->id;
				$this->global_xml[$id] = $sect;
			}
		}

		// replace globals with stuff from _global.xml
		foreach($xml->xpath('//global') as $node){
			$id = (string)$node->attributes()->id;

			$target_dom = dom_import_simplexml($node);

			foreach ($this->global_xml[$id]->children() as $child)
			{
				$insert_dom = $target_dom->ownerDocument->importNode(dom_import_simplexml($child), true);
				if ($target_dom->nextSibling) {
					$target_dom->parentNode->insertBefore($insert_dom, $target_dom->nextSibling);
				} else {
					$target_dom->parentNode->appendChild($insert_dom);
				}
			}
			$target_dom->parentNode->removeChild($target_dom);
		}

		if ($xml->ignore_first_node)
		{
			$this->ignore_first_node = 1;
		} else {
			$this->ignore_first_node = 0;
		}
	}

	public function add_node_menu_items(&$node)
	{
		$nodeid = $node->getId();

		if ($this->dx_auth->get_user_id() > 0)
		{
			$node_Type = $node->GetTypeObj();

			//print_p($node->perms);
			if ($node->can_edit())
				$this->menu->AddNodeMenuItem("Edit",'node/edit/' . $nodeid, 'node/edit.png', '');

			if ($node->can_create())
			{
				$heading = false;
				foreach ($node_Type->create as $type)
				{
					$ct = $this->CI->node_types->GetType($type);
					if ($ct->hide_create) continue;

					if (!$heading)
					{
						$this->menu->AddNodeMenuItem("Create New ...",'', 'node/create.png', '');
						$heading = true;
					}

					$this->menu->AddNodeMenuItem($ct->shortname, 'node/create/' . $nodeid . "/" . $ct->gdbid, 'node/add-16x16.png', '', '', 2);
				}
			}

			if ($node->can_link())
			{
				if (count($node_Type->link) > 0)
					$this->menu->AddNodeMenuItem("Link Node To ...", '', 'node/link.png');

					foreach ($node_Type->link as $type)
				{
					$ct = $this->CI->node_types->GetType($type);
					$this->menu->AddNodeMenuItem($ct->shortname, 'link/' . $nodeid . "/" . $ct->id, 'node/add-16x16.png', '', '', 2);
				}
			}

			//$this->menu->AddNodeMenuItem("No Template",'view/node/' . $nodeid . '/notemplate', 'node/notemplate.png');
			if ($node->can_perms())
				$this->menu->AddNodeMenuItem("Permissions",'perms/index/' . $nodeid, 'node/perms.png');

			if ($node->can_history())
				$this->menu->AddNodeMenuItem("History",'node/history/' . $nodeid, 'node/history.png', 'a_popup');
			if ($node->can_clone())
				$this->menu->AddNodeMenuItem("Clone",'clonenode/index/' . $nodeid, 'node/clone.png');
			if ($node->can_delete())
				$this->menu->AddNodeMenuItem("Delete",'node/delete/' . $nodeid, 'misc/delete.png');

			if ($this->dx_auth->is_role("lecturer") && $node_Type->hasAPI)
			{
				$this->menu->AddNodeMenuItem("Reports",'node/reporting/' . $nodeid, 'node/reporting.png');
			}
		}
	}

	protected function getNode($nodeid)
	{
		// try to load the node requested
		$this->data['node'] = $this->graph_db->loadNode($nodeid);

		// not found the node so show a 404 error
		if (!$this->data['node'])
		{
			if ($this->graph_db->load_error == 1)
			{
				show_error("The item you are trying to load does not exist");
			} else {
				show_error("You do not have permission to view this item");
			}
		}

		$this->data['type_obj'] = $this->data['node']->GetTypeObj();

	}

	protected function getXMLFile(&$node, &$filename, $notemplate = false, $subtemplate = '', $altfile = '')
	{
		//echo "getXMLFile(x, $filename, $notemplate, $subtemplate, $altfile)<br>";

		if (empty($this->xml_file))
		{
			// if no path specified, then we build from current role, and the current node type
			$role = $this->dx_auth->get_current_role();
			$nodetype = $node->GetTypeId();

			if ($altfile)
				$nodetype = $altfile;

			$basedir = $this->CI->node_def;
			$subtemplateus = "";
			if ($subtemplate) $subtemplateus = "_" . $subtemplate;
			$basepath = "nodedef/$basedir/{$this->xml_base}";

			$try = array();
			if (!$notemplate && !$this->dx_auth->hidetemplates() && $role != "admin")
			{
				$try[] = "$basepath/$role/$nodetype$subtemplateus.xml";
				$try[] = "$basepath/default/$nodetype$subtemplateus.xml";
				if ($subtemplate)
					$try[] = "$basepath/default/$subtemplate.xml";
			}

			$try[] = "$basepath/default/default.xml";

			$this->view_try = $try;
			//print_p($try);

			foreach ($try as $xmlfile)
			{
				if (file_exists($xmlfile))
				{
					$this->xml_file = simplexml_load_file($xmlfile);

					if ($this->xml_file->altfile)
					{
						$alt = (string)$this->xml_file->altfile;
						unset ($this->xml_file);
						//echo "Getting ALT $alt<br>";
						return $this->getXMLFile($node, $filename, $notemplate, '', $alt);
					}

					//echo "Got File : $xmlfile<br>";
					$filename = $xmlfile;
					return $this->xml_file;
				}
			}
			// no folder for the role, set to default role
			show_error("View::getXMLFile Cant find template in<br><ul><li>" . implode("</li><li>",$try) ."</li></ul>");
		}

		return $this->xml_file;
	}

	private function loadBaseNode($base)
	{
		$base = (string)$base;

		if ($base == "")
			return;

		$this->oldbase = $this->data['node'];

		// FIND NODE
		if ($base == "root")
			$this->data['node'] = $this->graph_db->loadNode(1);

		if (!empty($this->graph_db->active_dataset->nodes->$base))
		{

			$newid = $this->graph_db->active_dataset->nodes->$base;
			$this->data['node'] = $this->graph_db->loadNode($newid);
		}
	}

	private function restoreBaseNode($base)
	{
		$base = (string)$base;

		if ($base == "")
			return;

		$this->data['node'] = $this->oldbase;
	}

	protected function outputSection($section)
	{
		if (!$this->shouldDisplay($this->data['node'], $section)) return;

		if ($section->attributes()->hide == 1) return;

		$this->loadBaseNode($section->attributes()->base);

		$stype = (string)$section->attributes()->type;

		$func = "outputSection$stype";

		if (method_exists($this,$func))
		{
			$this->$func($section);
		} else {
			echo "View::$func does not exist<br>";
			exit;
		}

		$this->restoreBaseNode($section->attributes()->base);
	}

	protected function outputSectionTemplate($section)
	{
		$template = $this->vn . "/" . (string)$section->attributes()->template;
		$this->load->view($template, $this->data);
	}

	protected function shouldDisplay(&$node, $xml)
	{
		if (!$xml->if)
			return true;

		if ($xml->section->start) {
			$basenode =& $this->data['node'];
			$startnode = $this->FindNode($basenode, $xml->section->start);
		} else {
			$startnode = $node;
		}

		foreach ($xml->if as $if)
		{
			$type = (string)$if->attributes()->type;
			if ($type == "has_relation")
			{
				$reltype = (string)$if->attributes()->node_type;
				$rels = $startnode->getRelations($reltype);
				if (count($rels) == 0)
					return false;
			} else if ($type == "no_relation")
			{
				$reltype = (string)$if->attributes()->node_type;
				$rels = $startnode->getRelations($reltype);
				if (count($rels) > 0)
					return false;
			} else if ($type == "has_attribute")
			{
				$attrid = (string)$if->attributes()->attr;
				if ($startnode->getAttributeValue($attrid) == "")
					return false;
			}/* else if ($type == "parent_not_collection")
			{
				$parent = $startnode->getParent();
				if ($parent->getTypeObj()->id == "collection")
				{
					return false;
				}
			}*/
		}
		//$startnode->dump();

		return true;
	}
	protected function outputSectionTabs($section)
	{
		$sectionid = (string)$section->attributes()->id;
		$active = (string)$section->attributes()->active;
		$this->output->append_output($this->CI->tabs->startPane($sectionid));
		foreach ($section->children() as $child)
		{
			$name = $child->getName();
			if ($name == "tab")
			{
				// check for if
				if ($child->if && !$this->shouldDisplay($this->data['node'], $child))
					continue;

				$needrole = (string)$child->attributes()->needrole;
				if ($needrole != '' && !$this->dx_auth->is_role($needrole)) continue;

				$tab = $child;
				$title = (string)$tab->attributes()->title;
				$id = (string)$tab->attributes()->id;

				$classextra = "";
				if ((string)$tab->attributes()->align == "right")
					$classextra = "right";


				if ($active)
				{
					if ($active == $id)
					{
						$this->CI->tabs->opened = false;
					} else {
						$this->CI->tabs->opened = $active;
					}
				}
				$this->output->append_output($this->CI->tabs->addPanel($title, $id, $classextra));
			} else if ($name == "extratabs")
			{
				// need to list the extra tab info
				if ((string)$child->attributes()->type == "relations")
					$this->Tabs_Extra_Header($child);
				if ((string)$child->attributes()->type == "collections")
					$this->Tabs_ExtraColl_Header($child);
			}
		}

		if ($active)
			$this->CI->tabs->opened = $active;

		$this->output->append_output($this->CI->tabs->endHead());

		foreach ($section->children() as $child)
		{
			$name = $child->getName();
			if ($name == "tab")
			{
				// check for if
				if ($child->if && !$this->shouldDisplay($this->data['node'], $child))
					continue;

				$tab = $child;
				$title = (string)$tab->attributes()->title;
				$id = (string)$tab->attributes()->id;

				$this->output->append_output($this->CI->tabs->startPanel($id));
				foreach ($tab as $section)
				{
					if ($section->getName() != "section") continue;
					$this->outputSection($section);
				}
				$this->output->append_output($this->CI->tabs->endPanel());
			} else if ($name == "extratabs")
			{
				// need to list the extra tab info
				if ((string)$child->attributes()->type == "relations")
					$this->Tabs_Extra_Body($child);
				if ((string)$child->attributes()->type == "collections")
					$this->Tabs_ExtraColl_Body($child);
			}
		}

		$this->output->append_output($this->CI->tabs->endPane());
	}

	function ProcessIf($ifxml)
	{

	}

	protected function outputSectionTable($section)
	{
		$this->output->append_output("<table class='niceround'>");

		foreach ($section->children() as $child)
		{
			$name = $child->getName();
			if ($name == "tr")
			{
				if (!$this->shouldDisplay($this->data['node'], $child)) continue;

				$head = $child->attributes()->label;
				$this->output->append_output("<tr><th>$head</th><td>");

				foreach ($child as $section)
				{
					$sname = $section->getName();
					if ($sname != "section") continue;

					$this->outputSection($section);
				}

				$this->output->append_output("</td></tr>");
			}
		}

		$this->output->append_output("</table>");
	}

	protected function Tabs_ExtraColl_Header($xml)
	{
		$rels = $this->data['node']->getRelations("collection");
		foreach ($rels as &$node)
		{
			$this->output->append_output($this->CI->tabs->addPanel($node->GetTitleDisp(), "coll" . $node->GetID()));
		}
	}

	protected function Tabs_Extra_Header($xml)
	{
		// at moment, only relations are allowed as extra tabs, with parameter to not list certain node type
		$rels = $this->data['node']->getRelations_GroupByNodeType();

		$skip = array();
		if ($xml->skip)
			foreach ($xml->skip as $skipnode)
				$skip[(string)$skipnode] = (string)$skipnode;

		$only = array();
		if ($xml->only)
			foreach ($xml->only as $onlynode)
				$only[(string)$onlynode] = (string)$onlynode;

		foreach($rels as $reltype => &$rel)
		{
			$nodetype =& $this->node_types->GetType($reltype);

			// skip root node
			if ($nodetype->getId() == "root" && $this->dx_auth->get_current_role() != "admin") continue;

			// hide kv_pair
			if ($nodetype->getId() == "kv_pair" && $this->dx_auth->get_current_role() != "admin") continue;

			// not list perm nodes
			if (substr($nodetype->getId(),0,5) == "perm_" && $this->dx_auth->get_current_role() != "admin") continue;

			// skip any that have been defined as to skip
			if (array_key_exists($nodetype->getId(), $skip)) continue;

			// if in only mode, make sure to skip any necessary
			if (count($only) > 0 && !array_key_exists($nodetype->getId(), $only)) continue;

			$tabtitle = $nodetype->name;
			if (!empty($nodetype->tabparams->tab_title))
				$tabtitle = $nodetype->tabparams->tab_title;
			$this->output->append_output($this->CI->tabs->addPanel($tabtitle, "nodetype_" . $nodetype->id));
		}
	}

	protected function Tabs_ExtraColl_Body($xml)
	{
		$rels = $this->data['node']->getRelations("collection");
		foreach ($rels as &$node)
		{

			$this->output->append_output($this->CI->tabs->startPanel("coll" . $node->GetID()));

			$this->data['collnode'] = $node;
			$this->data['nodelist'] = $node->getRelations_GroupByRelType_GroupByNodeType();

			$this->_include('_misc/collection', $this->data);

			$this->output->append_output($this->CI->tabs->endPanel());
		}

	}

	protected function Tabs_Extra_Body($xml)
	{
		// at moment, only relations are allowed as extra tabs, with parameter to not list certain node type
		$rels = $this->data['node']->getRelations_GroupByNodeType();

		$skip = array();
		if ($xml->skip)
			foreach ($xml->skip as $skipnode)
				$skip[(string)$skipnode] = (string)$skipnode;

		$only = array();
		if ($xml->only)
			foreach ($xml->only as $onlynode)
				$only[(string)$onlynode] = (string)$onlynode;

		foreach($rels as $reltype => &$rel)
		{
			$nodetype =& $this->node_types->GetType($reltype);

			// skip root node
			if ($nodetype->getId() == "root" && $this->dx_auth->get_current_role() != "admin") continue;

			// hide kv_pairs
			if ($nodetype->getId() == "kv_pair" && $this->dx_auth->get_current_role() != "admin") continue;

			// not list perm nodes
			if (substr($nodetype->getId(),0,5) == "perm_" && $this->dx_auth->get_current_role() != "admin") continue;

			// skip any that have been defined as to skip
			if (array_key_exists($nodetype->getId(), $skip)) continue;

			// if in only mode, make sure to skip any necessary
			if (count($only) > 0 && !array_key_exists($nodetype->getId(), $only)) continue;

			$this->output->append_output($this->CI->tabs->startPanel("nodetype_" . $nodetype->id));

			$this->data['relnodes'] = $rel;

			// remove the current node from the results so not to cause confusion
			foreach ($this->data['relnodes'] as $offset => $node)
			{
				//echo "Node : {$node->getId()} ?= $nodeid<br>";
				if ($node->getId() == $this->data['node']->GetId())
				{
					unset($this->data['nodetree']['nodes'][$offset]);
					break;
				}
			}

			$this->graph_db->sort_nodes($this->data['relnodes'],"title");

			if (!empty($nodetype->tabparams->no_expand))
			{
				$this->_include('_misc/node_related_type_min', $this->data);
			} else {
				// need to outout a list of related nodes here
				$this->_include('_misc/node_related_type', $this->data);
			}

			$this->output->append_output($this->CI->tabs->endPanel());
		}
	}

	protected function outputSectionNodeList($section)
	{
		$nodetype = (string)$section->attributes()->node_type;
		$rel_type = (string)$section->attributes()->rel_type;

		//print_p($this->graph_db->active_dataset->nodes);
		$basenode =& $this->data['node'];

		$type = '';
		if ($rel_type)
		{
			$type = GDB_Rel::Relation;
			if ($rel_type == "child")
				$type = GDB_Rel::ChildOf;
			if ($rel_type == "parent")
				$type = GDB_Rel::ParentOf;
		}


		$this->data['nodelist'] = $basenode->getRelations($nodetype, $type);

		//echo "Sort : {$section->attributes()->sort}<Br>";
		$this->graph_db->sort_nodes($this->data['nodelist'], (string)$section->attributes()->sort);

		$this->data['longtitle'] = (string)$section->attributes()->longtitle;
		$this->data['tools'] = (string)$section->attributes()->tools;

		$this->data['groupby'] = (string)$section->attributes()->groupby;
		$this->data['class'] = (string)$section->attributes()->class;
		$this->data['tooltip'] = (string)$section->attributes()->tooltip;
		$this->data['linkto'] = (string)$section->attributes()->linkto;

		if (!$this->data['class']) $this->data['class'] = "nodelist_node";
		$this->_include('_misc/list_nodes', $this->data);
	}

	protected function &NodeTree_LoadLayers($section)
	{
		$result = array();
		foreach ($section->layer as $layer)
		{
			$layerobj = new TreeLayer();
			$layerobj->path = (string)$layer->attributes()->path;
			$layerobj->skip_display = (int)$layer->attributes()->skip_display;
			$layerobj->expand = (int)$layer->attributes()->expand;
			$layerobj->only_linkd_to = (string)$layer->attributes()->only_linkd_to;
			$layerobj->sort = (string)$layer->attributes()->sort;
			$layerobj->nolink = (string)$layer->attributes()->nolink;
			$layerobj->tools = (string)$layer->attributes()->tools;
			if ($layerobj->tools == "custom") $layerobj->tools = $layer;
			$layerobj->longtitle = (string)$layer->attributes()->longtitle;
			$layerobj->find = (string)$layer->attributes()->find;
			$layerobj->layer = $layer;
			$layerobj->header = '';

			if ($layer->header)
			{
				$layerobj->header = (string)$layer->header;
				$layerobj->header_raw = (int)$layer->header->attributes()->raw;
			}

			$result[] = $layerobj;
		}

		return $result;
	}

	protected function outputSectionNodeTree($section, $template = "tree_node")
	{
		//echo "outputSectionNodeTree<br>";
		// need to display a tree list of the layers specified
		$layers = $this->NodeTree_LoadLayers($section);

		$layer = $layers[0];
		$basenode =& $this->data['node'];
		$sectionid = (string)$section->attributes()->id;

		$subtemplate = "";
		if ($this->subtemplate)
			$subtemplate = "/tree_node/" . $this->subtemplate;

		$this->data['nodetree']['url'] = site_url($this->vn . "/nodetree/" . $basenode->getID() . '/' . $sectionid . '/' . "XXIDXX$subtemplate");
		$this->data['nodetree']['sectionid'] = $sectionid;
		$this->_include('_misc/tree_root', $this->data);

		if ($layer->find)
		{
			// FIND NODE
			$thissel = "";
			$base = $layer->find;
			if ($base == "root")
			{
				$thissel = 1;
			} else if (!empty($this->graph_db->active_dataset->nodes->$base))
			{
				$thissel = $this->graph_db->active_dataset->nodes->$base;
			}

			if ($thissel != "")
				return $this->nodetree($basenode->getId(), $sectionid, $thissel);
		}

		if ($layer->skip_display)
		{
			$thissel = array();
			$rels = $basenode->getRelations($layer->path);
			foreach ($rels as &$node)
			{
				$thissel[] = $node->getId() ;
			}
			$thissel = implode("-",$thissel);
			//echo "skip_display<br>";
			//print_p($thissel);
			return $this->nodetree($basenode->getId(), $sectionid, $thissel);
		}

		$this->data['nodetree']['selected'] = "";
		$this->data['nodetree']['nodes'] = $basenode->getRelations($layer->path);
		$this->data['nodetree']['layer'] = $layer;

		if ((int)$section->attributes()->notable == 1)
			$template .= "_notable";

		$this->graph_db->sort_nodes($this->data['nodetree']['nodes'], $layer->sort);

		$curlayer = 0;
		if ($curlayer == count($layers) - 1)
		{
			$this->data['nodetree']['finallayer'] = 1;
		} else {
			$this->data['nodetree']['finallayer'] = 0;
		}

		$this->_include("_misc/$template", $this->data);
	}

	public function TreeLayerOutputSet($curlayer, $selected)
	{
		// need to add the current layer to the layerpath var
		$offset = count($this->data['tree']['layerpath']);
		foreach ($curlayer->layers as $layer)
		{
			$this->data['tree']['layerpath'][$offset] = $layer;
			$this->TreeLayerOutputLayer($layer, $selected);
		}
	}

	protected function TreeLayerOutputLayer($layer, $selected)
	{
		//echo "Layer : {$layer->id} ($selected)<br>";
		// normal layer
		//print_p($layer);

		// work out the base nodes we have
		// we can assume that we are displaying the layer relevant to the last set of selected nodes
		$selected = explode("|", $selected);
		$cursel = $selected[count($selected)-1];
		//echo "Selected:<br>";
		//print_p($selected);

		// build a list of all the nodes we have seen so far so they can be excluded from the list of ones to display
		$all_seen = array();
		$first = false;
		if ($layer->ignore_first_node || $this->ignore_first_node) $first = true;

		foreach ($selected as $offset => $sel)
		{
			if ($first)
			{
				$first = false;
				continue;
			}

			// make sure that any layers that are set to skip_display are not added to the list so they can be displayed
			//echo "Seen: $offset => $sel ";
			if (count($this->data['tree']['layerpath']) > 0 && array_key_exists($offset, $this->data['tree']['layerpath']))
			{
				$thislayer = $this->data['tree']['layerpath'][$offset];

				//echo " ({$thislayer->path}";
				//if (!empty($thislayer->skip_display)) echo " - {$thislayer->skip_display})";

				if (!empty($thislayer->skip_display) && $thislayer->skip_display)
				{
				//	echo " - SKIPPING<br>";
					continue;
				}
			}

			//echo "<br>";

			if (strpos($sel,"-") > 0)
			{
				$nodeids = explode("-",$cursel);
				foreach ($nodeids as $id)
					$all_seen[$id] = 1;
			} else {
				$all_seen[$sel] = 1;
			}
		}

		$this->data['tree']['all_seen'] =&$all_seen;
		//echo "All Seen Nodes:<br>";
		//print_p($all_seen);

		if (strpos($cursel,"-") > 0)
		{
			$nodeids = explode("-",$cursel);
			$basenode = $this->graph_db->loadNodes($nodeids);
		} else {
			$basenode = $this->graph_db->loadNode($cursel);
		}

		if (!$basenode)
		{
			// none found
			if ($layer->nonefound)
			{
				$this->output->append_output("<div class='rel_title'>{$layer->nonefound}</div>");
			} else {
				$this->output->append_output("<div class='rel_title'>None found</div>");
			}
			return;
		}

		//echo "Cur Node : $cursel<br>";

		if ($layer->find) // if we are a find layer, then add the found node to the selected list, and call again with the sub layers
		{
			if (is_array($basenode))
				show_error("Find one a layer with multiple nodes");

			if ((string)$layer->find == "layer")
			{
				// need to traverse upwards through the layers until we find one with the right id
				$offset = count($selected);
				//echo "Finding layer ID : {$layer->find_id}<br>";
				$parlayer = $layer->parent;

				while ($parlayer)
				{
					$offset--;
					//echo "Par Layer : $parlayer->id<br>";

					if ($parlayer->id == $layer->find_id)
					{
						//echo "Found Layer! - Offset = $offset<br>";
						//print_p($selected);
						break;
					}

					if (!empty($parlayer->parent))
					{
						$parlayer = $parlayer->parent;
					} else {
						break;
					}
				}
				$foundnode = $this->graph_db->loadNode($selected[$offset]);

				//echo "Using {$foundnode->getTitle()}<br>";
				//exit;
			} else {
				$foundnode = $this->FindNode($basenode, (string)$layer->find);
			}

			if ($foundnode)
			{
				$this->TreeLayerOutputSet($layer, implode("|",$selected) . "|" . $foundnode->getID());
				return;
			}
		}

		// load in a list of nodes along the path specified
		if (is_array($basenode))
		{
			// need to load all the related nodes for the list of base nodes
			$this->data['tree']['nodes'] = array();
			foreach ($basenode as &$node)
			{
				$rels = $node->getRelations($layer->path);
				foreach ($rels as &$relnode)
				{
					$this->data['tree']['nodes'][$relnode->getId()] = $relnode;
				}
			}
			//return;
		} elseif (count($layer->extpath) > 0){
			$this->data['tree']['nodes'] = $this->FindNodesOnPaths($basenode, $layer->extpath);
		} else {
			$this->data['tree']['nodes'] = $basenode->getRelations($layer->path);
		}

		// if we have only_linkd_to set, then remove the nodes from the list that shouldnt be there
		if ($layer->only_linkd_to)
		{
			//echo "Sel Count : " . count($selected) . "<br>";

			if ($layer->only_linkd_to == "node")
			{
				$link_node = $this->data['node'];
			} else {

				$offset = count($selected);

				$parlayer = $layer->parent;
				while ($parlayer)
				{
					$offset--;
					//echo "Par Layer : $parlayer->id<br>";

					if ($parlayer->id == $layer->only_linkd_to)
					{
						//echo "Found Layer! - Offset = $offset<br>";
						//print_p($selected);
						break;
					}

					if (!empty($parlayer->parent))
					{
						$parlayer = $parlayer->parent;
					} else {
						break;
					}
				}

				if ($offset < 0)
				{
					// not found it!
					show_error("Unable to find only_linkd_to referenced layer {$layer->only_linkd_to}");
				}

				if (strpos($selected[$offset],"-") > 0)
				{
					$selnodelist = explode("-",$selected[$offset]);
					//echo "Nodes : " . implode(", ", $selnodelist) . "<br>";
					//show_error("only_linkd_to layer {$layer->only_linkd_to} is a skipped layer, this isnt allowed!");

					$link_node = $this->graph_db->loadNodes($selnodelist);
				} else {
					$link_node = $this->graph_db->loadNode($selected[$offset]);
				}
			}

			if (!$link_node)
				show_error("Cannot load referenced only_linkd_to node");

			//if (is_object($link_node))
			//	echo "Link Nodes : {$link_node->getTitle()}<br>";
			// any nodes that arent related to the selected link_node should be ditched!
			foreach ($this->data['tree']['nodes'] as $offset => &$node)
			{
				if (is_array($link_node))
				{
					$found = false;
					foreach ($link_node as &$testnode)
					{
						//echo "Link Nodes : {$testnode->getTitle()}<br>";
						if ($testnode->isRelatedTo($node))
							$found = true;
					}
					if (!$found)
					{
						unset($this->data['tree']['nodes'][$offset]);
					}
				} else {
					//echo "Checking if {$link_node->getID()} related to {$node->getId()}<br>";
					if (!$link_node->isRelatedTo($node))
					{
						//echo "Not Related<Br>";
						unset($this->data['tree']['nodes'][$offset]);
					} else {
						//echo "Node OK to Display!<br>";
					}
				}
			}

		}

		//echo "Count 1 : ".count($this->data['tree']['nodes'])."<br>";

		// remove any nodes that dont fit with the req_attr specified
		if (count($layer->req_attrs) > 0)
		{
			//echo "Removing based on <br>";
			//print_p($layer->req_attrs);
			foreach ($layer->req_attrs as &$req_attr)
			{
				foreach ($this->data['tree']['nodes'] as $offset => &$node)
				{
					$attrval = $node->getAttributeValue($req_attr->id);
					//echo "Node : " . $node->getTitle() . ", attr : " . $attrval . "<br>";

					$ismatch = $attrval == $req_attr->value;
					$required = $req_attr->not != 1;
					//echo "Match : $ismatch, $required<br>";
					if ($ismatch != $required)
					{
						unset($this->data['tree']['nodes'][$offset]);
					}
				}
			}
		}

		//echo "Count 2 : ".count($this->data['tree']['nodes'])."<br>";


		// remove any nodes that dont find a reltype we have specified
		if ($layer->reltype)
		{
			//echo "Remove nodes not related as {$layer->reltype}<br>";
			foreach ($this->data['tree']['nodes'] as $offset => &$node)
			{
				$reltype = $basenode->getRelationTo($node);

				if (
					($layer->reltype == "parent" && $reltype != GDB_Rel::ParentOf) ||
						($layer->reltype == "child" && $reltype != GDB_Rel::ChildOf) ||
						($layer->reltype == "relation" && $reltype != GDB_Rel::Relation)
					)
				{
					unset($this->data['tree']['nodes'][$offset]);

					continue;
				}

			}
		}

		//echo "Count 3 : ".count($this->data['tree']['nodes'])."<br>";

		// work out weights to display
		if ($layer->weight && count($this->data['tree']['nodes']) > 0)
		{
			// need to find what node that the weight is relating to from the layer param. Iterate up the selected items
			// and find what matches the node type specified
			//echo "Layer Weight : {$layer->weight}<br>";
			//echo "Layer Path : {$layer->path}<br>";

			$this->data['tree']['weight_node'] = null;

			if ($layer->weight == "node")
			{
				// node specified so using pages main node
				$this->data['tree']['weight_node'] = $this->data['node'];
				//echo "Weight Node : {$this->data['node']->getTitle()}<br>";
			} else {
				// need to iterate up through the selected nodes to find one that is of the correct type
				//print_p($selected);
				for ($i = count($selected) - 1 ; $i >= 0 ; $i--)
				{
					$selid = $selected[$i];
					if (strpos($selid,"-") > 0)
						continue;
					//echo "Checking $selid<br>";
					$tempnode = $this->graph_db->loadNode($selid);
					//echo "node is {$tempnode->getTitle()} => {$tempnode->getTypeID()}<br>";

					$type = $tempnode->getTypeID();

					if ($type == $layer->weight)
					{
						//echo "Using<br>";
						$this->data['tree']['weight_node'] =& $tempnode;
						break;
					}
				}
			}

			//echo "Base Node : {$this->data['tree']['weight_node']->getTitle()}<br>";

			// have we found the node we are showing the weights to
			if ($this->data['tree']['weight_node'])
			{

				// for all nodes we still have, sort out the weight display
				foreach ($this->data['tree']['nodes'] as $offset => &$node)
				{
					$node->weight = $this->data['tree']['weight_node']->getWeightTo($node);
				}

			} else { // not found one, so dont display weights
				echo "Cannot find {$layer->weight}<br>";
				$layer->weight = "";
			}
			//echo "Node Weight : {$basenode->GetID()} => {$node->getId()} ({$node->getTitle()}) = {$node->weight}<br>";
		}

		//echo "Count 4 : ".count($this->data['tree']['nodes'])."<br>";

		// need to work out the counts for the next layer!
		// this is rather complex i think!

		$can_expand = array();
		$node_ids = array();
		foreach ($this->data['tree']['nodes'] as &$node)
		{
			$can_expand[$node->getID()] = 0;
			$node_ids[] = $node->getID();
		}

		//print_p($node_ids);
		$relcounts = $this->graph_db->GetRelCounts($node_ids);
		//print_p($relcounts);
		$this->data['tree']['always_expand'] = false;

		// for each sub layer we have available
		foreach ($layer->layers as &$sublayer)
		{
			// load the relation counts for a set of nodes
			$newpath = $sublayer->path;

			//echo "Checking Layer For Possible Expands : {$sublayer->id}<br>";



			if (count($sublayer->extpath) > 0)
			{
				//echo "Settings as ALWAYS CAN EXPAND due to extpath<br>";
				//print_p($sublayer->extpath);
				$this->data['tree']['always_expand'] = true;

				continue;
			}

			if ($sublayer->find)
			{
				//echo "Settings as ALWAYS CAN EXPAND due to find ({$sublayer->find})<br>";
				$this->data['tree']['always_expand'] = true;
				continue;
			}


			if (!$newpath) continue;
			$typeobj = $this->node_types->getType($newpath);

			foreach ($can_expand as $expid => $junk)
			{
				if (!array_key_exists($expid, $relcounts)) continue;
				if (!array_key_exists($typeobj->gdbid, $relcounts[$expid])) continue;

				if ($relcounts[$expid][$typeobj->gdbid] > 0)
				{
					//print_p($relcounts);
					// need to check all of the nodes in the count and make sure we havent seen any of em yet!
					foreach ($relcounts[$expid]['ids'][$typeobj->gdbid] as $nodeid)
					{
						if (array_key_exists($nodeid, $all_seen))
							$relcounts[$expid][$typeobj->gdbid]--;
					}

					// now we have removed the seen nodes, do we still have child nodes?
					if ($relcounts[$expid][$typeobj->gdbid] > 0)
						$can_expand[$expid] = 1;
				}
			}
		}

		//print_p($can_expand);
		$this->data['tree']['can_expand'] =& $can_expand;

		if ($layer->skip_display) // skip display layer, build a list of node ids that the are the options for the layer, and call again
		{
			//$newsel = "";
			$newsel = array();
			foreach ($this->data['tree']['nodes'] as &$node)
			{
				$newsel[] = $node->getID();
			}
			$newsel = implode("-",$newsel);

			//echo "Skip Display! - $newsel<br>";

			$this->TreeLayerOutputSet($layer, implode("|",$selected) . "|" . $newsel);
			return;
		}

		$this->data['tree']['layer'] = &$layer;
		$this->data['tree']['selected'] = implode("|",$selected) . "|";

		//echo "Layer : {$layer->id}, Group : {$layer->group}, Sort : {$layer->sort}<br>";

		// sort the nodes we have
		$this->graph_db->sort_nodes($this->data['tree']['nodes'], $layer->sort);

		// include template
		$this->data['view'] = &$this;
		$this->_include("tree/" .$this->data['tree']['template'], $this->data);
	}

	protected function outputSectionTree($section)
	{
		$layer = new TreeLayer();

		// need to display a tree list of the layers specified
		$layer->layers = $this->Tree_LoadLayers($section);

		$template = "node";
		if ((string)$section->attributes()->template != "")
			$template = (string)$section->attributes()->template;

		$basenode =& $this->data['node'];
		$sectionid = (string)$section->attributes()->id;
		$selected = $basenode->getID();

		$this->data['tree']['url'] = site_url($this->vn . "/tree/" . $basenode->getID() . '/' . $sectionid . '/' . "XXIDXX");
		$this->data['tree']['sectionid'] = $sectionid;
		$this->data['tree']['template'] = $template;
		$this->data['tree']['layerpath'] = array();
		$this->data['tree']['layerpath'][] = $layer;

		$this->DoWeightPopup();

		$this->_include('tree/root', $this->data);

		// instead of having 2 sets of code, just call the sub layer code with no selected nodes
		$this->TreeLayerOutputSet($layer, $selected);
	}

	protected function DoWeightPopup()
	{
		if (empty($this->has_weight_popup))
		{
			$this->has_weight_popup = 1;
			$this->_include('tree/weight_popup', $this->data);
		}
	}

	public function tree($nodeid, $sectionid, $selected, $subtmpl = '')
	{
		$this->getNode($nodeid);
		$filename = "";
		$xml = $this->getXMLFile($this->data['node'], $filename, false, $subtmpl);
		$this->ParseGlobals($xml);

		$section = $this->FindXMLSection($xml, $sectionid);
		if (!$section)
		{
			//print_p($xml);
			show_error("View::tree Unable to find section $sectionid in nodetree($nodeid, $sectionid, $selected), file : $filename");
		}

		$template = "node";
		if ((string)$section->attributes()->template != "")
			$template = (string)$section->attributes()->template;
		$this->data['tree']['template'] = $template;

		$layer = new TreeLayer();
		$sublayer = new TreeLayer();
		// need to display a tree list of the layers specified
		$sublayer->layers = $this->Tree_LoadLayers($section);
		$sublayer->path = "node";
		$layer->layers = array();
		$layer->layers[] = $sublayer;

		//$layers = array();
		$selected = urldecode($selected);
		//echo "Node Tree Ajax Inner : $nodeid, $sectionid, $selected<br>";

		$exselected = explode("|", $selected);

		// transverse the tree for each selected node, and determine the path we need to take to get to the current selected layer node

		$layerpath = array();

		foreach ($exselected as $cursel)
		{
			//echo "Selected : $cursel<br>";
			if (count($layer->layers) == 1) // we have a single layer! its easy
			{
				$layer =& $layer->layers[0];
				$layerpath[] = &$layer;
				//echo "Picked : {$layer->path}<br>";
			} else {
				//echo "Multiple layer options, ";
				if (strpos($cursel,"-") > 0)
				{
					$cursel = substr($cursel,0,strpos($cursel,"-"));
				}
				//echo "Selected: $cursel, ";

				$curselnode = $this->graph_db->loadNode($cursel);
				$typeobj = $curselnode->getTypeObj();
				//echo "Select Type : {$typeobj->id}<br>";

				foreach ($layer->layers as &$templayer)
				{
					//echo "Option : {$templayer->path}<br>";
					if ($templayer->path == $typeobj->id)
					{
						$layer = $templayer;
						$layerpath[] = &$layer;
						//echo "Picked {$templayer->id}<br>";
					}
				}
				//exit;
			}
		}
		$this->data['tree']['layerpath'] = &$layerpath;
		$this->data['tree']['sectionid'] = $sectionid;

		$this->TreeLayerOutputSet($layer, $selected);
	}

	protected function &Tree_LoadLayers($section, $parent = null)
	{
		$this->all_layers = array();

		$layers = $this->Tree_LoadLayers_Inner($section, $parent);

		//foreach ($this->all_layers as &$layer)
		//	unset($layer->parent);

		//echo "Final Layers<br>";
		//print_p($layers);
		return $layers;
	}

	private function &Tree_LoadLayers_Inner($section, &$parent)
	{
		$result = array();
		foreach ($section->layer as $layer)
		{
			if ($layer->attributes()->ref)
			{
				//echo "Ref Layer : {$layer->attributes()->ref}<br>";
				$ref = (string)$layer->attributes()->ref;

				if ($ref == "parent")
				{
					$result[] = $parent;
				} elseif (array_key_exists($ref, $this->all_layers)) {
					$result[] = $this->all_layers[$ref];
				}
				continue;
			}

			$layerobj = new TreeLayer();
			$layerobj->parent = $parent;

			if ($layer->attributes()->id)
			{
				$layerobj->id = (string)$layer->attributes()->id;
			} else {
				$layerobj->id = mt_rand();
			}

			$this->all_layers[$layerobj->id] =& $layerobj;


			$layerobj->path = (string)$layer->attributes()->path;
			$layerobj->skip_display = (int)$layer->attributes()->skip_display;
			$layerobj->expand = (int)$layer->attributes()->expand;
			$layerobj->only_linkd_to = (string)$layer->attributes()->only_linkd_to;
			$layerobj->sort = (string)$layer->attributes()->sort;
			$layerobj->group = (string)$layer->attributes()->group;
			$layerobj->nolink = (string)$layer->attributes()->nolink;
			$layerobj->tools = (string)$layer->attributes()->tools;
			if ($layerobj->tools == "custom") $layerobj->tools = $layer;
			$layerobj->longtitle = (string)$layer->attributes()->longtitle;
			$layerobj->find = (string)$layer->attributes()->find;
			$layerobj->find_id = (string)$layer->attributes()->find_id;
			$layerobj->noindent = (string)$layer->attributes()->noindent;
			$layerobj->reltype = (string)$layer->attributes()->reltype;
			$layerobj->ignore_first_node = (string)$layer->attributes()->ignore_first_node;
			$layerobj->indent = (string)$layer->attributes()->indent;
			$layerobj->weight = (string)$layer->attributes()->weight;
			$layerobj->can_weight = (int)$layer->attributes()->can_weight;
			$layerobj->nonefound = (string)$layer->nonefound;
			$layerobj->layer = $layer;
			$layerobj->header = '';
			$layerobj->req_attrs = array();
			$layerobj->extpath = array();

			if ($layer->header)
			{
				$layerobj->header = (string)$layer->header;
				$layerobj->header_raw = (int)$layer->header->attributes()->raw;
			}

			if ($layer->extpath)
			{
				foreach ($layer->extpath as $extpath)
					$layerobj->extpath[] = (string)$extpath;
			}

			if ($layer->req_attr)
			{
				foreach ($layer->req_attr as $req_attr)
				{
					$req_attr_obj = new stdClass();
					$req_attr_obj->id = (string)$req_attr->attributes()->id;
					$req_attr_obj->value = (string)$req_attr->attributes()->value;
					$req_attr_obj->not = (int)$req_attr->attributes()->not;

					$layerobj->req_attrs[] = $req_attr_obj;
				}
			}

			$layerobj->layers = $this->Tree_LoadLayers_Inner($layer, $layerobj);

			$result[] = $layerobj;
		}

		return $result;
	}
	private function FindNode(&$basenode, $name)
	{
		// allows following:
		/*
		root
		parent
		name of node specified my definition
		interger node number
		*/

		$sname = (string)$name;
		$iname = (int)$name;
		//echo "Find Node: SName : $sname, IName : $iname<Br>";

		// FIND NODE
		$node = null;

		if ($sname == "parent")
			$node = $basenode->getParent();
		elseif ($sname == "node")
			$node = $this->data['node'];
		elseif ($sname == "root")
			$node = $this->graph_db->loadNode(1);
		elseif ($sname == "user")
			$node = $this->graph_db->loadNode($this->dx_auth->get_my_node());
		elseif ($iname > 0)
			$node = $this->graph_db->loadNode($iname);
		else
			$node = $this->graph_db->loadNode($this->graph_db->active_dataset->nodes->$sname);

		//echo "Found Node : {$node->getTitle()}<br>";

		return $node;
	}

	// needs to be public as called from perms user pick
	public function outputSectionLongTree($section, $page = 1, $ajax = false, $search = '', $startnodeid = '')
	{
		//echo "outputSectionLongTree($section, $page = 1, $ajax = false, $search = '', $startnodeid = '')<br>";
		if ($search == "-" ) $search = "";
		//print_p($section);

		// need to display a tree list of the layers specified
		$layers = $this->NodeTree_LoadLayers($section);

		$layer = $layers[0];
		$basenode =& $this->data['node'];
		$sectionid = (string)$section->attributes()->id;

		//echo "Search : $search, Page : $page<br>";

		$subtemplate = "";
		if ($this->subtemplate)
			$subtemplate = "/tree_node/" . $this->subtemplate;

		//echo "Sub Template : {$this->subtemplate}<br>";

		$this->data['longtree']['url'] = site_url($this->vn . "/nodetree/" . $basenode->getID() . '/' . $sectionid . '/' . "XXIDXX$subtemplate");
		$this->data['longtree']['sectionid'] = $sectionid;
		$this->data['longtree']['ajax'] = $ajax;
		$this->_include('longtree/longtree_root', $this->data);
		$this->data['sectionid'] = $sectionid;
		$this->data['ajax'] = $ajax;

		$perpage = (int)$section->attributes()->perpage;
		if ($perpage < 1)
			$perpage = 15;

		//echo "Start : {$section->start}<br>";
		if ($section->start)
		{
			$startnode = $this->FindNode($basenode, $section->start);
		} else {
			$startnode = $basenode;
		}

		//$startnode->dump();

		$this->data['longtree']['offset'] = ($page - 1) * $perpage;
		$this->data['longtree']['page'] = $page;
		$this->data['longtree']['perpage'] = $perpage;
		$this->data['longtree']['selected'] = "";
		$this->data['longtree']['nodes'] = $startnode->getRelations($layer->path);
		$this->data['longtree']['layer'] = $layer;


		// de we have an optional dropdown select to pick the start node
		$this->data['select'] = 0;
		if ($section->select)
		{
			$this->data['select'] = 1;
			$this->data['select_title'] = (string)$section->select->attributes()->title;
			$select_options = $this->GetSelectOptions($this->data['node'], $section->select);
			$sel = "<select name='select_{$sectionid}' onchange='>\n";
			foreach ($select_options as $opt)
			{
				$sel .= "<option value='{$opt['id']}' ";
				if ($opt['id'] == $startnode->getID())
					$sel .= " selected";
				$sel .= ">{$opt['title']}</option>\n";
			}
			$sel .= "</select>\n";
			$this->data['select_dropdown'] = $sel;
		}


		if ((int)$section->attributes()->notable == 1)
			$template = "_notable";

		// we need to remove any nodes that dont match the search if one is specified
		if ($search)
		{
			$search = urldecode($search);
			$search = explode(" ",$search);
			$words = array();

			foreach ($search as &$word)
			{
				$word = trim($word);
				if (!$word) continue;
				$words[] = $word;
			}

			$unset = array();
			foreach ($this->data['longtree']['nodes'] as $offset => &$node)
			{
				$title = $node->getTitleDisp();

				$found = true;

				foreach ($words as $word)
					if (stripos($title, $word) === false)
						$found = false;

				if (!$found)
					$unset[] = $offset;
			}

			foreach ($unset as $offset)
				unset($this->data['longtree']['nodes'][$offset]);

			if (count($this->data['longtree']['nodes']) == 0)
			{
				$this->output->append_output($this->head->MessageText("No results found"));
				return;
			}
		}

		$this->graph_db->sort_nodes($this->data['longtree']['nodes'], $layer->sort);

		$curlayer = 0;
		if ($curlayer == count($layers) - 1)
		{
			$this->data['longtree']['finallayer'] = 1;
		} else {
			$this->data['longtree']['finallayer'] = 0;
		}

		//$this->output->append_output("1111111");
		if (!$ajax)	$this->_include("longtree/longtree_search", $this->data);
		//$this->output->append_output("2222222");
		//if (!$ajax)	$this->output->append_output("<div id='longtree_$sectionid'>");
		$this->_include('longtree/longtree_node', $this->data);
		//if (!$ajax)	$this->output->append_output("</div>");
		//$this->output->append_output("5555555");
	}

	protected function GetSelectOptions(&$node, $xml)
	{
		$type = (string)$xml->attributes()->type;
		$this->select_node_type = (string)$xml->attributes()->node_type;
		$root = (string)$xml->attributes()->root;
		$rootnode = $node;
		if ($root)
		{
			// FIND NODE
			if ($root == "root")
				$rootnode = $this->graph_db->loadNode(1);
			else
				$rootnode = $this->graph_db->loadNode($this->graph_db->active_dataset->nodes->$root);
		}

		$output = array();
		if ($type == "tree") // list of children nodes in a tree
		{
			$out = array();
			$out['id'] = $rootnode->getId();
			$out['title'] = $rootnode->getTitleDisp();
			$output[] = $out;

			$this->GetSelectOptionsChildren($rootnode, $xml, $output, 1);
		}

		//print_p($output);
		return $output;
	}

	protected function GetSelectOptionsChildren(&$node, $xml, &$output, $depth)
	{
		// load in all children for the node
		$children = $node->getRelations($this->select_node_type, GDB_Rel::ChildOf);

		// for each child, add the item to the out array
		foreach ($children as &$child)
		{
			$out['id'] = $child->getId();
			$out['title'] = str_repeat(" - ", $depth) .  $child->getTitleDisp();
			$output[] = $out;

			$this->GetSelectOptionsChildren($child, $xml, $output, $depth+1);
		}
		// call again

		return $output;
	}

	protected function ajax_longtree_page($nodeid, $sectionid, $offset, $search = "")
	{
		//echo "ajax_longtree_page($nodeid, $sectionid, $offset, $search)<br>";
		$this->getNode($nodeid);
		$filename = "";

		$subtemplate = "";
		if ($this->subtemplate)
			$subtemplate = $this->subtemplate;

		//echo "Sub Template : {$this->subtemplate}<br>";


		$xml = $this->getXMLFile($this->data['node'], $filename, false, $subtemplate);
		$this->ParseGlobals($xml);

		$section = $this->FindXMLSection($xml, $sectionid);

		$this->outputSectionLongTree($section, $offset, true, $search);
	}

	protected function FindXMLSection($xml, $sectionid)
	{
		foreach ($xml->section as $section)
		{
			$sid = (string)$section->attributes()->id;
			//echo "Found $sid<br>";
			if ($sid == $sectionid)
				return $section;

			$stype = (string)$section->attributes()->type;
			if ($stype == "tabs")
			{
				//echo "Tabs<br>";
				foreach ($section->tab as $tab)
				{
					$result = $this->FindXMLSection($tab, $sectionid);
					if ($result)
						return $result;
				}
			}
			if ($stype == "table")
			{
				//echo "Tabs<br>";
				foreach ($section->tr as $tab)
				{
					$result = $this->FindXMLSection($tab, $sectionid);
					if ($result)
						return $result;
				}
			}
		}

		//show_error("Unable to find $sectionid (FindXMLSection)");
		return null;
	}

	function nodetree($nodeid, $sectionid, $selected = '', $template = "tree_node", $subtemplate = '')
	{
		$this->getNode($nodeid);
		$filename = "";
		$xml = $this->getXMLFile($this->data['node'], $filename, false, $subtemplate);
		$this->ParseGlobals($xml);

		$section = $this->FindXMLSection($xml, $sectionid);
		if (!$section)
			show_error("View::nodetree Unable to find section $sectionid in nodetree($nodeid, $sectionid, $selected), file : $filename");

		$layers = $this->NodeTree_LoadLayers($section);
		$selected = urldecode($selected);
		//echo "Node Tree Ajax Inner : $nodeid, $sectionid, $selected<br>";

		$selected = explode("|", $selected);

		// work out which layer we are in
		$curlayer = count($selected);
		//echo "Cur Layer : $curlayer, Layer Count : " . count($layers) . "<br>";
		$layer =& $layers[$curlayer];

		//print_p($layers);
		// load node for parent layer
		//echo "cur layer : $curlayer<br>";
		$parnode = $selected[$curlayer-1];

		// do we have multiple nodes passed?
		if (strpos($parnode,"-") > 0)
		{
			$nodeids = explode("-",$parnode);
			$basenode = $this->graph_db->loadNodes($nodeids);
		} else {
			$basenode = $this->graph_db->loadNode($parnode);
		}

		if (!$basenode)
		{
			// none found
			//$this->output->append_output("<div class='rel_title'>None found</div>");
			return;
		}

		if ($layer->skip_display)
		{
			// need to build a list of all the nodes at this layer
			//echo "Skip Layer<Br>";
			$thissel = array();

			if (is_array($basenode)) // base is array, so use a list
			{
				foreach ($basenode as &$node)
				{
					$rels = $node->getRelations($layer->path);
					foreach ($rels as &$node)
						$thissel[] = $node->getId();
				}
			} else { // single base node
				$rels = $basenode->getRelations($layer->path);
				foreach ($rels as &$node)
					$thissel[] = $node->getId();
			}

			$thissel = implode("-",$thissel);

			return $this->nodetree($nodeid, $sectionid, implode("|",$selected) . "|" . $thissel);
		}

		// load nodes for the new layer
		if (is_array($basenode))
		{
			// need to load all the related nodes for the list of base nodes
			$this->data['nodetree']['nodes'] = array();
			foreach ($basenode as &$node)
			{
				$rels = $node->getRelations($layer->path);
				foreach ($rels as &$relnode)
				{
					$this->data['nodetree']['nodes'][$relnode->getId()] = $relnode;
				}
			}
			//return;
		} else {
			$this->data['nodetree']['nodes'] = $basenode->getRelations($layer->path);
		}

		if ($layer->only_linkd_to)
		{
			// find the layer

			if ($layer->only_linkd_to == "node")
			{
				$linkednode = &$this->data['node'];
			} else {
				$foundid = -1;

				foreach ($layers as $lid => &$findlayer)
				{
					if ($findlayer->path == $layer->only_linkd_to)
					{
						$foundid = $lid;
						break;
					}
				}

				if ($foundid == -1)
				{
					show_error("Cannot find only_linkd_to layer {$layer->only_linkd_to} in section {$sectionid} ($foundid)");
				}

				//print_p($selected);
				$linkid = $selected[$foundid];
				$linkednode = $this->graph_db->loadNode($linkid);
			}
			$rels = $linkednode->getRelations($layer->path);

			$allowedids = array();

			foreach ($rels as &$rel)
				$allowedids[$rel->getId()] = 1;

			//print_p($allowedids);

			$unset = array();
			foreach ($this->data['nodetree']['nodes'] as $offset => $node)
			{
				if (!array_key_exists($node->getId(), $allowedids))
				{
					$unset[] = $offset;
				}
			}

			foreach ($unset as $offset)
				unset($this->data['nodetree']['nodes'][$offset]);
			//echo "Trimming results to those only linked to {$layer->only_linkd_to} $foundid => {$linkednode->getTitle()}<br>";
		}

		//echo "ME ME ME ME ME";
		// remove the current node from the results so not to cause confusion
		foreach ($this->data['nodetree']['nodes'] as $offset => $node)
		{
			//echo "Node : {$node->getId()} ?= $nodeid<br>";
			if ($node->getId() == $nodeid)
			{
				unset($this->data['nodetree']['nodes'][$offset]);
				break;
			}
		}

		$this->graph_db->sort_nodes($this->data['nodetree']['nodes'], $layer->sort);

		$this->data['nodetree']['url'] = '';
		$this->data['nodetree']['sectionid'] = $sectionid;
		$this->data['nodetree']['layer'] = $layer;

		if ((int)$section->attributes()->notable == 1)
			$template .= "_notable";

		// setup layers and selected for next layer
		$this->data['nodetree']['selected'] = implode("|",$selected) . "|";


		if ($curlayer == count($layers) - 1)
		{
			$this->data['nodetree']['finallayer'] = 1;
		} else {
			$this->data['nodetree']['finallayer'] = 0;
		}

		// load view
		$this->_include("_misc/$template", $this->data);
	}

	protected function outputSectionNodeAttr($section)
	{
		if ($section->attributes()->attr)
		{
			if ($section->prefix)
				$this->output->append_output((string)$section->prefix);

			$elem = "";
			if ($section->attributes()->wrapper)
			{
				$class = (string)$section->attributes()->class;
				if ($class) $class = " class='$class'";
				$elem = (string)$section->attributes()->wrapper;
				$this->output->append_output("<$elem $class>");
			}

			$attr = & $this->data['node']->getAttribute((string)$section->attributes()->attr);

			$value = $attr->format($section->attributes()->format);

			if ($value == "" && (string)$section->attributes()->no_value)
				$value = (string)$section->attributes()->no_value;

			$this->output->append_output($value);

			if ($section->attributes()->wrapper)
			{
				$this->output->append_output("</$elem>");
			}

			if ($section->postfix)
				$this->output->append_output((string)$section->postfix);

		} else {
			$this->_include('_misc/node_attr_list', $this->data);
		}
	}

	protected function outputSectionNodeTitle($xml)
	{
		if ((int)$xml->attributes()->link == 1)
		{
			$this->output->append_output("<a href='" . site_url("view/" . $this->data['node']->getID()) . "'>" . $this->data['node']->getTitleDisp() . "</a>");
		} else {
			$this->output->append_output($this->data['node']->getTitleDisp());
		}
	}

	protected function outputSectionNodeType()
	{
		$to = $this->data['node']->getTypeObj();
		$this->output->append_output($to->name);
	}

	protected function outputSectionNodeDetails()
	{
		$this->_include('_misc/node_details', $this->data);
	}

	function ajax($type, $param1 = "", $param2 = "", $param3 = "", $param4 = "")
	{
		$function = "ajax_$type";
		if (method_exists($this,$function))
		{

			$debug = array_key_exists('debug', $_GET);
			$data =& $this->$function($param1, $param2, $param3, $param4);
			if ($debug == 1)
			{
				$this->load->view('templates/header', $data);
				if ($data)
					$this->_include("ajax/$type", $data);
				$this->load->view('templates/footer');
			} else {
				if ($data)
					$this->_include("ajax/$type", $data);
			}
		} else {
			echo json_encode(array('error' => "Invalid AJAX request, 'Node::ajax_$type()' does not exist"));
		}
	}

	protected function ajax_related($baseid, $nodeid)
	{
		$node = $this->graph_db->loadNode($nodeid);
		$data['subdata']['rels'] = $node->getRelations_GroupByNodeType();
		$data['subdata']['node'] =	$this->graph_db->loadNode($baseid);

		// remove the current node from the results so not to cause confusion
		foreach ($data['subdata']['rels'] as $offset => &$nodes)
		{
			foreach ($nodes as $offset2 => $node)
			{
				//echo "Node : {$node->getId()} ?= $nodeid<br>";
				if ($node->getId() == $baseid)
				{
					unset($nodes[$offset2]);
					if (count($nodes) == 0)
						unset($data['subdata']['rels'][$offset]);
					break;
				}
			}
		}

		$data['subdata']['baseid'] = $baseid;
		return $data;
	}

	protected function outputSectionHeader($xml)
	{
		$this->data['header'] = (string)$xml->attributes()->title;
		$this->_include('_misc/header', $this->data);
	}

	protected function outputSectionHtml($xml)
	{
		$html = (string)$xml;
		$html = str_replace("%BASE%",asset_url(),$html);
		$html = str_replace("%SITE%",site_url(''),$html);
		$this->output->append_output((string)$html);
	}

	protected function outputSectionNodeTreeRev($section)
	{
		// need to display a tree list of the layers specified
		$layers = $this->NodeTree_LoadLayers($section);

		// for each layer defined, find the items, linked to the parent layer

		// then display the tree in reverse

		// i think for coding sanity, this will have to be done in one go instead of
		// usign ajax, as it will make the code horrid otherwise

		$basenode =& $this->data['node'];

		// setup a fake layer for the base node
		$layernodes = array();
		$layernodes[-1][$basenode->getId()] = $basenode;

		// for all the defined layers
		foreach ($layers as $id => &$layer)
		{
			// store the layer offset (makes determining where in the layers we are later on easier
			$layer->offset = $id;

			// for each node in the parent layer
			if (array_key_exists($id-1, $layernodes) && is_array($layernodes[$id-1]))
			{
				foreach ($layernodes[$id-1] as &$basenode)
				{
					// save the layer for later use
					$basenode->layer = $layer;

					// get the related nodes based on the layer type
					$rels = $basenode->getRelations($layer->path);

					// for each related node
					foreach ($rels as &$rel)
					{
						// store the node in the parent nodes child list
						$basenode->children[] = $rel;

						// stick the node out list
						$layernodes[$id][$rel->getId()] = $rel;

						// store the parent node in the node we just got
						$rel->parents[] = $basenode;
					}
				}
			}
		}

		// got the node tree with all nodes linked to each other, so now we need to output em from last layer
		// descending upwards to first layer
		$this->data['nodelist'] = &$layernodes[count($layers) - 1];
		$this->data['layer'] = &$layers[count($layers) - 1];
		$this->data['curlayer'] = count($layers) - 1;
		$this->data['me'] = &$this;
		$this->data['layers'] =& $layers;

		$this->_include('_misc/treerev_main', $this->data);
	}


	protected function outputSectionRedirect($xml)
	{
		$nodetype = (string)$xml->attributes()->node;
		$parent = (string)$xml->attributes()->parent;

		$basenode =& $this->data['node'];

		if ($parent)
		{
			$parent = $basenode->getParent();
			redirect('view/' . $parent->getId());
			exit;
		}
		$rels = $basenode->getRelations($nodetype);
		if (count($rels) > 0)
		{
			redirect('view/' . $rels[0]->getId());
			exit;
		}
		$this->output->append_output("<h3>Unable to redirect to node of type '$nodetype', no target node found</h3>");
	}

	protected function outputSectionKVRedirect($xml)
	{
		$basenode =& $this->data['node'];
		$parent = $basenode->getParent();
		redirect('view/kv/' . $parent->getId());
	}

	protected function outputSectionTools($section)
	{
		$class = (string)$section->attributes()->class;
		if (!$class) $class = "small_tools";

		$this->output->append_output("<div class='$class'>");

		$output = ViewTools::ToolsFromXML($section, $this->data['node']);
		$this->output->append_output($output);

		$this->output->append_output("</div>");
	}

	protected function outputSectionClear($section)
	{
		$this->output->append_output("<div class='clear'></div>");
	}

	function _include($file, &$data, $source = '')
	{
		if ($source == '')
			$source = $this->vn;

		$filename = "application/views/$source/$file.php";
		if (!file_exists($filename))
		{
			// try source of view
			if ($source != "view")
			{
				$source = "view";
				$filename = "application/views/$source/$file.php";
				if (!file_exists($filename))
				{
					show_error("Cant find $file<br>");
				}
			} else {
				show_error("Cant find $file<br>");
			}
		}
		//echo "Using $source/$file<br>";

		$this->load->view("$source/$file", $data);
	}


	function &FindNodesOnPaths(&$basenode, $paths)
	{
		$results = array();

		$start = microtime(true);
		foreach ($paths as $path)
		{
			//echo "<br><h3>Doing path : $path</h3>";
			$parts = explode("/",$path);

			$nodes = array();
			$nodes[] = $basenode;

			foreach ($parts as $nodetype)
			{
				$newnodes = array();
				//echo "<h4>Path Part : $nodetype</h4>";
				foreach ($nodes as &$node)
				{
					//echo "Checking Node {$node->getID()} => {$node->GetTitle()} ({$node->getType()})<br>";
					$rels = $node->getRelations($nodetype);
					foreach ($rels as &$relnode)
					{
						$newnodes[$relnode->getID()] = $relnode;
						//echo "Found Node : {$relnode->getID()} => {$relnode->getTitle()} ({$relnode->getType()})<br>";
					}
				}

				$nodes = $newnodes;
			}

			//echo "<h4>Found Nodes:</h4>";
			foreach ($nodes as &$node)
			{
				//echo "Chosen Node : {$node->getID()} => {$node->getTitle()} ({$node->getType()})<br>";
				$results[$node->getID()] =& $node;
			}
		}

		$end = microtime(true);

		$diff = round(($end-$start) * 1000,2);
		//echo "Time Taken : $diff ms<br>";
		//exit;

		return $results;
	}
}
