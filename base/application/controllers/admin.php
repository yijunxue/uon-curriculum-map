<?php

/**
 * admin main menu, just show a list of datasets and the brief stats for them
 * also allows changing of current dataset
 */
class Admin extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		
		$this->load->library('Form_validation');
		$this->load->library('tabs');
		//$this->load->library('DX_Auth');			
		
		$this->load->helper('url');
		$this->load->helper('form');

    $this->session->set_userdata('redirect', uri_string());
		$this->dx_auth->check_uri_permissions();

		setup_main_menu($this->menu);	
		setup_admin_menu($this->menu);	
	}
	
	/**
	 * PAGE: Main Admin Page
	 * 
	 * Just shows the admin menu, and a list of datasets at the moment
	 */	
	function index()
	{
		$this->data['title'] = "Admin Main Menu";
		// This page should use the admin database user.
		$this->load->database('admin', false, true);
		
		$this->data['stats'] = $this->graph_db->getStats();

		$this->load->view('templates/header', $this->data);
		$this->load->view('admin/index', $this->data);
		$this->load->view('templates/footer');
	}

}
?>