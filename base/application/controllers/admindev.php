<?php

/**
 * Admin development tools controller.
 *
 * Allows setting of debug information to be displayed
 *
 * Also can turn off the usage of templates, so the default template will always be used
 *
 * There is also template testing functionality here
 */

class AdminDev extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->library('Form_validation');
		$this->load->library('tabs');

		$this->load->helper('url');
		$this->load->helper('form');

		$this->session->set_userdata('redirect', uri_string());
		$this->dx_auth->check_uri_permissions();

		setup_main_menu($this->menu);
		setup_admin_menu($this->menu);
	}

	/**
	 * PAGE: Main Developer Menu
	 **/
	public function index()
	{
		$this->data['title'] = "Admin Development Tools Menu";

		$this->head->AddJS("js/admin/dev.js");

		$this->load->view('templates/header', $this->data);
		$this->load->view('admin/dev/index', $this->data);
		$this->load->view('templates/footer');
	}
}