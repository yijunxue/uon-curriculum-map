<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class FixClonedWeight extends CI_Controller {

  function __construct()
  {
    parent::__construct();

    $this->dx_auth->check_uri_permissions();
  }

	public function fix($source_dataset)
	{
		$this->source_dataset = $source_dataset;
		$this->target_dataset = $this->graph_db->active_dataset->table;

    if ($this->source_dataset == $this->target_dataset)
    {
      $this->graph_source =& $this->graph_db;
    } else {
      $this->graph_source = new Graph_DB();
      $this->graph_source->setDataset($this->source_dataset, false);
    }

    $source_gdb = $this->graph_source->gdb;
    $target_gdb = $this->graph_db->gdb;

		$sql = "SELECT * FROM !!_graph_rel WHERE weight < 1 ORDER BY source_id, dest_id";
		$query = $source_gdb->query($sql);
		foreach($query->result() as $row)
		{
      // Get the nodes at each end of the link
			$source_s_node = $this->graph_source->loadNode($row->source_id);
			$source_d_node = $this->graph_source->loadNode($row->dest_id);

      if ($source_s_node and $source_d_node)
      {
        // Get the GUID of the nodes
        $s_guid = $source_s_node->getGuid();
        $d_guid = $source_d_node->getGuid();

        if ($s_guid and $d_guid)
        {
          // See if nodes exist in target dataset with those GUIDs
          $target_s_node = $this->graph_db->loadNodeByGuid($s_guid);
          $target_d_node = $this->graph_db->loadNodeByGuid($d_guid);

          if ($target_s_node and $target_d_node)
          {
            $target_s_id = $target_s_node->getID();
            $target_d_id = $target_d_node->getID();

            // Update in target dataset with existing weight
            $sql = "UPDATE !!_graph_rel SET weight = ? WHERE source_id = ? AND dest_id = ?";
            $target_gdb->query($sql, array($row->weight, $target_s_id, $target_d_id));
          }
        }
      }
		}
 	}
}
