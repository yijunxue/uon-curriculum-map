<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

/**
 * Clone a node view.
 *
 * Requires some tidying
 */
class CloneNode extends CI_Controller {
    public function __construct() {
        // call parent constructor
        parent::__construct();

        // setup the main menu items for the page (same for all controllers)
        setup_main_menu($this->menu);
    }

    public function index($nodeid = '') {
        // show display the users initial node here
        $this->load->helper('form');
        $this->load->library('form_validation');

        // if we have a page passed
        $this->data['title'] = "Clone Node"; // Capitalize the first letter
        $this->data['node'] = $this->graph_db->loadNode($nodeid);
        $this->data['type_obj'] = $this->data['node']->getTypeObj();

        $datasets_select = array();
        foreach ($this->graph_db->datasets as $setid => &$dataset) {
            $datasets_select[$setid] = $dataset->name;
        }
        $this->data['datasets_select'] = & $datasets_select;
        $this->data['datasets_current'] = $this->graph_db->active_dataset->table;

        $this->head->AddJS("js/jquery/jquery-ui-1.8.19.custom.min.js");
        $this->head->AddJS("js/node/clone.js");

        $hidden = array('nodeid' => $nodeid);
        $this->head->AddLeading(form_open("clonenode/process", array('id' => 'clone_form'), $hidden));

        //add form close to bottom of popup
        $this->head->AddTrailing(form_close());

        //$this->head->AddFooter("<div style='float:left'><span class='req_field'>Please Note: This feature is currently unfinished. It is currently disabled.</span></div>");
        // add save button to footer
        $this->head->AddFooter("<button id=\"edit_save\" class=\"footer_button\">Clone</button>");

        // add cancel button to footer
        $this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.location=\"" . site_url("view/" . $nodeid) . "\";return false;'>Cancel</button>");

        $this->data['relations'] = $this->data['node']->getRelations_GroupByNodeType_GroupByRelType();
        $this->data['relcounts'] = $this->graph_db->GetRelCounts($this->data['node']->getRelationIDs(GDB_Rel::ChildOf), GDB_Rel::ChildOf);

        $this->data['parentnode'] = $this->data['node']->getParent();

        $this->load->view('templates/header', $this->data);
        $this->load->view('clone/index', $this->data);
        $this->load->view('templates/footer', $this->data);
    }

    function relnodes($nodeid) {
        $this->data['node'] = $this->graph_db->loadNode($nodeid);
        $this->data['relations'] = $this->data['node']->getRelations_GroupByNodeType_GroupByRelType();
        $this->data['relcounts'] = $this->graph_db->GetRelCounts($this->data['node']->getRelationIDs(GDB_Rel::ChildOf), GDB_Rel::ChildOf);

        $this->load->view('clone/relations', $this->data);
    }

    public function process()
    {
        set_time_limit(0);

        $this->target_dataset = $this->input->post('dataset');
        $this->current_dataset = $this->graph_db->active_dataset->table;
        $this->current_dataset_id = $this->graph_db->active_dataset->id;

        if ($this->current_dataset == $this->target_dataset) {
            $this->graph_target = &$this->graph_db;
        } else {
            $this->graph_target = new Graph_DB();
            $this->graph_target->setDataset($this->target_dataset, false);
        }

        $nodeid = $this->input->post('nodeid');

        $this->node_exclude = json_decode($this->input->post('node_exclude'));
        $this->title_edits = json_decode($this->input->post('title_edits'));

        // check for new title on the base node
        $node = $this->graph_db->loadNode($nodeid);

        if ($node->getTitle() != $this->input->post('title'))
            $this->title_edits->$nodeid = $this->input->post('title');

        //storage for relations within the colned data so can be applied later on
        $this->node_rels = array();

        $parentid = $this->input->post('parentid');

        // Clone node relations
        $this->_todo_rels = array();
        $this->_id_map = array();

        // call clonenode on main node, this will copy all the children nodes
        $clone_parent = ($this->input->post('skip_parent') == false);
        $this->clonenode($nodeid, $parentid, $clone_parent);

        if (isset($this->new_old)) {
            $rels_added = 0;
            // Clone node relations
            $link_outside = ($this->input->post('link_outside') == true);

            foreach ($this->_todo_rels as $relation) {
                if ($rels_added > 0 and $rels_added % 300 == 0) {
                    $this->flushcaches();
                }

                $old_node = $this->graph_db->loadNode($relation);
                $rels = $old_node->getRelations_OfType(GDB_Rel::Relation);

                $node = $this->graph_target->loadNode($this->_id_map[$relation]);
                foreach ($rels as $targetrel) {
                    $targetrelid = $targetrel->getId();
                    $targetrelguid = $targetrel->getGuid();
                    $targetrelweight = $targetrel->getWeightTo($old_node);

                    if (array_key_exists($targetrelid, $this->_id_map)) { // lookup new id of the rels target node
                        $targetrelid = $this->_id_map[$targetrelid];
                    } elseif ($link_outside and $this->graph_target->doesGuidExist($targetrelguid)) {
                        $target_node = $this->graph_target->loadNodeByGuid($targetrelguid);
                        if (is_null($target_node) || $target_node === false) {
                            continue;
                        }
                        $targetrelid = $target_node->getid();
                    } else {
                        // destination node of the relationship is outside the copied node set and doesn't exist in
                        // target dataset so skip it
                        continue;
                    }

                    $node->addRelate($targetrelid, GDB_Rel::Relation, $targetrelweight);
                }

                $node->save();
                $rels_added++;
            }

            if ($clone_parent) {
                // lookup new node id from the old one
                $newid = $this->new_old[$nodeid];
                $newnode = $this->graph_target->loadNode($newid);

                $this->head->AddMessage("Item has been cloned. New Item : " . anchor(root_url() . $this->target_dataset . "/view/" . $newid, $newnode->getTitleDisp()));
            } else {
                $parentnode = $this->graph_target->loadNode($parentid);
                $this->head->AddMessage("Child items have been cloned. New Parent : " . anchor(root_url() . $this->target_dataset . "/view/" . $parentid, $parentnode->getTitleDisp()));
            }
        } else {
            $this->head->AddMessage('No valid nodes found to clone');
        }

        $this->head->AddMessage('Memory: ' . memory_get_peak_usage(true));
        redirect("view/$nodeid");

    }


    /**
     * Clones a node and all it's children.
     * 
     * @param int $nodeid The id of the node to be cloned.
     * @param int $parentid The id of the parent for the new node.
     * @param boolean $cloneme Should this node be cloned, or just it's children?
     * @return void
     */
    private function clonenode($nodeid, $parentid, $cloneme = true) {
        $this->load->helper('clone');
        if (!empty($this->node_exclude->$nodeid) && $this->node_exclude->$nodeid == 2) {
            //echo "Skipping $nodeid<br>";
            return;
        }

        $node = $this->graph_db->loadNode($nodeid);

        if ($node->deleted) {
            // We do not wish to clone deleted nodes.
            return;
        }

        $new_node_id = 0;

        // copy node
        // create new node with appropriate title and GUID
        $title = $node->getTitle();
        $guid = ($this->current_dataset == $this->target_dataset) ? false : $node->getGuid();

        if (!empty($this->title_edits->$nodeid)) {
            $title = $this->title_edits->$nodeid;
        } else if ($node->getType() != 100 && $this->input->post('rename') == 1) { //
            $title = "Copy of " . $title;
        }

        if ($cloneme) {
            // Check if the GUID already exists (we're trying to clone something into the target dataset twice)
            if ($guid !== false and $this->graph_target->doesGuidExist($guid)) {
                $this->head->AddMessage('Skipping node - already exists in target dataset: ' . $node->getTitle());
                return;
            }

            $newnode = $this->graph_target->createNode($title, $node->getType(), $parentid, $guid);
            $newnode->save();

            // add history item to the source node
            $node->_addHistory(GDB_Hist_Action::Node_CloneTo, $newnode->GetID(), $this->target_dataset);
            $node->save();

            // copy all attributes
            $attrs = $node->getAttributes();
            foreach ($attrs as $attr) {
                $newnode->setAttribute($attr->GetName(), $attr->GetValue(), $attr->GetType());
            }
            $newnode->save();

            // clear permissions and copy the original nodes permissions, fixing any target node references
            // 1. Users.
            $source_user_nodes = get_user_nodes($this->current_dataset_id, $this->db);
            $targetid = get_id_from_table($this->target_dataset, $this->db);
            foreach ($source_user_nodes as $user_node) {
                $id = get_dataset_user_node($targetid, $user_node['userid'], $this->db);
                $newnode->replace_and_update_perms($node->perms, $user_node['nodeid'], $id);
            }
            // 2. Groups.
            $source_group_nodes = get_role_group_nodes($this->current_dataset, $this->db);

            foreach ($source_group_nodes as $group_node) {
                $loadnode = $this->graph_db->loadNode($group_node['id']);
                $nodeguid = $loadnode->getGuid();
                if ($this->graph_target->doesGuidExist($nodeguid)) {
                    $target_group_node = $this->graph_target->loadNodeByGuid($nodeguid);
                    if (is_null($target_group_node) || $target_group_node === false) {
                        continue;
                    }
                    $newnode->replace_and_update_perms($newnode->perms, $group_node['id'], $target_group_node->getId());
                }
            }
            // 3. Programmes.
            $source_programme_nodes = get_programme_nodes($this->current_dataset, $this->db);
            foreach ($source_programme_nodes as $programme_node) {
                $loadnode = $this->graph_db->loadNode($programme_node['id']);
                $nodeguid = $loadnode->getGuid();
                if ($this->graph_target->doesGuidExist($nodeguid)) {
                    $target_programme_node = $this->graph_target->loadNodeByGuid($nodeguid, false);
                    if (is_null($target_programme_node) || $target_programme_node === false) {
                        continue;
                    }
                    $newnode->replace_and_update_perms($newnode->perms, $programme_node['id'], $target_programme_node->getId());
                }
            }
            // store mapping in $this->new_old[$oldid] = $newid;
            $this->new_old[$nodeid] = $newnode->getid(); //$new_node_id;

            $new_parent_id = $newnode->getid();
        } else {
            $new_parent_id = $parentid;
        }

        if (!empty($this->node_exclude->$nodeid) && $this->node_exclude->$nodeid == 1) {
            //echo "Skipping children of $nodeid<br>";
            return;
        }

        $rels = $node->getRelations_OfType(GDB_Rel::ChildOf);
        foreach ($rels as &$rel) {
            $this->clonenode($rel->getId(), $new_parent_id);
        }

        if ($cloneme) {
            if (count($this->_todo_rels) > 0 and count($this->_todo_rels) % 300 == 0) {
                $this->flushcaches();
            }

            // clear the nodes history, as the clone process creates loads of random stuff there
            $newnode->_clearHistory();

            // add history item to the dest node
            $newnode->_addHistory(GDB_Hist_Action::Node_CloneFrom, $node->GetID(), $this->current_dataset);
            $newnode->save();

            //Clone node relations
            // need to store the nodes that have been copied
            $before = memory_get_usage();

            $this->_todo_rels[] = $node->getid();

            $this->_id_map[$node->getid()] = $newnode->getId(); // map of old to new ids
        }
    }

    /**
     * Flush the node cache for the source and target graph databases
     */
    private function flushcaches() {
        $this->graph_db->flushLoadedNodes();
        $this->graph_target->flushLoadedNodes();

        if (function_exists('gc_collect_cycles'))
            gc_collect_cycles();
    }
}
