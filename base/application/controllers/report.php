<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

class Report extends CI_Controller {

	public function __construct()
	{
		// call parent constructor
		parent::__construct();
		$this->CI =& get_instance();

		// need to check an api key, and change the auth temporarily based on this
		$this->dx_auth->APILogin();

		// setup the main menu items for the page (same for all controllers)
		setup_main_menu($this->menu);
	}

	public function index($nodeid = '', $report = '')
	{
    $this->do_full($nodeid, $report);
	}

	public function raw($nodeid = '', $report = '')
	{
		$this->node = $this->graph_db->loadNode($nodeid);

		// load in the report xml file.
		$this->report = $report;
		$this->filename = "";

		$this->xslt = null;

		if ($report == "custom" || $report == "")
		{
			// we have a custom report specified, try to get the xml and xstl data from the post
			$xml = null;

			// xml file post
			if (array_key_exists("xml_file",$_FILES) && $_FILES['xml_file']['tmp_name'] != "")
			{
				$xml = @simplexml_load_file($_FILES['xml_file']['tmp_name']);
				if (!$xml)
					$this->show_error("Invalid XML file posted");
			}

			// xml text post
			if (!$xml && array_key_exists("xml",$_POST) && $_POST['xml'] != "")
			{
				$xml = @simplexml_load_string($_POST['xml']);
				if (!$xml)
					$this->show_error("Invalid XML text posted");
			}

			// no xml, show error
			if (!$xml)
			{
				$this->show_error("No XML file or text posted");
			}

			// uploaded xml types are always XSLT
			$type = "xsl";

			// try to get xstl from posted file
			$this->xslt = null;
			if (array_key_exists("xslt_file",$_FILES) && $_FILES['xslt_file']['tmp_name'] != "")
			{
				$this->xslt = @simplexml_load_file($_FILES['xslt_file']['tmp_name']);
				if (!$this->xslt)
					$this->show_error("Invalid XSLT file posted");
			}

			// try to get xslt from posted text
			if (!$this->xslt && array_key_exists("xslt",$_POST) && $_POST['xslt'] != "")
			{
				$this->xslt = @simplexml_load_string($_POST['xslt']);
				if (!$this->xslt)
					$this->show_error("Invalid XSLT text posted");
			}

			// no xslt show error
			if (!$this->xslt)
			{
				$this->show_error("No XSLT file or text posted");
			}
		} else {
			// load the report xml file as normal
			$xml = $this->getXMLFile($this->node, $this->filename, $report);
			$type = (string)$xml->type;

			// xslt file will be loaded later (base on {$report}.xslt in same folder)
		}

		// clear posted data so not to confuse the api code later on
		unset($_POST);
		unset($_FILES);

		// get API data
		$api_result = null;

		// we have an api specified, so need to load the required data
		if ($xml->api)
		{
			require_once("application/controllers/api.php");
			$api = new API();

			if ((string)$xml->api->attributes()->custom == "1")
			{
				// have custom="1" specified on the report api tag, so use the contents of te tag as the api xml
				$api->report_xml = $xml->api;
				$api_result = $api->GetXML($nodeid, "report");
			} else {
				// normal api request, will load api/nodetype/{$api}.xml
				$api_result = $api->GetXML($nodeid, (string)$xml->api);
			}
		}

		// call sub section for required processing
		$func = "RunType_$type";
		if (method_exists($this,$func))
		{
			$result = $this->$func($xml, $api_result);
		} else {
			show_error("Unable to find Report::$func");
		}

		$this->output->append_output($result);
	}

	public function csv($nodeid = '', $report = '')
	{
		// TODO: If can't serve CSV, revert to raw?

		$this->node = $this->graph_db->loadNode($nodeid);

		// load in the report xml file.
		$this->report = $report;
		$this->filename = "";

		$this->xslt = null;

		// load the report xml file as normal
		$xml = $this->getXMLFile($this->node, $this->filename, $report);
		$type = (string)$xml->type;
		$csv_type = (string)$xml->csv_type;
		if ($csv_type == '' or $csv_type != 'csv') {
      $this->xml_file = null;
			$this->do_full($nodeid, $report);
		} else {
      // clear posted data so not to confuse the api code later on
      unset($_POST);
      unset($_FILES);

      // call sub section for required processing
      $func = "RunType_$type";
      if (method_exists($this,$func))
      {
        $result = $this->$func($xml, null);
      } else {
        show_error("Unable to find Report::$func");
      }

      $this->load->helper('download');

      force_download('report.csv', $result);
      // $this->output->set_content_type('text/csv');
      // $this->output->append_output($result);
    }
	}

	protected function RunType_xsl($xml, $result)
	{
		// need to load in the xsl file

		// no XSLT loaded already, try to load with extension changed
		if (!$this->xslt)
		{
			$xslfile = str_ireplace(".xml",".xslt", $this->filename);
			//echo "XSL : $xslfile<br>";
			if (!file_exists($xslfile))
				return show_error("Unable to open file $xslfile");

			$this->xslt = simplexml_load_file($xslfile);
		}

		// create processor and load xslt file
		$proc = new XSLTProcessor();
		$proc->importStyleSheet($this->xslt);

		// return transformed XML
		return $proc->transformToXML($result);
	}

	protected function RunType_php($xml, $result)
	{
		// need to load in the xsl file

		// no XSLT loaded already, try to load with extension changed

		$phpfile = str_ireplace(".xml",".php",$this->filename);
		if (!file_exists($phpfile))
		{
			$this->show_error("Cannot find $phpfile");
		}

		require_once ($phpfile);

		$classname = "Report_" . $this->report;

		if (!class_exists($classname))
			$this->show_error("Unable to find class $classname");

		$repclass = new $classname();
		$repclass->data_xml = $result;
		$repclass->report_xml = $xml;
		$repclass->node = &$this->node;

		ob_start(null, 100 * 1024 * 1024);
		$repclass->Process();

		$result = ob_get_clean();

		return $result;
	}

	function show_error($error)
	{
		echo "<error>".$error."</error>";
		exit;
	}

	protected function getXMLFile(&$node, &$filename, $template = '')
	{
		if (empty($this->xml_file))
		{
			// if no path specified, then we build from current role, and the current node type
			$nodetype = $node->GetTypeId();

			$basedir = $this->CI->node_def;
			$basepath = "nodedef/$basedir/reports";

			$try = array();
			$try[] = "$basepath/$nodetype/$template.xml";

			foreach ($try as $xmlfile)
			{
				if (file_exists($xmlfile))
				{
					$this->xml_file = simplexml_load_file($xmlfile);

					if ($this->xml_file->altfile)
					{
						$alt = (string)$this->xml_file->altfile;
						unset ($this->xml_file);
						//echo "Getting ALT $alt<br>";
						return $this->getXMLFile($node, $filename, $alt);
					}

					$filename = $xmlfile;
					return $this->xml_file;
				}
			}
			// no folder for the role, set to default role
			$this->show_error("Report::getXMLFile Cant find template in<br><ul><li>" . implode("</li><li>",$try) ."</li></ul>");
		}

		return $this->xml_file;
	}

  private function do_full($nodeid = '', $report = '') {
    // output a page surrounding the report content
    $data['node'] = $this->graph_db->loadNode($nodeid);

    if (!$data['node'])
    {
    show_error("report/index Unable to find node $nodeid");
    }

    $data['title'] = $data['node']->getTitleDisp();

    // add cancel button to footer
    //$this->setup_sub_menu($data['node']->GetID());
    $this->load->view('templates/header', $data);

    // output report here
    $this->raw($nodeid, $report);

    $this->load->view('templates/footer');
  }
}

class ReportBase
{
	var $data_xml;
	var $report_xml;
	var $node;

	function Process()
	{

	}

}