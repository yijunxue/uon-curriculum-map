<?php

class AdminUsers extends CI_Controller
{
	function __construct()
	{
		parent::__construct();

		$this->load->library('Table');
		$this->load->library('Pagination');
		$this->load->library('DX_Auth');

		$this->load->helper('form');
		$this->load->helper('url');

		setup_main_menu($this->menu);
		setup_admin_menu($this->menu);
		$this->setup_sub_menu();

		// Protect entire controller so only admin,
		// and users that have granted role in permissions table can access it.
		$this->session->set_userdata('redirect', uri_string());
		$this->dx_auth->check_uri_permissions();
	}

	private function setup_sub_menu()
	{
		//$this->menu->AddNodeMenuItem("Backend:");
		$this->menu->AddNodeMenuItem("Users",'adminusers/users');
		$this->menu->AddNodeMenuItem("Create User",'auth/create');
		$this->menu->AddNodeMenuItem("Unactivated Users",'adminusers/unactivated_users');
		//$this->menu->AddNodeMenuItem("Roles",'adminusers/roles');
		//$this->menu->AddNodeMenuItem("URI Permissions",'adminusers/uri_permissions');
		//$this->menu->AddNodeMenuItem("Custom Permissions",'adminusers/custom_permissions');
	}

	function index()
	{
		$this->users();
	}

	function users()
	{
		$this->load->model('dx_auth/users', 'users');

		// Search checkbox in post array
		foreach ($_POST as $key => $value)
		{
			// If checkbox found
			if (substr($key, 0, 9) == 'checkbox_')
			{
				// If ban button pressed
				if (isset($_POST['ban']))
				{
					// Ban user based on checkbox value (id)
					$this->users->ban_user($value);
				}
				// If unban button pressed
				else if (isset($_POST['unban']))
				{
					// Unban user
					$this->users->unban_user($value);
				}
				else if (isset($_POST['change_role']))
				{
					// Change user role
					$role_data = $this->dx_auth->_get_role_data($_POST['role']);

					$this->users->change_user_role($role_data, $value, $_POST['role']);
				}
				else if (isset($_POST['reset_pass']))
				{
					// Set default message
					$data['reset_message'] = 'Reset password failed';

					// Get user and check if User ID exist
					if ($query = $this->users->get_user_by_id($value) AND $query->num_rows() == 1)
					{
						// Get user record
						$user = $query->row();

						// Create new key, password and send email to user
						if ($this->dx_auth->forgot_password($user->username, true))
						{
							// Query once again, because the database is updated after calling forgot_password.
							$query = $this->users->get_user_by_id($value);
							// Get user record
							$user = $query->row();

							// Reset the password
							if ($this->dx_auth->reset_password($user->username, $user->newpass_key))
							{
								$data['reset_message'] = 'Reset password success';
							}
						}
					}
				}
			}
		}

		$this->load->helper('pagination');

		// get role list

		$CI =& get_instance();
		$qry = "SELECT * FROM roles WHERE id != 0 ORDER BY id DESC";
		$result = $CI->db->query($qry);

		$data['roles'] = array();
		$data['rolesany'] = array();
		$data['rolesany']['any'] = " - Any Role - ";

		foreach ($result->result() as $row)
		{
			$data['roles'][$row->id] = $row->description;
			$data['rolesany'][$row->id] = $row->description;
			$data['role_lookup'][$row->name] = $row->description;

		}

		/* Showing page to user */

		// Get offset and limit for page viewing
		//$offset = (int) $this->uri->segment(3);

		// Number of record showing per page
		$row_count = 15;

		$cur_page = 1;

		if (array_key_exists('page_no',$_POST))
			$cur_page = $_POST['page_no'];

		$offset = $row_count * max(0,($cur_page - 1));

		// Get all users
		$data['users'] = $this->users->get_all($offset, $row_count)->result();

		$p = new Pagination();
		$p->template = "<a href='#' onclick=\"setOffset(%%PAGE%%);return false;\">%%NAME%%</a>";
		$p->prevnexttmpl = $p->template;
		$data['pages'] = $p->OutputPages($this->users->get_all()->num_rows(), $cur_page, $row_count);

		// Load view
		$data['title'] = "Users";
		$this->load->view('templates/header', $data);
		$this->load->view('admin/users/users', $data);
		$this->load->view('templates/footer');
	}

	function unactivated_users()
	{
		$this->load->model('dx_auth/user_temp', 'user_temp', array('target' => "_blank"));

		/* Database related */

		// If activate button pressed
		if ($this->input->post('activate'))
		{
			// Search checkbox in post array
			foreach ($_POST as $key => $value)
			{
				// If checkbox found
				if (substr($key, 0, 9) == 'checkbox_')
				{
					// Check if user exist, $value is username
					if ($query = $this->user_temp->get_login($value) AND $query->num_rows() == 1)
					{
						// Activate user
						$this->dx_auth->activate($value, $query->row()->activation_key);
					}
				}
			}
		}

		/* Showing page to user */

		// Get offset and limit for page viewing
		$offset = (int) $this->uri->segment(3);
		// Number of record showing per page
		$row_count = 10;

		// Get all unactivated users
		$data['users'] = $this->user_temp->get_all($offset, $row_count)->result();

		// Pagination config
		$p_config['base_url'] = '/backend/unactivated_users/';
		$p_config['uri_segment'] = 3;
		$p_config['num_links'] = 2;
		$p_config['total_rows'] = $this->user_temp->get_all()->num_rows();
		$p_config['per_page'] = $row_count;

		// Init pagination
		$this->pagination->initialize($p_config);
		// Create pagination links
		$data['pagination'] = $this->pagination->create_links();

		// Load view
		$data['title'] = "Unactivated Users";
		$this->load->view('templates/header', $data);
		$this->load->view('admin/users/unactivated_users', $data);
		$this->load->view('templates/footer');
	}

	function create_user_nodes() {
		// If import button pressed
		if ($this->input->post('create') and $_POST['usernames'] != '')
		{
			$CI =& get_instance();

			$this->load->model('dx_auth/users', 'users');

			$usernames = explode("\n", $_POST['usernames']);

			$count = 0;

			foreach ($usernames as $username)
			{
				if ($query = $this->users->get_user_by_username($username) AND $query->num_rows() == 1) {
					$user_id = $query->row()->id;
					$role = $query->row()->role_id;

					$datasetid = $this->graph_db->active_dataset->id;

			    $qry = "SELECT nodeid FROM user_node WHERE userid = ? AND dataset = ?";
					$node_query = $CI->db->query($qry, array($user_id, $datasetid));
					foreach ($node_query->result() as $row)
					{
						$node = $this->graph_db->loadNode($row->nodeid);
						if ($node)
						{
							continue 2;
						}
						// if we cant load the current node, make a new one! (this should only happen if the node is deleted, or permission broken somehow
					}

					// Create the user's node
					$usernode = $this->users->create_user_node($username, $this->graph_db->active_dataset->nodes->all_users, $user_id, $datasetid, false);
					$usernode->save();
					$this->users->set_base_user_perms($usernode);

					// Change user role
					$role_data = $this->dx_auth->_get_role_data($role);
					$this->users->change_user_role($role_data, $user_id, $role);

					$count++;
				}
			}

			$this->head->AddMessage($count . " user nodes created");
		}

		$data['title'] = "Create User Nodes";
		$this->load->view('templates/header', $data);
		$this->load->view('admin/users/create_user_nodes', $data);
		$this->load->view('templates/footer');
	}

	function roles()
	{
		$this->load->model('dx_auth/roles', 'roles');

		/* Database related */

		// If Add role button pressed
		if ($this->input->post('add'))
		{
			// Create role
			$this->roles->create_role($this->input->post('role_name'), $this->input->post('role_parent'));
		}
		else if ($this->input->post('delete'))
		{
			// Loop trough $_POST array and delete checked checkbox
			foreach ($_POST as $key => $value)
			{
				// If checkbox found
				if (substr($key, 0, 9) == 'checkbox_')
				{
					// Delete role
					$this->roles->delete_role($value);
				}
			}
		}

		/* Showing page to user */

		// Get all roles from database
		$data['roles'] = $this->roles->get_all()->result();

		// Load view
		$data['title'] = "Role Edit";
		$this->load->view('templates/header', $data);
		$this->load->view('admin/users/roles', $data);
		$this->load->view('templates/footer');
	}

	function uri_permissions()
	{
		function trim_value(&$value)
		{
			$value = trim($value);
		}

		$this->load->model('dx_auth/roles', 'roles');
		$this->load->model('dx_auth/permissions', 'permissions');

		if ($this->input->post('save'))
		{
			// Convert back text area into array to be stored in permission data
			$allowed_uris = explode("\n", $this->input->post('allowed_uris'));

			// Remove white space if available
			array_walk($allowed_uris, 'trim_value');

			// Set URI permission data
			// IMPORTANT: uri permission data, is saved using 'uri' as key.
			// So this key name is preserved, if you want to use custom permission use other key.
			$this->permissions->set_permission_value($this->input->post('role'), 'uri', $allowed_uris);
		}

		/* Showing page to user */

		// Default role_id that will be showed
		$role_id = $this->input->post('role') ? $this->input->post('role') : 1;

		// Get all role from database
		$data['roles'] = $this->roles->get_all()->result();
		// Get allowed uri permissions
		$data['allowed_uris'] = $this->permissions->get_permission_value($role_id, 'uri');

		// Load view
		$data['title'] = "URI Permissions";
		$this->load->view('templates/header', $data);
		$this->load->view('admin/users/uri_permissions', $data);
		$this->load->view('templates/footer');
	}

	function custom_permissions()
	{
		// Load models
		$this->load->model('dx_auth/roles', 'roles');
		$this->load->model('dx_auth/permissions', 'permissions');

		/* Get post input and apply it to database */

		// If button save pressed
		if ($this->input->post('save'))
		{
			// Note: Since in this case we want to insert two key with each value at once,
			// it's not advisable using set_permission_value() function
			// If you calling that function twice that means, you will query database 4 times,
			// because set_permission_value() will access table 2 times,
			// one for get previous permission and the other one is to save it.

			// For this case (or you need to insert few key with each value at once)
			// Use the example below

			// Get role_id permission data first.
			// So the previously set permission array key won't be overwritten with new array with key $key only,
			// when calling set_permission_data later.
			$permission_data = $this->permissions->get_permission_data($this->input->post('role'));

			// Set value in permission data array
			$permission_data['edit'] = $this->input->post('edit');
			$permission_data['delete'] = $this->input->post('delete');

			// Set permission data for role_id
			$this->permissions->set_permission_data($this->input->post('role'), $permission_data);
		}

		/* Showing page to user */

		// Default role_id that will be showed
		$role_id = $this->input->post('role') ? $this->input->post('role') : 1;

		// Get all role from database
		$data['roles'] = $this->roles->get_all()->result();
		// Get edit and delete permissions
		$data['edit'] = $this->permissions->get_permission_value($role_id, 'edit');
		$data['delete'] = $this->permissions->get_permission_value($role_id, 'delete');

		// Load view
		$data['title'] = "Custom Permissions";
		$this->load->view('templates/header', $data);
		$this->load->view('admin/users/custom_permissions', $data);
		$this->load->view('templates/footer');
	}
}
?>