<?php

include "view.php";
/**
 * API view class
 *
 * Inherits lots of functionality from the node View class
 *
 *
 */
class API extends View {

	var $xml_base = "api";
	var $vn = 'api';
	var $nohead = false;

	/**
	 * PAGE: API index page, redirect to node
	 */
	public function index($nodeid = '')
	{
		// redirect to node internally
		$this->node($nodeid);
	}

	/**
	 * PAGE: Main entry point
	 *
	 * Needs a nodeid, and and optionally a API xml template to use
	 */
	public function node($nodeid = '', $template = 'node')
	{
		// we are outputting xml, so spit out a header
		header("Content-type: text/xml");

		$output = $this->get_output($nodeid, $template);
		$this->output->set_output($output);
		// create a DOMDocument, and output it
		// this allows us to take the xml, and format it nicely
		/*$foo = new DOMDocument();
		$foo->preserveWhiteSpace = false;
		$foo->formatOutput = true;
		if ($foo->loadXML($output))
		{
			$this->output->set_output($foo->saveXML());
		}*/
	}


	public function json($nodeid = '', $template = 'node')
	{
		require_once("xml2json/xml2json.php");

		// we are outputting json, so spit out a header
		header('Content-Type: application/json');

		$output = $this->get_output($nodeid, $template);
		$jsonContents = xml2json::transformXmlStringToJson($output);

		//$sxml = simplexml_load_string($output);
		$this->output->set_output($jsonContents);

	}

	function get_output($nodeid, $template)
	{
		// need to check an api key, and change the auth temporarily based on this
		$this->dx_auth->APILogin();

		// show display the users initial node here
		$this->getNode($nodeid);

		// check we can load the node
		if (!$this->data['node'])
		{
			return $this->show_error("Unable to load node");
		}

		// sort out the xml file, and load the data
		$this->BuildXML($template, $nodeid);

		// get output
		return $this->output->get_output();
	}

	/**
	 * Builds the api xml result based on the template passed
	 */
	private function BuildXML($template, $nodeid)
	{
		if ($template == "custom") // if we have a custom template specified (ie, one have been passed as part of the post, then use it
		{
			// try to get custom xml as a posted file
			$xml = null;
			if (array_key_exists("xml_file",$_FILES) && $_FILES['xml_file']['tmp_name'] != "")
			{
				$xml = @simplexml_load_file($_FILES['xml_file']['tmp_name']);
				if (!$xml)
					$this->show_error("Invalid XML file posted");
			}

			// file failed, so try as a post
			if (!$xml && array_key_exists("xml",$_POST) && $_POST['xml'] != "")
			{
				$xml = @simplexml_load_string($_POST['xml']);
				if (!$xml)
					$this->show_error("Invalid XML text posted");
			}

			if (!$xml) // both failed, show an error
			{
				$this->show_error("No XML file or text posted");
			}

		} else if ($template == "report") // if we are coming from the reporting engine, use the XML it has provided
		{
			$xml = $this->report_xml;

		} else { // load the xml template file based on what the url was

			$filename = "";
			$xml = $this->getXMLFile($this->data['node'], $filename, $template);

			$this->data['filename'] = $filename;
		}


		// xml header
		if (!$this->nohead)
		{
			$this->output->append_output('<?xml version="1.0" encoding="utf-8" ?>'."\n");
			$this->output->append_output('<cmapi>'."\n");
		}

    $type = (string)$xml->type;
    if ($type == '') {
      // output all the sections in the xml template
      foreach ($xml->section as $section)
      {
        $this->outputSection($section);
      }
    } else {
      // call sub section for required processing
      $this->node = $this->graph_db->loadNode($nodeid);

      $func = "RunType_$type";
      if (method_exists($this, $func))
      {
        $this->output->append_output($this->$func($xml, $filename, $template));
      } else {
        show_error("Unable to find Report::$func");
      }
    }

		// xml footer
		if (!$this->nohead)
		{
			$this->output->append_output('</cmapi>'."\n");
		}
	}

	/**
	 * Gets the XML based on the nodeid and template Returns a simple xml object.
	 *
	 * This is the same as calling the node function, except it parses the output into a simple xml object and returns it
	 *
	 * Needs to be public as accessed from the report class
	 */
	public function GetXML($nodeid = '', $template = 'node')
	{

		$this->getNode($nodeid);

		// store output, and clear
		$orig_output = $this->output->get_output();
		$this->output->set_output("");

		// build the api xml result
		$this->BuildXML($template, $nodeid);

		// get output
		$result = $this->output->get_output();

		// restore output
		$this->output->set_output($orig_output);

		// return simple xml of the output
		return simplexml_load_string($result);
	}


	/**
	 * Load in the API definition XML file
	 *
	 * This is different from the base View class
	 */
	protected function getXMLFile(&$node, &$filename, $template)
	{
		if (empty($this->xml_file)) // dont do this multiple times, as its called from different places
		{
			// if no path specified, then we build from current role, and the current node type
			$role = $this->dx_auth->get_current_role();
			$nodetype = $node->GetTypeId();

			$basedir = $this->CI->node_def;
			$basepath = "nodedef/$basedir/{$this->xml_base}";

			$try = array();
			$try[] = "$basepath/$nodetype/$template.xml";
			$try[] = "$basepath/default/$template.xml";

			//print_p($try);

			foreach ($try as $xmlfile)
			{
				if (file_exists($xmlfile))
				{
					$this->xml_file = simplexml_load_file($xmlfile);
					$filename = $xmlfile;
					return $this->xml_file;
				}
			}
			// no folder for the role, set to default role
			$this->show_error("API::getXMLFile Cant find template in<br><ul><li>" . implode("</li><li>",$try) ."</li></ul>");
		}

		return $this->xml_file;
	}

	/**
	 * Show error as an XML tag
	 */
	private function show_error($error)
	{
		echo "<error>".$error."</error>";
		exit;
	}


	/**
	 * Internal output functions used when processing the xml template
	 */

	/**
	 * OUTPUT: Output a node, including its attributes and other information, based on $section XML
	 */
	protected function outputSectionnode($section)
	{
		// store the node
		$secdata['node'] =& $this->data['node'];

		// dont show anything by default
		$secdata['show_attrs'] = 0;
		$secdata['show_details'] = 0;
		$secdata['show_rels'] = 0;
		$secdata['show_hist'] = 0;

		// name of the xml attribute to put the node into
		$secdata['tag'] = (string)$section->attributes()->tag;
		if (!$secdata['tag']) $secdata['tag'] = 'node';

		// if full passed, show all but history
		if ($section->full)
		{
			$secdata['show_attrs'] = 1;
			$secdata['show_details'] = 1;
			$secdata['show_rels'] = 1;
		}

		// if hist passed, show history
		if ($section->hist)
		{
			$secdata['show_hist'] = 1;
		}

		// dont close the node tag within the node/node template
		$secdata['noclose'] = 1;

		// include the template
		$this->_include("node/node", $secdata);

		// output any attributes specified in the xml tempalte
		$this->OutputAttributes($secdata['node'], $section);

		// output any sections we have
		foreach ($section->section as $subsect)
			$this->outputSection($subsect);

		// close the node tag
		$this->output->append_output("</{$secdata['tag']}>\n");
	}

	/**
	 * Output the nodes attributes
	 */
	private function OutputAttributes(&$node, $xml)
	{
		// for all the attributes on the node
		foreach ($xml->attribute as $attr)
		{
			$attrid = (string)$attr;

			// work out the tag to use
			$tag = (string)$attr->attributes()->tag;
			if (!$tag) $tag = $attrid;

			// so we have an if_exists tag
			$if_exists = (string)$attr->attributes()->if_exists;

			// get the attribute and its value
			$attrib = $node->getAttribute($attrid);
			$value = $node->getAttributeValue($attrid);

			// if we have set is_exists, and the value is blank, skip the output
			if ($if_exists && $value == "")
			{
				continue;
			}

			$output = "";
			// parse the attribute and output dependant on the node type
			switch ($attrib->GetType())
			{
				case GDB_Attrib_Type::_String:
				case GDB_Attrib_Type::_Text:
					$output = "<$tag>". cdata($value) . "</$tag>";
					break;
				case GDB_Attrib_Type::_Int:
				case GDB_Attrib_Type::_Float:
				case GDB_Attrib_Type::_Bool:
				case GDB_Attrib_Type::_Time:
				case GDB_Attrib_Type::_DateTime:
				case GDB_Attrib_Type::_Date:
				case GDB_Attrib_Type::_SelectInt:
				case GDB_Attrib_Type::_SelectStr:
					$output = "<$tag raw='{$value}'>".$attrib->format()."</$tag>";
					break;
				case GDB_Attrib_Type::_KVPair:
					$val = trim($attrib->getValue());
					if ($val)
					{
						$bits = explode("~",$val,2);
						if (count($bits) == 1)
						{
							if ($bits[0] == "")
							{
								$id = "";
							} else {
								$id = $bits[0];
							}
						} else {
							$id = $bits[0];
						}
						$id = htmlentities($id);
						$output = "<$tag id='$id'>".$attrib->AsXML()."</$tag>";
						break;
					}
			}

			$this->output->append_output("$output\n");
		}
	}

	/**
	 * OUTPUT: Display any relations we have specified for the node being displayed
	 */
	function outputSectionrelations($section)
	{
		// foreach layer we have defined in out section, output it
		// child layers will call this recursivly

		$tag = "";

		if ($section->attributes()->tag)
		{
			$tag = (string)$section->attributes()->tag;
			$this->output->append_output("<$tag>\n");
		}


		foreach ($section->layer as $layer)
		{
			$this->outputLayer($this->data['node'], $layer);
		}

		if ($tag)
		{
			$this->output->append_output("</$tag>\n");
		}
	}

	/**
	 * Display any relation layers from outputSectionrelations
	 */
	function outputLayer(&$node, $layer)
	{
		$path = (string)$layer->attributes()->path;
		$nodetag = (string)$layer->attributes()->nodetag;
		$minimal = (string)$layer->attributes()->minimal;
		$group = (string)$layer->attributes()->group;
		$sort = (string)$layer->attributes()->sort;
		$weight = (string)$layer->attributes()->weight;

		// allow the output to be put in a sub tag
		// without this, the elements will be placed in the parent nodes context
		$tag = (string)$layer->attributes()->tag;
		if ($tag)
			$this->output->append_output("<{$tag}>\n");

		// load all the relations of the specified type
		$rels = $node->GetRelations($path);

		// order nodes by grouping if requested
		if ($sort)	$this->graph_db->sort_nodes($rels, $sort);
		$groupstart = "-------------------";
		$groupopen = false;

		foreach ($rels as &$rel)
		{
			if ($group)
			{
				$current = $rel->getAttributeValue($group);
				if ($groupstart != $current)
				{
          if ($current == '')
          {
            if ($groupopen)
            {
              $this->output->append_output("</group>\n");
            }

            $this->output->append_output("<group id='none'>\n");
            $this->output->append_output("<group_title></group_title>\n");
            $this->output->append_output("<group_desc />\n");
            $groupstart = $current;
            $groupopen = true;
          } else {
            $group_node = $this->graph_db->loadNodeByGuid($current);
            if ($group_node instanceof Graph_DB_Node_Typed) {
              $current_id = $group_node->getID();
              $curtitle = $group_node->getTitle();
              $curdesc = $group_node->getAttributeValue('desc');

              if ($groupopen)
              {
                $this->output->append_output("</group>\n");
              }

              $groupstart = $current;
              $this->output->append_output("<group id=\"$current_id\" guid=\"$current\">\n");
              if ($rel->getAttribute($group)->getType() == GDB_Attrib_Type::_KVPair)
              {
                $this->output->append_output("<group_title status=\"deprecated\">".cdata($curtitle)."</group_title>\n");
                $this->output->append_output("<title>".cdata($curtitle)."</title>\n");
                $this->output->append_output("<group_desc status=\"deprecated\">".cdata($curdesc)."</group_desc>\n");
                $this->output->append_output("<desc>".cdata($curdesc)."</desc>\n");
              }
              $groupopen = true;
            }
          }
				}
			}

			$subdata['weight'] = $weight;

			// set up the node to display
			$subdata['node'] = $rel;
			$subdata['parnode'] = $node;

			// set up what to show (as we are in a section, this is nothing)
			$subdata['show_attrs'] = 0;
			$subdata['show_details'] = 0;
			$subdata['show_rels'] = 0;

			// dont close the node tag as we are outputting attributes and sub layers
			$subdata['noclose'] = 1;

			// work out what to use as a tag for the sub node
			if ($nodetag)
			{
				$subdata['tag'] = $nodetag;
			} else {
				$subdata['tag'] = $rel->getTypeObj()->id;
			}

			// include the node template
			if ($minimal)
			{
				$this->_include("node/node_minimal", $subdata);

			} else {
				$this->_include("node/node", $subdata);
				// add attributes specified to the output
				$this->OutputAttributes($rel, $layer);

				// include sub layers
				foreach ($layer->layer as $sublayer)
				{
					$this->outputLayer($rel, $sublayer);
				}

				// close the node tag
				$this->output->append_output("</{$subdata['tag']}>\n");
			}


		}

		if ($groupopen)
		{
			$this->output->append_output("</group>\n");
		}

		// close sub tag if it was opened
		if ($tag)
			$this->output->append_output("</{$tag}>\n");
	}

  protected function RunType_php($xml, $filename, $template)
  {
    $phpfile = str_ireplace(".xml", ".php", $filename);
    if (!file_exists($phpfile))
    {
      $this->show_error("Cannot find $phpfile");
    }

    require_once ($phpfile);

    $classname = "API_" . $template;

    if (!class_exists($classname))
      $this->show_error("Unable to find class $classname");

    $repclass = new $classname();
    $repclass->data_xml = '';
    $repclass->report_xml = $xml;
    $repclass->node = &$this->node;

    ob_start(null, 100 * 1024 * 1024);
    $repclass->Process();

    $result = ob_get_clean();

    return $result;
  }

  function find()
	{
		// the following url segments are available:

		/*

		url /index.php/api/find?OPTIONS

		default output is xml, for json, the url is

		/index.php/api/find_json?OPTIONS

		Options:

		search - text to search for (no default) - REQUIRED
		type - node type. Can be either gdbid or name of the type. Also allows | separated listed of different types to search. (default all nodes)
		where - can be title, attribute, or default both. If both then the attributes searched will be taken from the nodedefs.
				If no node type specified, then will always be title. (default both)
		attrib - attribut to search (no default)
		exact - if 1 then exact value is required. Only applies to attributes (default no)
		limit - how many nodes to return (default 50)
		rels - list relations as well as nodes (default no)
		output - basic or full. Basic is node it, type and title, full is full node info and its attributes (same as api main call)

		*/

		return $this->find_xml();
	}

	function find_json()
	{
		require_once("xml2json/xml2json.php");

		// we are outputting json, so spit out a header
		header('Content-Type: application/json');

		$output = $this->_find();

		$jsonContents = xml2json::transformXmlStringToJson('<?xml version="1.0" encoding="utf-8" ?><cmapi>'.$output.'</cmapi>');
    
		$this->output->set_output($jsonContents);

	}

	function find_xml()
	{
		header("Content-type: text/xml");

		$output = $this->_find();

		$output = '<?xml version="1.0" encoding="utf-8" ?><cmapi>'.$output.'</cmapi>';

		if ($this->input->get('nice') == 1)
		{
			// create a DOMDocument, and output it
			// this allows us to take the xml, and format it nicely
			$foo = new DOMDocument();
			$foo->preserveWhiteSpace = false;
			$foo->formatOutput = true;
			if ($foo->loadXML($output))
			{
				$this->output->set_output($foo->saveXML());
			} else {
				$this->output->set_output($output);
			}
		} else {
			$this->output->set_output($output);
		}
	}

	function _find()
	{
		$this->dx_auth->APILogin();

		$nodetype = $this->input->get('type');
		$types = array();
		if (strpos($nodetype,"|") > 0)
		{
			$types = explode("|", $nodetype);
		} else if ($nodetype != "") {
			$types[] = $nodetype;
		}

		foreach ($types as $offset => $type)
		{
			$typeobject = $this->node_types->GetType($type);
			$types[$offset] = $typeobject->gdbid;
		}

		$where = $this->input->get('where');
		if ($where == "") $where = "both";
		$attr = $this->input->get('attrib');
		$search = $this->input->get('search');
		$exact =(int)$this->input->get('exact');
		$limit = (int)$this->input->get('limit');
		if ($limit < 1) $limit = 50;
		$rels = (int)$this->input->get('rels');
		$output = $this->input->get('output');
		if ($output == "") $output = "basic";

		/*echo "<h1>Find Node</h1>";
		echo "Node Types:<br>";
		print_p($types);

		echo "where - $where<br>";
		echo "attrib - $attr<br>";
		echo "search - $search<br>";
		echo "exact - $exact<br>";
		echo "limit - $limit<br>";
		echo "rels - $rels<br>";*/

		$have_nodes = array();
		$search_nodes = array();

		// if we are not just searching an attribute, search in titles.
		if ($where != "attrib")
		{
			$search_nodes = $this->graph_db->findNodes($search, $types, $limit);

			if (array_key_exists('error',$search_nodes))
			{
				return "<error>".$search_nodes['error']."</error>";
			}

			foreach ($search_nodes as &$node)
			{
				$node->match = "title";
				$node->match_attr = "";

				$have_nodes[$node->getID()] = 1;
			}
		}

		// if we have a type specified, check for any search attributes,
		// and find nodes based on the attributes too. Only do this if
		// we are not just searching for a title.
		if ($where != "title" && count($types) > 0)
		{
			foreach ($types as &$type) // for each type we are interested in
			{

				$node_type = $this->node_types->getType($type);

				// list its attributes
				$attribs = $node_type->GetAttributes();
				//print_p($attribs);

				// iterate through attribs
				foreach ($attribs as $attrib)
				{
					// if we have an attribute specified, and its not this one skip
					if ($attr && $attrib->id != $attr) continue;

					// if we are searching all attributes, skip any with out search defined.
					if ($attr == "" && !$attrib->search) continue;

					// do actual attrib search
					$attr_res = $this->graph_db->findNodeFromAttr($type,$attrib->id, $search, false);

					// if we have some results, store em in the $search_nodes array
					if (is_array($attr_res))
					{
						foreach ($attr_res as &$node)
						{
							if (!array_key_exists($node->GetID(),$have_nodes))
							{
								$node->match = "attrib";
								$node->match_attr = $attrib->id;

								$have_nodes[$node->getID()] = 1;
								$search_nodes[] = $node;
							}
						}
					}
				}
			}
		}

		// do we have any results or not?
		if (count($search_nodes) == 0)
		{
			return "<nonefound />";
			return null;
		}

		// we have an api template to output!
		if ($output != "basic" && $output != "full" && count($types) == 1)
		{
			$this->nohead = true;
			$result = "";
			foreach ($search_nodes as &$node)
			{
				$result .= $this->get_output($node->getId(), $output);
				$this->output->set_output("");
			}

			return $result;
		}

		// set up output information
		$secdata['show_attrs'] = 0;
		$secdata['show_details'] = 0;
		$secdata['show_rels'] = 0;
		$secdata['show_hist'] = 0;

		// if full passed, show all but history
		if ($output == "full")
		{
			$secdata['show_attrs'] = 1;
			$secdata['show_details'] = 1;
			//
		}

		if ($rels) $secdata['show_rels'] = 1;

		$secdata['tag'] = 'node';

		// output all of the found nodes
		foreach ($search_nodes as &$node)
		{
			$secdata['node'] = $node;

			// include the template
			$this->_include("node/node", $secdata);
			//$this->output->append_output("<node><![CDATA[Result : " . $node->getTitleDisp() . " - {$node->match}]]></node>");
		}

		$output = $this->output->get_output();
		$this->output->set_output("");
		return $output;
	}
}

class APIBase
{
  var $data_xml;
  var $report_xml;
  var $node;

  function Process()
  {

  }

}
