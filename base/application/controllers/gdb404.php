<?php

/**
 * Custom 404 error page, instead of showing the normal one. 
 *
 * It includes the template header and footer
 */
class gdb404 extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
		setup_main_menu($this->menu);	
		$this->ci =& get_instance();
		$data['title'] = "Page not found!";
        $this->output->set_status_header('404');
        $this->ci->load->view('templates/header', $data);
        $this->ci->output->append_output($this->ci->head->ErrorText("Unfortunately, the page you have requested cannot be found"));
		$this->ci->load->view('templates/footer');
    }
}

?>