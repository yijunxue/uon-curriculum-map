<?php
class Migrate extends CI_Controller {

  function __construct()
  {
    parent::__construct();

    // Protect entire controller so only admin can access it.
    $this->load->helper('url');
    $this->session->set_userdata('redirect', uri_string());
    $this->dx_auth->check_uri_permissions();
  }

  public function index()
  {
    // Switch to a DB user with greater privileges so that structural changes can be made
    $this->load->database('admin', FALSE, TRUE);
    $this->load->library('migration');

    if ($this->migration->current() === FALSE) {
      show_error($this->migration->error_string());
    }

    $CI =& get_instance();
    $this->data['version'] = $CI->config->item('migration_version');

    $this->data['title'] = 'Migration';
    $this->load->view('templates/header', $this->data);
    $this->load->view('migrate/index', $this->data);
    $this->load->view('templates/footer');
  }
}