<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

// select a node. 

// Will need to pass in the source node as well as a single parameter on the 
// url which will be the target field that will
// be updated with the value of the node that has been selected

// THIS REQUIRES SOME ATTENTION - NOT VERY GOOD FUNCTIONALITY

class Select extends CI_Controller {
	public function __construct()
	{
		// call parent constructor
		parent::__construct();	
		$this->load->library('tabs');
		
		$this->ci =& get_instance();
	}
	
	function title($nodeid)
	{
		$node = $this->graph_db->loadNode($nodeid);
		echo $node->getTitleDisp();
		exit;
	}
	
	function index($sourceid = 0, $targetfield = "", $targettype = 0)
	{
		$data['types'] = $this->node_types->GetTypesForForm();
		$data['type'] = 0;
		
		if ($sourceid > 0)
		{
			$data['source_node'] = $this->graph_db->loadNode($sourceid);
		} else {
			$data['source_node'] = $this->graph_db->loadNode(1);
		}
		
		if ($targettype == 0)
		{
			$targettype = $data['source_node']->getType();
		}
		
		$targettype_obj = $this->node_types->GetType($targettype);

		$data['title'] = "Select a '{$targettype_obj->name}'";
		
		$this->head->AddRawScript('var ajax_url_search = "' . site_url('select/ajax/XXTYPEXX/'.$sourceid.'/XXSEARCHXX/XXTYPEIDXX') . '";');
		$this->head->AddRawScript('var ajax_base_url = "' . site_url('select/ajax/XXTYPEXX/'.$sourceid.'/XXIDXX') . '";');
		$this->head->AddJS('js/node/subnodes.js');
		$this->head->AddJS('js/node/search.js');

		// find the year node off the root node
		$rootnode = $this->graph_db->loadNode(1);
		$yearnodes = $rootnode->getRelations("year");
	
		$data['year_node'] = &$yearnodes[0];
		$data['sourceid'] = $sourceid;
		$data['targetfield'] = $targetfield;
			
		$data['target_types'] = array();
		$data['target_types'][$targettype_obj->gdbid] = $targettype_obj->id;
			
		$this->data =&$data;
			
		$this->load->helper('form');
		$this->load->library('form_validation');


		// add cancel button to footer
		$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.parent.TINY.box.hide();return false;'>Cancel</button>");
	
		$data['popupheight'] = 601;

		$this->load->view('templates/popup/header', $data);
		$this->load->view('select/main', $data);
		$this->load->view('templates/popup/footer');
	}
	
	function type($nodetype, $target = '', $targetfield = "")
	{
		if (is_numeric($nodetype))
		{
            $nodetype_num = $nodetype;
			$nodetype = $this->ci->node_types->getType($nodetype)->id;
		} else {
            $nodetype_num = $this->ci->node_types->getType($nodetype)->gdbid;
        }

		// if $target is "parent" then we are picking nodes of a type that can create $nodetype
		// otherwise we are picking nodes of $nodetype	
        if ($target == 'parent') {
            $data['target_types'] = $this->ci->node_types->can_be_created_from[$nodetype];
        } else {
            $data['target_types'] = array($nodetype_num => $nodetype);
        }

		$data['types'] = $this->node_types->GetTypesForForm();
		$data['type'] = 0;
		
		$data['source_node'] = $this->graph_db->loadNode(1);
		
		if (count($data['target_types']) > 1)
		{
			$data['title'] = "Select an item";
		} else {
			foreach ($data['target_types'] as $type_name)
			{	
				$data['title'] = "Select a '{$type_name}'";
				break;
			}
		}
		
		
		$this->head->AddRawScript('var ajax_url_search = "' . site_url('select/ajax/XXTYPEXX_type/'.$nodetype.'/XXSEARCHXX/XXTYPEIDXX') . '";');
		$this->head->AddRawScript('var ajax_base_url = "' . site_url('select/ajax/XXTYPEXX_type/'.$nodetype.'/XXIDXX') . '";');
		$this->head->AddJS('js/node/subnodes.js');
		$this->head->AddJS('js/node/search.js');

		// find the year node off the root node
		$rootnode = $this->graph_db->loadNode(1);
		$yearnodes = $rootnode->getRelations("year");
		
		$data['year_node'] = &$yearnodes[0];
		$data['sourceid'] = 0;
		$data['targetfield'] = $targetfield;
		
		$this->data =&$data;
		
		$this->load->helper('form');
		$this->load->library('form_validation');


		// add cancel button to footer
		$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.parent.TINY.box.hide();return false;'>Cancel</button>");
		
		$data['popupheight'] = 601;

		$this->load->view('templates/popup/header', $data);
		$this->load->view('select/main', $data);
		$this->load->view('templates/popup/footer');
	}
	/**
	 * This is method ajax
	 *
	 * @param mixed $type This is a description
	 * @param mixed $param1 This is a description
	 * @param mixed $param2 This is a description
	 * @param mixed $param3 This is a description
	 * @param mixed $param4 This is a description
	 * @return mixed This is the return value description
	 *
	 */
	function ajax($type, $param1 = "", $param2 = "", $param3 = "", $param4 = "")
	{
		$function = "ajax_$type";
		if (method_exists($this,$function))
		{
			
			$debug = array_key_exists('debug', $_GET);
			$data =& $this->$function($param1, $param2, $param3, $param4);	
			if ($debug == 1)
			{
				$this->load->view('templates/header', $data);
				if ($data)
					$this->load->view("select/ajax/$type", $data);
				$this->load->view('templates/footer');	
			} else {
				if ($data)
					$this->load->view("select/ajax/$type", $data);
			}
		} else {
			echo json_encode(array('error' => "Invalid AJAX request, 'Node::ajax_$type()' does not exist"));		
		}
	}
	/**
	 * This is method ajax_related
	 *
	 * @param mixed $baseid This is a description
	 * @param mixed $nodeid This is a description
	 * @return mixed This is the return value description
	 *
	 */
	private function ajax_related($baseid, $nodeid)
	{
		$node = $this->graph_db->loadNode($nodeid);
		$data['subdata']['rels'] = $node->getRelations_GroupByNodeType();
		$data['subdata']['node'] =	$this->graph_db->loadNode($baseid);
		$data['subdata']['target_types'] = array();
		$data['subdata']['target_types'][$data['subdata']['node']->getType()] = $data['subdata']['node'];
		$data['subdata']['path'] = $this->input->get('path');
		$data['subdata']['baseid'] = $baseid;
		return $data;
	}
	
	
	private function ajax_related_type($type, $nodeid)
	{
		$node = $this->graph_db->loadNode($nodeid);
		$data['subdata']['target_types'] = $this->ci->node_types->can_be_created_from[$type];
		$data['subdata']['rels'] = $node->getRelations_GroupByNodeType();
		$data['subdata']['node'] =	$this->graph_db->loadNode(1);
		$data['subdata']['path'] = $this->input->get('path');
		$data['subdata']['baseid'] = 1;
		return $data;
	}
	
	
	/**
	 * Search for nodes
	 *
	 * @param mixed $baseid This is a description
	 * @param mixed $string This is a description
	 * @return mixed This is the return value description
	 *
	 */
	private function ajax_search($baseid, $string, $type = 0)
	{
		$basenode = $this->graph_db->loadNode($baseid);
		$search_nodes = $this->graph_db->findNodes($string, $type, 50);
		
		if (array_key_exists("error", $search_nodes))
		{
			echo $this->head->MessageText($search_nodes['error']);
			return array();
		} else {
			
			$data['subdata']['rels'] = $this->graph_db->groupNodes_Type($search_nodes);
			$data['subdata']['node'] =	$this->graph_db->loadNode($baseid);
			$data['subdata']['target_types'] = array();
			$data['subdata']['target_types'][$data['subdata']['node']->getType()] = $data['subdata']['node'];
			$data['subdata']['baseid'] = $baseid;
			$data['subdata']['path'] = 0;
			
			if (count($search_nodes) == 0)
				echo $this->head->MessageText("No results found");
			return $data;
		}
	}
	
	private function ajax_search_type($nodetype, $string, $type = 0)
	{
		$basenode = $this->graph_db->loadNode(1);
		$search_nodes = $this->graph_db->findNodes($string, $type, 50);
		
		if (array_key_exists("error", $search_nodes))
		{
			echo $this->head->MessageText($search_nodes['error']);
			return array();
		} else {
			
			$data['subdata']['target_types'] = $this->ci->node_types->can_be_created_from[$nodetype];
			$data['subdata']['rels'] = $this->graph_db->groupNodes_Type($search_nodes);
			$data['subdata']['node'] =	$basenode;
			$data['subdata']['baseid'] = 1;
			$data['subdata']['path'] = 0;
			
			if (count($search_nodes) == 0)
				echo $this->head->MessageText("No results found");
			return $data;
		}
	}
	
}