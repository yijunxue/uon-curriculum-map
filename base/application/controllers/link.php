<?php

/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

include "view.php";

/**
 * Node link class
 * 
 * a lot of this is based off View class, ought to combine really, but that makes the code horrible!
 * 
 */

class Link extends View {

	var $xml_base = "link";
	var $vn = 'link';
	
	public function index($nodeid = '', $subtmpl = '')
	{
		$this->head->AddJS("js/node/link.js");
		$this->load->helper('form');
		$this->load->library('form_validation');

		// show display the users initial node here
		$this->getNode($nodeid);	
			
		$this->data['subtmpl'] = $subtmpl;

    $ajax_subtmpl = ($subtmpl == '') ? '-' : $subtmpl;
		
		if (!$this->data['node'])
			return redirect("/");
		
		if (!$this->data['node']->can_link())
		{
			return show_error("You do not have permission to link this item");
			//return redirect("/");
		}
			
		$this->head->AddRawScript('var ajax_base_url = "' . site_url($this->vn . '/ajax_st/'.$ajax_subtmpl.'/XXTYPEXX/'.$this->data['node']->getID().'/XXIDXX') . '";');
		
		$filename = "";
		$xml = $this->getXMLFile($this->data['node'], $filename, false, $subtmpl);
		$this->subtemplate = $subtmpl;
		
		$this->ParseGlobals($xml);
		
		$this->data['filename'] = $filename;
			
		$this->data['title'] = "Link '" . $this->data['node']->getTitleDisp() . "'";
		if ($xml->description)
			$this->data['title'] .= " to '" . (string)$xml->description . "'";

		$this->data['tree']['link'] = 1;
		
		if ($xml->onlylink && $xml->onlylink->type)
		{
			foreach ($xml->onlylink->type as $type)
			{
				ViewTools::$onlylink[(string)$type] = 1;
			}
		}
				
		foreach ($xml->section as $section)
		{
			$this->outputSection($section);	
		}
		
		$hidden = array('nodeid' => $nodeid);
		$this->head->AddLeading(form_open("link/process", array('id' => 'link_form'), $hidden));					
					
		$this->head->AddLeading(form_input(array('name' => 'return', 'id' => 'return', 'type' => 'hidden', 'value' => set_value('return', get_referer()))));

		//add form close to bottom of popup
		$this->head->AddTrailing(form_close());					
	
			// add cancel button to footer
		$this->head->AddFooter("<button id=\"save_link\" class=\"footer_button\" disabled=\"disabled\">Save</button>");

		// add cancel button to footer
		$this->head->AddFooter("<button id='edit_cancel' class='footer_button' onclick='window.location=\"".site_url(set_value('return', get_referer())) ."\";return false;'>Cancel</button>");
		
		$output = $this->output->get_output();
		$this->output->set_output("");
		
		$this->load->view('templates/header', $this->data);
		$this->output->append_output($output);		
		if ($this->dx_auth->showdebug())
			$this->load->view('view/_misc/debug', $this->data);
		$this->load->view('templates/footer');
	}
	

	function ajax_st($subtemplate, $type, $param1 = "", $param2 = "", $param3 = "", $param4 = "")
	{
		$this->subtemplate = ($subtemplate == '-' ) ? '' : $subtemplate;
		
		$this->ajax($type, $param1, $param2, $param3, $param4);
	}

	/**
	 * This is method ajax
	 *
	 * @param mixed $type This is a description
	 * @param mixed $param1 This is a description
	 * @param mixed $param2 This is a description
	 * @param mixed $param3 This is a description
	 * @param mixed $param4 This is a description
	 * @return mixed This is the return value description
	 *
	 */
	
	protected function outputSectionchanges($section)
	{
		// need to output a list of the current references
		
		// need to have 2 arrays in js, one for adds, one for removes
		
		// after a load of some data, always do a refresh on the link images (COULD BE TRICKY!)
		
		// 4 link image states :
		
		/*
			not linked
			linked
			
			set to be linked
			set to be unlinked
		*/
		
		// load list of all current relations
		$rels = $this->data['node']->getRelations('',GDB_Rel::Relation);
		$this->data['changes']['relids'] = array();
		foreach ($rels as $rel)
		{
			$this->data['changes']['relids'][] = $rel->getId();	
		}	
		
		$this->data['changes']['test'] = 1;
		
		$this->_include("changes",$this->data['changes']);
	}
	
	public function title($nodeid)
	{
		$node = $this->graph_db->loadNode($nodeid);
		echo $node->getTitleDisp();
		exit;	
	}
	
	public function process()
	{
		$nodeid = $this->input->post('nodeid');
		
		$node = $this->graph_db->loadNode($nodeid);
		
		if (!$node->can_link())
		{
			redirect("view/$nodeid");
			return;	
		}
		
		//print_p($_POST);	
		
		$this->link_add = json_decode($this->input->post('link_add'));
		$this->link_remove = json_decode($this->input->post('link_remove'));
		$this->weight_change = json_decode($this->input->post('weight_change'));
		
		//print_p($this->link_add);
		//print_p($this->link_remove);
		//print_p($this->weight_change);
		
		if (is_object($this->link_add))
		{
			foreach ($this->link_add as $addid => $weight)
			{
				//echo "Add : " . $addid . " ($weight)<br>";
				
				$addnode = $this->graph_db->loadNode($addid);	
				if (!$addnode) continue;
				
				$node->addRelate($addnode, GDB_Rel::Relation, $weight / 100);
			}
		}
		
		if (is_object($this->link_remove))
		{
			foreach ($this->link_remove as $removeid => $temp)
			{
				//echo "Remove : " . $removeid."<br>";	
				
				$removenode = $this->graph_db->loadNode($removeid);	
				if (!$removenode) continue;
				
				$node->removeRelate($removenode);
			}
		}
		
		if (is_object($this->weight_change))
		{
			foreach ($this->weight_change as $nodeid => $newweight)	
			{	
				$weightnode = $this->graph_db->loadNode($nodeid);
				if (!$weightnode) continue;
				
				$node->editRelateWeight($weightnode, $newweight/100);	
			}
		}
		
			
		$node->save();
		//print_p($node->queries);
		//exit;
		$this->head->AddMessage("Link changes have been saved.");
		if (array_key_exists('return', $_POST) && $_POST['return'] != "")
		{
			//$this->head->ParentRefresh($referer);  
			redirect($_POST['return']);
		} else {
			redirect("view/$nodeid");
		}
	}
	
	public function tree($nodeid, $sectionid, $selected, $subtmpl = '')
	{
		$this->getNode($nodeid);
		$filename = "";	
		$xml = $this->getXMLFile($this->data['node'], $filename, false, $subtmpl);
		$this->ParseGlobals($xml);

		if ($xml->onlylink && $xml->onlylink->type)
		{
			foreach ($xml->onlylink->type as $type)
			{
				ViewTools::$onlylink[(string)$type] = 1;
			}
		}		

		$this->data['tree']['link'] = 1;

		parent::tree($nodeid, $sectionid, $selected, $subtmpl);
	}	
	
	protected function outputSectionTree($section)
	{
		$layer = new TreeLayer();
		
		// need to display a tree list of the layers specified
		$layer->layers = $this->Tree_LoadLayers($section);
		
		$template = "node";
		if ((string)$section->attributes()->template != "")
			$template = (string)$section->attributes()->template;
		
		$basenode =& $this->data['node'];
		$sectionid = (string)$section->attributes()->id;
		$selected = $basenode->getID();

		$this->data['tree']['url'] = site_url($this->vn . "/tree/" . $basenode->getID() . '/' . $sectionid . '/' . "XXIDXX/" . $this->data['subtmpl']);
		$this->data['tree']['sectionid'] = $sectionid;
		$this->data['tree']['template'] = $template;
		$this->data['tree']['layerpath'] = array();
		$this->data['tree']['layerpath'][] = $layer;

		$this->DoWeightPopup();
		
		$this->_include('tree/root', $this->data);
		
		// instead of having 2 sets of code, just call the sub layer code with no selected nodes
		$this->TreeLayerOutputSet($layer, $selected);
	}
}
