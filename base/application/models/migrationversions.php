<?php
/**
 * Get a list of migration versions by scanning the migrations directory
 * 
 * @package		uon_curriculum_map
 * @author		Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 * @copyright	Copyright (c) 2014, University of Nottingham
 * 
 */
class migrationversions extends CI_Model {

	/**
	 * Constructor
	 */
	function __construct() {
		// Call the Model constructor
		parent::__construct();
	}

	/**
	 * Get current migration version
	 * 
	 * @return int
	 */
	function get_current_version() {
		$currentversion = $this->db->get('migrations')->row()->version;
		return $currentversion;
	}

	/**
	 * Get an array of migration versions
	 * 
	 * @return array Migration versions
	 */
	function get_versions() {
		$migrationfiles = scandir(__DIR__ . '/../migrations/');
		$versions = array();
		foreach ($migrationfiles as $filename)
		{
			$version = ltrim(substr($filename, 0, 3), '0');
			if (is_numeric($version) && $version !== '.' && $version !== '..') {
				$versions[(int) $version] = substr(str_replace('_', ' ', $filename), 0, -4);
					}
		}
		return $versions;
	}
}
