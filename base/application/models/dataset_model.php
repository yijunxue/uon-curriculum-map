<?php
class Dataset_model extends CI_Model {
  private $id = -1;
  private $table = -1;

  function __construct()
  {
      // Call the Model constructor
      parent::__construct();
      $this->load->helper('node_types');
  }

  /**
   * Add a new dataset
   * @param array $data Associative array of data for the new dataset
   */
  public function add_new($data) {
    if ($data['isdefault'] == 1) {
      $this->unset_default();
    }
    $this->db->insert('datasets', $data);
    $this->id = $this->db->insert_id();
    $this->table = $data['table'];
  }


  /**
   * Clone to a new dataset form the source dataset
   * @param  string $source_table Table name of the source dataset
   */
  public function clone_nodes_db($source_dataset){
    $ci =& get_instance();
    $gdb = $ci->graph_db; //could get $db use $ci->graph_db->gdb , it's better if DB is Global
    $gdb->loadDatasets();

    $this->target_dataset = $this->table;
    $this->graph_target = new Graph_DB();
    $this->graph_target->setDataset($this->target_dataset, false);
    $this->graph_target->checkForTables($source_dataset);
    $this->graph_target->truncatedataset($this->target_dataset);

    $sqls = array("INSERT INTO gdb_" . $this->target_dataset . "_graph_attr SELECT * FROM gdb_" . $source_dataset . "_graph_attr;",
      "INSERT INTO gdb_" . $this->target_dataset . "_graph_hist SELECT * FROM  gdb_" . $source_dataset . "_graph_hist;",
      "INSERT INTO gdb_" . $this->target_dataset . "_graph_node  SELECT * FROM gdb_" . $source_dataset . "_graph_node;",
      "INSERT INTO gdb_" . $this->target_dataset . "_graph_rel  SELECT * FROM gdb_" . $source_dataset . "_graph_rel;");

    foreach($sqls as $statement){
      $this->db->query($statement);
    }

    $targetid = get_id_from_table($this->target_dataset, $this->db);
    $sourceid = get_id_from_table($source_dataset, $this->db);

    //copy user_role for this user in sourece table into target table
    $sqls = "REPLACE INTO user_node SELECT userid," . $targetid . ",nodeid FROM user_node where dataset = " . $sourceid . ";";
    $this->db->query($sqls);

    $target_year_end = intval($this->target_dataset) + 1;
    $newdatasetname = "Year $this->target_dataset/$target_year_end";

    $sqls = "UPDATE gdb_" . $this->target_dataset . "_graph_node set title = ? where id =2";
    $this->db->query($sqls, array($newdatasetname));
  }


  /**
   * Clone the user nodes, user -> node and custom node records form the source dataset
   * @param  string $source_table Table name of the source dataset
   */
  public function clone_nodes($source_table) {
    // Get the user and node IDs for the source DS
    $source_id = get_id_from_table($source_table, $this->db);
    $user_nodes = get_user_nodes($source_id, $this->db);

    // 1. Clone user nodes.
    if (count($user_nodes) > 0)
    {
      $ci =& get_instance();
      $gdb = $ci->graph_db;
      $gdb->loadDatasets();

      $this->target_dataset = $this->table;

      $this->graph_target = new Graph_DB();
      $this->graph_target->setDataset($this->target_dataset, false);
      $this->graph_target->checkForTables($source_table);

      foreach ($user_nodes as $user_node) {
        // Create a new user node in the destination dataset
        $node = $gdb->loadNode($user_node['nodeid']);

        if ($gdb->load_error === 0) {
          $title = $node->getTitle();
          $nodeguid = $node->getGuid();
          $usernode = $this->graph_target->createNode($title, 502, $this->graph_target->active_dataset->nodes->all_users, $nodeguid);
          $usernode->save();

          $userid = $usernode->getId();
          // add history item to the source node
          $usernode->_addHistory(GDB_Hist_Action::Node_CloneCreateDataset, $userid, $this->target_dataset);
          $usernode->save();

          // copy all attributes
          $attrs = $node->getAttributes();
          foreach ($attrs as $attr) {
            $usernode->setAttribute($attr->GetName(), $attr->GetValue(), $attr->GetType());
          }
          $usernode->save();

          // clear permissions and copy the original nodes permissions, fixing any target node references
          $usernode->replace_and_update_perms($node->perms, $user_node['nodeid'], $userid);

          // Add a new user node relationship for the new dataset
          $qry = "REPLACE INTO user_node (userid, dataset, nodeid) VALUES (?, ?, ?)";
          $this->graph_target->gdb->query($qry, array($user_node['userid'], $this->id, $usernode->getId()));
        }
      }
    }

    // 2. Clone custom nodes.
    $role_group_nodes = get_role_group_nodes($source_table, $this->db);

    if (count($role_group_nodes) > 0)
    {
      foreach ($role_group_nodes as $role_group_node) {
        // Create a new role group node in the destination dataset
        $node = $gdb->loadNode($role_group_node['id']);
        $nodeguid = $node->getGuid();
        if ($gdb->load_error === 0) {
          $title = $node->getTitle();

          if ($this->graph_target->doesGuidExist($nodeguid)) {
            $found = $this->graph_target->loadNodeByGuid($nodeguid);
            // clear permissions and copy the original nodes permissions, fixing any target node references
            foreach ($user_nodes as $user_node) {
              $roleid = get_dataset_user_node($this->id, $user_node['userid'], $this->db);
              $found->replace_and_update_perms($node->perms, $user_node['nodeid'], $roleid);
            }
            $this->fix_group_members($node);
            continue;
          }

          $oldparent = $node->getParent()->getTitle();
          $newparent = $this->graph_target->findNodes($oldparent, 501, 1, false, "Mysql_Fulltext");

          $rolegroupnode = $this->graph_target->createNode($title, 501, $newparent[0], $nodeguid);
          $rolegroupnode->save();

          // add history item to the source node
          $rolegroupnode->_addHistory(GDB_Hist_Action::Node_CloneCreateDataset, $rolegroupnode->getId(), $this->target_dataset);
          $rolegroupnode->save();

          // copy all attributes
          $attrs = $node->getAttributes();
          foreach ($attrs as $attr) {
            $rolegroupnode->setAttribute($attr->GetName(), $attr->GetValue(), $attr->GetType());
          }
          $rolegroupnode->save();
          // Clear permissions and copy the original nodes permissions, fixing any target node references
          foreach ($user_nodes as $user_node) {
            $roleid = get_dataset_user_node($this->id, $user_node['userid'], $this->db);
            $rolegroupnode->replace_and_update_perms($node->perms, $user_node['nodeid'], $roleid);
          }
          $this->fix_group_members($node);
        }
      }
    }

    // 3. Clone session node permission.
    $target_node = get_session_node($source_table, $this->db);
    $node = $gdb->loadNode($target_node);

    if ($gdb->load_error === 0) {

      $target_node = get_session_node($this->table, $this->db);
      $target_node = $this->graph_target->findNodes($target_node, 1, 1, false, "Mysql_Nodeid");
      // clear permissions and copy the original nodes permissions, fixing any target node references

      // first do user nodes
      foreach ($user_nodes as $user_node) {
        $id = get_dataset_user_node($this->id, $user_node['userid'], $this->db);
        $target_node[0]->replace_and_update_perms($node->perms, $user_node['nodeid'], $id);
      }
      // second do group nodes
      foreach ($role_group_nodes as $role_group_node) {
        $loadnode = $gdb->loadNode($role_group_node['id']);
        $nodeguid = $loadnode->getGuid();
        if ($this->graph_target->doesGuidExist($nodeguid)) {
          $target_role_group_node = $this->graph_target->loadNodeByGuid($nodeguid);
          $target_node[0]->replace_and_update_perms($node->perms, $role_group_node['id'], $target_role_group_node->getId());
        }
      }
    }
  }

  /**
   * Recreate all the relationships for this group.
   * This in effect will add all the members back into it.
   *
   * @param Graph_DB_Node $group_node The node of a group
   * @return void
   */
  protected function fix_group_members(Graph_DB_Node $group_node) {
    if ($group_node->getType() != NTypes::GROUP) {
        // Invalid node type.
        return;
    }
    $relations = $group_node->getRelations_OfType(GDB_Rel::Relation);
    $count = 0;
    foreach ($relations as $relative) {
      $new_relation = $this->graph_target->loadNodeByGuid($group_node->getGuid());
      if ($new_relation) {
        // The relation exists.
        $relative_guid = $relative->getGuid();
        $relative_weight = $relative->getWeightTo($group_node);
        if ($this->graph_target->doesGuidExist($relative_guid)) {
          // Get the id of the cloned target of the relationship.
          $target_id = $this->graph_target->loadNodeByGuid($relative_guid)->getId();
          $new_relation->addRelate($target_id, GDB_Rel::Relation, $relative_weight);
          $new_relation->save();
        }
      }
    }
  }

  /**
   * Get dataset record given its table name
   * @param  string $source_table  Table name
   * @return integer               Database ID
   */
  public function get_dataset($source_table) {

    $entries = $this->db->get_where('datasets', array('table' => $source_table));

    if (count($entries) != 1) {
      return false;
    }

    $row = $entries->row();
    return $row;
  }
  
  /**
   * Update a dataset
   * @param  string $data
   */
  public function update_dataset($data) {
    if ($data['isdefault'] == 1) {
      $this->unset_default();
    }
    $this->db->update('datasets', $data, "id = " . $data['id']);
  }	

  /**
   * Reset the 'isdefault' flag for all datasets
   */
  private function unset_default() {
    $data = array('isdefault' => 0);
    $this->db->update('datasets', $data);
  }

  /**
   * Set the default dataset
   */
  public function set_default($table) {
    $this->unset_default();
    $this->db->where('table', $table);
    $data = array('isdefault' => 1);
    $this->db->update('datasets', $data);
  }

  /**
   * Get the name of a dataset
   * @param int $table
   * @return string
   */
  public function get_dataset_name($table) {
    $this->db->where('table', $table);
    return $this->db->get('datasets')->row()->name;
  }
}
