<?php

class Users extends CI_Model 
{
	function __construct()
	{
		parent::__construct();

		// Other stuff
		$this->_prefix = $this->config->item('DX_table_prefix');
		$this->_table = $this->_prefix.$this->config->item('DX_users_table');	
		$this->_roles_table = $this->_prefix.$this->config->item('DX_roles_table');
	}
	
	// General function
	
	function get_all($offset = 0, $row_count = 0)
	{
		$users_table = $this->_table;
		$roles_table = $this->_roles_table;

		$CI =& get_instance();
		$DS = $CI->graph_db->active_dataset->table;
		$DSID = $CI->graph_db->active_dataset->id;

		$this->db->select("users.*", FALSE);
		$this->db->select("n.nodeid", FALSE);
		$this->db->select("r.name AS role_name", FALSE);
		$this->db->select("r.description AS role_description", FALSE);
		$this->db->select("fullname.value_str as fullname", FALSE);
		$this->db->select("yearofstudy.value_str as yearofstudy", FALSE);
		$this->db->join("roles as r", 'r.id = users.role_id', 'left');
		$this->db->join("(SELECT * FROM user_node WHERE dataset = {$DSID}) as n", 'n.userid = users.id', 'left');
		$this->db->join("(SELECT node_id, value_str FROM gdb_{$DS}_graph_attr WHERE attrib = 'fullname') as fullname", 'fullname.node_id = n.nodeid', 'left');
		$this->db->join("(SELECT node_id, value_str FROM gdb_{$DS}_graph_attr WHERE attrib = 'yearofstudy') as yearofstudy", 'yearofstudy.node_id = n.nodeid', 'left');

		$filter_search = strtolower(set_value('filter_search'));
		
		if ($filter_search != "")
		{
			$this->db->like('LOWER(username)', $filter_search);
			$this->db->or_like('LOWER(fullname.value_str)', $filter_search);
			$this->db->or_like('LOWER(email)', $filter_search);
		}
		
		$role = set_value('filter_role');
		if ($role != "any" && $role > 0)
		{
			$this->db->where('users.role_id', $role);	
		}
		
		// allowed order listing to prevent hacking
		$allow_order = array(
			'username' => 1,
			'fullname' => 1,
			'yearofstudy' => 1,
			'role_name' => 1,
			'last_login' => 1,
			'created' => 1,
			'email' => 1);
		
		$sort = set_value('sort');
		//echo "Sort : $sort<br>";
		if ($sort && array_key_exists($sort, $allow_order))
		{
			$sort_dir = set_value('sort_dir');
			if ($sort_dir != "asc" && $sort_dir != "desc") $sort_dir = "asc";
			$this->db->order_by($sort, $sort_dir);
		}
		
		if ($offset >= 0 AND $row_count > 0)
		{
		
			$query = $this->db->get($this->_table, $row_count, $offset);
		} else {
			$query = $this->db->get($this->_table);	
		}
		
		//echo $this->db->last_query() . "<br>";
		
		return $query;
	}

	function get_user_by_id($user_id)
	{
		$this->db->where('id', $user_id);
		return $this->db->get($this->_table);
	}

	function get_user_by_username($username)
	{
		$this->db->where('username', $username);
		return $this->db->get($this->_table);
	}
	
	function get_user_by_email($email)
	{
		$this->db->where('email', $email);
		return $this->db->get($this->_table);
	}
	
	function get_login($login, $pass = "")
	{
		//echo "get_login<br>";
		$this->db->where('username', $login);
		$this->db->or_where('email', $login);
		$query = $this->db->get($this->_table);
		
		$CI =& get_instance();
		
			if ($query->num_rows() == 1)
		{
			return $query;
		} else if ($pass && $CI->config->item('UONCM_LDAP_Enable') && $CI->config->item('UONCM_LDAP_UsernameFallback')) {
			// not found, try and do ldap auth	
			//echo "Trying LDAP for $login, $pass<br>";
			$CI->load->helper('ldap');
			
			$email = "";
			
			if (ldap_authenticate($login, $pass, $email))
			{
				// can auth with ldap, so we need to create a new user account, and request an email?
				$CI->dx_auth->register($login, $pass, $email);
				
				$qry = "UPDATE users SET role_id = 4 WHERE username = ?";
				$this->db->query($qry, array($login));
				
				$this->db->where('username', $login);
				$query = $this->db->get($this->_table);
				
				return $query;
			} else {
				// cant login with ldap, so return the original empty query and carry on as before
				return $query;	
			}
		} else {
			return $query;	
		}
	}
	
	function check_ban($user_id)
	{
		$this->db->select('1', FALSE);
		$this->db->where('id', $user_id);
		$this->db->where('banned', '1');
		return $this->db->get($this->_table);
	}
	
	function check_username($username)
	{
		$this->db->select('1', FALSE);
		$this->db->where('LOWER(username)=', strtolower($username));
		return $this->db->get($this->_table);
	}

	function check_email($email)
	{
		$this->db->select('1', FALSE);
		$this->db->where('LOWER(email)=', strtolower($email));
		return $this->db->get($this->_table);
	}
		
	function ban_user($user_id, $reason = NULL)
	{
		$data = array(
			'banned' 			=> 1,
			'ban_reason' 	=> $reason
		);
		return $this->set_user($user_id, $data);
	}
	
	function unban_user($user_id)
	{
		$data = array(
			'banned' 			=> 0,
			'ban_reason' 	=> NULL
		);
		return $this->set_user($user_id, $data);
	}
		
	function set_role($user_id, $role_id)
	{
		$data = array(
			'role_id' => $role_id
		);
		return $this->set_user($user_id, $data);
	}

	// User table function

	function create_user($data)
	{
		$data['created'] = date('Y-m-d H:i:s', time());
		return $this->db->insert($this->_table, $data);
	}

	function get_user_field($user_id, $fields)
	{
		$this->db->select($fields);
		$this->db->where('id', $user_id);
		return $this->db->get($this->_table);
	}

	function get_node_id($user_id) {
		$CI =& get_instance();
		$DSID = $CI->graph_db->active_dataset->id;
		$id = false;

		$this->db->select('nodeid', -1);
		$this->db->where('userid', $user_id);
		$this->db->where('dataset', $DSID);
		$result = $this->db->get('user_node');
		if ($result->num_rows() > 0) {
			$row = $result->row();
			$id = $row->nodeid;
		}
		return $id;
	}

	function set_user($user_id, $data)
	{
		$this->db->where('id', $user_id);
		return $this->db->update($this->_table, $data);
	}
	
	function delete_user($user_id)
	{
		$this->db->where('id', $user_id);
		$this->db->delete($this->_table);
		return $this->db->affected_rows() > 0;
	}
	
	// Forgot password function

	function newpass($user_id, $pass, $key)
	{
		$data = array(
			'newpass' 			=> $pass,
			'newpass_key' 	=> $key,
			'newpass_time' 	=> date('Y-m-d h:i:s', time() + $this->config->item('DX_forgot_password_expire'))
		);
	
		//print_p($data);
		
		return $this->set_user($user_id, $data);
	}

	function activate_newpass($user_id, $key)
	{
		//echo "activate_newpass<br>";
		$this->db->set('password', 'newpass', FALSE);
		$this->db->set('newpass', NULL);
		$this->db->set('newpass_key', NULL);
		$this->db->set('newpass_time', NULL);
		$this->db->where('id', $user_id);
		$this->db->where('newpass_key', $key);
		
		return $this->db->update($this->_table);
	}

	function clear_newpass($user_id)
	{
		//echo "clear_newpass<br>";
		$data = array(
			'newpass' 			=> NULL,
			'newpass_key' 	=> NULL,
			'newpass_time' 	=> NULL
		);
		return $this->set_user($user_id, $data);
	}
	
	// Change password function

	function change_password($user_id, $new_pass)
	{
		$this->db->set('password', $new_pass);
		$this->db->where('id', $user_id);
		return $this->db->update($this->_table);
	}

	function change_user_role($role_data, $userid, $role) {
		$CI =& get_instance();

		// Change user role
		$this->set_role($userid, $role);
		$nid = $this->get_node_id($userid);

		if ($nid !== false) {
			$node = $CI->graph_db->loadNode($nid);
			if ($node) {
				$CI->load->helper('roles');
				$roles = array_push($role_data['parent_roles_id'], $role);
				reset_roles($node, $this, $role_data['parent_roles_id']);
				$node->save();
			}
		}
	}

	function create_user_node($username, $parent, $userid, $datasetid, $reset_roles = true) {
		$CI =& get_instance();

		// not found, we need to create it
		$usernode = $CI->graph_db->createNode($username, 502, $this->graph_db->active_dataset->nodes->all_users);

		if ($reset_roles) {
			$CI->load->helper('roles');

			reset_roles($usernode, $this);
		}

		$usernode->save();

		$qry = "REPLACE INTO user_node (userid, dataset, nodeid) VALUES (?, ?, ?)";
		$this->db->query($qry, array($userid, $datasetid, $usernode->getId()));

		return $usernode;
	}

	function set_base_user_perms(&$usernode) {
		$CI =& get_instance();

		// set permissions on the users node

		// default is nothing
		$permobj = new Perm();
		$permobj->groupid = 0;
		$permobj->read = -1;
		$usernode->addPermSet($permobj);

		// user needs read
		$permobj = new Perm();
		$permobj->groupid = $usernode->getId();
		$permobj->read = 1;
		$permobj->create = 1;
		$usernode->addPermSet($permobj);

		// lec needs read
		$CI->load->helper('roles');
		$role_nodes = load_role_nodes($CI, $this->db);

		$lecnode = $role_nodes[3]->nodeid;
		$permobj = new Perm();
		$permobj->groupid = $lecnode;
		$permobj->read = 1;
		$permobj->edit = 1;
		$permobj->link = 1;
		$permobj->create = 1;
		$permobj->hist = 1;
		$usernode->addPermSet($permobj);
	}
}

?>