<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CSV helper
 * 
 * Load in a csv file into an array
 * 
 * will take the fist line as a header unless $noheader is set
 **/
if ( ! function_exists('load_csv_file'))
{
	function &load_csv_file($filename, $noheader = false)
	{
		
		if (!file_exists($filename))
		{
			show_error("Unable to load CSV file $filename");
			return;
		}
		
		$header = array();
		$seen_header = false;
		if ($noheader)
			$seen_header = true;
		
		$result = array();
		
		if (($handle = fopen($filename, "r")) !== FALSE) 
		{
			// got file open, read in as csv
			while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) 
			{
				
				// load in header of file
				if (!$seen_header)
				{
					$header = $data;
					$seen_header = true;
					continue;	
				}
				//print_p($data);
				
				$row = Array();
				foreach ($data as $id => $value)
				{
					if ($noheader)
					{
						$row->$id = $value;	
					} else {	
						if (array_key_exists($id, $header))
						{
							$fieldname = $header[$id];
							$row[$fieldname] = $value;	
						}
					}
				}
				$result[] = $row;
			}
			
			// close file when done
			fclose($handle);
		} else {
			// error opening file
			show_error("Error opening CSV file $filename");
			return;
		}
		
		return $result;
	}
}
