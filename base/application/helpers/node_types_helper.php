<?php

class NTypes {
  const YEAR = 1;
  const PROGRAMME = 2;
  const MODULE = 3;
  const SESSION = 4;
  const ACCREDITING_BODY = 5;
  const BODY_OUTCOME = 6;
  const PROGRAMME_OUTCOME = 7;
  const MODULE_OUTCOME = 8;
  const SESSION_OUTCOME = 9;
  const COLLECTION = 10;
  const RESOURCE = 11;
  const LEARNING_ACT = 12;
  const LEARNING_ACT_OUTCOME = 13;
  const STUDY_YEAR = 14;
  const TOPIC = 15;
  const TOPIC_OUTCOME = 16;
  const KV_PAIR = 100;
  const MAIN = 500;
  const GROUP = 501;
  const USER = 502;
}
