<?php

function ldap_authenticate($username, $password, &$email) {

	$CI =& get_instance();

	$ldapconfig['host'] = $CI->config->item('UONCM_LDAP_Host');
	$ldapconfig['port'] = $CI->config->item('UONCM_LDAP_Port');
	$ldapconfig['binduser'] = $CI->config->item('UONCM_LDAP_BindRDN');
	$ldapconfig['bindpw'] = $CI->config->item('UONCM_LDAP_Password');
	$ldapconfig['basedn'] = $CI->config->item('UONCM_LDAP_BaseDN');
	$ldapconfig['userprefix'] = $CI->config->item('UONCM_LDAP_UserPrefix');
	$ldapconfig['authrealm'] = $CI->config->item('UONCM_LDAP_AuthRealm');

	if ($username != "" && $password != "") {
		$ds = @ldap_connect($ldapconfig['host']);

		if (ldap_bind($ds, $ldapconfig['binduser'], $ldapconfig['bindpw'])) {
			$r = @ldap_search( $ds, $ldapconfig['basedn'], $ldapconfig['userprefix'] . $username);
			if ($r) {
				$result = @ldap_get_entries( $ds, $r);
				if ($result['count'] == 1) {
					if (@ldap_bind( $ds, $result[0]['dn'], utf8_encode($password)) ) {
						ldap_unbind($ds);
						return true;
					}
				}
			}
		}
	}
	return false;
}

function ldap_get_details($username) {

	$CI =& get_instance();

	$ldapconfig['host'] = $CI->config->item('UONCM_LDAP_Host');
	$ldapconfig['port'] = $CI->config->item('UONCM_LDAP_Port');
	$ldapconfig['binduser'] = $CI->config->item('UONCM_LDAP_BindRDN');
	$ldapconfig['bindpw'] = $CI->config->item('UONCM_LDAP_Password');
	$ldapconfig['basedn'] = $CI->config->item('UONCM_LDAP_BaseDN');
	$ldapconfig['userprefix'] = $CI->config->item('UONCM_LDAP_UserPrefix');
	$ldapconfig['authrealm'] = $CI->config->item('UONCM_LDAP_AuthRealm');

	if ($username != "") {
		$ds = @ldap_connect($ldapconfig['host']);

		if (ldap_bind($ds, $ldapconfig['binduser'], $ldapconfig['bindpw'])) {
			$r = @ldap_search( $ds, $ldapconfig['basedn'], $ldapconfig['userprefix'] . $username);
			if ($r) {
				$result = @ldap_get_entries( $ds, $r);
				if ($result['count'] == 1) {
					return $result;
				}
			}
		}
	}
	return false;
}