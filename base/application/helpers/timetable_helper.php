<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Helper functions for accessing timetabling system
 */

/**
 * Import sessions from the timetabling system
 * @param $curl
 * @param $baseid
 * @param $module_id
 * @param $academic_year
 * @param $tt_sessions_created
 * @return string JSON encoded result of query to Timetabling system
 */
function import_tt_sessions(&$caller, &$ci, $baseid, $module_id, $academic_year, &$tt_sessions_created) {
  $ci->load->helper('node');

  // Call the IMAT web service
  $apikey = $ci->config->item('UONCM_TimetableAPIKey');
  $url = $ci->config->item('UONCM_TimetableURL') . "/$academic_year/activities/module/$module_id/?apiKey=$apikey/";
  setup_curl($ci, $url);

  $res = $ci->curl->execute();

  if ($res === false) {
    show_error("Error fetching session data from timetable for  {$url}: " . $ci->curl->error_string);
  }

  $map_data = json_decode($res, true);

  // Be really sure we have some data
  if ($map_data != ''
      and isset($map_data['Activities'])
      and isset($map_data['Activities']['Module'])
      and isset($map_data['Activities']['Module']['Activity'])
      and count($map_data['Activities']['Module']['Activity']) > 0) {
    foreach ($map_data['Activities']['Module']['Activity'] as $t_session) {
      $lecturer = '';
      if (is_array($t_session['Staff']) and is_array($t_session['Staff']['Person'])) {
        if (isset($t_session['Staff']['Person']['Name'])) {
          $lecturer .= $t_session['Staff']['Person']['Name'];
        } else {
          foreach ($t_session['Staff']['Person'] as $person) {
            $lecturer .= $person['Name'] . ' / ';
          }
          $lecturer = rtrim($lecturer, " /");
        }
      }

      // Standard data
      $new_data = array();
      $new_data['type_obj'] = $ci->node_types->GetType(4);
      $new_data['type'] = 4;
      $new_data['start'] = $t_session['Date']['Year'] . '-' . str_pad($t_session['Date']['Month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($t_session['Date']['Day'], 2, '0', STR_PAD_LEFT) . ' ' . str_pad($t_session['StartTime']['Hours'], 2, '0', STR_PAD_LEFT) . ':' . str_pad($t_session['StartTime']['Minutes'], 2, '0', STR_PAD_LEFT) . ':00';
      $new_data['end'] = $t_session['Date']['Year'] . '-' . str_pad($t_session['Date']['Month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($t_session['Date']['Day'], 2, '0', STR_PAD_LEFT) . ' ' . str_pad($t_session['EndTime']['Hours'], 2, '0', STR_PAD_LEFT) . ':' . str_pad($t_session['EndTime']['Minutes'], 2, '0', STR_PAD_LEFT) . ':00';
      $new_data['staff'] = $lecturer;
      $new_data['title'] = $t_session['Description'];
      $new_data['code'] = $t_session['Code'];

      // Stuff that might be school specific in shared modules
      $found_shared = false;
      if (isset($t_session['Shared']) and is_array($t_session['Shared'])
          and isset($t_session['Shared']['School']) and is_array($t_session['Shared']['School'])) {
        foreach ($t_session['Shared']['School'] as $school) {
          if (isset($school['Module']) and is_array($school['Module']) and isset($school['Module']['Code'])) {
            if ($module_id == $school['Module']['Code'] and isset($school['Module']['Activity']) and is_array($school['Module']['Activity'])) {
              $new_data['ttguid'] = $school['Module']['Activity']['Guid'];
              $found_shared = true;
            }
          }
        }
      }
      if(!$found_shared) {
        $new_data['ttguid'] = $t_session['Guid'];
      }

      create_node($new_data, $baseid);
      $tt_sessions_created++;
    }
  }

  return $res;
}

/**
 * Get all the modules that have timetabled sessions
 * @return array All timetabled modules
 */
function get_tt_modules()
{
  $ci =& get_instance();

  return $ci->graph_db->findNodeFromAttr(NTypes::MODULE, 'ttsync', 1, true, 1000);
}

/**
 * Get the current timetable data for sessions in a timetabled module
 * @param  string $module_code Code (module ID) if the module to load
 * @param  int    $year        Year to query in the timnetabling system
 * @return array               Data for sessions in the module
 */
function get_tt_sessions_for_module($module_code, $year)
{
  $ci =& get_instance();
  $sessions = array();

  // Call the IMAT web service
  $apikey = $ci->config->item('UONCM_TimetableAPIKey');
  $url = $ci->config->item('UONCM_TimetableURL') . "/$year/activities/module/$module_code/?apiKey=$apikey/";
  setup_curl($ci, $url);

  $res = $ci->curl->execute();

  if ($res === false) {
    show_error("Error fetching session data from timetable for  {$url}: " . $ci->curl->error_string);
  }

  $map_data = json_decode($res, true);

  // Be really sure we have some data
  if ($map_data != ''
      and isset($map_data['Activities'])
      and isset($map_data['Activities']['Module'])
      and isset($map_data['Activities']['Module']['Activity'])
      and count($map_data['Activities']['Module']['Activity']) > 0) {
    foreach ($map_data['Activities']['Module']['Activity'] as $t_session) {
      $lecturer = '';
      if (is_array($t_session['Staff']) and is_array($t_session['Staff']['Person'])) {
        if (isset($t_session['Staff']['Person']['Name'])) {
          $lecturer .= $t_session['Staff']['Person']['Name'];
        } else {
          foreach ($t_session['Staff']['Person'] as $person) {
            $lecturer .= $person['Name'] . ' / ';
          }
          $lecturer = rtrim($lecturer, " /");
        }
      }

      // Standard data
      $session = array();
      $session['start'] = $t_session['Date']['Year'] . '-' . str_pad($t_session['Date']['Month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($t_session['Date']['Day'], 2, '0', STR_PAD_LEFT) . ' ' . str_pad($t_session['StartTime']['Hours'], 2, '0', STR_PAD_LEFT) . ':' . str_pad($t_session['StartTime']['Minutes'], 2, '0', STR_PAD_LEFT) . ':00';
      $session['end'] = $t_session['Date']['Year'] . '-' . str_pad($t_session['Date']['Month'], 2, '0', STR_PAD_LEFT) . '-' . str_pad($t_session['Date']['Day'], 2, '0', STR_PAD_LEFT) . ' ' . str_pad($t_session['EndTime']['Hours'], 2, '0', STR_PAD_LEFT) . ':' . str_pad($t_session['EndTime']['Minutes'], 2, '0', STR_PAD_LEFT) . ':00';
      $session['staff'] = $lecturer;
      $session['title'] = $t_session['Description'];
      $session['code'] = $t_session['Code'];

      // Stuff that might be school specific in shared modules
      $found_shared = false;
      if (isset($t_session['Shared']) and is_array($t_session['Shared'])
          and isset($t_session['Shared']['School']) and is_array($t_session['Shared']['School'])) {
        foreach ($t_session['Shared']['School'] as $school) {
          if (isset($school['Module']) and is_array($school['Module']) and isset($school['Module']['Code'])) {
            if ($module_code == $school['Module']['Code'] and isset($school['Module']['Activity']) and is_array($school['Module']['Activity'])) {
              $session['ttguid'] = $school['Module']['Activity']['Guid'];
              $found_shared = true;
            }
          }
        }
      }
      if(!$found_shared) {
        $session['ttguid'] = $t_session['Guid'];
      }

      $sessions[] = $session;
    }
  }

  return $sessions;
}

/**
 * Get the old guid for a timetable session
 * @param string $guid The current guid
 * @param int $year The previous year
 * @return boolean||string
 */
function get_tt_session_guid_old($guid, $year) {
  $ci = & get_instance();

  //  Build the URL for the IMAT web service
  $apikey = $ci->config->item('UONCM_TimetableAPIKey');
  $url = $ci->config->item('UONCM_TimetableURL') . "/$year/activity/$guid/?apiKey=$apikey";

  // Call the IMAT web service
  setup_curl($ci, $url);

  $response = $ci->curl->execute();
  $timetable = json_decode($response, true);

  if (is_array($timetable) && array_key_exists('guids', $timetable)
    && is_array($timetable['guids']) 
    && array_key_exists('guid', $timetable['guids'])
    && array_key_exists('value', $timetable['guids']['guid'])) {
    return $timetable['guids']['guid']['value'];
  }
  return false;
}

/**
 * Get an array of years with data from the timetable system
 * @return array
 */
function get_tt_years() {
  $ci = & get_instance();

  // Build the IMAT web service as url/1.1/timetabling/years
  $apikey = $ci->config->item('UONCM_TimetableAPIKey');
  $url = $ci->config->item('UONCM_TimetableURL') . "/years/?apiKey=$apikey";
  setup_curl($ci, $url);

  $response = $ci->curl->execute();
  $timetable = json_decode($response, true);

  if (is_array($timetable) && array_key_exists('Years', $timetable)) {
    return $timetable['Years']['Year'];
  }
  return array();
}
