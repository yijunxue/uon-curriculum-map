<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

class PermList 
{
	var $perms = array(
		'read' => array( 'name' =>'View node', 'help' => 'Can the node be viewed.'),
		'edit' => array( 'name' =>'Edit title and attributes', 'help' => 'Can the nodes attributes and title be changed.'),
		'link' => array( 'name' =>'Link to other nodes', 'help' => 'Can the node be linked to other nodes. The user will also require permission to link the target node.'),
		'create' => array( 'name' =>'Create children', 'help' => 'Can new children nodes be created from this node.'),
		'hist' => array( 'name' =>'View history', 'help' => 'Can the nodes history be viewed.'),
		'perms' => array( 'name' =>'Manage permissions', 'help' => 'Can the user manage the permissions of this node.'),
		'delete' => array( 'name' =>'Delete node', 'help' => 'Can the node be deleted.'),
		'clone' => array( 'name' =>'Clone node and children', 'help' => 'Can the node be cloned. If this is set, than any children of the node will also be able to be cloned.'),
		);
	function GetNames()
	{
		$out = array();
		foreach ($this->perms as $name => &$data)	
			$out[] = $name;
		return $out;
	}
}

class Perm
{
	var $nodeid;
	var $groupid;
	
	var $target = ''; // temp var, only used from perms view class
	
	var $from_date = 0;
	var $to_date = 0;
	
	/*var $apply_this = 0;
	var $apply_children = 0;*/

	static $pl = null;

	public function __construct()
	{
		if (!Perm::$pl)
			Perm::$pl = new PermList;
		
		foreach (Perm::$pl->perms as $name => &$data)
			$this->$name = 0;
	}

	public function AllowAll()
	{
		foreach (Perm::$pl->perms as $name => &$data)
			$this->$name = 1;
	}

	public function from_post(&$input)
	{
		//echo "From post<br>";
		$this->groupid = $input->post('groupid'); // group or user id, 0 for node default
		$this->origgroupid = $input->post('origgroupid'); // original group if reassigning permissions
		$this->target = $input->post('target'); // applies to
		$this->nodeid = $input->post('nodeid'); // base node
		
		
		// store dates if there
		$date = $input->post("date");
		if ($date == "range" || $date == "from")
			$this->from_date = strtotime($input->post("date_from"));
		if ($date == "range" || $date == "to")
			$this->to_date = strtotime($input->post("date_to"));
		
		foreach (Perm::$pl->perms as $name => &$data)
		{
			//echo "Getting p_$name<br>";
			$this->$name = $input->post("p_$name");
		}
		
		//print_p($this);
		
		//print_p(Perm::$pl);
	}
	
	public function getkey()
	{
		return $this->groupid;
		//return implode(",",array($this->groupid, $this->apply_this, $this->apply_children, $this->nodeid));
	}
	
	function init_for_root_node()
	{
		$this->nodeid = 1;	
		$this->groupid = 0;
		
		foreach (Perm::$pl->perms as $name => &$data)
			$this->$name = -1;
		
		$this->read = 1;

	}
	
	function describe()
	{
		$perm_txt = array();
		$pl = new PermList();
		foreach ($pl->perms as $name => &$data)
		{
			$val = $this->$name;
			if ($val == 0)
				$val = "<img style='position:relative;top:3px;' src='" .  asset_url() . "image/misc/delete.png'>";
			if ($val == -1)
				$val = "<img style='position:relative;top:3px;' src='" .  asset_url() . "image/clone/cross.png'>";
			if ($val == 1)
				$val = "<img style='position:relative;top:3px;' src='" .  asset_url() . "image/clone/tick.png'>";
			
			$perm_txt[] = ucwords($name) . " $val"	;
		}
		if ($this->from_date > 0)
			$perm_txt[] = "From " . nice_date($this->from_date);
		if ($this->to_date > 0)
			$perm_txt[] = "To " . nice_date($this->to_date);
		
		return $perm_txt;			
	}
	
	function AsXML()
	{
		$perm_txt = array();
		$pl = new PermList();
		foreach ($pl->perms as $name => &$data)
		{
			$val = $this->$name;
			if ($val == 0)
				$val = "<$name>inherit</$name>";
			if ($val == -1)
				$val = "<$name>deny</$name>";
			if ($val == 1)
				$val = "<$name>allow</$name>";
			
			$perm_txt[] = $val;
		}
		if ($this->from_date > 0)
			$perm_txt[] = "<from raw_date='{$this->from_date}'>" . nice_date($this->from_date) . "</from>";
		if ($this->to_date > 0)
			$perm_txt[] = "<to raw_date='{$this->to_date}'>" . nice_date($this->to_date) . "</to>";
		
		return $perm_txt;			
	}
}