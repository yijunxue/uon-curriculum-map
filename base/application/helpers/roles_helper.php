<?php

/**
 * Check if a user should have a relationship with a particular node. Add or remove role relationships as required
 * @param $usernode
 * @param $ci
 */
function reset_roles($usernode, $ci, $check_array = '') {

	foreach ($ci->graph_db->active_dataset->nodes->role_group as $roleid => $rolenodeid)
	{
		$rolenode = $ci->graph_db->loadNode($rolenodeid);
		if ($ci->dx_auth->is_role($roleid, false, true, $check_array))
		{
			if (!$usernode->isRelatedTo($rolenode)) {
				$usernode->addRelate($rolenodeid);
			}
		} elseif ($usernode->isRelatedTo($rolenode)) {
			$usernode->removeRelate($rolenode);
		}
	}
}

	
function load_role_nodes($ci, $db)
{
	$datasetid = $ci->graph_db->active_dataset->id;
	$qry = "SELECT * FROM role_node WHERE dataset = ?";
	$query = $db->query($qry, array($datasetid));
	$role_nodes = array();

	foreach ($query->result() as $row)
	{
		$role_nodes[$row->role] = $row;
	}

	return $role_nodes;
}
