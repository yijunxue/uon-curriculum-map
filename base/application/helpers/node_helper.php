<?php
/**
 * Perform the actual node creation, including link back to its parent node
 *
 * @param mixed $data Array of data from create
 *
 */
function create_node(&$data, $baseid = -1, $type = 0)
{
  // called with a data array containing several things from the create function

  $ci =& get_instance();

  // store node
  $node_type = $data['type'];
  $node_title = $data['title'];

  // create the node
  $node = $ci->graph_db->createNode($node_title, $node_type, $baseid);

  // set up its attributes based on the node type
  foreach($data['type_obj']->GetAttributes() as $attr)
  {
    if (isset($data[$attr->id])) {
      $node->setAttribute($attr->id, $data[$attr->id], $attr->type);
    }
  }

  // save the node being created
  $node->save();

  return $node->GetID();
}