<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

function mysql_datetime()
{
	date_default_timezone_set('Europe/London');
	return date("Y-m-d H:i:s", time());
}

function get_referer()
{
	return ( ! isset($_SERVER['HTTP_REFERER']) OR $_SERVER['HTTP_REFERER'] == '') ? '' : trim($_SERVER['HTTP_REFERER']);
}

function gen_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}

function elipsis($string, $length, $stopanywhere=false) {
	//truncates a string to a certain char length, stopping on a word if not specified otherwise.
	if (strlen($string) > $length) {
		//limit hit!
		$string = substr($string,0,($length -3));
		if ($stopanywhere) {
			//stop anywhere
			$string .= '...';
		} else{
			//stop on a word.
			$string = substr($string,0,strrpos($string,' ')).'...';
		}
	}
	return $string;
}

function nice_date($mysqldate, $long = true, $timeifrecent = 3)
{
	//echo $mysqldate."<br>";
	if (is_string($mysqldate))
	{
		$time =	strtotime($mysqldate);
	} else {
		$time = $mysqldate;
	}

	if ($long)
	{
		$result = date("F j, Y", $time);
	} else {
		$result = date("d/m/y", $time);
	}

	if ($timeifrecent > 0)
	{
		$now = time();
		$diff = abs($time - $now);
		if ($diff < 86400 * $timeifrecent) // last 3 days, show time too
		{
			if ($long)
			{
				$result .= date(", g:i a", $time);
			} else {
				$result .= date(", h:i", $time);
			}
		}
	}

	return $result;
}
