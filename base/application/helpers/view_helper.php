<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

class ViewTools
{
	static $onlylink = array();
	static function NodeTools(&$node, $tools = 1, $mainnode = null)
	{
		// $tools:
		/*
			1: link, edit
			2: link, edit, delete, perms
			3: edit
			4: edit, delete, perms
		*/

		$CI =& get_instance();

		if (is_object($tools))
		{
			$output = "<div class='small_tools'>";
			$output .= ViewTools::ToolsFromXML($tools, $node, $mainnode);
			$output .= "</div>";

			return $output;
		}

		$base = asset_url();
		$linkurl = site_url("link/" . $node->getId());
		$editurl = site_url("node/edit/" . $node->getId());
		$delurl = site_url("node/delete/" . $node->getId());
		$perurl = site_url("perms/" . $node->getId());

		$can_edit = $node->can_edit();
		$can_link = $node->can_link();
		$can_delete = false;
		$can_perm = false;

		if ($tools > 1)
		{
			$can_delete = $node->can_delete();
			$can_perm = $node->can_perms();
		}

		if ($tools == 4)
		{
			$can_link = false;
		}

		if ($tools == 3)
		{
			$can_edit = $node->can_edit();
			$can_link = false;
			$can_delete = false;
			$can_perm = false;
		}

		if (!$can_edit && !$can_link && !$can_delete && !$can_perm) return "";

		$output = "<div class='small_tools'>";
		if ($can_link)
			$output .= "<a href='{$linkurl}' class='hasTooltip_bl' title='Link item to other items'><img src='{$base}image/node/link.png' alt='Link'></a>\n";
		if ($can_edit)
			$output .= "<a href='{$editurl}' class='hasTooltip_bl' title='Edit this item'><img src='{$base}image/node/edit.png' alt='Edit'></a>\n";
		if ($can_delete)
			$output .= "<a href='{$delurl}' class='hasTooltip_bl' title='Delete this item'><img src='{$base}image/misc/delete.png' alt='Delete'></a>\n";
		if ($can_perm)
			$output .= "<a href='{$perurl}' class='hasTooltip_bl' title='Change items permissions'><img src='{$base}image/node/perms.png' alt='Permissions'></a>\n";
		$output .= "</div>";

		return $output;
	}

	static function ToolsFromXML($section, $node, $mainnode = null)
	{
		$CI =& get_instance();

		$output = "";
		$base = asset_url();
		foreach ($section->tool as $tool)
		{
			$type = (string)$tool->attributes()->type;
			$icon = (string)$tool->attributes()->icon;
			$title = (string)$tool->attributes()->title;
			$help = (string)$tool->attributes()->help;
			$icontype = (string)$tool->attributes()->icontype;

			$ok = false;
			$url = "";
			$confirm = "";

			switch ($type)
			{
				case 'link':
					if ($node->can_link())
					{
						$ok = true;
						$link_tmpl = (string)$tool->attributes()->tmpl;
						if ($link_tmpl)
						{
							$url = site_url("link/" . $node->getID() . "/" . $link_tmpl);
						} else {
							$url = site_url("link/" . $node->getID());
						}

					}
					break;
				case 'unlink':
					if ($mainnode->can_link())
					{
						$ok = true;
						$url = site_url("node/unlink/" . $mainnode->getID() . "/" . $node->getID());
						if ($tool->attributes()->confirm)
							$confirm = (string)$tool->attributes()->confirm;
					}
					break;
				case 'create':
					if ($node->can_create())
					{
						$ok = true;
						$ct = (string)$tool->attributes()->create_type;
						$node_type = $CI->node_types->GetType($ct);


						$append = "";

						if ($tool->preset) // do we have any preset values specified? If so append them to the url
						{
							$append = array();
							foreach ($tool->preset as $preset)
							{
								$key = (string)$preset->attributes()->id;
								$value = (string)$preset->attributes()->value;
								$append[] = "$key=$value";
							}
							if (count($append) > 0)
							{
								$append = "?" . implode("&",$append);
							} else {
								$append = "";
							}
						}

						$url = site_url("node/create/" . $node->getID() . "/" . $node_type->gdbid . "/1" . $append);
					}
					break;
				case 'kv_pair':
					if ($node->can_create())
					{
						$ok = true;
						$template = (string)$tool->attributes()->template;
						if ($template)
							$template = "/" . $template;
						$url = site_url("view/kv/" . $node->getID() . $template);
					}
					break;
				case 'order':
					if ($node->can_create())
					{
						$ok = true;
						$nodetype = (string)$tool->attributes()->nodetype;
						$url = site_url("view/order/" . $node->getID() . "/" . $nodetype);
					}
					break;
				case 'edit':
					if ($node->can_edit())
					{
						$ok = true;
						$url = site_url("node/edit/" . $node->getID());
					}
					break;
				case 'delete':
					if ($node->can_delete())
					{
						$ok = true;
						$url = site_url("node/delete/" . $node->getID());
					}
					break;
				case 'perms':
					if ($node->can_perms())
					{
						$ok = true;
						$url = site_url("perms/" . $node->getID());
					}
					break;
				case 'import':
					if ($node->can_link())
					{
						$ok = true;
						$link_tmpl = (string)$tool->attributes()->tmpl;
						if ($link_tmpl)
						{
							$url = site_url($link_tmpl . '/import/' . $node->getID());
						} else {
							$ok = false;
						}

					}
					break;
				case 'synchronise':
					if ($node->can_edit() && $node->getAttributeValue('ttsync')) {
						// The user is able to edit and the node allows timetable synchronsation.
						$ok = true;
						$url = site_url("timetable/preview/" . $node->getID());
					}
			}

			if ($ok)
			{
				if ($confirm)
					$confirm = " onclick='return confirm(\"$confirm\");'";
				if ($icontype == "button")
				{
					$output .= "<a href='$url' class='hasTooltip button' title='$help' $confirm><img src='{$base}image/$icon.png' alt='$title'>$title</a>\n";
				} else {
					$output .= "<a href='$url' class='hasTooltip' title='$help' $confirm><img src='{$base}image/$icon.png' alt='$title'></a>\n";
				}
			}
		}

		return $output;
	}

	static function LinkNode($relnode, $node, $layer = null)
	{
		if (!$node->canLink($relnode))
			return;

		if ($node->GetId() == $relnode->GetId())
			return;

		if (count(ViewTools::$onlylink) > 0)
		{
			$type = $relnode->getTypeObj()->id;
			if (!array_key_exists($type, ViewTools::$onlylink))
				return;
		}



		if ($node->getParent()->getID() == $relnode->getID() ||
			$relnode->getParent()->getID() == $node->getID())
		{
?>
	<div class='small_tools'>
		<?php echo ViewTools::LinkWeight($relnode, $node, $layer); ?>
		<img class="link_img link_img_<?php echo $relnode->GetId(); ?>" src='<?php echo asset_url(); ?>image/link/parchild.png'>
	</div>
<?php
			return;
		}
?>
	<div class='small_tools'>
		<?php echo ViewTools::LinkWeight($relnode, $node, $layer); ?>
		<a href="#" onclick='ToggleLink(<?php echo $relnode->GetId(); ?>);return false;'>
			<img class="link_img link_img_<?php echo $relnode->GetId(); ?>" src='<?php echo asset_url(); ?>image/link/dounlink.png'>
		</a>
	</div>
<?php
	}

	static function LinkWeight($relnode, $node, $layer)
	{
		if ($layer && ($layer->weight == "node" || $layer->can_weight))
		{
			$w = $relnode->getWeightTo($node);

			if (!$relnode->isRelatedTo($node))
				$w = 1;

			echo "<div class='link_weight link_weight_". $relnode->GetId()." hasTooltip_bl' title='".round($w * 100,0)."% weight<br>Click to change'";
			echo " id='link_weight_". $relnode->GetId()."'";
			echo " onclick='WeightLink(this)' ";
			if (!$relnode->isRelatedTo($node))
				echo " style='display: none;' ";

			echo ">";
			echo WeightImage($w);
			echo "</div>";
		}
	}
}

class TreeLayer
{
	var $path = "";

	function GetHeader()
	{
		if (!$this->header)
			return "";

		if ($this->header_raw)
			return $this->header;

		return "<div class='rel_title'>" . $this->header . "</div>";
	}
}

function WeightImage($weight)
{
	$weight = $weight * 10;
	$weight = round($weight,0);
	$weight = $weight * 10;
	return "<img src='" . asset_url() . "image/weight/$weight.png' width='16' height='16'>";
}

/**
 * Return a dummy Graph_DB_Attribute object based on the title of the node identified by the
 * GUID. This is so that it can be formatted by the default formatter of the Attribute type
 * @param  string 							$guid 	GUID of group node holding the title and description of the group
 * @return Graph_DB_Attribute       		Dummy Graph_DB_Attribute object
 */
function groupAttribFromGuid($guid) {
	$CI =& get_instance();
	$title = '';

	$node = $CI->graph_db->loadNodeByGuid($guid);
	if ($node) {
    $string = $node->getTitle();
    if ($node->getAttributeValue('desc') != '') {
      $string .= '~' . $node->getAttributeValue('desc');
    }
		$title = new Graph_DB_Attribute(8, 'group', GDB_Attrib_Type::_KVPair, $string);
	} else {
		// Prevent fatal errors when there is an invalid node.
		$title = new Graph_DB_Attribute(8, 'group', GDB_Attrib_Type::_KVPair, 'Error: Invalid node');
	}

	return $title;
}