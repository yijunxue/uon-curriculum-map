<?php

function GetNodeHeadingText(&$node, $show_title = true, $longtitle = false)
{
	$max_head_length = 50;

	if (!$show_title) {
		$title = $node->getAttributeValue('code');
		if ($title == '') $show_title = true;
	}

	if ($show_title) {
		$title = $node->getTitleDisp($longtitle);
		if (!$title)
		{
			$title = $node->getTitleDisp(true);
		}
		$title = strip_tags($title, '<h2>');

		if (strlen($title) > $max_head_length)
			$title = substr($title, 0, $max_head_length) . "...";
	}

	return $title;
}
