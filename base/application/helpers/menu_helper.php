<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

function setup_main_menu($menu)
{
	$menu->AddMenuItem("Main Menu");
	$menu->AddMenuItem("Home","main/year");
	$CI =& get_instance();
	if ($CI->dx_auth->is_logged_in())
		$menu->AddMenuItem("My Account","auth");
	$menu->AddMenuItem("Search","node/find");
	$menu->AddMenuItem("Overview","view");
	//$menu->AddMenuItem("Orphans","node/orphans");
	$CI =& get_instance();
	if ($CI->dx_auth->is_admin())
		$menu->AddMenuItem("Admin","admin");
}

function setup_admin_menu($menu)
{
	$menu->AddSubMenuItem("Admin Menu");
	$menu->AddSubMenuItem("Overview","admin");
	$menu->AddSubMenuItem("User Management","adminusers");
	$menu->AddSubMenuItem("Dev Tools","admindev");
	$menu->AddSubMenuItem("Data Tools","admindata");
}

function _format_bytes($a_bytes, $round = 3)
{
	if ($a_bytes < 1024) {
		return $a_bytes .' B';
	} elseif ($a_bytes < 1048576) {
		return _sf_round($a_bytes / 1024, $round) .' KB';
	} elseif ($a_bytes < 1073741824) {
		return _sf_round($a_bytes / 1048576, $round) . ' MB';
	} elseif ($a_bytes < 1099511627776) {
		return _sf_round($a_bytes / 1073741824, $round) . ' GB';
	}
}


function _sf_round($value, $sf)
{
	$sf = $sf - 2;
	if ($value < 1)
		return round($value,$sf + 2);
	if ($value < 10)
		return round($value,$sf + 1);
	if ($value < 100)
		return round($value,$sf);
	if ($value < 1000)
		return round($value,$sf - 1);
	if ($value < 10000)
		return round($value,$sf - 2);
}