<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

class Pagination
{
	var $prevname = "&lt; Prev";
	var $nextname = "Next &gt;";
	var $adjacents = 3;
	var $spacer = "...";
	
	var $template = "<a href='%%PAGE%%'>%%NAME%%</a>";
	var $prevnexttmpl = "<a href='%%PAGE%%'>%%NAME%%</a>";
	var $prevnextdistmpl = "<span class=\"disabled\">%%NAME%%</span>";
	var $curtmpl = "<span class=\"current\">%%NAME%%</span>";
	
	function SetTemplate($template = "", $prevnexttmpl = "", $prevnextdistmpl = "", $curtmpl = "")
	{
		if ($template)
			$this->template = $template;
		if ($prevnexttmpl)
			$this->prevnexttmpl = $prevnexttmpl;
		if ($prevnextdistmpl)
			$this->prevnextdistmpl = $prevnextdistmpl;
		if ($curtmpl)
			$this->curtmpl = $curtmpl;
	}
	
	function OutputPages($_count, $_curpage, $_perpage)
	{		
		// How many adjacent pages should be shown on each side?

		$total_pages = $_count;
		$limit = $_perpage; 								//how many items to show per page
		$page = $_curpage;
	
		if ($page == 0) $page = 1;					//if no page var is given, default to 1.
		$prev = $page - 1;							//previous page is page - 1
		$next = $page + 1;							//next page is page + 1
		$lastpage = ceil($total_pages/$limit);		//lastpage is = total pages / items per page, rounded up.
		$lpm1 = $lastpage - 1;						//last page minus 1
	
		/* 
			Now we apply our rules and draw the pagination object. 
			We're actually saving the code to a variable in case we want to draw it more than once.
		*/
		$pagination = "";
		
		if($lastpage > 1)
		{	
			$pagination .= "<div class=\"pagination\">";
			//previous button
			
			if ($page > 1) 
				$pagination.= $this->prevnext_link($prev, true);
			else
				$pagination.= $this->prevnext_link(-1, true);	
		
			//pages	
			if ($lastpage < 7 + ($this->adjacents * 2))	//not enough pages to bother breaking it up
			{	
				for ($counter = 1; $counter <= $lastpage; $counter++)
				{
					if ($counter == $page)
						$pagination.= $this->current_link($counter);
					else
						$pagination.= $this->page_link($counter);					
				}
			}
			elseif($lastpage > 5 + ($this->adjacents * 2))	//enough pages to hide some
			{
				//close to beginning; only hide later pages
				if($page < 1 + ($this->adjacents * 2))		
				{
					for ($counter = 1; $counter < 4 + ($this->adjacents * 2); $counter++)
					{
						if ($counter == $page)
							$pagination.= $this->current_link($counter);
						else
							$pagination.= $this->page_link($counter);					
					}
					$pagination.= $this->spacer;
					$pagination.= $this->page_link($lpm1);
					$pagination.= $this->page_link($lastpage);		
				}
				//in middle; hide some front and some back
				elseif($lastpage - ($this->adjacents * 2) > $page && $page > ($this->adjacents * 2))
				{
					$pagination.= $this->page_link(1);
					$pagination.= $this->page_link(2);
					$pagination.= $this->spacer;
					for ($counter = $page - $this->adjacents; $counter <= $page + $this->adjacents; $counter++)
					{
						if ($counter == $page)
							$pagination.= $this->current_link($counter);
						else
							$pagination.= $this->page_link($counter);					
					}
					$pagination.= $this->spacer;
					$pagination.= $this->page_link($lpm1);
					$pagination.= $this->page_link($lastpage);		
				}
				//close to end; only hide early pages
				else
				{
					$pagination.= $this->page_link(1);
					$pagination.= $this->page_link(2);
					$pagination.= $this->spacer;
					for ($counter = $lastpage - (2 + ($this->adjacents * 2)); $counter <= $lastpage; $counter++)
					{
						if ($counter == $page)
							$pagination.= $this->current_link($counter);
						else
							$pagination.= $this->page_link($counter);					
					}
				}
			}
		
			//next button
			if ($page < $counter - 1) 
				$pagination.= $this->prevnext_link($next, false);
			else
				$pagination.= $this->prevnext_link(-1, false);
			$pagination.= "</div>\n";		
		}
		
		return $pagination;
	}
	
	function prevnext_link($page, $is_prev = false)
	{
		$name = $this->nextname;
		if ($is_prev) $name = $this->prevname;
	
		if ($page == -1)
			return $this->parse_template($page, $name, $this->prevnextdistmpl);

		return $this->parse_template($page, $name, $this->prevnexttmpl);
	}
	
	function current_link($page)
	{
		return $this->parse_template($page, $page, $this->curtmpl);
	}
	
	function page_link($page)
	{
		return $this->parse_template($page, $page, $this->template);
	}
	
	function parse_template($page, $name, $template)
	{
		$template = str_replace("%%NAME%%", $name, $template);
		$template = str_replace("%%PAGE%%", $page, $template);
		
		return $template;
	}
}