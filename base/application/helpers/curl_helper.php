<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Helper for CURL functions
 *
 */

/**
 * Set up the curl options and authentication to access a web service
 *
 * @param $module
 * @param $session
 */
function setup_curl(&$controller, $uri) {
  $controller->curl->create($uri);

  $controller->curl->options(array(
    CURLOPT_TIMEOUT => 15,  // Seconds
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_HTTPHEADER => array('Accept: application/json', 'Content-Type: application/json'),
    CURLOPT_USERAGENT => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13',
    CURLOPT_SSL_VERIFYHOST => false,
    CURLOPT_SSL_VERIFYPEER => false,
    CURLOPT_SSLVERSION, 3
  ));
}

function curl_login(&$controller) {
  $controller->curl->http_login('currmap', 'm4pp1ng');
}
