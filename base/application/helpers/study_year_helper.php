<?php
function get_modules_by_studyyear($node, $order = '', &$all_modules = array()) {
  $CI =& get_instance();

  $studyyears = $node->getRelations("studyyear");
  $CI->graph_db->sort_nodes($studyyears, "order|title");

  $mods_by_year = array();

  foreach ($studyyears as &$studyyear)
  {
    $year_modules = array('id' => $studyyear->getId(), 'title' => $studyyear->getTitle(), 'modules' => array());

    $modules = $studyyear->getRelations("module");

    if ($order != '') {
      $CI->graph_db->sort_nodes($modules, $order);
    }

    foreach ($modules as &$module)
    {
      $all_modules[$module->getId()] = $module;
      $year_modules['modules'][] = $module;
    }

    $mods_by_year[] = $year_modules;
  }

  // Find all modules not attached to a studyyear, first get all related modules.
  $modules = $node->getRelations("module");

  if ($order != '') {
    $CI->graph_db->sort_nodes($modules, $order);
  }

  $title_output = false;

  $year_modules = array('id' => -1, 'title' => 'none', 'modules' => array());
  foreach ($modules as &$module)
  {
    if (array_key_exists($module->getId(), $all_modules)) continue; // Found in a studyyear already.

    $all_modules[$module->getId()] = $module;
    $year_modules['modules'][] = $module;
  }
  if (count($year_modules['modules']) > 0)
  {
    $mods_by_year[] = $year_modules;
  }

  return $mods_by_year;
}
