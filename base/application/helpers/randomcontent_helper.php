<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */


class RandContent
{
	var $nextwords = array();
	var $words = array();
	var $wcount = 0;
	var $nextcounts = array();
	
	function __construct()
	{
		$data = explode(" ",file_get_contents("random/text.txt"));
		$this->data = array();
		
		for($i = 0 ; $i < count($data)-1 ; $i++)
		{
			$word = $data[$i];
			$nextword = $data[$i+1];
			
			$this->words[] = $word;
			
			if (!array_key_exists($word,$this->nextwords))
			{
				$this->nextwords[$word] = array();
			}	
			
			$this->nextwords[$word][] = $nextword;
		}	
		
		$this->wcount = count($this->words);
		foreach ($this->nextwords as $word => &$data)
		{
			$this->nextcounts[$word] = count($data);	
		}
	}
	
	function string($count)
	{
		global $table;
		
		$words = array();
		//$word = array_rand($this->data);
		
		$word = $this->words[mt_rand(0,$this->wcount - 1)];
		$words[] = strtoupper(substr($word,0,1)) . substr($word,1);
		
		for ($i = 1 ; $i < $count ; $i++)
		{
			$word = $this->nextwords[$word][mt_rand(0,$this->nextcounts[$word]-1)];
			$words[] = $word;
		}
		
		return implode(" ",$words);
	}
	
	function sentance()
	{
		global $table;
		$count = mt_rand(4,12);
		
		$words = array();
		//$word = array_rand($this->data);
		
		$word = $this->words[mt_rand(0,$this->wcount - 1)];
		$words[] = strtoupper(substr($word,0,1)) . substr($word,1);
		
		for ($i = 1 ; $i < $count ; $i++)
		{
			$word = $this->nextwords[$word][mt_rand(0,$this->nextcounts[$word]-1)];
			$words[] = $word;
		}
		
		return implode(" ",$words) . ".";
	}
	
	function paragraph()
	{
		$output = array();
		$count = mt_rand(4,8);
		for ($i = 0 ; $i < $count ; $i++)
		{
			$output[] = $this->sentance();	
		}	
		return implode(" ",$output);
	}
	
	function text()
	{
		$output = array();
		$count = mt_rand(1,3);
		for ($i = 0 ; $i < $count ; $i++)
		{
			$output[] = $this->paragraph();	
		}	
		return "<p>".implode("</p><p>",$output) . "</p>";;		
	}
	
	function genRandomString($length = 10) {
		$characters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		$string = "";    

		for ($p = 0; $p < $length; $p++) {
			$string .= $characters[mt_rand(0, strlen($characters)-1)];
		}
		return $string;
	}
	
	function date($start,$end)
	{
		$end = strtotime($end);
		$start = strtotime($start);
		$diff = $end - $start;
		$date = mt_rand(0, $diff);
		
		$time = $date + $start;
		
		return date("Y-m-d",$time);
		
		/*$year = mt_rand(1990,2020);
		$month = mt_rand(1,12);
		$day = mt_rand(1,28);
		return sprintf("%04d-%02d-%02d",$year, $month, $day);*/
	}
		
	function time($start, $end, $nearestmin = 1)
	{
		$hour = mt_rand($start,$end);
		$minute = mt_rand(0,59);
		$minute = floor($minute / $nearestmin) * $nearestmin;
		$value = sprintf("%02d:%02d",$hour,$minute);
		return $value;
	}
}
