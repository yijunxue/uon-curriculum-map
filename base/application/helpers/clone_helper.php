<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Helper functions for cloning datasets
 */


/**
 * Get node id given dataset and user id
 *
 * @param int $source_id source dataset
 * @param int $userid source user id
 * @param object data set model
 * @return int target node id, or -1 if none were found
 */
function get_dataset_user_node($source_id, $userid, $db) {
    $db->select('nodeid');
    $entries = $db->get_where('user_node', array('dataset' => $source_id, 'userid' => $userid));
    if (count($entries) != 1) {
        return -1;
    }

    $row = $entries->row();
    if (empty($row)) {
        return -1;
    }
    return $row->nodeid;
}

/**
 * Get user_node records for the given dataset
 * @param  integer $source_id Database ID of the source dataset
 * @param object data set model
 * @return array              Array of user_node records
 */
function get_user_nodes($source_id, $db) {
    $db->select('userid, nodeid');
    $entries = $db->get_where('user_node', array('dataset' => $source_id));
    return $entries->result_array();
}

/**
 * Get database ID of a dataset givcen its table name
 * @param  string $source_table  Table name
 * @param object data set model
 * @return integer               Database ID
 */
function get_id_from_table($source_table, $db) {
    $db->select('id');
    $entries = $db->get_where('datasets', array('table' => $source_table));

    if (count($entries) != 1) {
        return false;
    }

    $row = $entries->row();
    return $row->id;
}

/**
 * Get graph_node records for the given dataset
 * @param  integer $source_table Database ID of the source dataset
 * @param object data set model
 * @return array              Array of user_node records
 */
function get_role_group_nodes($source_table, $db) {
    $db->select('id');
    $entries = $db->get_where('gdb_' . $source_table . '_graph_node', array('type' => 501, 'deleted' => 0));
    return $entries->result_array();
}

/**
 * Get graph_node programme records for the given dataset
 * @param  integer $source_table Database ID of the source dataset
 * @param object data set model
 * @return array              Array of programme records
 */
function get_programme_nodes($source_table, $db) {
    $db->select('id');
    $entries = $db->get_where('gdb_' . $source_table . '_graph_node', array('type' => 2, 'deleted' => 0));
    return $entries->result_array();
}

/**
 * Get graph_node session record for the given dataset
 * @param  integer $source_table Database ID of the source dataset
 * @param object data set model
 * @return array|false   Array of user_node records, or false if there is not exactly one record.
 */
function get_session_node($source_table, $db) {
    $db->select('id');
    $entries = $db->get_where('gdb_' . $source_table . '_graph_node', array('type' => 1));
    if (count($entries) != 1) {
        return false;
    }

    $row = $entries->row();
    return $row->id;
}