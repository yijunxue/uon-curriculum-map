<?php ob_clean(); ?>
	<?php $data['title'] = $heading; ?>
	<?php $this->ci =& get_instance()?>
	<?php $this->ci->output->set_output(""); ?>
    <?php $this->ci->load->view('templates/header', $data)?>
	<div class='error round'><img src='<?php echo asset_url(); ?>image/misc/error.png'>
        <?php echo $message; ?>
		<?php //echo dumpStack(); ?>
	</div>
    <?php echo $this->ci->load->view('templates/footer'); ?>
<?php exit; ?>