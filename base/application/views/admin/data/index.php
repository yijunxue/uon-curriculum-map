
<script>

</script>

<h4>Current active Dataset : <?php echo $this->graph_db->active_dataset->name; ?></h4>
<?php echo $this->tabs->startPane("ao"); ?>
<?php echo $this->tabs->addPanel("Current Dataset","stats"); ?>
<?php echo $this->tabs->addPanel("Node Stats","nodes"); ?>
<?php echo $this->tabs->addPanel("Datasets","sets"); ?>
<?php echo $this->tabs->addPanel("Clear Database","clear"); ?>
<?php echo $this->tabs->addPanel("Migrate Version","migrate"); ?>

<?php echo $this->tabs->endHead(); ?>

<?php echo $this->tabs->startPanel("stats"); ?>
<button onclick='loadstats();'>Refresh</button>
<div id="dataset_stats">
<?php $this->load->view('admin/dataset/dataset_size', $this->data); ?>
</div>
<?php echo $this->tabs->endPanel(); ?>

<?php echo $this->tabs->startPanel("nodes"); ?>
<button onclick='loadnodestats();'>Refresh</button>
<div id="node_stats">
<?php $this->load->view('admin/dataset/nodedetails', $this->data); ?>
</div>
<?php echo $this->tabs->endPanel(); ?>

<?php echo $this->tabs->startPanel("sets"); ?>
<?php $this->load->view('admin/dataset/datasets', $this->data); ?>
<?php echo $this->tabs->endPanel(); ?>

<?php echo $this->tabs->startPanel("clear"); ?>
<?php $this->load->view('admin/data/reset_confirm', $this->data); ?>
<?php echo $this->tabs->endPanel(); ?>

<?php echo $this->tabs->startPanel("migrate"); ?>
<?php $this->load->view('admin/data/migrate_confirm', $this->data); ?>
<?php echo $this->tabs->endPanel(); ?>

<?php echo $this->tabs->endPane(); ?>
