<p>Are you sure you want to upgrade to the next version? </p><br>
<p>This will alter the structure of your database tables and fields and/or add/remove/update existing data<br><br></p>
<div class="debug_main round">
	<div class="debug_head">
		<div class="debug_text">
			<div><?php echo anchor('migrate/index', 'Yes, upgrade to the next version now','id="migrate_data_confirm"'); ?></div>
			<div id='clear_complete' style='display: none;margin-top:16px;'>Database has been migrated</div>
		</div>
		<div class="clear"></div>
	</div>
</div>

<script>

</script>