<div class="debug_main round">
	<div class="debug_head">
		<div>
			<div id="migratestatus">
				<?php
				$this->load->helper('form');
				echo form_open();
				?>
				<div id="migrateconfirmation">
					<p>
						<?php echo form_label('Current Version: ' . $this->data['currentversion']); ?>
					</p>
					<br><br>
					<p>Are you sure you want to migrate the version?</p><br>
					<p><strong>Risks you are taking if you migrate to a different migration version:</strong></p>
					<ul>
					<li>Your system might stop working if your current codebase is not compatible with version you migrate to</li>
					<li>Please make a backup of your database first as you might not be able to retrieve any lost data</li>
					<li>This will alter the structure of your database tables and fields and/or add/remove/update existing data</li>
					</ul>
				</div>
				<br>
				<p>
					<?php echo form_label('Version: ', 'version'); ?>
					<?php echo form_dropdown('version', $this->data['versions'], $this->data['currentversion'], 'id="versionselector"'); ?>
				</p>
				<p>
					<?php
					echo form_label('Do you understand the risks?', 'confirm_migrate');
					$checkbox = array('name' => 'confirm_migrate', 'id' => 'migrationconfirmed', 'checked' => false);
					echo form_checkbox($checkbox);
					?>
				</p>
				<br>
				<p>
					<?php
					echo form_label('Have you backed up your data?', 'confirm_backup'); 
					$checkbox = array('name' => 'confirm_backup', 'id' => 'backupconfirmed', 'checked' => false);
					echo form_checkbox($checkbox);
					?>
				</p>
				<p>
					<?php echo form_submit('migratenow', 'Migrate now'); ?>
				</p>
				<div id='clear_complete' style='display: none;margin-top:16px;'>Database has been migrated</div>
				<div class="clear"></div>
			</div>
		</div>
	</div>
</div>

<script>
$("#migratestatus").submit(function(event) {
		event.preventDefault();
		var version;
		var migrationurl;
		var confirmed;
		var backedup;
		version = $("#versionselector").val();
		confirmed = $('#migrationconfirmed').is(':checked');
		backedup = $('#backupconfirmed').is(':checked');
		migrationurl = "<?php echo base_url() . 'admindata/migrate/yes/'; ?>" + version;

		if (confirmed && backedup) {
				$.ajax({url: migrationurl, success: function(result) {
								$("#migrateconfirmation").html(result).animate({top: "-=25px"}, 500);
								$("#migrateconfirmation").html(result).animate({top: "0px"}, 500);
								$('#migrationconfirmed').prop('checked', false);
								$('#backupconfirmed').prop('checked', false);
						}});
		} else {
				$("#migrateconfirmation").html('<strong>You need to backup your data confirm that you understand the risks.</strong>').animate({top: "-=20px"}, 500);
				$("#migrateconfirmation").html('<strong>You need to backup your data confirm that you understand the risks.</strong>').animate({top: "0px"}, 500);
		}
});
</script>