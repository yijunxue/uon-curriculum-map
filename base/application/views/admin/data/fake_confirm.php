Create a fake course, along with associated modules, sessions, resources and outcomes.<br />
<br />
<div class="debug_main round">
	<div class="debug_head">
		<div class="debug_text">
			<div id='fake_link'><?php echo anchor('admindata/fake/yes', 'Create a fake course', 'id="fake_start"'); ?></div>
			<div id='fake_inprogress' style='display: none'>Creating fake course, please wait... (this will take a couple of minutes)</div>
			<div id='fake_complete' style='display: none;margin-top:16px;'>Course Created - <span id='fake_result'></span></div>
		</div>
		<div class="clear"></div>
	</div>
</div>
