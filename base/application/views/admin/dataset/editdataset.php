<script type="text/javascript">
  $(function() {
    $("input[type=color]").spectrum({
      showInput: true,
      showInitial: true,
      showPalette: true,
      showPaletteOnly: true,
      showSelectionPalette: false,
      preferredFormat: "hex",
      palette: [
        ['#ddd9c3', '#c6d9f0', '#dbe5f1', '#f2dcdb', '#ebf1dd', '#e5e0ec', '#dbeef3', '#fdeada'],
        ['#c4bd97', '#8db3e2', '#b8cce4', '#e5b9b7', '#d7e3bc', '#ccc1d9', '#b7dde8', '#fbd5b5'],
        ['#938953', '#548dd4', '#95b3d7', '#d99694', '#c3d69b', '#b2a2c7', '#92cddc', '#fac08f'],
        ['#366092', '#76923c', '#31859b']
      ]
    });
  });
</script>

<?php echo $this->head->ValidationErrors(); ?>
<table class="form" summary="Edit dataset">
  <tr>
    <th>Table prefix <span class="req_field"> *</span>:</th>
    <td><?php echo form_input(array('name' => 'table', 'id' => 'table', 'type' => 'text'), $this->data['table']); ?></td>
  </tr>
  <tr>
    <th>Name <span class="req_field"> *</span>:</th>
    <td><?php echo form_input(array('name' => 'name', 'id' => 'name', 'type' => 'text'), $this->data['name']); ?></td>
  </tr>
  <tr>
    <th>Description <span class="req_field"> *</span>:</th>
    <td><?php echo form_textarea(array('name' => 'description', 'id' => 'description'), $this->data['description']); ?></td>
  </tr>
  <tr>
    <th>Colour <span class="req_field"> *</span>:</th>
    <td><?php echo form_input(array('name' => 'colour', 'id' => 'colour', 'type' => 'color'), $this->data['color']); ?></td>
  </tr>
  <tr>
    <th>Admin only?</th>
    <td><?php echo form_checkbox(array('name' => 'adminonly', 'id' => 'adminonly'), '1', $this->data['adminonly']); ?></td>
  </tr>
  <tr>
    <th>Is Default?</th>
    <td><?php echo form_checkbox(array('name' => 'isdefault', 'id' => 'isdefault'), '1', $this->data['isdefault']); ?></td>
  </tr>
</table>