<table class='niceround'>
<tr>
	<th width="100">Data</th>
	<th width="100">Count</th>
	<th width="100">Index Size</th>
	<th width="100">Data Size</th>
	<th width="100">Total Size</th>
</tr>
<?php
outputSet($stats, "graph_node", "Nodes");
outputSet($stats, "graph_attr", "Attributes");
outputSet($stats, "graph_rel", "Relations");
outputSet($stats, "graph_hist", "History");
outputSet($stats, "Totals", "Totals","th");
?>
</table>

<?php 
function outputSet(&$stats, $key, $title, $el = "td")
{
	$data = $stats[$key]; 
?>
	<tr>
		<<?php echo $el; ?>><?php echo $title; ?></td>
		<<?php echo $el; ?>><?php echo number_format($data['rows']); ?></<?php echo $el; ?>>
		<<?php echo $el; ?>><?php echo _format_bytes($data['index']); ?></<?php echo $el; ?>>
		<<?php echo $el; ?>><?php echo _format_bytes($data['data']); ?></<?php echo $el; ?>>
		<<?php echo $el; ?>><?php echo _format_bytes($data['total']); ?></<?php echo $el; ?>>
	</tr>
<?php } 
?>