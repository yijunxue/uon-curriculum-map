<?php foreach ($this->graph_db->datasets as $setid => &$dataset) : ?>
<div style="background-color: <?php echo $dataset->color; ?>" class='dataset round'>
	<?php $data = $this->graph_db->getStats($setid); ?>


	<div style='float:right;'>Nodes: <b><?php echo $data['graph_node']['rows']; ?></b>,
	Size: <b><?php echo _format_bytes($data['Totals']['total']); ?></b></div>

	<?php if ($this->graph_db->active_dataset->table == $setid): ?>
		<img src='<?php echo asset_url(); ?>image/misc/star.png' style='float: left;top: -2px;position: relative;'>
	<?php else : ?>
		<img src='<?php echo asset_url(); ?>image/misc/blank.png' style='float: left;top: -2px;position: relative;'>
	<?php endif;?>&nbsp;
	<a href="<?php echo "$setid/auth/dataset/" ?>"><?php echo $dataset->name ?></a>

	<?php
	$image = '<img src="' . asset_url() . 'image/clone/edit.png" >';
	echo anchor("admindata/editdataset/$dataset->table", $image . 'Edit', array('style' => 'padding-left: 40px;'));  
	?>
	<?php
	if ($dataset->isdefault) {
		echo '<span style="padding-left: 40px;"><strong>(Default dataset)</strong><span>';
	} ?>
</div>
<?php endforeach; ?>
<div class="round"><?php echo anchor("admindata/createdataset", "Create new dataset") ?></div>
