<script type="text/javascript">
  $(function() {
    $("input[type=color]").spectrum({
      showInput: true,
      showInitial: true,
      showPalette: true,
      showPaletteOnly: true,
      showSelectionPalette: false,
      preferredFormat: "hex",
      palette: [
        ['#ddd9c3', '#c6d9f0', '#dbe5f1', '#f2dcdb', '#ebf1dd', '#e5e0ec', '#dbeef3', '#fdeada'],
        ['#c4bd97', '#8db3e2', '#b8cce4', '#e5b9b7', '#d7e3bc', '#ccc1d9', '#b7dde8', '#fbd5b5'],
        ['#938953', '#548dd4', '#95b3d7', '#d99694', '#c3d69b', '#b2a2c7', '#92cddc', '#fac08f'],
        ['#366092', '#76923c', '#31859b']
      ]
    });
  });
</script>

<?php echo $this->head->ValidationErrors(); ?>
<table class="form" summary="Create a new dataset">
  <tr>
    <th>Source dataset:</th>
    <td><?php echo form_dropdown('source_ds', $datasets, set_value('source_ds')); ?></td>
  </tr>
  <tr>
    <th>Table prefix <span class="req_field"> *</span>:</th>
    <td><?php echo form_input(array('name' => 'table', 'id' => 'table', 'type' => 'text', 'value' => set_value('table'))); ?></td>
  </tr>
  <tr>
    <th>Name <span class="req_field"> *</span>:</th>
    <td><?php echo form_input(array('name' => 'name', 'id' => 'name', 'type' => 'text', 'value' => set_value('name'))); ?></td>
  </tr>
  <tr>
    <th>Description <span class="req_field"> *</span>:</th>
    <td><?php echo form_textarea(array('name' => 'description', 'id' => 'description'), set_value('description')); ?></td>
  </tr>
  <tr>
    <th>Colour <span class="req_field"> *</span>:</th>
    <td><?php echo form_input(array('name' => 'colour', 'id' => 'colour', 'type' => 'color', 'value' => set_value('colour'))); ?></td>
  </tr>
  <tr>
    <th>Node defnition <span class="req_field"> *</span>:</th>
    <td><?php echo form_input(array('name' => 'nodedef', 'id' => 'nodedef', 'type' => 'text', 'value' => set_value('nodedef', $nodedef))); ?></td>
  </tr>
  <tr>
    <th>Admin only?</th>
    <td><?php echo form_checkbox(array('name' => 'adminonly', 'id' => 'adminonly'), '1', set_checkbox('adminonly', '1')); ?></td>
  </tr>
  <tr>
    <th>Is Default?</th>
    <td><?php echo form_checkbox(array('name' => 'isdefault', 'id' => 'isdefault'), '1', set_checkbox('isdefault', '1')); ?></td>
  </tr>
</table>