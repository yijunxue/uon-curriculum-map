<?php echo $this->tabs->startPane("ao"); ?>
<?php echo $this->tabs->addPanel("Debug","debug"); ?>
<?php echo $this->tabs->endHead(); ?>

<?php echo $this->tabs->startPanel("debug"); ?>
<table class='niceround'>
<tr><td>Current Debug Status:&nbsp;&nbsp;&nbsp;&nbsp;</td>
<th>
<a href='#' id='debug_stats'>
Debug <?php echo $this->dx_auth->showdebug() ? "ON" : "OFF"; ?></a>
</th>
</tr>
<tr><td>Current Template Status:&nbsp;&nbsp;&nbsp;&nbsp;</td>
<th>
<a href='#' id='debug_template'>
<?php echo $this->dx_auth->hidetemplates() ? "Dont Use Templates" : "Use Templates"; ?></a>
</th>
</tr>
</table>
<?php echo $this->tabs->endPanel(); ?>

<?php echo $this->tabs->endPane(); ?>
<div class='clear'></div>
