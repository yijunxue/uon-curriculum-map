<style>
.niceround th
{
	width: auto;
}
.niceround
{
	width: 100%;
}
</style>
	<?php
	// Show reset password message if exist
	if (isset($reset_message))
		echo $reset_message;

	// Show error
	echo validation_errors();

	$this->table->set_heading('',
		'<a href="#" onclick="setSort(\'username\');return false;">Username</a>',
		'<a href="#" onclick="setSort(\'email\');return false;">Email</a>',
		'<a href="#" onclick="setSort(\'fullname\');return false;">Full Name</a>',
		'<a href="#" onclick="setSort(\'yearofstudy\');return false;">Year Of Study</a>',
		'<a href="#" onclick="setSort(\'role_name\');return false;">Role</a>',
		//'<a href="#" onclick="setSort(\'banned\');return false;">Banned</a>',
		'<a href="#" onclick="setSort(\'last_login\');return false;">Last login</a>',
		'<a href="#" onclick="setSort(\'created\');return false;">Created</a>',
		'Node');

	foreach ($users as $user)
	{
		$banned = ($user->banned == 1) ? 'Yes' : 'No';

		$role_desc = $user->role_name;
		if ($role_desc == "none") $role_desc = "None";
		if (array_key_exists($role_desc, $role_lookup))
		{
			$role_desc = $role_lookup[$role_desc];
		}

		$last_login_date = strtotime($user->last_login);
		if ($last_login_date == '' or $last_login_date == '-62169984000') {
			$last_login = 'Never';
		} else {
			$last_login = date('Y-m-d', $last_login_date);
		}


		$this->table->add_row(
			form_checkbox('checkbox_'.$user->id, $user->id),
			$user->username,
			$user->email,
			$user->fullname,
			$user->yearofstudy,
			$role_desc,
			//$banned,
			$last_login,
			date('Y-m-d', strtotime($user->created)),
			$user->nodeid > 0 ? "<a href='" . site_url("view/" . $user->nodeid) . "' target='_blank'>Node</a>" : ''
		);
	}

	$attributes = array('id' => 'users_form');
	echo form_open($this->uri->uri_string(), $attributes);

	//echo "<a href='" . site_url('auth/create') . "' class='button'>Create User</a>";

	$search = array(
		'name'	=> 'filter_search',
		'id'	=> 'filter_search',
		'maxlength'	=> 80,
		'size'	=> 30,
		'value'	=> set_value('filter_search')
		);

	$sort = array(
		'name'	=> 'sort',
		'id'	=> 'sort',
		'maxlength'	=> 80,
		'size'	=> 30,
		'value'	=> set_value('sort'),
		'type' => 'hidden'
		);

	$sort_dir = array(
		'name'	=> 'sort_dir',
		'id'	=> 'sort_dir',
		'maxlength'	=> 80,
		'size'	=> 30,
		'value'	=> set_value('sort_dir'),
		'type' => 'hidden'
		);

	$page_input = array(
		'name'	=> 'page_no',
		'id'	=> 'page_no',
		'maxlength'	=> 80,
		'size'	=> 30,
		'value'	=> set_value('page_no'),
		'type' => 'hidden'
		);

	// search for user table
	?>

	<br />
	<?php
	echo "<div class='clear'></div>";
	echo "<div style='font-size:110%;float:left;position:relative;top:4px;width:200px;'>Filter:&nbsp;&nbsp;</div>";

	echo form_input($search);
	echo form_dropdown('filter_role', $rolesany, set_value('filter_role', 'any'), 'id="filter_role"');
	echo form_submit('do_filter', 'Filter', 'class="button"');
	echo form_submit('reset', 'Reset', 'class="button" onclick="resetForm();return false;"');

	echo "<div class='clear' style='height:6px;'></div>";
	echo "<div style='font-size:110%;float:left;position:relative;top:4px;width:200px;'>Modify Selected Users:&nbsp;&nbsp;</div>";
	//echo form_submit('ban', 'Ban user', 'class="button"');
	//echo form_submit('unban', 'Unban user', 'class="button"');
	echo form_submit('reset_pass', 'Reset password', 'class="button"');


	echo form_submit('change_role', 'Change Role To', 'class="button"');
	echo form_dropdown('role', $roles, set_value('role'));


	echo $this->table->generate();

	echo $pages;

	echo form_input($page_input);
	echo form_input($sort);
	echo form_input($sort_dir);

	echo form_close();
	echo "<div class='clear' style='height:1px;'>&nbsp;</div>";

	?>

<script>
function setOffset(offset)
{
	$('#page_no').val(offset);

	$('#users_form').submit();
}

function setSort(field)
{
	if ($('#sort').val() == field)
	{
		if ($('#sort_dir').val() == 'asc')
		{
			$('#sort_dir').val('desc');
		} else {
			$('#sort_dir').val('asc');
		}
	} else {
		$('#sort').val(field);
		$('#sort_dir').val('asc');
	}

	$('#users_form').submit();
}

function resetForm()
{
	$('#filter_search').val("");
	$('#filter_role').val("any");
	$('#page_no').val("1");
	$('#sort').val("");
	$('#sort_dir').val("");

	$('#users_form').submit();
}


</script>