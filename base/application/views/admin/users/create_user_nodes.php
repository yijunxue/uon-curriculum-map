<html>
	<head><title>Create User Nodes</title></head>
	<body>
	<?php  				
		// Show error
		echo validation_errors();
		
		echo form_open($this->uri->uri_string());

		echo "<p>Enter a list of usernames for which to create nodes, one per line</p>";
		
		echo form_textarea(array('name' => 'usernames', 'cols' => '30', 'rows' => 5));

		echo '<br>';

		echo form_submit('create', 'Create');
		
		echo form_close();
					
	?>
	</body>
</html>