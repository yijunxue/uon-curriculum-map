<?php
if ($type == 'module') {
?>
<p>Created <?php echo $no_mod_outcomes ?> module learning outcomes, <?php echo $no_sessions ?> sessions, <?php echo $no_outcomes ?> session learning outcomes, <?php echo $no_mod_session ?> module to session mappings and <?php echo $no_mod_td ?> module to Tomorrow's Doctors mappings.</p>
<?php
} elseif ($type == 'modulentt') {
?>
<p>Created <?php echo $no_mod_outcomes ?> module learning outcomes, <?php echo $no_sessions ?> sessions, <?php echo $no_sess_outcomes ?> session learning outcomes, <?php echo $no_learning_acts ?> learning activities, <?php echo $no_act_outcomes ?> learning activity learning outcomes, <?php echo $no_mod_session ?> module to session mappings and <?php echo $no_mod_td ?> module to Tomorrow's Doctors mappings.</p>
<?php
} elseif ($type == 'degree') {
  ?>
<p>Created <?php echo $no_deg_outcomes ?> programme learning outcomes.</p>
<?php
} elseif ($type == 'fixshared') {
  ?>
<p>Updated <?php echo $no_sess_updated ?> sessions. Added <?php echo $no_outcomes ?> session learning outcomes and <?php echo $no_mod_session ?> module to session mappings</p>
<?php
} else {
?>
<p>Created <?php echo $no_body_outcomes ?> Tomorrow's Doctors learning outcomes.</p>
<?php
}
if (isset($json_raw)) {
?>
<pre>
  <?php echo $json_raw ?>
</pre>
<?php
}
?>