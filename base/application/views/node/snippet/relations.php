<table class="relations" width="100%">
<?php 
// need to allow the + to expand and display the listed nodes relations

$seenlist = explode(",",$path);
$seen = array();
foreach ($seenlist as $seenid)
{
	$seen[$seenid] = 1;
}

foreach ($rels as $nodetype => $relnodes)
{
	if ($nodetype == 0) continue;
	$type = $this->node_types->GetType($nodetype);
	
	$header = false;
	// need to find the type from the id
foreach ($relnodes as &$relnode): ?>
	<?php $subid = mt_rand(); ?>
	<?php if ($relnode->getId() == $baseid) continue; ?>
	<?php if (array_key_exists($relnode->getId(), $seen)) continue; ?>
	<?php if (!$header): ?>
		<tr><td colspan=2><?php echo $type->getName(); ?></td></tr>
		<?php $header = true; ?>
	<?php endif; ?>
<tr>
<td class='td_expand'>
				<a href="#" onclick="return LoadSubNodes(<?php echo $relnode->getId(); ?>, <?php echo $subid; ?>, '<?php echo $path; ?>');">
					<img src='<?php echo asset_url(); ?>image/misc/add.png' id='image_<?php echo $subid; ?>'>
				</a>
			</td>
			<td>
				<?php if ($node): ?>
				<div class="relations_link">
					<?php if ($node->GetId() == $relnode->GetId()): ?>
						<img src='<?php echo asset_url(); ?>image/misc/link_na.png' id='image_<?php echo $subid; ?>'>
					<?php elseif ($node->isRelatedTo($relnode)): ?>
						<a href="#" onclick='return ToggleLink(<?php echo $node->GetId(); ?>, <?php echo $relnode->GetId(); ?>);'>
							<img class="link_img_<?php echo $relnode->GetId(); ?>" src='<?php echo asset_url(); ?>image/misc/link_yes.png'>
						</a>
					<?php else: ?>
						<a href="#" onclick='return ToggleLink(<?php echo $node->GetId(); ?>, <?php echo $relnode->GetId(); ?>);'>
							<img class="link_img_<?php echo $relnode->GetId(); ?>" src='<?php echo asset_url(); ?>image/misc/link_no.png'>
						</a>
					<?php endif; ?>
				</div>
				<?php endif; ?>
<div class="relations_title">
					<a href='<?php echo site_url('view/node/' . $relnode->getId()); ?>'>
						<?php echo $relnode->getTitleDisp(); ?>	
					</a>
				</div>
				<div class='sub_rels' style="display:none" id='subrels_<?php echo $subid; ?>'></div>
			</td>
		</tr>
<?php
	endforeach;
}
?>
</table>