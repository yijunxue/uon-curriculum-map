<?php echo $this->tabs->startPane("ao"); ?>
<?php echo $this->tabs->addPanel("Reports","reports"); ?>
<?php echo $this->tabs->addPanel("API","api"); ?>
<?php echo $this->tabs->addPanel("Custom Report","cust_report"); ?>
<?php echo $this->tabs->addPanel("Custom API","cust_api"); ?>
<?php echo $this->tabs->addPanel("Help","help"); ?>
<?php echo $this->tabs->endHead(); ?>

<?php echo $this->tabs->startPanel("reports"); ?>
<?php $this->load->view('node/report/reports', $this->data); ?>
<?php echo $this->tabs->endPanel(); ?>

<?php echo $this->tabs->startPanel("api"); ?>
<?php $this->load->view('node/report/api', $this->data); ?>
<?php echo $this->tabs->endPanel(); ?>

<?php echo $this->tabs->startPanel("cust_api"); ?>
<?php $this->load->view('node/report/cust_api', $this->data); ?>
<?php echo $this->tabs->endPanel(); ?>

<?php echo $this->tabs->startPanel("cust_report"); ?>
<?php $this->load->view('node/report/cust_report', $this->data); ?>
<?php echo $this->tabs->endPanel(); ?>

<?php echo $this->tabs->startPanel("help"); ?>
<?php $this->load->view('node/report/help', $this->data); ?>
<?php echo $this->tabs->endPanel(); ?>

<?php echo $this->tabs->endPane(); ?>