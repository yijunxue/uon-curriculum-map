<?php echo form_input(array('name' => 'sourceid', 'id' => 'sourceid', 'type' => 'hidden', 'value' => '')); ?>
<?php echo form_input(array('name' => 'return', 'id' => 'return', 'type' => 'hidden', 'value' => set_value('return', get_referer()))); ?>

<table width="100%" class="niceround">
<tr>
	<th valign="top">
		Node:
	</th>
	<td>
		<?php echo $node->getTypeObj()->name; ?> - <?php echo $node->getTitleDisp(); ?>
	</td>
</tr>
<?php if ($has_children): ?>
	<tr id='children_list'>
		<th valign="top">
			Also will be deleted:
		</th>
		<td>
	<?php
	$subdata['rels'] = $children;
	$subdata['baseid'] = 0;
	$subdata['node'] = 0;
	$subdata['path'] = 0;

	// uses the list children from the permission code
	$this->load->view('perms/snippet/list_children',$subdata);
	?>
		</td>
	</tr>
<?php endif; ?>
<tr>
	<th valign="top">
		Are you sure:
	</th>
	<td>
		<?php echo form_checkbox("areyousure", "yes", 0, 'id="areyousure"'); ?><label for="areyousure">Are you sure you want to delete this node?</label>
	</td>
</tr>
</table>
