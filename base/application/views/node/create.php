<?php echo $this->head->ValidationErrors(); ?>
<?php echo form_input(array('name' => 'sourceid', 'id' => 'sourceid', 'type' => 'hidden', 'value' => $baseid,'maxlength' => '12', 'size' => '12', 'style' => '')); ?>
<?php echo form_input(array('name' => 'what', 'id' => 'what', 'type' => 'hidden', 'value' => '')); ?>
<?php echo form_input(array('name' => 'return', 'id' => 'return', 'type' => 'hidden', 'value' => set_value('return', get_referer()))); ?>

<Table>
<?php $hide = ''; if ($nochange) $hide = 'style="display:none;"'; ?>
<tr <?php echo $hide; ?>>
	<td>Node Type :</td>
	<td><?php echo form_dropdown('node_type',$types, $type, $type_js); ?></td>
</tr>
<?php if ($type > 0): ?>
<?php $typeobj = $this->node_types->getType($type); ?>
	<tr><td>Title <?php if (!$typeobj->no_title_req) :?><span class="req_field"> *</span><?php endif; ?> :</td><td> <?php echo form_input(array('name' => 'title', 'id' => 'title', 'value' => set_value('title'),'maxlength' => '250', 'size' => '50', 'style' => '')); ?></td></tr>

	<?php // output the node type fields here ?>
	<?php foreach($type_obj->GetAttributes() as $attr): ?>
		<?php if ($attr->nocreate) continue; ?>

		<?php
		if (array_key_exists($attr->id, $_GET))
		{
			$value = set_value($attr->id, $_GET[$attr->id]);
		} else {
			$value = set_value($attr->id, $attr->GetDefault());
		}

		if ($attr->hidden)
		{
			echo form_input(array('name' => $attr->id, 'id' => $attr->id, 'type' => 'hidden', 'value' => $value));
			continue;
		}

		?>
	<tr>
		<td valign="top"><?php echo $attr->OutputHeader(); ?> :</td>
		<td><?php echo $attr->OutputField($value, $basenode, true); ?></td>
	</tr>
	<?php endforeach; ?>

<?php endif; ?>
</table>

<script>
jQuery(document).ready(function () {
<?php foreach($type_obj->GetAttributes() as $attr): ?>
<?php echo $attr->OutputJS(set_value($attr->id, $basenode->getAttribute($attr->id))); ?>
<?php endforeach; ?>
});

</script>