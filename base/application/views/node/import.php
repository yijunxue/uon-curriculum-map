<div style="padding:4px;">
<div>To bulk import data, you will need to create a CSV file with a header row. 1 item will be created for each additional row in your CSV file. The following columns are valid in your CSV file: </div>
<ul>
	<li><b>title</b> - Items title</li>

<?php foreach ($nodetype->attrs as &$attr): ?>
	<?php if ($attr->nocreate) continue; ?>
	<?php if ($attr->type == 20) continue; ?>
	<li><b><?php echo $attr->id; ?></b> - <?php echo $attr->title; ?></li>
<?php endforeach; ?>
</ul>

<div>&nbsp;</div>
Please Select a CSV File: <input type="file" name="csvfile">
<div>&nbsp;</div>
</div>