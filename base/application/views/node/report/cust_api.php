<?php
$attributes = array('id' => 'cust_api', 'target' => '_new', 'enctype' => "multipart/form-data");
echo form_open("api/$nodeid/custom", $attributes);					
?>
<h3>Test custom API XML template:</h3>
<Table class="niceround">
<tr>
	<th>Upload XML File: </th>
	<td><input type="file" name="xml_file"></td>
</tr>
<tr>
	<th colspan="2" style="text-align: center;color:red"> - or - </th>
</tr>	
<tr>
	<th>Enter XML: </th>
	<td><textarea rows="10" cols="80" name="xml"></textarea></td>
</tr>	
<tr>
	<td colspan="2" align="center"><input type="submit" value="Test XML" class="button"/><input type="reset" value="Reset" class="button"/></td>	
</tr>
</table>
<?php echo form_close(); ?>