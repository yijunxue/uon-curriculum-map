<?php if (count($items) > 0) : ?>
<h3>The following API interfaces are available for this item:</h3>
	
<table width="100%" class="niceround">
	<tr>
		<th style="width:250px !important;">Name</th>
		<th style="width:auto !important;" >Description</th>
		<th style="width:100px !important;">View</th>
	</tr>
	
<?php foreach ($items as $item): ?>
	<tr>
		<td nowrap><span title="<?php echo $item->id; ?>" class="hasTooltip"><?php echo $item->title; ?></span></td>
		<td><?php echo $item->description; ?></td>
		<td>
			<a href='<?php echo site_url('api/' . $node->GetID() . '/' . $item->id); ?>' target='_blank'>As XML</a>
			<a href='<?php echo site_url('api/json/' . $node->GetID() . '/' . $item->id); ?>' target='_blank'>As JSON</a>
			</td>
	</tr>
<?php endforeach; ?>
</table>
	
<?php else: ?>
<h3>No API interfaces are available for this item.</h3>
<?php endif; ?>