<?php if (count($reports) > 0) : ?>
<h3>The following Reports are available for this item:</h3>
<p>If a CSV version is not available for a report then the CSV link will display the standard output.</p>
	<table width="100%" class="niceround">
	<tr>
	<th style="width:250px !important;">Name</th>
	<th style="width:auto !important;" >Description</th>
	<th style="width:100px !important;">View</th>
	</tr>

<?php foreach ($reports as $item): ?>
	<tr>
		<td nowrap><span title="<?php echo $item->id; ?>" class="hasTooltip"><?php echo $item->title; ?></span></td>
		<td><?php echo $item->description; ?></td>
<td>
			<a href='<?php echo site_url('report/index/' . $node->GetID() . '/' . $item->id); ?>' target='_blank'>View</a>
			<a href='<?php echo site_url('report/raw/' . $node->GetID() . '/' . $item->id); ?>' target='_blank'>Raw</a>
			<?php if (property_exists($item, 'csv')) { ?>
			<a href='<?php echo site_url('report/csv/' . $node->GetID() . '/' . $item->id); ?>' target='_blank'>CSV</a>
			<?php } ?>
</td>
</tr>
<?php endforeach; ?>
</table>
<?php else: ?>
<h3>No Reports are available for this item.</h3>
<?php endif; ?>