<?php echo $this->head->ValidationErrors(); ?>
<?php $typeobj = $node->getTypeObj(); ?>
<?php
echo form_input(array('name' => 'return', 'id' => 'return', 'type' => 'hidden', 'value' => set_value('return', get_referer())));
$title_attrs = array('name' => 'title', 'id' => 'title', 'value' => set_value('title', $node->getTitle()),'maxlength' => '250', 'size' => '100', 'style' => '');
$title_label = 'Title';

// Change output slightly for User node
if ($typeobj->gdbid == 502) {
  $title_attrs['readonly'] = 'readonly';
  $title_attrs['class'] = 'readonly';
  $title_label = 'Username';
}
?>

<table class='niceround'>
	<tr>
		<th><?php echo $title_label; if (!$typeobj->no_title_req) :?><span class="req_field"> *</span><?php endif; ?> :</th>
		<td><?php echo form_input($title_attrs); ?></td>
	</tr>

	<?php // output the node type fields here ?>
	<?php foreach($type_obj->GetAttributes() as $attr): ?>
	<?php if ($attr->noedit) continue;
	
		$value = set_value($attr->id, $node->getAttribute($attr->id));
		if ($attr->hidden)
		{
			echo form_input(array('name' => $attr->id, 'id' => $attr->id, 'type' => 'hidden', 'value' => $value));
			continue;
		}
		
	 ?>
	<tr>
		<th><?php echo $attr->OutputHeader(); ?> :</th>
		<td><?php echo $attr->OutputField($value, $node); ?></td>
	</tr>
	<?php endforeach; ?>

</table>

<script>
jQuery(document).ready(function () {
<?php foreach($type_obj->GetAttributes() as $attr): ?><?php echo $attr->OutputJS(set_value($attr->id, $node->getAttribute($attr->id))); ?><?php endforeach; ?>
	$('#title').focus();
});
</script>