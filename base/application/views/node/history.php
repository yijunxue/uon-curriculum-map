<table width="100%" class="niceround">
<?php foreach ($history as &$hist): ?>
	<tr>
	<th valign="top">
			<?php echo GDB_Hist_Action::string_nice($hist->action); ?><br />
			<?php echo nice_date($hist->when, false); ?><br />
			By <?php echo $this->dx_auth->getUser($hist->user)->username; ?><br />
</th>
		<td><?php $hist_action = new GDB_Hist_Action(); echo $hist_action->DescribeShort($hist, false, ", "); ?></td>
</tr>
<?php endforeach; ?>
</table>
