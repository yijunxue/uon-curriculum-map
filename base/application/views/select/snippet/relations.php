<table class="relations" width="100%">
<?php 

// get rel counts using quick method
$ids = array();
foreach ($rels as $nodetype => $relnodes)
{
	foreach ($relnodes as &$relnode)
	{
		$ids[] = $relnode->getID();
	}
}

$rel_counts = $this->graph_db->GetRelCounts($ids, GDB_Rel::ChildOf);

// need to allow the + to expand and display the listed nodes relations
$seenlist = explode(",",$path);
$seen = array();
foreach ($seenlist as $seenid)
{
	$seen[$seenid] = 1;
}

foreach ($rels as $nodetype => $relnodes)
{
	$type = $this->node_types->GetType($nodetype);
	
	$header = false;
	
	
	// skip certain types we dont want to display
	if ($type->getId() == "root") continue;
	if ($type->getId() == "kv_pair") continue;
	if ($type->getId() == "perm_group") continue;
	if ($type->getId() == "perm_user") continue;
	if ($type->getId() == "perm_main") continue;
	
	// need to find the type from the id
foreach ($relnodes as &$relnode): ?>
	<?php $subid = mt_rand(); ?>
	<?php if (array_key_exists($relnode->getId(), $seen)) continue; ?>
	<?php if (!$header): ?>
		<tr><td colspan=2><div class='rel_title'><?php echo $type->getName(); ?></div></td></tr>
		<?php $header = true; ?>
	<?php endif; ?>
<tr>
<td class='td_expand'>
				<?php if (array_key_exists($relnode->getId(), $rel_counts) && $rel_counts[$relnode->getId()]['total'] > 0): ?>
				<a href="#" onclick="return LoadSubNodes(<?php echo $relnode->getId(); ?>, <?php echo $subid; ?>, '<?php echo $path; ?>');">
					<img src='<?php echo asset_url(); ?>image/misc/add.png' id='image_<?php echo $subid; ?>'>
				</a>
				<?php endif; ?>
			</td>
			<td>
				<div class="relations_title">
				<?php if (array_key_exists($relnode->getType(), $target_types)): ?>
					<a href='#' onclick='return ChooseNode(<?php echo $relnode->getId(); ?>);'>
						<?php echo $relnode->getTitleDisp(); ?>	
					</a>
				<?php else: ?>
					<?php echo $relnode->getTitleDisp(); ?>	
				<?php endif; ?>
				</div>
				<div class='sub_rels' style="display:none" id='subrels_<?php echo $subid; ?>'></div>
			</td>
		</tr>
<?php
	endforeach;
}
?>
</table>