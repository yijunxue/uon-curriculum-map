<table class="relations" width="100%">
<?php 
// need to allow the + to expand and display the listed nodes relations

foreach ($nodes as &$relnode): ?>
	<?php $subid = mt_rand(); ?>
	<tr>
	<td class='td_expand'>
				<a href="#" onclick="return LoadSubNodes(<?php echo $relnode->getId(); ?>, <?php echo $subid; ?>, <?php echo $path; ?>);">
					<img src='<?php echo asset_url(); ?>image/misc/add.png' id='image_<?php echo $subid; ?>'>
				</a>
			</td>
			<td>
				<div class="relations_title">
				<?php if (array_key_exists($relnode->getType(), $target_types)): ?>
					<a href='#' onclick='return ChooseNode(<?php echo $relnode->getId(); ?>);'>
						<?php echo $relnode->getTitleDisp(); ?>	
					</a>
				<?php else: ?>
					<?php echo $relnode->getTitleDisp(); ?>	
				<?php endif; ?>
				</div>
				<div class='sub_rels' style="display:none" id='subrels_<?php echo $subid; ?>'></div>
			</td>
		</tr>
<?php endforeach; ?>
</table>