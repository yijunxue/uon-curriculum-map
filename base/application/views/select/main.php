<?php

if (count($target_types) > 1)
	echo "You can pick an item of one of the following types: '" . implode("', '",$target_types) ."'<br>";

echo $this->tabs->startPane("choose_node");


if ($sourceid) echo $this->tabs->addPanel("Browse", "browse");
echo $this->tabs->addPanel("Search", "search");
echo $this->tabs->addPanel("Courses", "courses");
echo $this->tabs->addPanel("Accrediting Bodies", "bodys");
echo $this->tabs->addPanel("Year", "year");
echo $this->tabs->endHead();



echo $this->tabs->startPanel("courses");
$subdata['nodes'] = $year_node->getRelations("course");
$subdata['baseid'] = $year_node->getId();
$subdata['node'] =& $year_node;
$subdata['path'] = $year_node->getId();
$subdata['target_type'] = $source_node->getType();
$this->load->view('select/snippet/nodes',$subdata);
echo $this->tabs->endPanel();


echo $this->tabs->startPanel("bodys");
$subdata['nodes'] = $year_node->getRelations("body");
$subdata['baseid'] = $year_node->getId();
$subdata['node'] =& $year_node;
$subdata['target_type'] = $source_node->getType();
$this->load->view('select/snippet/nodes',$subdata);
echo $this->tabs->endPanel();


echo $this->tabs->startPanel("year");
$subdata['nodes'] = array($year_node);
$subdata['baseid'] = $year_node->getId();
$subdata['node'] =& $year_node;
$subdata['path'] = $year_node->getId();
$subdata['target_type'] = $source_node->getType();
$this->load->view('select/snippet/nodes',$subdata);
echo $this->tabs->endPanel();


if ($sourceid)
{
	echo $this->tabs->startPanel("browse");
	$this->load->view('select/browse', $this->data);
	echo $this->tabs->endPanel();
}


echo $this->tabs->startPanel("search");
$this->load->view('general/search', $this->data);
echo $this->tabs->endPanel();



echo $this->tabs->endPane();

?>

<script>
function ChooseNode(nodeid)
{
	window.parent.TINY.box.hide();
	window.parent.ChooseNode(nodeid);
}
</script>