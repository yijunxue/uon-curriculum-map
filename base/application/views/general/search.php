<?php echo form_open("node/find"); ?>
<table class='niceround'>
<tr>
	<th>Node Type</th>
	<td><?php echo form_dropdown('node_type',$types, $type, 'id="node_type"'); ?></td>
</tr>
<tr>
	<th>Search</th>
	<td><?php 
	$data = array(
		'name'        => 'search_box',
		'id'          => 'search_box',
		'value'       => '',
		'maxlength'   => '100',
		'size'        => '50'
		);
	echo form_input($data); ?></td>
</tr>	
</table>

<?php echo form_close(); ?>

<div class="search_results" id="search_results"></div>
