<div style="padding: 4px">
	<form method="post" action="<?php echo $form_url ?>">
		<dl class="form">
			<dt><label for="via_body">Select main body</label></dt>
			<dd>
				<select id="via_body" name="via_body">
					<option value="">-- All  --</option>
					<?php
					foreach ($bodies as $body) :
						?>
						<option value="<?php echo $body->getID() ?>"><?php echo $body->getTitle() ?></option>
						<?php
					endforeach;
					?>
				</select>
			</dd>
			<dt><label for="dest_body">Select supplementary body</label></dt>
			<dd>
				<select id="dest_body" name="dest_body">
					<option value="">-- None --</option>
					<?php
					foreach ($bodies as $body) :
					?>
					<option value="<?php echo $body->getID() ?>"><?php echo $body->getTitle() ?></option>
					<?php
					endforeach;
					?>
				</select>
			</dd>
			<dt>Unmapped Body Outcomes</dt>
			<dd>
				<input type="radio" id="only_mapped_show" name="only_mapped" value="show" checked /> <label for="only_mapped_show">Show</label><br>
				<input type="radio" id="only_mapped_hide" name="only_mapped" value="hide" /> <label for="only_mapped_hide">Hide</label><br>
				<input type="radio" id="only_mapped_highlight" name="only_mapped" value="highlight"/> <label for="only_mapped_highlight">Highlight</label>
			</dd>
			<dd style="float: left; width: 350px">
				<input type="checkbox" id="only_code" name="only_code" /> <label for="only_code">Only show module codes, not titles</label>
			</dd>
			<dd style="float: left; width: 350px">
				<input type="checkbox" id="use_percs" name="use_percs" /> <label for="use_percs">Use percentage based weights</label>
			</dd>
			<dd style="clear: left; float: left; width: 350px">
				<input type="checkbox" id="render" name="render" /> <label for="render">Display ticks instead of numbers</label>
			</dd>
			<dd style="float: left; width: 350px">
				<input type="checkbox" id="count_cells" name="count_cells" /> <label for="count_cells">Count mapped modules not outcomes</label>
			</dd>
			<dd style="clear: both">&nbsp;</dd>
		</dl>

		<input type="submit" value="Show report" />
		<input type="checkbox" id="csv" name="csv" /> <label for="csv">Download as CSV</label>
	</form>
</div>
