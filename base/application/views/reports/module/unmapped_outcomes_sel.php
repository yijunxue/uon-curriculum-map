<div style="padding: 4px">
	<form method="post" action="<?php echo $form_url ?>">
		<dl class="form">
			<dt><label for="via_body">Select body</label></dt>
			<dd>
				<select id="dest_body" name="dest_body">
					<option value="">-- None --</option>
					<?php
					foreach ($bodies as $body) :
					?>
					<option value="<?php echo $body->getID() ?>"><?php echo $body->getTitle() ?></option>
					<?php
					endforeach;
					?>
				</select>
			</dd>
			<dd style="clear: both">&nbsp;</dd>
		</dl>

		<input type="submit" value="Show report" />
	</form>
</div>
