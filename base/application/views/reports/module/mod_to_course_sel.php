<div style="padding: 4px">
	<form method="post" action="<?php echo $form_url ?>">
		<dl class="form">
			<dt><label for="via_course">Select programme</label></dt>
			<dd>
				<select id="via_course" name="via_course">
					<option value="">-- All  --</option>
					<?php
					foreach ($courses as $course) :
						?>
						<option value="<?php echo $course->getID() ?>"><?php echo $course->getAttributeValue('code') . ' - ' . $course->getTitle() ?></option>
						<?php
					endforeach;
					?>
				</select>
			</dd>
			<dt>Unmapped Programme objectives</dt>
			<dd>
				<input type="radio" id="only_mapped_show" name="only_mapped" value="show" checked /> <label for="only_mapped_show">Show</label><br>
				<input type="radio" id="only_mapped_hide" name="only_mapped" value="hide" /> <label for="only_mapped_hide">Hide</label><br>
				<input type="radio" id="only_mapped_highlight" name="only_mapped" value="highlight"/> <label for="only_mapped_highlight">Highlight</label>
			</dd>
			<dd style="clear: left; float: left; width: 350px">
				<input type="checkbox" id="render" name="render" /> <label for="render">Display ticks instead of numbers</label>
			</dd>
			<dd style="clear: both">&nbsp;</dd>
		</dl>

		<input type="submit" value="Show report" />
	</form>
</div>
