<?php if (count($nodetree['nodes']) > 0):?>

	<?php echo $nodetree['layer']->GetHeader(); ?>
	
<table class="relations" width="100%">
	<?php foreach ($nodetree['nodes'] as &$relnode): ?>
		<?php $subid = mt_rand(); ?>
		<tr>
			<?php if (!$nodetree['finallayer']) : ?>
				<td class='td_expand'>
					<a href="#" onclick="return ExpandNodeTree('<?php echo $nodetree['sectionid'] ?>', '<?php echo $nodetree['selected']; ?><?php echo $relnode->getId(); ?>', <?php echo $subid; ?>);">
						<img src='<?php echo asset_url(); ?>image/misc/add.png' id='image_<?php echo $subid; ?>'>
					</a>
				</td>
			<?php else: ?>
				<td class='td_expand'>
					<img src='<?php echo asset_url(); ?>image/misc/sphere.png' id='image_<?php echo $subid; ?>' width="16" height="16">
				</td>
			<?php endif; ?>
			<td>
				<?php echo ViewTools::LinkNode($relnode, $node); ?>
				<div class="relations_title">
					<?php echo $relnode->getTitleDisp($nodetree['layer']->longtitle); ?>	
				</div>
				<div class='sub_rels' style="display:none" id='subrels_<?php echo $subid; ?>'></div>
			</td>
		</tr>
	<?php endforeach; ?>
</table>
<?php else: ?>
<!--	No nodes found -->
<?php endif; ?>