
	<?php echo $layer->GetHeader(); ?>

<table class="relations" width="100%">

<?php // output nodes based on curlayer and layernodes
if (is_array($nodelist)) foreach ($nodelist as &$relnode): ?>
	<?php $subid = mt_rand(); ?>
	<tr>
		<?php if ($curlayer > 0) : ?>
			<td class='td_expand'>
				<a href="#" onclick="return ToggleElement('<?php echo $subid; ?>');return false;">
					<img src='<?php echo asset_url(); ?>image/misc/add.png' id='image_<?php echo $subid; ?>'>
				</a>
			</td>
			<?php endif; ?>
			<td>
				<?php echo ViewTools::LinkNode($relnode, $node); ?>
				<div class="relations_title">
					<?php echo $relnode->getTitleDisp(); ?>	
				</div>
				<?php if ($curlayer > 0) : ?>
					<div class='sub_rels' style='display:none' id='subrels_<?php echo $subid; ?>'>
					<?php 
						$this->data['curlayer'] = $curlayer - 1;
						$this->data['nodelist'] = $relnode->parents;
						$this->data['layer'] = $layers[$this->data['curlayer']];
						//$this->data['me'] = &$me;
		
						$me->_include('_misc/treerev_main', $this->data);
					?>
					</div>
				<?php endif; ?>
			</td>
		</tr>
	<?php endforeach; ?>
	
</table>
