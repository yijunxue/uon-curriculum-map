<img src='<?php echo asset_url(); ?>image/link/blank.png' id='link_img_src' style='display: none'>
<div id='link_title_url' style='display: none'><?php echo site_url("link/title/XXXX"); ?></div>

<div class='rel_title'>Node to add links to</div>
<div id='add_list_empty'>No links have been added</div>
<div id='add_list'></div>
<div class='rel_title'>Node to remove links from</div>
<div id='remove_list_empty'>No links have been removed</div>
<div id='remove_list'></div>
<div id='weight_list_title' class='rel_title' style="display:none;">Changed Weightings</div>
<div id='weight_list'></div>

<script>
var link_current = Array();
<?php foreach ($relids as $id): ?>
link_current[<?php echo $id; ?>] = 1;
<?php endforeach; ?>
</script>

<textarea id='link_add' name='link_add' style="display:none;"></textarea>
<textarea id='link_remove' name='link_remove' style="display:none;"></textarea>
<textarea id='weight_change' name='weight_change' style="display:none;"></textarea>
