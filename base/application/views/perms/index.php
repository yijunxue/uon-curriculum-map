<h3><?php echo $type_obj->name; ?>: <?php echo $node->getTitleDisp(); ?></h3>


<?php 

echo $this->tabs->startPane("choose_node");
echo $this->tabs->addPanel("Permissions", "current");
echo $this->tabs->addPanel("Parent and Children", "children");
echo $this->tabs->endHead();


function curperm($perm, $base = '')
{
	if ($perm == 1)
		return "<img src='" .  asset_url() . "image/clone/tick.png'>";
	if ($perm == -1)
		return "<img src='" .  asset_url() . "image/clone/cross.png'>";
	
	if ($base == 1)
		return "<img src='" .  asset_url() . "image/clone/tick_grey.png'>";
	if ($base == -1)
		return "<img src='" .  asset_url() . "image/clone/cross_grey.png'>";

	return $perm;	
}
$pl = new PermList();

// current permissions
echo $this->tabs->startPanel("current");

?>
<div class="small_heading">Current Permissions</div>
<?php

$c_current = 0;
$c_expired = 0;
$c_pending = 0;

// count what permission sets we have available
// NEEDS TO BE MOVED INTO VIEW CLASS
foreach ($perms as &$perm)
{

	if (empty($perm->from_date)) $perm->from_date = 0;
	if (empty($perm->to_date)) $perm->to_date = 0;

	if ($perm->from_date && $perm->from_date > time())
	{
		$perm->is_expired = 1;
		$c_pending++;
	} else if ($perm->to_date && $perm->to_date < time())
	{
		$perm->is_expired = -1;
		$c_expired++;
	} else {
		$perm->is_expired = 0;	
		$c_current++;
	}	
}

$subdata = array();
$subdata['pl'] = &$pl;
$subdata['perms'] = &$perms;
$subdata['node'] = &$node;
$subdata['expired'] = 0;
$this->load->view('perms/snippet/perms_full',$subdata);

// show pending permissions
if ($c_pending > 0)
{
	echo '<div class="small_heading">Permissions not yet active</div>';
	$subdata['expired'] = 1;
	$this->load->view('perms/snippet/perms_full',$subdata);
}

// show expired permissions
if ($c_expired > 0)
{
	echo '<div class="small_heading">Expired Permissions</div>';
	$subdata['expired'] = -1;
	$this->load->view('perms/snippet/perms_full',$subdata);
}

echo $this->tabs->endPanel(); 

?>


<?php echo $this->tabs->startPanel("children"); ?>
<div class='small_heading'>Parent</div>
<?php $parent = $node->getParent(); ?>
<a href='<?php echo site_url("view/" . $parent->getID()); ?>' target='_blank'><?php echo $parent->getTypeObj()->name; ?>: <?php echo $parent->getTitleDisp(); ?></a><br />
<div class='small_heading'>Children</div>

<?php
$subdata['rels'] = $children;
$subdata['baseid'] = 0;
$subdata['node'] = 0;
$subdata['path'] = 0;
$this->load->view('perms/snippet/list_children',$subdata);
if (count($children) == 0)
	echo "No children";
echo $this->tabs->endPanel(); 
?>

<?php echo $this->tabs->endPane(); ?>
<script>

$(document).ready(function () {
    setupTooltips();
    //setupHelpTooltips();
});

function setupTooltips() {
    $('.hasTooltipHelp').qtip({
        position: {
            my: 'bottom middle', 
            at: 'top middle'
			,
            adjust: {
                x: 0,
                y: -5
            }
        },
        style: {
            classes: 'ui-tooltip-rounded ui-tooltip-shadow ui-tooltip-pad'
        },
    });
}

</script>

