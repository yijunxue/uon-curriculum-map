<!-- Add page when dealing with base node permissions. This is slightly different to the add for a group or user, and too complex
to be in same page so uses a different template -->
<h3><?php echo $type_obj->name; ?>: <?php echo $node->getTitleDisp(); ?></h3>
<?php 
	function curperm($perm)
	{
		if ($perm == 1)
			return "<img src='" .  asset_url() . "image/clone/tick.png'>";
	if ($perm == -1)
		return "<img src='" .  asset_url() . "image/clone/cross.png'>";
		return $perm;	
	}
	
	$pl = new PermList();
?>
<?php echo form_input(array('name' => 'groupid', 'id' => 'groupid', 'type' => 'hidden', 'value' => 0)); ?>
<table class='niceround' width="100%" id="main_table">
	<tr>
		<th>Select Group or User :</th>
		<td>	
			<span style='position: relative;top: 1px;left: 4px;font-size:110%;' id="node_title">Base Node Permissions</span>
		</td>
	</tr>
	<tr>
		<th valign="top">Permissions :</th>
		<td>
			<table class='niceround'>
				<tr>
					<th style='width: 50px;text-align: center;white-space: nowrap;'>Current</th>
					<th style='width: 50px;text-align: center;white-space: nowrap;'><a href='#' onclick='setAll("n");return false;'>No Change</a></th>
					<th style='width: 50px;text-align: center;white-space: nowrap;'><a href='#' onclick='setAll("d");return false;'>Deny</a></th>
					<th style='width: 50px;text-align: center;white-space: nowrap;'><a href='#' onclick='setAll("a");return false;'>Allow</a></th>
					<th>Permission</th>
				</tr>
<?php foreach ($pl->perms as $name => $info): ?>
				<tr class='hasTooltipHelp' title="<?php echo $info['help']; ?>">
					<td align="center"><?php echo curperm($base_perms->$name); ?></td>
					<td align="center"><?php echo form_radio("p_$name", '', true,"id = pn_$name"); ?></td>
					<td align="center"><?php echo form_radio("p_$name", '-1', false,"id = pd_$name"); ?></td>
					<td align="center"><?php echo form_radio("p_$name", '1', false,"id = pa_$name"); ?></td>
					<td><?php echo $info['name']; ?></td>
				</tr>
<?php endforeach; ?>
			</table>
		</td>
	</tr>
<?php if ($child_count > 0): ?>
	<tr>
		<th valign="top">Apply Permissions To :</th>
		<td valign="top">
			<table class='niceround'>
				<tr class='hasTooltipHelp' title="The permissions will be applied to this node, and the selected children.">
					<td><?php echo form_radio('target', 'both', TRUE); ?></td>
					<td>This node and its children</td>
				</tr>
				<tr class='hasTooltipHelp' title="The permissions will be applied to this node only. Its children will remain unchanged.">
					<td><?php echo form_radio('target', 'node', FALSE); ?></td>
					<td>This node only</td>
				</tr>
				<tr class='hasTooltipHelp' title="Permissions will be applied to the selected children of this node only. The permissions for this node will remain unchanged.">
					<td><?php echo form_radio('target', 'children', FALSE); ?></td>
					<td>Children only.</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr id='children_disp'>
		<th valign="top">
		<div>Select Children :</div>
		<div id='helpbtn' class='clone_key_item' style='margin-top:6px;margin-bottom:6px;'><button onclick='$("#helpbtn").hide();$("#help").show();return false;'>Show Help</button></div>
		<div id='help' style='display:none;margin-top:10px;margin-bottom:10px;'>
			<div class='clone_key_head'>Key:</div>
			
			<div class='clone_key_item hasTooltipHelp' title='The node, and all of its children are included.'><img src='<?php echo asset_url(); ?>image/clone/tick.png'> Node and children</div>
			<div class='clone_key_item hasTooltipHelp' title='The node is include, but not its children.'><img src='<?php echo asset_url(); ?>image/clone/partial.png'> Node only</div>
			<div class='clone_key_item hasTooltipHelp' title='The node is not included'><img src='<?php echo asset_url(); ?>image/clone/cross.png'> Node exclued</div>
<div class='clone_key_info'>Denotes which nodes the permissions should be added to. All children are included by default. Hover over each option for more information.</div>
</div>
</th>
		<td valign="top"><?php $this->load->view('perms/snippet/add_children', $this->data); ?></td>
	</tr>
<?php else:  // no children, so create hidden field for target as node?>
<?php echo form_input(array('name' => 'target', 'id' => 'target', 'type' => 'hidden', 'value' => 'both')); ?>
	<tr>
		<th valign="top">Apply Permissions To:</th>
		<td valign="top">
			<div style='padding-top:2px;padding-left:6px;'>Node only, no children found.</div>
		</td>
	</tr><?php endif; ?>
</table>
<textarea name='node_exclude' id='node_exclude' style='display: none;'></textarea>

<div class='please_wait'>
	<div>Please Wait</div>
	<img src='<?php echo asset_url(); ?>image/misc/loading_bar.gif'>
</div>

<script>
<?php $pl = new PermList(); ?>
var perms = new Array("<?php echo implode('","',$pl->GetNames()); ?>");
</script>


<iframe id='form_result' name='form_result' style='display:none'></iframe>