<?php

echo $this->tabs->startPane("choose_node");
echo $this->tabs->addPanel("Group", "groups");
echo $this->tabs->addPanel("User", "users");
echo $this->tabs->endHead();


echo $this->tabs->startPanel("groups");

$basenode = $this->graph_db->loadNode($this->graph_db->active_dataset->nodes->users_and_groups);
$subdata['nodes'] = $basenode->getRelations("perm_group");
$subdata['baseid'] = $basenode->getId();
$subdata['node'] =& $basenode;
$subdata['path'] = $basenode->getId();
?>
<table class='niceround'>
<tr>
	<th>Search:</th>
	<td><input name='group_search' id='group_search'></td>
	<td class='pagination'><a href='#' onclick='resetGroups();return false;'>Reset</a></td>
</tr>	
</table>

<?php
$this->load->view('perms/snippet/groups',$subdata);
echo $this->tabs->endPanel();


echo $this->tabs->startPanel("users");
$this->load->view('perms/snippet/users', $this->data);
echo $this->tabs->endPanel();


echo $this->tabs->endPane();

?>

<script>

</script>