<table class="relations" width="100%">
<?php 
// need to allow the + to expand and display the listed nodes relations
$seenlist = explode(",",$path);
$seen = array();
foreach ($seenlist as $seenid)
{
	$seen[$seenid] = 1;
}

foreach ($rels as $nodetype => $relnodes)
{
	$type = $this->node_types->GetType($nodetype);
	
	$header = false;

	if ($type->getId() == "root") continue;
	// need to find the type from the id
foreach ($relnodes as &$relnode): ?>
	<?php $subid = mt_rand(); ?>
	<?php if (array_key_exists($relnode->getId(), $seen)) continue; ?>
	<?php if (!$header): ?>
		<tr><td colspan=2><?php echo $type->getName(); ?></td></tr>
		<?php $header = true; ?>
	<?php endif; ?>
		<tr>
			<?php 
			if (array_key_exists($relnode->getId(),$relcounts)) 
			{
				$relcount_array = $relcounts[$relnode->getId()];
				$relcount = $relcount_array['total'];
				unset($relcount_array['total']);
			} else {
				$relcount_array = array();
				$relcount = 0;
			}
			?>
			
			<td class='td_expand'>
				<?php if ($relcount > 0): ?>
				<a href="#" onclick="return ExpandListChildren(<?php echo $relnode->getId(); ?>, <?php echo $subid; ?>, '<?php echo $path; ?>');">
					<img src='<?php echo asset_url(); ?>image/misc/add.png' id='image_<?php echo $subid; ?>'>
				</a>
				<?php else: ?>
					<img src='<?php echo asset_url(); ?>image/misc/sphere.png' id='image_<?php echo $subid; ?>' width="16" height="16">
				<?php endif; ?>
			</td>
			<td>
				<div class="relations_title">
					<a href='<?php echo site_url("view/" . $relnode->getId()); ?>' target='_blank'>
						<?php echo $relnode->getTitleDisp(); ?>	
					</a>
				</div>
				<div class='sub_rels' style="display:none" id='subrels_<?php echo $subid; ?>'></div>
			</td>
		</tr>
<?php
	endforeach;
}
?>
</table>