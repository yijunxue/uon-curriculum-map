<table class="relations" width="100%">
<?php 
// need to allow the + to expand and display the listed nodes relations
$seenlist = explode(",",$path);
$seen = array();
foreach ($seenlist as $seenid)
{
	$seen[$seenid] = 1;
}

	//echo $path;
foreach ($nodes as &$relnode): ?>
	<?php if (array_key_exists($relnode->getId(), $seen)) continue; ?>
	<?php // check if the node we are dealing with has any children that we havet seen
		$curnoderels = $relnode->GetRelations('perm_group'); 
		$anyunseen = false;
		foreach ($curnoderels as &$curnoderel)
		{
			//$curnoderel->dump();			
			if (!array_key_exists($curnoderel->getId(), $seen))
			{
				//echo $relnode->getTitle() . " -> Unseen " . $curnoderel->getTitle(). "<br>";
				$anyunseen = true;
				break;	
			}
		}			
	?>
	<?php if ($relnode->getType() != 501 && $relnode->getType() != 502) continue; // make sure we only display users and groups here ?>
	<?php $subid = mt_rand(); ?>
	<?php /*if (!$header): ?>
		<tr><td colspan=2><?php echo $type->getName(); ?></td></tr>
		<?php $header = true; ?>
	<?php endif;*/ ?>
		<tr>
			<td class='td_expand'>
			<?php if ($anyunseen): ?>
				<a href="#" onclick="return ExpandGroup(<?php echo $relnode->getId(); ?>, <?php echo $subid; ?>, '<?php echo $path; ?>');">
					<img src='<?php echo asset_url(); ?>image/misc/add.png' id='image_<?php echo $subid; ?>'>
				</a>
			<?php endif; ?>
			</td>
			<td>
				<div class="relations_title">
					<a href='#' onclick='return Popup_ChooseGroup(<?php echo $relnode->getId(); ?>);'>
						<?php echo $relnode->getTitleDisp(); ?>	
					</a>
				</div>
				<div class='sub_rels' style="display:none" id='subrels_<?php echo $subid; ?>'></div>
			</td>
		</tr>
<?php
	endforeach;
?>
</table>