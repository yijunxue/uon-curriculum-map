
<table class='niceround'>
	<tr>
		<th style='width: 250px;text-align: left;white-space: nowrap;'>User / Group</th>
<?php foreach ($pl->perms as $name => $data): ?>
		<th style='width: 50px;text-align: center;white-space: nowrap;' class='hasTooltipAbove' title="<?php echo $data['help']; ?>">
			<?php echo ucwords($name); ?>
		</th>
<?php endforeach; ?>
	</tr>
	
<?php foreach ($perms as $groupid => &$perm): ?>
	<?php if ($perm->is_expired != $expired) continue; ?>
	<?php $pnode = $this->graph_db->loadNode($groupid); ?>
	<tr>
		<th style='width: 250px;text-align: left;white-space: nowrap;'>
			<?php if ($groupid == 0) : ?>
				Base Node Permissions
			<?php else: ?>
				<a href='<?php echo site_url("view/$groupid"); ?>' target='_blank'><?php echo is_object($pnode) ? trim($pnode->getTypeObj()->name,"s") . " - " . $pnode->getTitleDisp() : "Unknown"; ?></a>
				<?php if (isset($perm->doesnotexist) && $perm->doesnotexist): ?>
					<p class="error"><b>Error</b>: Permissions node does not exist.</p>
				<?php endif; ?>
				<?php if (isset($perm->typeerror) && $perm->typeerror): ?>
					<p class="error"><b>Warning</b>: Incorrect node type. <a href='<?php echo site_url("perms/reassign/{$node->getId()}/$groupid"); ?>'>Reassign</a></p>
				<?php endif; ?>
			<?php endif; ?>
		</th>
	<?php foreach ($pl->perms as $name => $data): ?>
		<td align="center"><?php echo curperm($perm->$name,$perms[0]->$name); ?></td>
	<?php endforeach; ?>
		<td align="center" nowrap>
			<a class='link_image' href='<?php echo site_url("perms/add/{$node->getId()}/$groupid"); ?>'>
				<img src='<?php echo asset_url(); ?>image/clone/edit.png'>Edit
</a>
</td>
<?php if ($groupid != 0): ?>
		<td align="center" width="65" nowrap>
			<div>
			<a class='link_image a_popup' href='<?php echo site_url("perms/delete/{$node->getId()}/$groupid"); ?>'>
				<img style='top:3px;' src='<?php echo asset_url(); ?>image/misc/delete.png'>Delete
			</a>
			</div>
		</td>
<?php else: ?>
		<td width="65"></td>
<?php endif; ?>

<?php if (1): ?>
		<td align="left">
			<?php if ($expired == -1): ?>
				Expired on <?php echo nice_date($perm->to_date); ?>
			<?php elseif ($expired == 1) : ?>
				From <?php echo nice_date($perm->from_date); ?><?php if ($perm->to_date > 0) : ?>, until <?php echo nice_date($perm->to_date); ?><?php endif; ?>
			<?php else: ?>
				<?php 
				$parts = array();
				if ($perm->from_date > 0) $parts[] = "from ". nice_date($perm->from_date);
				if ($perm->to_date > 0) $parts[] = "until ". nice_date($perm->to_date);
				if (count($parts) > 0)
				{
					$out = implode(", ",$parts);
					$out = ucfirst($out);
					echo $out;
				}
				?>
			<?php endif; ?>
		</td>
<?php else: ?>
		<td></td>
<?php endif; ?>	
	</tr>
<?php endforeach; ?>
</table>
