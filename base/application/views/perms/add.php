<!-- Add or edit permission set for a user. The base permission use a separate template -->
<h3><?php echo $type_obj->name; ?>: <?php echo $node->getTitleDisp(); ?></h3>
<?php 
function curperm($perm, $base = 0)
{
	if ($perm == 1)
		return "<img src='" .  asset_url() . "image/clone/tick.png'>";
	if ($perm == -1)
		return "<img src='" .  asset_url() . "image/clone/cross.png'>";
	
	if ($base == 1)
		return "<img src='" .  asset_url() . "image/clone/tick_grey.png'>";
	if ($base == -1)
		return "<img src='" .  asset_url() . "image/clone/cross_grey.png'>";

	return $perm;	
}
	
$pl = new PermList();
	
if (array_key_exists($groupid, $perms))
{
	$curperms = $perms[$groupid];
} else {
	$curperms = new Perm();
	$curperms->nodeid = $node->getId();
	$curperms->groupid = $groupid;
}


if ($is_reassign)
{
?>
<div class="message">Choose a new group or user to which the permissions should be assigned. <b>Note</b>: the change of group will apply to this node and its children only.
</div>
<?php
	echo form_input(array('name' => 'origgroupid', 'id' => 'origgroupid', 'type' => 'hidden', 'value' => $origgroupnode->getID()));
}
?>
<?php echo form_input(array('name' => 'groupid', 'id' => 'groupid', 'type' => 'hidden', 'value' => $groupid)); ?>
<table class='niceround' width="100%" id="main_table">
	<tr>
		<?php if ($is_add): ?>
			<th>Select Group or User :</th>
		<?php else: ?>
			<th><?php echo trim($groupnode->getTypeObj()->name,"s"); ?> :</th>
		<?php endif; ?>
		<td>	
			<?php if ($is_add || $is_reassign): ?>
			<div class='choose_group' id='choose_groupid' style='position:relative;top:-1px;'>
				Change<img src='<?php echo asset_url(); ?>image/change_node.png'>
			</div>
			<span style='position: relative;top: 2px;left: 6px;font-size:110%;' id="node_title"><?php echo $groupid > 0 ? trim($groupnode->getTypeObj()->name,"s") . " - " . $groupnode->getTitleDisp() : "None Selected"; ?></span>
			<?php else: ?>
				<span style='font-size:110%;' id="node_title"><?php echo $groupnode->getTitleDisp(); ?></span>
			<?php endif; ?>
		</td>
	</tr>
	<tr>
		<th valign="top">Permissions :</th>
		<td>
			<table class='niceround'>
				<tr>
					<th style='width: 50px;text-align: center;white-space: nowrap;'>Current</th>
					<th style='width: 50px;text-align: center;white-space: nowrap;'><a href='#' onclick='setAll("n");return false;'>No Change</a></th>
					<th style='width: 50px;text-align: center;white-space: nowrap;'><a href='#' onclick='setAll("i");return false;'>Inherit</a></th>
					<th style='width: 50px;text-align: center;white-space: nowrap;'><a href='#' onclick='setAll("d");return false;'>Deny</a></th>
					<th style='width: 50px;text-align: center;white-space: nowrap;'><a href='#' onclick='setAll("a");return false;'>Allow</a></th>
					<th>Permission</th>
				</tr>
<?php foreach ($pl->perms as $name => $info): ?>
				<tr class='hasTooltipHelp' title="<?php echo $info['help']; ?>">
					<td align="center" id='curperm_<?php echo $name; ?>'>
					<?php 
					if (array_key_exists($groupid, $perms))
					{
						echo curperm($perms[$groupid]->$name,$perms[0]->$name); 
					} else {
						echo curperm(0,$perms[0]->$name); 
					}

					$disabled = ($is_reassign) ? ' disabled' : '';
					?>
</td>
					<td align="center"><?php echo form_radio("p_$name", '', true,"id = pn_$name" . $disabled); ?></td>
					<td align="center"><?php echo form_radio("p_$name", '0', false,"id = pi_$name" . $disabled); ?></td>
					<td align="center"><?php echo form_radio("p_$name", '-1', false,"id = pd_$name" . $disabled); ?></td>
					<td align="center"><?php echo form_radio("p_$name", '1', false,"id = pa_$name" . $disabled); ?></td>
					<td><?php echo $info['name']; ?></td>
				</tr>
<?php endforeach; ?>
			</table>
		</td>
	</tr>
	<tr>
		<th valign="top">Apply Permissions To :</th>
		<td valign="top">
<?php if ($child_count > 0): ?>
			<table class='niceround'>
				<tr class='hasTooltipHelp' title="The permissions will be applied to this node, and the selected children.">
					<td><?php echo form_radio('target', 'both', TRUE); ?></td>
					<td>This node and its children</td>
				</tr>
				<tr class='hasTooltipHelp' title="The permissions will be applied to this node only. Its children will remain unchanged.">
					<td><?php echo form_radio('target', 'node', FALSE); ?></td>
					<td>This node only</td>
				</tr>
				<tr class='hasTooltipHelp' title="Permissions will be applied to the selected children of this node only. The permissions for this node will remain unchanged.">
					<td><?php echo form_radio('target', 'children', FALSE); ?></td>
					<td>Children only.</td>
				</tr>
			</table>
<?php else:  // no children, so create hidden field for target as node?>
		<?php echo form_input(array('name' => 'target', 'id' => 'target', 'type' => 'hidden', 'value' => 'both')); ?>
		<div style='padding-top:2px;padding-left:6px;'>Node only, no children found.</div>
<?php endif; ?>
		</td>
	</tr>
	<tr>
		<th valign="top">Date Restrictions :</th>
		<td valign="top">
			<table class='niceround'>
				<tr>
					<td><?php echo form_radio('date', 'any', $curperms->from_date == 0 && $curperms->to_date == 0 ? true : false, 'id = "date_opt_any"'); ?></td>
					<td width="300" class='hasTooltipHelp' title="The permissions will be applied to this node on a permanant basis/">Not date restricted</td>
					<td rowspan="4" valign="middle">
					
						<table class='niceround'>
							<tr class='from_date'>
								<th>From Date:</th>
								<td><?php echo form_input(array('name' => "date_from", 'id' => "date_from", 'value' => $curperms->from_date == 0 ? '' : date('Y-m-d H:i',$curperms->from_date), 'onclick' => "clickFrom()")); ?></td>
							</tr>
							<tr class='to_date'>
								<th>To Date:</th>
								<td><?php echo form_input(array('name' => "date_to", 'id' => "date_to", 'value' => $curperms->to_date == 0 ? '' : date('Y-m-d H:i',$curperms->to_date), 'onclick' => "clickTo()")); ?></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr>
					<td><?php echo form_radio('date', 'from', $curperms->from_date > 0 && $curperms->to_date == 0 ? true : false, 'id = "date_opt_from"'); ?></td>
					<td class='hasTooltipHelp' title="The permissions will be only be applicable from a specific date.">Applies from a specific date</td>
				</tr>
				<tr>
					<td><?php echo form_radio('date', 'to', $curperms->from_date == 0 && $curperms->to_date > 0 ? true : false, 'id = "date_opt_to"'); ?></td>
					<td class='hasTooltipHelp' title="The permissions will be only be applicable upto a specific date.">Applies upto a specific date</td>
				</tr>
				<tr>
					<td><?php echo form_radio('date', 'range', $curperms->from_date > 0 && $curperms->to_date > 0 ? true : false, 'id = "date_opt_range"'); ?></td>
					<td class='hasTooltipHelp' title="The permissions will be only be applicable between specific dates.">Applies between dates</td>
				</tr>
			</table>
		</td>
	</tr>
<?php if ($child_count > 0): ?>
	<tr id='children_disp'>
		<th valign="top">
		<div>Select Children :</div>
		<div id='helpbtn' class='clone_key_item' style='margin-top:6px;margin-bottom:6px;'><button onclick='$("#helpbtn").hide();$("#help").show();return false;'>Show Help</button></div>
		<div id='help' style='display:none;margin-top:10px;margin-bottom:10px;'>
			<div class='clone_key_head'>Key:</div>
			
			<div class='clone_key_item hasTooltipHelp' title='The node, and all of its children are included.'><img src='<?php echo asset_url(); ?>image/clone/tick.png'> Node and children</div>
			<div class='clone_key_item hasTooltipHelp' title='The node is include, but not its children.'><img src='<?php echo asset_url(); ?>image/clone/partial.png'> Node only</div>
			<div class='clone_key_item hasTooltipHelp' title='The node is not included'><img src='<?php echo asset_url(); ?>image/clone/cross.png'> Node exclued</div>
<div class='clone_key_info'>Denotes which nodes the permissions should be added to. All children are included by default. Hover over each option for more information.</div>
</div>
</th>
		<td valign="top"><?php $this->load->view('perms/snippet/add_children', $this->data); ?></td>
	</tr>
<?php endif; ?>
</table>
<textarea name='node_exclude' id='node_exclude' style='display: none;'></textarea>

<div class='please_wait'>
	<div>Please Wait</div>
	<img src='<?php echo asset_url(); ?>image/misc/loading_bar.gif'>
</div>

<script>

<?php $pl = new PermList(); ?>
var perms = new Array("<?php echo implode('","',$pl->GetNames()); ?>");

</script>

<iframe id='form_result' name='form_result' style='display:none'></iframe>