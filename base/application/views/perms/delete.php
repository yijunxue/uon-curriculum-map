<table class='niceround' width="100%" id='main_table'>
	<tr>
		<th><?php echo is_object($group) ? trim($group->getTypeObj()->name,"s") : "Unknown"; ?> :</th>
		<td>	
			<span style='position: relative;top: 3px;left: 4px;' id="node_title">
				<?php echo is_object($group) ? $group->getTitleDisp() : "Unknown"; ?>
			</span>
		</td>
	</tr>
	<tr>
		<th>Node :</th>
		<td>	
			<span style='position: relative;top: 3px;left: 4px;' id="node_title"><?php echo $node->getTitleDisp(); ?></span>
		</td>
	</tr>
	<tr>
		<th valign="top">Remove Permissions From:</th>
		<td valign="top">
			<table class='niceround'>
				<tr class='hasTooltipHelp' title="The permissions will be removed from this node, and the selected children.">
					<td><?php echo form_radio('target', 'both', TRUE); ?></td>
					<td>This node and its children</td>
				</tr>
				<tr class='hasTooltipHelp' title="The permissions will be removed from this node only. Its children will remain unchanged.">
					<td><?php echo form_radio('target', 'node', FALSE); ?></td>
					<td>This node only</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<div class='please_wait'>
	<div>Please Wait</div>
	<img src='<?php echo asset_url(); ?>image/misc/loading_bar.gif'>
</div>
<iframe name='delete_result' id='delete_result' style='display: none'></iframe>
