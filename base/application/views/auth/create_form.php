<?php
$username = array(
	'name'	=> 'username',
	'id'	=> 'username',
	'size'	=> 30,
	'value' =>  set_value('username')
);

$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'size'	=> 30
);

$confirm_password = array(
	'name'	=> 'confirm_password',
	'id'	=> 'confirm_password',
	'size'	=> 30
);

$role = array(
	'name'	=> 'role',
	'id'	=> 'role',
	'value'	=> set_value('role')
	);

$email = array(
	'name'	=> 'email',
	'id'	=> 'email',
	'maxlength'	=> 80,
	'size'	=> 30,
	'value'	=> set_value('email')
);

?>


<div class="login_error">
<?php echo $this->dx_auth->get_auth_error(); ?>
</div>

<table class='niceround'>	
<tr>
	<th><?php echo form_label('Username', $username['id']);?></th>
	<td>
		<?php echo form_input($username)?>
	</td>
	<td class="form_error">
		<?php echo form_error($username['name']); ?>
	</td>
</tr>

<tr>
	<th><?php echo form_label('Password', $password['id']);?></th>
	<td>
		<?php echo form_password($password)?>
	</td>
	<td class="form_error">
		<?php echo form_error($password['name']); ?>
	</td>
</tr>

<tr>
	<th><?php echo form_label('Confirm Password', $confirm_password['id']);?></th>
	<td>
		<?php echo form_password($confirm_password)?>
	</td>
	<td class="form_error">
		<?php echo form_error($confirm_password['name']); ?>
	</td>
</tr>

<tr>
	<th><?php echo form_label('Email Address', $email['id']);?></th>
	<td>
		<?php echo form_input($email)?>
	</td>
	<td class="form_error">
		<?php echo form_error($email['name']); ?>
	</td>
</tr>

<tr>
	<th><?php echo form_label('Role', $role['id']);?></th>
	<td>
		<?php echo form_dropdown('role', $roles, set_value('role'))?>
	</td>
	<td class="form_error">
		<?php echo form_error($role['name']); ?>
	</td>
</tr>

</table>

</fieldset>
</body>
</html>