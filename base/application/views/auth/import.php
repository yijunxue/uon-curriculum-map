<div style="padding:4px;">
<div>To bulk import data, you will need to create a CSV file with a header row. 1 item will be created for each additional row in your CSV file. The following columns are valid in your CSV file: </div>
<ul>
	<li><b>username</b> - Username</li>
	<li><b>email</b> - Email address</li>
	<li><b>password</b> - Optional password. If not supplied a random password will be generated. This will be overridden when using LDAP authentication</li>
</ul>

<div>&nbsp;</div>
Please Select a CSV File: <input type="file" name="csvfile">
<div>&nbsp;</div>
</div>