<?php
$username = array(
	'name'	=> 'username',
	'id'	=> 'username',
	'size'	=> 30,
	'value' => set_value('username')
);

$password = array(
	'name'	=> 'password',
	'id'	=> 'password',
	'size'	=> 30
);

$remember = array(
	'name'	=> 'remember',
	'id'	=> 'remember',
	'value'	=> 1,
	'checked'	=> set_value('remember'),
	'style' => 'margin:0;padding:0'
);

$gohome = array(
	'name'	=> 'gohome',
	'id'	=> 'gohome',
	'value'	=> 1,
	'checked'	=> set_value('gohome', true),
	'style' => 'margin:0;padding:0'
);

$confirmation_code = array(
	'name'	=> 'captcha',
	'id'	=> 'captcha',
	'maxlength'	=> 8
);

?>

<div class="login_error">
<?php echo $this->dx_auth->get_auth_error(); ?>
<?php echo $this->head->ValidationErrors(); ?>
</div>

<table class='niceround'>	
	<tr>
	<th><?php echo form_label('Username', $username['id']);?></th>
	<td>
		<?php echo form_input($username)?>
	</td>
	</tr>
	
	<tr>
  <th><?php echo form_label('Password', $password['id']);?></th>
	<td>
		<?php echo form_password($password)?>
	</td>
	</tr>
<?php if ($show_captcha): ?>
<tr>
	<th>
		<?php echo form_label('Confirmation Code', $confirmation_code['id']);?>
	</th>
	<td>
		<?php echo form_input($confirmation_code);?>
	</td>
</tr>
<tr>
	<td colspan="2" align="center">
		<?php echo $this->dx_auth->get_captcha_image(); ?><br />
		Enter the code exactly as it appears. There is no zero.
	</td>
</tr>

<?php endif; ?>

<tr>
<td></td>
	<td>
		<?php echo form_checkbox($remember);?> <?php echo form_label('Remember me', $remember['id']);?> 
		
	</td>
</tr>
<tr>
<td></td>
	<td>
		<?php echo form_checkbox($gohome);?> <?php echo form_label('Go to my homepage', $gohome['id']);?> 
		
	</td>
</tr>
<!--<tr>
	<td></td>
	<td>
		<?php echo anchor($this->dx_auth->forgot_password_uri, 'Forgot password');?> 
		
	</td>
</tr>
<?php if ($this->dx_auth->allow_registration): ?>
<tr>
	<td></td>
	<td>
		<?php echo anchor($this->dx_auth->register_uri, 'Register'); ?>
	</td>
</tr>
<?php endif; ?>-->
</table>

<script>

$(document).ready( function () {
	$('#username').focus();
});

</script>