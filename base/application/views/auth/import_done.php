<div style="padding:4px;">

<?php if (count($results['success']) > 0) : ?>
<h3>The following users have been successfully imported:</h3>

<?php foreach ($results['success'] as &$user) : ?>
<div>
	<?php echo $user; ?>
</div>

<?php endforeach; ?>
<?php endif; ?>

<?php if (count($results['failure']) > 0) : ?>
<h3>The following users failed to import. This could be because they already exist in the system:</h3>
<?php
if ($for_group) {
?>
<p>Users who already existed in the system <i>will</i> be added to the group.</p>
<?php
}

foreach ($results['failure'] as &$user) : ?>
<div>
	<?php echo $user; ?>
</div>

<?php endforeach; ?>
<?php endif; ?>
</div>