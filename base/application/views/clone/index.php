<h3><?php echo $type_obj->name; ?>: <?php echo $node->getTitleDisp(); ?></h3>
<script>
var current_dataset = '<?php echo $this->graph_db->active_dataset->table; ?>';
var node_type = '<?php echo $node->getType(); ?>';
</script>
<?php
echo form_input(array('name' => 'orig_parentid', 'id' => 'orig_parentid', 'type' => 'hidden', 'value' => $parentnode->GetID(),'maxlength' => '12', 'size' => '12', 'style' => ''));
echo form_input(array('name' => 'parentid', 'id' => 'parentid', 'type' => 'hidden', 'value' => $parentnode->GetID(),'maxlength' => '12', 'size' => '12', 'style' => ''));
?>
<table class='niceround' width="100%">
	<tr>
		<th>New Title :</th>
		<td><?php echo form_input(array('name' => 'title', 'id' => 'title', 'value' => set_value('title', $node->getTitle()),'maxlength' => '100', 'size' => '50', 'style' => '')); ?></td>
	</tr>
	<tr>
		<th>Target Dataset :</th>
		<td><?php echo form_dropdown('dataset', $datasets_select, $datasets_current, "id='dataset'"); ?></td>
	</tr>
	<tr>
		<th>New Parent :</th>
		<td>

			<div class='choose_node' id='choose_sourceid' style='position:relative;top:-1px;'>
				Change<img src='<?php echo asset_url(); ?>image/change_node.png'>
			</div>
			<span style='position: relative;top: 3px;left: 4px;' id="node_title"><?php echo $parentnode->getTitleDisp(); ?></span>
		</td>
	</tr>
	<!--<tr>
		<th>Relink new nodes :</th>
		<td><?php echo form_checkbox('relink', '1', TRUE); ?>If the target dataset is the same as the source dataset, then should the nodes be relinked to any nodes outside the set being copied.</td>
		<td><?php echo form_checkbox('relink', '1', TRUE); ?>Should the nodes be relinked to any nodes outside the set being copied.</td>
	</tr>-->
  <tr>
      <th>Rename new nodes :</th>
      <td>
      <?php
      	$data = array('name' => 'rename', 'id' => 'rename', 'value' => '1', 'checked' => TRUE);
      	echo form_checkbox($data) . form_label('Prepend "Copy of" to the title. (This is recommended)', 'rename');
      ?>
      </td>
  </tr>
  <tr>
      <th>Relationships:</th>
      <td>
      <?php
      	$data = array('name' => 'link_outside', 'id' => 'link_outside', 'value' => '1', 'checked' => TRUE);
      	echo form_checkbox($data) . form_label('Attempt to recreate relationships to nodes outside the copied set', 'link_outside');
      ?>
      </td>
  </tr>
  <tr>
      <th>Clone this node :</th>
      <td>
      <?php
      	$data = array('name' => 'skip_parent', 'id' => 'skip_parent', 'value' => '1', 'checked' => FALSE);
      	echo form_checkbox($data) . form_label('Skip this node and clone only its children (generally, cloning the current node is recommended)', 'skip_parent');
      ?>
      </td>
  </tr>
	<tr>
		<th valign="top">
		Copy Nodes :<br />
		<br />
			<div class='clone_key_head'>Key:</div>

			<div class='clone_key_item hasTooltipHelp' title='The node, and all of its children are included.'><img src='<?php echo asset_url(); ?>image/clone/tick.png'> Node and children</div>
			<div class='clone_key_item hasTooltipHelp' title='The node is include, but not its children.'><img src='<?php echo asset_url(); ?>image/clone/partial.png'> Node only</div>
			<div class='clone_key_item hasTooltipHelp' title='The node is not included'><img src='<?php echo asset_url(); ?>image/clone/cross.png'> Node exclued</div>
			<div class='clone_key_info'>Denotes which nodes are included in the copy. All children are included by default. Hover over each option for more information.</div>
		</th>
		<td><?php $this->load->view('clone/relations', $this->data); ?></td>
	</tr>

</table>
<textarea name='node_exclude' id='node_exclude' style='display: none;'></textarea>
<textarea name='title_edits' id='title_edits' style='display: none;'></textarea>
