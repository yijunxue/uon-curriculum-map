<table class="niceround" width="100%">
<?php 
// need to allow the + to expand and display the listed nodes relations

foreach ($relations as $nodetype => $reltypes)
{
	if ($nodetype < 1) continue;
	$type = $this->node_types->GetType($nodetype);
	$header = ""; 
	$headershown = false;
	// need to find the type from the id
	foreach ($reltypes as $reltype => $relnodes):
		if ($reltype != GDB_Rel::ChildOf) continue;
	$sectid = mt_rand();
foreach ($relnodes as &$relnode): ?>
	<?php $subid = mt_rand(); ?>
	
	<?php if (!$headershown) : ?>
	<tr>
	<th colspan=3 class='clone_nodetype_head'>
	<div style='float: right'>
				<img src='<?php echo asset_url(); ?>image/clone/tick.png' id='type_toggle_<?php echo $sectid; ?>' onclick='toggleNodeType(<?php echo $sectid; ?>); return false'>
			</div>
			<?php echo $type->getName(); ?>	
		</th>
	</tr>
		
	<?php $headershown = true; endif; ?>
		<tr class='section_row_<?php echo $sectid; ?>' id='node_<?php echo $subid; ?>'>	
			<div style='display:none' id='node_id_<?php echo $subid; ?>'><?php echo $relnode->getId(); ?></div>
			<?php 
			if (array_key_exists($relnode->getId(),$relcounts)) 
			{
				$relcount_array = $relcounts[$relnode->getId()];
				$relcount = $relcount_array['total'];
				unset($relcount_array['total']);
			} else {
				$relcount_array = array();
				$relcount = 0;
			}
			?>
			<?php 
				// built children count tooltip
				$html = "<div class='tt_rc_total'>$relcount Sub nodes</div><div class='tt_rc_types bottom_round'>";
				foreach ($relcount_array as $type => $count)
				{
					if ($type < 1) continue;
					$html .= "<div class='tt_rc_type'>$count " . $this->node_types->getType($type)->name . "</div>";
				}
				$html .= "</div>";
				$html = htmlentities($html);
			?>
			<td class='td_expand' valign="top">
				<?php if ($relcount > 0): ?>
				<a href="#" onclick="return ExpandCloneChildren(<?php echo $relnode->getId(); ?>, <?php echo $subid; ?>);" class="hasTooltip" title="<?php echo $html; ?>" id='node_expand_<?php echo $subid; ?>'>
					<img src='<?php echo asset_url(); ?>image/misc/add.png' id='image_<?php echo $subid; ?>'>
				</a>
				<?php endif; ?>
			</td>
			<td>
				<div class="relations_title" id='node_title_<?php echo $subid; ?>'>
				<span class='clone_node_title'>
						<?php echo $relnode->getTitleDisp(); ?>	
</span>
					<span id='node_changed_<?php echo $subid; ?>' class='clone_node_changed hasTooltipChanged' title='Title Changed' style='display: none'><img src='<?php echo asset_url(); ?>image/clone/changed.png'></span>
				</div>
				<div class='sub_rels' style="display:none;" id='subrels_<?php echo $subid; ?>'></div>
</td>
<td align="right" class="td_clone_tools" valign="top">
				<img src='<?php echo asset_url(); ?>image/clone/edit.png' id='image_edit_<?php echo $subid; ?>' onclick='editNode(<?php echo $subid; ?>); return false'>
				<img src='<?php echo asset_url(); ?>image/clone/tick.png' id='image_toggle_<?php echo $subid; ?>' onclick='toggleNode(<?php echo $subid; ?>); return false'>
				<a href='<?php echo site_url('view/node/' . $relnode->getId()); ?>' target='_blank'>
					<img src='<?php echo asset_url(); ?>image/clone/view.png'>
				</a>
			</td>
		</tr>
<?php
	endforeach;	endforeach;
}
?>
</table>