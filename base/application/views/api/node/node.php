<<?php echo $tag; ?> id='<?php echo $node->getID(); ?>' guid='<?php echo $node->getGUID(); ?>' type='<?php echo $node->getTypeObj()->id; ?>'
<?php if (!empty($node->match)): ?> match='<?php echo $node->match; ?>' <?php endif; ?>
<?php if (!empty($node->match_attr)): ?> match_attr='<?php echo $node->match_attr; ?>' <?php endif; ?>
<?php if (!empty($weight) && $weight): ?> weight='<?php echo round($node->getWeightTo($parnode) * 100,0); ?>' weight_dest='<?php echo $parnode->getId(); ?>' <?php endif; ?>
>
	<title><?php echo cdata($node->getTitle()); ?></title>

<?php if ($show_details): ?>
	<orig_author user_id='<?php echo $node->orig_author; ?>'><?php echo $this->dx_auth->getUser($node->orig_author)->username; ?></orig_author>
	<last_author user_id='<?php echo $node->last_author; ?>'><?php echo $this->dx_auth->getUser($node->last_author)->username; ?></last_author>
	<created raw_date='<?php echo $node->created; ?>'><?php echo nice_date($node->created); ?></created>
	<modified raw_date='<?php echo $node->modified; ?>'><?php echo nice_date($node->modified); ?></modified>
	<revisions><?php echo $node->revised; ?></revisions>
<?php endif; ?>

<?php if ($show_attrs): ?>
	<attributes>
		<?php foreach($node->GetTypeObj()->GetAttributes() as $attr): ?>
		<attribute id='<?php echo $attr->id; ?>' name='<?php echo $attr->GetTitle(); ?>'><![CDATA[<?php echo $node->getAttribute($attr->id)->format(); ?>]]></attribute>
		<?php endforeach; ?>
	</attributes>
<?php endif; ?>

<?php if ($show_rels): ?>
	<?php $relations = $node->getRelationsRaw(); ?>
	<relations>
		<?php foreach ($relations as &$rel): ?>
			<?php $dnode =& $rel->getNode(); ?>
			<relation type='<?php echo GDB_Rel::GetNameSingle($rel->getType()); ?>' dest_id='<?php echo $dnode->GetID(); ?>' dest_type='<?php echo $dnode->GetTypeObj()->id; ?>' weight='<?php echo round($node->getWeightTo($dnode) * 100,0); ?>'>
				<![CDATA[<?php echo $dnode->GetTitle(); ?>]]>
			</relation>
		<?php endforeach; ?>
	</relations>
<?php endif; ?>

<?php if (!empty($show_hist)): ?>
	<?php $history = array_reverse($node->getHistory()); ?>
	<history>
		<?php foreach ($history as $id => &$hist): ?>
			<change no='<?php echo $id; ?>' date='<?php echo $hist->when; ?>' user='<?php echo $this->dx_auth->getUser($hist->user)->username; ?>' type='<?php echo GDB_Hist_Action::string($hist->action); ?>'>
				<?php echo GDB_Hist_Action::AsXML($hist); ?>
			</change>
		<?php endforeach; ?>
	</history>
<?php endif; ?>

<?php if (empty($noclose) || $noclose == 0): ?>
</<?php echo $tag; ?>>
<?php endif; ?>
