</div>
<?php if ($this->head->HasFooter()):?>
	<div class='popup_footer bottom_round'><?php echo $this->head->Footer(); ?></div>
<?php endif; ?>

<?php if ($this->head->HasTrailing()) echo $this->head->Trailing(); ?>

</body>
</html>

<script>
$(document).ready(function () {
	//setTimeout("resize_popup()",50);
	setTimeout("resize_popup()",250);
	setTimeout("resize_popup()",500);
});

function resize_popup()
{
	var height = 120;
	if ($(document.body).children('form').outerHeight() > 0)
	{
		height = $(document.body).children('form').outerHeight();
	} else {
		var head = $('.popup_title').outerHeight();
		var body = $('.popup_content').outerHeight();
		var foot = $('.popup_footer').outerHeight();
		
		//console.log("Resize Popup to " + (head + body + foot));
		//console.log("Resize Popup to " + $(document.body).height());
		height = head + body + foot
	}
	
<?php if (!empty($popupheight)): ?>
	height = <?php echo $popupheight; ?>;
<?php endif; ?>
	if (height > 600)
	{
		height = 600;
		// need to set the inner content height to 468
		$('.popup_content').css('height','468px');
	}
	
	window.parent.TINY.box.size(window.parent.TINY.box.pwidth(), height, true);
}
</script>