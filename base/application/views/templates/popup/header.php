<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
	<title><?php echo $title ?></title>
	<?php $this->head->Output(); ?>
	<?php echo !empty($library_src) ? $library_src : ''; ?>
	
</head>
<style>
.back_col {
	background-color: <?php echo $this->graph_db->active_dataset->color; ?>;
}
</style>
<script>
	var base_url = '<?php echo site_url(); ?>';
</script>
<body>
<?php if ($this->head->HasLeading()) echo $this->head->Leading(); ?>
	
<div class="popup_title back_col main_title_1 top_round"><?php echo !empty($title) ? $title : "No Title!" ?></div>
<div class="popup_content">
