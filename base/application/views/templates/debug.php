
<div class="debug_main round">
<div class="debug_head">
	<div class="debug_info">Debug Information</div>
	<div class="debug_info_nodes" onclick="Debug_Show_Nodes()">
		<img src='<?php echo asset_url(); ?>image/debug/nodes.png'>
		Loaded Nodes (<?php echo $this->graph_db->CountLoadedNodes(); ?>)
	</div>
<?php 
$qrycnt = 0;
$qrytime = 0;
$CI =& get_instance();
$this->gdb = $this->graph_db->gdb;
$times = $this->gdb->query_times;
$queries = $this->gdb->queries;
foreach ($this->gdb->queries as $key=>$query) {
	$qrycnt ++;
	$qrytime += $times[$key];
}
?>
	<div class="debug_info_sql" onclick="Debug_Show_SQL()">
		<img src='<?php echo asset_url(); ?>image/debug/query.png'>
		SQL Queries (<?php echo $qrycnt; ?>, <?php echo round($qrytime * 1000,2); ?>ms)
	</div>
	<div class="debug_info_session" onclick="Debug_Show_Session()">
		<img src='<?php echo asset_url(); ?>image/debug/session.png'>
		Session
	</div>
	<div class="debug_info_memory" onclick="Debug_Show_Memory()">
		<img src='<?php echo asset_url(); ?>image/debug/session.png'>
		Memory
	</div>
	<div class="clear"></div>
</div>
<div class="debug_nodes round">
<?php $this->graph_db->dump_loaded_nodes(null, true, true, true, false); ?>
</div>
<div class="debug_session round">
<?php print_p($this->session->userdata); ?>
</div>
<div class="debug_sql round">
<table>
<?php 
require "geshi/geshi.php";

$geshi = new GeSHi( '', 'sql' );
$geshi->set_header_type(GESHI_HEADER_DIV);
$newlineKeywords = '/<span style="color: #993333; font-weight: bold;">'
	.'(FROM|LEFT|INNER|OUTER|WHERE|SET|VALUES|ORDER|GROUP|HAVING|LIMIT|ON|AND)'
	.'<\\/span>/i'
;

$total_query_time = 0;
echo "<table class='sqllog'>";
$rn = 0;

foreach ($queries as $key=>$query) {
	$rn = 1 - $rn;
	$total_query_time += $times[$key];
	echo "<tr class='row_$rn'><td width='60'>".round($times[$key]*1000,2)."ms</td><td>";
	
	if (strlen($query) > 500) $query = substr($query, 0, 500);
	$geshi->set_source($query);
	$text = $geshi->parse_code();
	$text = preg_replace($newlineKeywords, '<br />&nbsp;&nbsp;\\0', $text);
	echo $text.'</td></tr>';
	//echo $sqlhl->highlight($query);
}
$rn = 1 - $rn;
?>

<tr class='row_<?php echo $rn; ?>'><td>Total: <?php echo round($total_query_time*1000,2); ?>ms</td><td>Count: <?php echo count($CI->db->queries); ?> queries</td></tr>
</table>
</div>
<div class="debug_memory round">
<?php $bytes = memory_get_peak_usage(true);
	if ($bytes > 0) {
		echo ($bytes / 1024) . ' MB';
	}
 ?>
</div>

</div>

