

		</div>
		<?php if ($this->head->HasFooter()):?>
			<div class="footer_padding"></div>
			<div class='main_footer bottom_round '><?php echo $this->head->Footer(); ?></div>
		<?php else: ?>
			<div class='main_footer_empty bottom_round'></div>
		<?php endif; ?>
		<?php if ($this->head->HasTrailing()) echo $this->head->Trailing(); ?>
		<div class="main_foot round">
			&copy; Copyright 2013 The University of Nottingham
		</div>
	</div>

	<?php if ($this->dx_auth->showdebug()) $this->load->view('templates/debug'); ?>
</body>
</html>

<?php $this->session->set_userdata('referrer', current_url());   ?>