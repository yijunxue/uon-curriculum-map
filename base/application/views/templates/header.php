<!DOCTYPE html>
<html>
<head>
  <meta http-equiv="X-UA-Compatible" content="IE=Edge"/>
  <title><?php echo !empty($title) ? $title : 'NO TITLE' ?></title>
	<?php $this->head->Output(); ?>
	<?php echo !empty($library_src) ? $library_src : ''; ?>
	<base href="<?php echo root_url(); ?>" />
</head>
<style>
.back_col {
	background-color: <?php echo $this->graph_db->active_dataset->color; ?>;
}
</style>
<script>
	var base_url = '<?php echo site_url(); ?>';
	var root_url = '<?php echo root_url(); ?>';
<?php if (!empty($node)): ?>
	var base_nodeid = <?php echo $node->getID(); ?>;
<?php endif; ?>
</script>

<body class='back_col'>
	<div class="main_cont">
		<div class="main_head">
			<img src="<?php echo asset_url(); ?>image/header/logo.png" style='float:left'>
				<?php $this->load->view('templates/login'); ?>
			<div class="main_title_padding main_title_1">Curriculum Mapping System
			<span style="color:black;">
				
			</span>
			</div>
			<div class="main_title_2 back_col round" style="width:50%">
				<a href='<?php echo site_url("main/year"); ?>'>
					<?php echo $this->graph_db->active_dataset->name; ?>
				</a>
				<a href='<?php echo site_url("main/listdatasets"); ?>' class='a_popup' rel='400x258'>
					<img src='<?php echo asset_url(); ?>image/misc/pick_year.png' style='position:relative;top:5px;padding-right:4px;'>
				</a>
			</div>
		</div>
		<div class="clear"></div>
		<div class="main_menu round">
			<?php echo $this->menu->OutputMainMenu(); ?>
		</div>
		<?php if ($this->menu->HasSubMenu()): ?>
		<div class="main_submenu round">
			<?php echo $this->menu->OutputSubMenu(); ?>
		</div>
		<?php endif;?>

		<?php if ($this->head->HasLeading()) echo $this->head->Leading(); ?>

		<div class="main_pagetitle top_round back_col">

		<?php if ($this->menu->HasNodeMenu()): ?>
			<div class="main_nodemenu round">
				<div class="main_nodemenu_actions">
					<img src='<?php echo asset_url(); ?>image/misc/actions.png'>
					<span>Actions</span>
				</div>
			</div>
		<?php endif;?>

			<?php echo !empty($title) ? $title : "No Title!" ?>
		</div>
		
		<?php if ($this->menu->HasNodeMenu()): ?>
				<div class="main_nodemenu_popupcont">
				<div class="main_nodemenu_popup round ui-tooltip-shadow">
					<?php echo $this->menu->OutputNodeMenu(); ?>
				</div>
				</div>
		<?php endif;?>

		<div class="main_body">

		<?php $this->head->ShowMessages(); ?>
		
		
