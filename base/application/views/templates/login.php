<?php //if ($this->router->class != "auth"): ?>
	
		<?php if ($this->dx_auth->is_logged_in()): ?>
				<?php 
					$roles = $this->dx_auth->list_roles(); 
					$current_role = $this->dx_auth->get_current_role();	
					$hasroles = false;
				if (count($roles) > 1) $hasroles = true; ?>

			<div class="<?php echo $hasroles ? 'login_details_withrole' : 'login_details'; ?> round">
			
			<div class='login_details_user'><a href='<?php echo site_url("auth"); ?>'><?php echo $this->dx_auth->get_username(); ?></a></div>
			<div>
				<div class="login_details_logout">
					<a class='button' href="<?php echo site_url("auth/logout"); ?>">
						<img src='<?php echo asset_url(); ?>image/misc/logout.png' alt="Logout">Logout
					</a>
					</div>
					
				<?php if ($hasroles): ?>
				<div class='login_details_role'>
					<select style="width:93px;" onchange="ChangeRole()" id="role">
						<?php if ($current_role == "default") :?>
							<option value='default' selected>Default</option>
						<?php endif; ?>	
						<?php foreach ($roles as &$role): ?>
							<option value='<?php echo $role->name; ?>' <?php if ($role->name == $current_role) echo " selected"; ?>><?php echo $role->desc; ?></option>
						<?php endforeach; ?>
					</select>
				</div>
				<?php endif; ?>
			</div>
		</div>
		<?php else : ?>
			<div class="login_details_guest round">
				<div class="login_details_login">
					<a class='a_popup button' rel='400x258' href="<?php echo site_url("auth/login/?popup=1"); ?>">
						<img src='<?php echo asset_url(); ?>image/misc/login.png' alt="Logout">Login
					</a>
				</div>
			</div>
		<?php endif; ?>
	
<?php // endif; ?>