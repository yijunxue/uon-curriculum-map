<?php foreach ($this->graph_db->datasets as $setid => &$dataset) : ?>
	<?php if (!$this->dx_auth->is_admin() && $dataset->adminonly) continue; ?>
<div style="background-color: <?php echo $dataset->color; ?>" class='dataset round'>
	<?php if ((string)$this->graph_db->active_dataset->table == (string)$setid): ?>
		<img src='<?php echo asset_url(); ?>image/misc/star.png' style='float: left;top: -2px;position: relative;'>
	<?php else : ?>
		<img src='<?php echo asset_url(); ?>image/misc/blank.png' style='float: left;top: -2px;position: relative;'>
	<?php endif;?>&nbsp;
	<a href="<?php echo root_url() . $setid; ?>/auth/dataset/" target='_parent'><?php echo $dataset->name; ?></a>
</div>
<?php endforeach; ?>