<div class="fsjtabcont synch">
    <dl class="fsjtabs" id="main">
        <dt id="summary" class="open top_round"><span>Summary</span></dt>
        <dt id="create" class="closed top_round"><span>New (<?php echo count($create); ?>)</span></dt>
        <dt id="update" class="closed top_round"><span>Updates (<?php echo count($update); ?>)</span></dt>
        <dt id="delete" class="closed top_round"><span>Delete (<?php echo count($delete); ?>)</span></dt>
    </dl>
    <div class="current" id="tab_summary">
        <p>Warning! Continuing with timetable synchronisation may affect mapped data.</p>
        <p>The following tabs show a summary of the changes that will take place if the synchronisation is continued. If a session is not mentioned there are no changes to it.</p>
        <p>Please review the changes to ensure that you are happy to proceed.</p>
    </div>
    <div class="current" id="tab_create" style="display: none">
        <p>The <?php echo count($create); ?> sessions below will be created if the synchronisation is continued:</p>
        <ul class="no_icon">
            <?php
            foreach ($create as $session) {
                $this->load->view('timetable/snippets/preview_create', $session);
            }
            ?>
        </ul>
    </div>
    <div class="current" id="tab_update" style="display: none">
        <p>The <?php echo count($update); ?> sessions below will be updated if the synchronisation is continued. Existing mapped relationships will be retained:</p>
        <ul class="no_icon">
            <?php
            foreach ($update as $session) {
                $this->load->view('timetable/snippets/preview_update', $session);
            }
            ?>
        </ul>
    </div>
    <div class="current" id="tab_delete" style="display: none">
        <p>The <?php echo count($delete); ?> sessions below will be removed if the synchronisation is continued. Existing mapped relationships will be deleted:</p>
        <ul class="no_icon">
            <?php
            foreach ($delete as $session) {
                $this->load->view('timetable/snippets/preview_delete', array('session' => $session));
            }
            ?>
        </ul>
    </div>
</div>
