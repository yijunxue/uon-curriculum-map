<p>This page lists the sessions found in the timetable.</p>
<p>You can select an existing session that should be mapped to it from the dropdown next to it.</p>
<p>You may only select a session a single time.</p>
<p>Any existing sessions not selected here will be marked for deletion.</p>
<ul class="sessions no_icon">
<?php
foreach ($timetabled as $session) {
?>
    <li class="mapping">
        <div class="new session">
            <table>
                <tr>
                    <th>Title</th>
                    <td><?php echo $session['title'] ?></td>
                </tr>
                <tr>
                    <th>Code</th>
                    <td><?php echo $session['code'] ?></td>
                </tr>
                <tr>
                    <th>Time</th>
                    <td><?php echo $session['start'] ?> - <?php echo $session['end'] ?></td>
                </tr>
                <tr>
                    <th>Staff</th>
                    <td><?php echo $session['staff'] ?></td>
                </tr>
            </table>
        </div>
        <div class="exisiting session">
<?php
    $select_options = array(
        'session' => $session,
        'exisiting' => $existing,
        'mappings' => $mapping,
    );
    $this->load->view('timetable/snippets/map_select', $select_options);
?>
        </div>
    </li>
<?php
}
?>
</ul>
