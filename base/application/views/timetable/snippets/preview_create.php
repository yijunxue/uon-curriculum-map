<li>
    <table>
        <tr><th>Title</th><td><?php echo $title; ?></td></tr>
        <tr><th>Code</th><td><?php echo $code; ?></td></tr>
        <tr><th>Time</th><td><?php echo "$start - $end"; ?></td></tr>
        <tr><th>Staff</th><td><?php echo $staff; ?></td></tr>
<?php if ($this->dx_auth->get_current_role() == 'admin') { // Global admins should be able to inspect the guid. ?>
        <tr><th>GUID</th><td><?php echo $ttguid; ?></td></tr>
<?php } ?>
    </table>
</li>
