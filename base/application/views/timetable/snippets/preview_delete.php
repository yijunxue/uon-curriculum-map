<li>
    <table>
        <tr><th>Title</th><td><a class="a_popup" href="<?php echo site_url('/view/' . $session->getId()); ?>"><?php echo $session->getTitle(); ?></a></td></tr>
        <tr><th>Code</th><td><?php echo $session->getAttributeValue('code'); ?></td></tr>
        <tr><th>Time</th><td><?php echo "{$session->getAttributeValue('start')} - {$session->getAttributeValue('end')}"; ?></td></tr>
        <tr><th>Staff</th><td><?php echo $session->getAttributeValue('staff'); ?></td></tr>
<?php if ($this->dx_auth->get_current_role() == 'admin') { // Global admins should be able to inspect the guid. ?>
        <tr><th>GUID</th><td><?php echo $session->getAttributeValue('ttguid'); ?></td></tr>
<?php } ?>
    </table>
</li>
