<select id="<?php echo $session['ttguid']; ?>" name="<?php echo $session['ttguid']; ?>" class="session_select">
    <option value="">New Session</option>
<?php
// Check if there is a mapping for the session.
if (isset($mapping[$session['ttguid']])) {
    $session_mapping = $mapping[$session['ttguid']];
} else {
    $session_mapping = null;
}
foreach ($existing as $node) {
    // Check if this existing session is mapped to this node.
    if ($node->getId() == $session_mapping) {
        $selected = ' selected="selected"';
    } else {
        $selected = '';
    }
    $title = $node->getTitle();
    $code = $node->getAttributeValue('code');
?>
    <option value="<?php echo $node->getId(); ?>"<?php echo $selected; ?>><?php echo $code; ?> - <?php echo $title; ?></option>
<?php
}
?>
</select>
