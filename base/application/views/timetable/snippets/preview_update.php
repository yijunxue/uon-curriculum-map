<?php
$titleclass = (isset($differences['title']))?' class="difference"':'';
$codeclass = (isset($differences['code']))?' class="difference"':'';
$staffclass = (isset($differences['staff']))?' class="difference"':'';
if (isset($differences['start']) || isset($differences['end'])) {
    $timeclass = ' class="difference"';
} else {
    $timeclass = '';
}
$guidclass = (isset($differences['ttguid']))?' class="difference"':'';
?>
<li>
    <table>
        <tr>
            <td></td>
            <th>Old details</th>
            <th>New details</th>
        </tr>
        <tr<?php echo $titleclass; ?>>
            <th>Title</th>
            <td><a class="a_popup" href="<?php echo site_url('/view/' . $old->getId()); ?>"><?php echo $old->getTitle(); ?></a></td>
            <td><?php echo $new['title']; ?></td>
        </tr>
        <tr<?php echo $codeclass; ?>>
            <th>Code</th>
            <td><?php echo $old->getAttributeValue('code'); ?></td>
            <td><?php echo $new['code']; ?></td>
        </tr>
        <tr<?php echo $timeclass; ?>>
            <th>Time</th>
            <td><?php echo "{$old->getAttributeValue('start')} - {$old->getAttributeValue('end')}"; ?></td>
            <td><?php echo "{$new['start']} - {$new['end']}"; ?></td>
        </tr>
        <tr<?php echo $staffclass; ?>>
            <th>Staff</th>
            <td><?php echo $old->getAttributeValue('staff'); ?></td>
            <td><?php echo $new['staff']; ?></td>
        </tr>
<?php if ($this->dx_auth->get_current_role() == 'admin') { // Global admins should be able to inspect the guid. ?>
        <tr<?php echo $guidclass; ?>>
            <th>GUID</th>
            <td><?php echo $old->getAttributeValue('ttguid'); ?></td>
            <td><?php echo $new['ttguid']; ?></td>
        </tr>
<?php } ?>
    </table>
</li>
