<table class="relations" width="100%">
<?php 
// need to allow the + to expand and display the listed nodes relations

foreach ($rels as $nodetype => $relnodes)
{
	$type = $this->node_types->GetType($nodetype);
	
	echo "<tr><td colspan=2>{$type->getName()}</td></tr>"; 
	// need to find the type from the id
foreach ($relnodes as &$relnode): ?>
	<?php $subid = mt_rand(); ?>
	<tr>
			<td class='td_expand'>
				<a href="#" onclick="return LoadSubNodes(<?php echo $relnode->getId(); ?>, <?php echo $subid; ?>);">
					<img src='<?php echo asset_url(); ?>image/misc/add.png' id='image_<?php echo $subid; ?>'>
				</a>
			</td>
			<td>
				<div class="relations_title">
					<a href='<?php echo site_url('view/' . $relnode->getId()); ?>'>
						<?php echo $relnode->getTitleDisp(); ?>	
					</a>
				</div>
				<div class='sub_rels' style="display:none" id='subrels_<?php echo $subid; ?>'></div>
			</td>
		</tr>
<?php
	endforeach;
}
?>
</table>	
