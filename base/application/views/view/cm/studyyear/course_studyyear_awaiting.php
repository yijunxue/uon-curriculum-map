<?php

// module node - display courses via year of study if available
$this->load->helper('study_year');

$mods_by_year = get_modules_by_studyyear($node);
if (count($mods_by_year) > 0) {
  $modules = array_pop($mods_by_year);

  if ($modules['title'] == 'none')
  {
    $modules = $modules['modules'];

    $title_output = false;

    foreach ($modules as &$module)
    {
      if (!$title_output)
      {
        echo "<div class='rel_title'>Modules with no Study Year</div>";
        $title_output = true;
      }

      echo "<div><a href='" . site_url("view/" . $module->getId()) . "'>" . $module->getTitleDisp() . "</a></div>";
    }
  }
}
