<?php

// module node - display courses via year of study if available

$module = $node->getParent();

$studyyears = $module->getRelations("studyyear");
$this->graph_db->sort_nodes($studyyears, "order|title");

$seen_course = array();

foreach ($studyyears as &$studyyear)
{
	$course = $studyyear->getParent();
	echo "<a href='" . site_url("view/" . $course->getId()) . "'>" . $course->getTitleDisp() . "</a> (" . $studyyear->getTitleDisp().")<br>";	
	$seen_course[$course->getId()] = $course->getId();
}

$courses = $module->getRelations("course");

foreach ($courses as &$course)
{
	if (array_key_exists($course->getId(), $seen_course)) continue;
	
	echo "<a href='" . site_url("view/" . $course->getId()) . "'>" . $course->getTitleDisp() . "</a><br>";
}