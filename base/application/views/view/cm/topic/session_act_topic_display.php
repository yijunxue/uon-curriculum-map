<?php

// session or learning activity node - display modules via topic if available
$topics = $node->getRelations("topic");
$this->graph_db->sort_nodes($topics, "order|title");

$seen_module = array();

foreach ($topics as &$topic)
{
	$module = $topic->getParent();
	$mod_id = $module->getId();

	if (!isset($seen_module[$mod_id])) {
		$seen_module[$mod_id] = array();
	}
	$seen_module[$mod_id][] = $topic->getTitleDisp();
}

$modules = $node->getRelations("module");

foreach ($modules as &$module)
{
	$mod_id = $module->getId();

	if (array_key_exists($mod_id, $seen_module)) {
		echo "<a href='" . site_url("view/" . $mod_id) . "'>" . $module->getTitleDisp() . '</a> (';
		$topics = '';
		foreach ($seen_module[$mod_id] as $topic) {
			$topics .= $topic . ',';
		}
		echo rtrim($topics, ',');
		echo ')<br>';
	} else {
		echo "<a href='" . site_url("view/" . $mod_id) . "'>" . $module->getTitleDisp() . "</a><br>";
	}
}