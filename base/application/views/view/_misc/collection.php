<div class='big_tools' style='right: -13px;'>
<?php if ($collnode->can_link()): ?>
	<a href='<?php echo site_url("link/" . $collnode->getId()); ?>' class='button'><img src='<?php echo asset_url(); ?>image/misc/add.png' alt='Add'>Add Item</a>
<?php endif; ?>
<?php if ($collnode->can_edit()): ?>
	<a href='<?php echo site_url("node/edit/" . $collnode->getId()); ?>' class='button'><img src='<?php echo asset_url(); ?>image/node/edit.png' alt='Edit'>Edit Collection</a>
<?php endif; ?>
<?php if ($collnode->can_delete()): ?>
	<a href='<?php echo site_url("node/delete/" . $collnode->getId()); ?>' class='button'><img src='<?php echo asset_url(); ?>image/misc/delete.png' alt='Delete'>Delete Collection</a>
<?php endif; ?>
<?php if ($collnode->can_perms()): ?>
	<a href='<?php echo site_url("perms/index/" . $collnode->getId()); ?>' class='button'><img src='<?php echo asset_url(); ?>image/node/perms.png' alt='Permissions'>Permissions</a>
<?php endif; ?>
<?php if ($collnode->can_create()): ?>
	<a href='<?php echo site_url("node/create/" . $collnode->getId() ."/11/1"); ?>' class='button'><img src='<?php echo asset_url(); ?>image/misc/add.png' alt='Create Resource'>Create Resource</a>
<?php endif; ?>
<?php if ($this->dx_auth->is_role("lecturer")): ?>
	<a href='<?php echo site_url("node/reporting/" . $collnode->getId()); ?>' class='button'><img src='<?php echo asset_url(); ?>image/node/reporting.png' alt='Reports'>Reporting</a>
<?php endif; ?>
&nbsp;
</div>
<div>
	<?php echo $collnode->getAttribute("desc"); ?>
</div>
<div class='clear'></div>
<?php foreach ($nodelist as $reltype => &$nodetypes): ?>
	<?php //if ($reltype != GDB_Rel::Relation && $reltype != GDB_Rel::ChildOf) continue; ?>
	<?php foreach ($nodetypes as $nodetype => &$nodes): ?>
		<?php $type = $this->node_types->GetType($nodetype); ?>
		<div class='rel_title'><?php echo $type->getName(); ?></div>
		<?php foreach ($nodes as &$relnode): ?>
			<div class='small_tools'>
				<?php if ($collnode->can_link() && $collnode->getParent()->getID() !== $relnode->getId()) { ?>
					<a class='hasTooltip_bl collremoveconfirm' href='<?php echo site_url("node/unlink/" . $collnode->getId() . "/" . $relnode->getId()); ?>' title='Remove this item from the collection'>
						<img src='<?php echo asset_url(); ?>image/misc/delete.png' alt='Remove'>
					</a>
				<?php } ?>
			</div>
			<div class="relations_title">
				<?php if ($nodetype == 11): ?>
					<a href="<?php echo $relnode->getAttributeValue('url'); ?>" target='_blank'>
						<?php echo $relnode->getTitleDisp(); ?>
					</a>
				<?php else: ?>
				<a href='<?php echo site_url('view/' . $relnode->getId()); ?>'>
					<?php echo $relnode->getTitleDisp(); ?>
				</a>
				<?php endif; ?>
			</div>
			<div class='clear'></div>
		<?php endforeach; ?>
	<?php endforeach; ?>
<?php endforeach; ?>

