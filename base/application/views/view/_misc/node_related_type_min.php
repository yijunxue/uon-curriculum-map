<?php 
// need to allow the + to expand and display the listed nodes relations
foreach ($relnodes as &$relnode): ?>
	<div class="relations_title">
		<a href='<?php echo site_url('view/' . $relnode->getId()); ?>'>
			<?php echo $relnode->getTitleDisp(); ?>	
		</a>
	</div>
<?php endforeach; ?>
