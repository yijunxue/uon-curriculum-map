<?php if (count($nodetree['nodes']) > 0):?>

	<?php echo $nodetree['layer']->GetHeader(); ?>

	<?php foreach ($nodetree['nodes'] as &$relnode): ?>
		<?php $subid = mt_rand(); ?>
		<?php if (!$nodetree['finallayer']) : ?>
			<a href="#" onclick="return ExpandNodeTree('<?php echo $nodetree['sectionid'] ?>', '<?php echo $nodetree['selected']; ?><?php echo $relnode->getId(); ?>', <?php echo $subid; ?>);">
				<img src='<?php echo asset_url(); ?>image/misc/add.png' id='image_<?php echo $subid; ?>'>
			</a>
		<?php endif; ?>
		<div class="relations_title">
			<a href='<?php echo site_url('view/' . $relnode->getId()); ?>'>
				<?php echo $relnode->getTitleDisp(); ?>	
			</a>
		</div>
		<div class='sub_rels' style="display:none" id='subrels_<?php echo $subid; ?>'></div>
	<?php endforeach; ?>
<?php else: ?>
<!--	No nodes found -->
<?php endif; ?>