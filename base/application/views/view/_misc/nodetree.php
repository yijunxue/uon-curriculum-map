<?php if (count($nodetree['nodes']) > 0):?>

	<?php if ($nodetree['layer']->header) :?>
		<?php if ($nodetree['layer']->header_raw) :?>
			<?php echo $nodetree['layer']->header; ?>	
		<?php else: ?>
			<h4>
				<?php echo $nodetree['layer']->header; ?>	
			</h4>
		<?php endif; ?>
	<?php endif; ?>
	
	<table class="relations" width="100%">
	<?php foreach ($nodetree['nodes'] as &$relnode): ?>
		<?php $subid = rand(10000000,99999999); ?>
		<tr>
			<?php if (!$nodetree['finallayer']) : ?>
			<td width="32">
				<a href="#" onclick="return LoadSubNodes('<?php echo $nodetree['sectionid'] ?>', '<?php echo $nodetree['selected']; ?><?php echo $relnode->getId(); ?>', <?php echo $subid; ?>);">
					<img src='<?php echo asset_url(); ?>image/misc/add.png' id='image_<?php echo $subid; ?>'>
				</a>
			</td>
			<?php endif; ?>
			<td>
				<?php /*if ($node): ?>
				<div class="relations_link">
					<?php if ($node->GetId() == $relnode->GetId()): ?>
						<img src='<?php echo asset_url(); ?>image/misc/link_na.png' id='image_<?php echo $subid; ?>'>
					<?php elseif ($node->isRelatedTo($relnode)): ?>
						<a href="#" onclick='return ToggleLink(<?php echo $node->GetId(); ?>, <?php echo $relnode->GetId(); ?>);'>
							<img class="link_img_<?php echo $relnode->GetId(); ?>" src='<?php echo asset_url(); ?>image/misc/link_yes.png'>
						</a>
					<?php else: ?>
						<a href="#" onclick='return ToggleLink(<?php echo $node->GetId(); ?>, <?php echo $relnode->GetId(); ?>);'>
							<img class="link_img_<?php echo $relnode->GetId(); ?>" src='<?php echo asset_url(); ?>image/misc/link_no.png'>
						</a>
					<?php endif; ?>
				</div>
				<?php endif;*/ ?>
				<div class="relations_title">
					<a href='<?php echo site_url('node/' . $relnode->getId()); ?>'>
						<?php echo $relnode->getTitle(); ?>	
					</a>
				</div>
				<div class='sub_rels' style="display:none" id='subrels_<?php echo $subid; ?>'></div>
			</td>
		</tr>
	<?php endforeach; ?>
	</table>
<?php else: ?>
	No nodes found
<?php endif; ?>