<?php $groupval = ""; ?>
<?php foreach ($nodelist as &$node): ?>
<?php if ($groupby) {
	$newval = $node->getAttributeValue($groupby);
	if ($newval != $groupval)
	{
		echo "<div class='nodelist_group'>" . $node->getAttribute($groupby)->format() . "</div>";
		$groupval = $newval;	
	}
} ?>
<div class="relations_title">
	<?php if ($tools): ?>
		<?php echo ViewTools::NodeTools($node, $tools); ?>
	<?php endif; ?>
	<div class="<?php echo $class; ?>">
		<a class="<?php if ($tooltip) echo ' hasTooltip_wide'; ?>" href="<?php 
		
		if ($linkto)
		{
			echo $node->getAttributeValue($linkto) . '" target="_new';
		} else {
			echo site_url("view/" . $node->getId()); 
		}
		
		?>"
	<?php if ($tooltip): ?>
		title="<?php echo htmlspecialchars($node->getAttributeValue($tooltip)); ?>"
	<?php endif; ?>		
		>
			<?php echo $node->getTitleDisp($longtitle); ?>
		</a>
	</div>
</div>
	<div class='clear'></div>
<?php endforeach; ?>