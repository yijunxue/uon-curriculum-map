<table class="niceround">
	<tr>
		<th valign="top" nowrap>Type</th>
		<td><?php echo $node->getTypeName(); ?></td>
	</tr>
	<tr>
		<th valign="top" nowrap>Orig Author</th>
		<td><?php echo $this->dx_auth->getUser($node->orig_author)->username; ?></td>
	</tr>
	<tr>
		<th valign="top" nowrap>Last Author</th>
		<td><?php echo $this->dx_auth->getUser($node->last_author)->username; ?></td>
	</tr>
	<tr>
		<th valign="top" nowrap>Created</th>
		<td><?php echo nice_date($node->created); ?></td>
	</tr>
	<tr>
		<th valign="top" nowrap>Modified</th>
		<td><?php echo nice_date($node->modified); ?></td>
	</tr>
	<tr>
		<th valign="top" nowrap>Revisions</th>
		<td><?php echo $node->revised; ?></td>
	</tr>
</table>
