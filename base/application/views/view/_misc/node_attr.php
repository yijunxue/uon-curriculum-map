<table class="niceround">
<?php foreach($node->GetTypeObj()->GetAttributes() as $attr): ?>
	<tr>
		<th valign="top" nowrap><?php echo $attr->GetTitle(); ?></th>
		<td><?php echo $node->getAttribute($attr->id); ?></td>
	</tr>
<?php endforeach; ?>
</table>