
	<?php echo $layer->GetHeader(); ?>

<table class="relations" width="100%">

<?php // output nodes based on curlayer and layernodes
if (is_array($nodelist)) foreach ($nodelist as &$node): ?>
	<?php $subid = mt_rand(); ?>
	<tr>
			<?php if ($curlayer > 0) : ?>
			<td class='td_expand'>
				<a href="#" onclick="return ToggleElement('<?php echo $subid; ?>');">
					<img src='<?php echo asset_url(); ?>image/misc/add.png' id='image_<?php echo $subid; ?>'>
				</a>
			</td>
			<?php endif; ?>
			<td>
				<div class="relations_title">
					<a href='<?php echo site_url('view/' . $node->getId()); ?>'>
						<?php echo $node->getTitleDisp(); ?>	
					</a>
				</div>
				<?php if ($curlayer > 0) : ?>
					<div class='sub_rels' style='display:none' id='subrels_<?php echo $subid; ?>'>
					<?php 
						$this->data['curlayer'] = $curlayer - 1;
						$this->data['nodelist'] = $node->parents;
						$this->data['layer'] = $this->layers[$this->data['curlayer']];
		
						$this->load->view('view/_misc/treerev_main', $this->data);
					?>
					</div>
				<?php endif; ?>
			</td>
		</tr>
	<?php endforeach; ?>
	
</table>
