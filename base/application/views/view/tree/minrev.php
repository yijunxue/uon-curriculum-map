<?php global $minrevdepth, $minrevmaxdepth, $minrevclass; ?>
<?php $done_header = false; $outputcount = 0; ?>
<?php if (count($tree['nodes']) > 0):?>
	<?php if ($minrevdepth > $minrevmaxdepth) $minrevmaxdepth = $minrevdepth; ?>
	<?php foreach ($tree['nodes'] as &$relnode): ?>
		<?php 
			if (array_key_exists($relnode->getID(), $tree['all_seen'])) 
				continue;
		
			$subid = mt_rand();
	
			if (!$done_header)
			{
				echo $tree['layer']->GetHeader();
				$done_header = true;
			}
		
			// work out if the node can be expanded or not
			$canexpand = true;
			if (count($tree['layer']->layers) == 0)
				$canexpand = false;
			
			if (!array_key_exists($relnode->getId(),$tree['can_expand']) ||!$tree['can_expand'][$relnode->getId()] )
			{
				if (!$tree['always_expand'])
					$canexpand = false;
			}
			
			$outputcount++;
		?>
				
				<div style="position:relative;left:-10px;">
				<?php if ($tree['layer']->expand): ?>
					<?php $minrevdepth++; ?>
					<?php $view->TreeLayerOutputSet($tree['layer'], $tree['selected'] . $relnode->getId()); ?>
					<?php $minrevdepth--; ?>
				<?php endif; ?>
					<div style="">
					<?php if ($tree['layer']->nolink): ?>
						<?php echo $relnode->getTitleDisp($tree['layer']->longtitle); ?>	
					<?php else: ?>
						<a href='<?php echo site_url('view/' . $relnode->getId()); ?>'>
							<?php echo $relnode->getTitleDisp($tree['layer']->longtitle); ?>	
						</a>
					<?php endif; ?>
					</div>
				</div>
				

	<?php endforeach; ?>
<?php endif; ?>

<?php if ($outputcount == 0 && $tree['layer']->nonefound): ?>
	<div class='none_found'><?php echo $tree['layer']->nonefound; ?></div>
<?php endif; ?>
