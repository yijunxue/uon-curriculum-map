<?php $done_header = false; $outputcount = 0; ?>
<?php if (count($tree['nodes']) > 0):?>
	
	<?php foreach ($tree['nodes'] as &$relnode): ?>
		<?php 
			if (array_key_exists($relnode->getID(), $tree['all_seen'])) 
				continue;
		
			$subid = mt_rand();
	
			if (!$done_header)
			{
				echo $tree['layer']->GetHeader();
				$done_header = true;
			}
		
			// work out if the node can be expanded or not
			$canexpand = true;
			if (count($tree['layer']->layers) == 0)
				$canexpand = false;
			
			if (!array_key_exists($relnode->getId(),$tree['can_expand']) ||!$tree['can_expand'][$relnode->getId()] )
			{
				if (!$tree['always_expand'])
					$canexpand = false;
			}
			
			$outputcount++;
		?>
		
				<?php if ($tree['layer']->tools): ?>
					<?php echo ViewTools::NodeTools($relnode, $tree['layer']->tools); ?>
				<?php endif; ?>
				<div class="relations_title">
					<?php if ($tree['layer']->nolink): ?>
						<?php echo $relnode->getTitleDisp($tree['layer']->longtitle); ?>	
					<?php else: ?>
						<a href='<?php echo site_url('view/' . $relnode->getId()); ?>'>
							<?php echo $relnode->getTitleDisp($tree['layer']->longtitle); ?>	
						</a>
					<?php endif; ?>
				</div>
				<?php if (!$tree['layer']->noindent): ?>
					<div class='sub_rels' <?php if (!$tree['layer']->expand): ?> style="display:none"<?php endif; ?> id='subrels_<?php echo $subid; ?>'>
					<?php if ($tree['layer']->expand): ?>
						<?php $view->TreeLayerOutputSet($tree['layer'], $tree['selected'] . $relnode->getId()); ?>
					<?php endif; ?>
					</div>
				<?php endif; ?>
	<?php endforeach; ?>
<?php endif; ?>

<?php if ($outputcount == 0 && $tree['layer']->nonefound): ?>
	<div class='none_found'><?php echo $tree['layer']->nonefound; ?></div>
<?php endif; ?>
