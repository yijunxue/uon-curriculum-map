<div class='wp_popup ui-tooltip-rounded ui-tooltip-shadow ui-tooltip-pad'>
<div class="wp_close hasTooltip" title="Cancel"><img src="<?php echo asset_url(); ?>image/misc/close.png"></div>
<div class='wp_title'>Select a new weight:</div>
<div class='wp_weights'>
<?php for ($i = 0 ; $i < 101 ; $i += 10): ?>
	<div class='wp_weight hasTooltip' title="<?php echo $i; ?>%" onclick="SetWeight(<?php echo $i; ?>)"><?php echo WeightImage($i/100); ?></div>
<?php endfor; ?>
</div>
</div>
<script>

</script>