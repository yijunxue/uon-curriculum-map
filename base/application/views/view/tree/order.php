<?php
$outputcount = 0;
$curgroup = "----------------------";
$groups = array();
$curgrouporder = 0;
$groupoffset = 0;
$curitemorder = 0;
?>
<div id='order_img_base' style="display:none;"><?php echo asset_url(); ?>image/misc/</div>
<?php
if ($tree['layer']->group)
{
?>
<p>Drag items to re-order within a group. To re-order the groups themselves check the box below. Press <b>Save</b> when finished.</p>
<p><label for="ordermode"><b>Order groups:</b></label> <input type="checkbox" name="ordermode" id="ordermode"></p>
<?php
} else {
?>
<p>Drag items to re-order and press <b>Save</b> when finished.</p>
<?php
}

if ($tree['layer']->group)
{
?>
<ul class="relations_group">
<?php
}

if (count($tree['nodes']) > 0):?>

	<?php foreach ($tree['nodes'] as &$relnode): ?>
		<?php
			$subid = mt_rand();

			$outputcount++;

			$groupid = 0;
			if ($tree['layer']->group)
			{
				$newgroup = $relnode->getAttributeValue($tree['layer']->group);
				if ($newgroup != $curgroup)
				{
					if ($curgroup != "----------------------")
					{
						echo "</ul></li>\n";
					}

					$group_order = $relnode->getAttributeValue("order");
					if ($group_order == "")
					{
						$group_order = str_pad($curgrouporder + $groupoffset, 3, '0', STR_PAD_LEFT);
						$groupoffset++;
					} else {
						$group_order = explode("|", $group_order);
						$curgrouporder = intval($group_order[0]) + $groupoffset;
						$group_order = str_pad($curgrouporder, 3, '0', STR_PAD_LEFT);
					}

					// $groupid = abs(crc32($curgroup));
					$groupid = $newgroup;
					$groups[$groupid] = array();
					$groups[$groupid]['title'] = $curgroup;
					$groups[$groupid]['order'] = $group_order;

					echo "<li data-rowid=\"$groupid\">";

					if ($newgroup == "")
					{
						echo '<span  class="relations_title kvpair_title">No Group</span>';
					} else {
						$title_attr = groupAttribFromGuid($relnode->getAttributeValue($tree['layer']->group));
						echo '<span  class="relations_title">' . $title_attr->format() . '</span>';
					}
					$curgroup = $newgroup;

					echo "<input name='group_order_$groupid' id='group_order_$groupid' class='groupinput' value='$group_order' type='hidden'>\n";

					$curitemorder = 0;

					echo "<ul class=\"relations_items\">\n";
				}
			} elseif ($curgroup == "----------------------") {
				echo "<ul class=\"relations_items\">\n";
				$curgroup = '';
			}

			$item_order = $relnode->getAttributeValue('order');
			if ($item_order == "")
			{
				$item_order = $curitemorder++;
			} else {
				$item_order = explode("|", $item_order);
				$item_order = (int)$item_order[1];
			}
		?>
		<li id="row_<?php echo $subid; ?>" class="relations_title item group_<?php echo ""; ?>" data-rowid="<?php echo $subid; ?>">
			<?php //echo ViewTools::OrderTools($relnode, $tree['layer']->tools); ?>
				<?php if ($tree['layer']->nolink): ?>
					<?php echo $relnode->getTitleDisp($tree['layer']->longtitle); ?>
				<?php else: ?>
					<a href='<?php echo site_url('view/' . $relnode->getId()); ?>'>
						<?php echo $relnode->getTitleDisp($tree['layer']->longtitle); ?>
					</a>
				<?php endif; ?>
			<input name="order_<?php echo $relnode->getId(); ?>" id="order_<?php echo $subid; ?>" value="<?php echo $item_order; ?>" type='hidden'>
		</li>
	<?php endforeach; ?>
</ul>
<?php endif; ?>

<?php
if ($tree['layer']->group)
{
?>
</li></ul>
<?php
}


if ($outputcount == 0 && $tree['layer']->nonefound): ?>
	<div class='none_found'><?php echo $tree['layer']->nonefound; ?></div>
<?php endif; ?>

<?php //print_p($groups); ?>