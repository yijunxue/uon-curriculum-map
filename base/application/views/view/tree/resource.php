<?php $done_header = false; $outputcount = 0; $curgroup = ""; ?>

	<?php if (count($tree['nodes']) > 0):?>

<?php if ($tree['layer']->indent): ?>
<style>
.table_<?php echo $tree['sectionid']; ?> .td_expand
{
	width:36px !important;
	text-align: right;
}
</style>
<?php endif; ?>

<table class="relations table_<?php echo $tree['sectionid']; ?>" width="100%">
	<?php foreach ($tree['nodes'] as &$relnode): ?>
		<?php
			if (array_key_exists($relnode->getID(), $tree['all_seen']))
				continue;

			$subid = mt_rand();

			if (!$done_header)
			{
				echo $tree['layer']->GetHeader();
				$done_header = true;
			}

			$outputcount++;

			if ($tree['layer']->group)
			{
				$newgroup = $relnode->getAttributeValue($tree['layer']->group);
				if ($newgroup != $curgroup)
				{
					if ($newgroup == "")
					{
						echo "<tr><td colspan='2'><div class='kvpair_title'>No Group</div></td></tr>";
					} else {
						$title_attr = groupAttribFromGuid($relnode->getAttributeValue($tree['layer']->group));
						echo "<tr><td colspan='2'>" . $title_attr->format() . "</td></tr>";
					}
					$curgroup = $newgroup;
				}
			}

		?>

		<tr>
			<td class='td_expand'>
				<img src='<?php echo asset_url(); ?>image/misc/blank.png' id='image_<?php echo $subid; ?>' width="16" height="16">
			</td>
			<td>
				<?php if (array_key_exists('link',$tree)): ?>
					<?php echo ViewTools::LinkNode($relnode, $node); ?>
				<?php endif; ?>
				<?php if ($tree['layer']->tools): ?>
					<?php echo ViewTools::NodeTools($relnode, $tree['layer']->tools); ?>
				<?php endif; ?>
				<div class="relations_title">
					<a href='<?php echo $relnode->getAttributeValue("url"); ?>' target='_newtab'>
						<?php echo $relnode->getTitleDisp($tree['layer']->longtitle); ?>
					</a>
				</div>
			</td>
		</tr>
	<?php endforeach; ?>
</table>
<?php endif; ?>

<?php if ($outputcount == 0 && $tree['layer']->nonefound): ?>
	<div class='none_found'><?php echo $tree['layer']->nonefound; ?></div>
<?php endif; ?>
