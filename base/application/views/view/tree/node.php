<?php $done_header = false; $outputcount = 0; $curgroup = "---------------"; ?>

	<?php if (count($tree['nodes']) > 0):?>

<?php if ($tree['layer']->indent): ?>
<style>
.table_<?php echo $tree['sectionid']; ?> .td_expand
{
	width:36px !important;
	text-align: right;
}
</style>
<?php endif; ?>

<table class="relations table_<?php echo $tree['sectionid']; ?>" width="100%">
	<?php foreach ($tree['nodes'] as &$relnode): ?>
		<?php
			if (array_key_exists($relnode->getID(), $tree['all_seen']))
			{
//				echo "Skipping {$relnode->getID()}, already seen<br>";
				continue;
			}

			$subid = mt_rand();

			if (!$done_header)
			{
				echo $tree['layer']->GetHeader();
				$done_header = true;
			}

			// work out if the node can be expanded or not
			$canexpand = true;
			if (count($tree['layer']->layers) == 0)
				$canexpand = false;

			if (!array_key_exists($relnode->getId(),$tree['can_expand']) ||!$tree['can_expand'][$relnode->getId()] )
			{
				if (!$tree['always_expand'])
					$canexpand = false;
			}

			$outputcount++;

			if ($tree['layer']->group)
			{
				$newgroup = $relnode->getAttributeValue($tree['layer']->group);

				if ($newgroup != $curgroup)
				{
					if ($newgroup == "")
					{
						echo "<tr><td colspan='3'><div class='kvpair_title'>No Group</div></td></tr>";
					} else {
						$title_attr = groupAttribFromGuid($relnode->getAttributeValue($tree['layer']->group));
            echo "<tr><td colspan='3'>" . $title_attr->format() . "</td></tr>";
					}
					$curgroup = $newgroup;
				}
			}

		?>
		<tr>
			<?php if ($tree['layer']->expand) : ?>
				<td class='td_expand'>
					<a href="#" onclick="return TreeExpand('<?php echo $tree['sectionid'] ?>', '<?php echo $tree['selected']; ?><?php echo $relnode->getId(); ?>', <?php echo $subid; ?>);">
						<img src='<?php echo asset_url(); ?>image/misc/delete.png' id='image_<?php echo $subid; ?>'>
					</a>
				</td>
			<?php elseif ($canexpand): ?>
				<td class='td_expand'>
					<a href="#" onclick="return TreeExpand('<?php echo $tree['sectionid'] ?>', '<?php echo $tree['selected']; ?><?php echo $relnode->getId(); ?>', <?php echo $subid; ?>);">
						<img src='<?php echo asset_url(); ?>image/misc/add.png' id='image_<?php echo $subid; ?>'>
					</a>
				</td>
			<?php else: ?>
				<td class='td_expand'>
					<img src='<?php echo asset_url(); ?>image/misc/sphere.png' id='image_<?php echo $subid; ?>' width="16" height="16">
				</td>
			<?php endif; ?>
			<?php if ($tree['layer']->weight): ?>
				<td class="td_weight">
					<?php $wi = round($relnode->weight*100,0) ."% to " . $tree['weight_node']->getTitleDisp($tree['layer']->longtitle); ?>
					<span class="hasTooltip" title="<?php echo htmlentities(strip_tags($wi)); ?>"><?php echo WeightImage($relnode->weight); ?></span>
				</td>
			<?php endif; ?>
			<td>
				<?php if (array_key_exists('link',$tree)): ?>
					<?php echo ViewTools::LinkNode($relnode, $node, $tree['layer']); ?>
				<?php endif; ?>
				<?php if ($tree['layer']->tools): ?>
					<?php echo ViewTools::NodeTools($relnode, $tree['layer']->tools); ?>
				<?php endif; ?>
				<div class="relations_title">
					<?php if ($tree['layer']->nolink): ?>
						<?php echo $relnode->getTitleDisp($tree['layer']->longtitle); ?>
					<?php else: ?>
						<a href='<?php echo site_url('view/' . $relnode->getId()); ?>'>
							<?php echo $relnode->getTitleDisp($tree['layer']->longtitle); ?>
						</a>
					<?php endif; ?>
				</div>
				<?php if (!$tree['layer']->noindent): ?>
					<div class='sub_rels' <?php if (!$tree['layer']->expand): ?> style="display:none"<?php endif; ?> id='subrels_<?php echo $subid; ?>'>
					<?php if ($tree['layer']->expand): ?>
						<?php $view->TreeLayerOutputSet($tree['layer'], $tree['selected'] . $relnode->getId()); ?>
					<?php endif; ?>
					</div>
				<?php endif; ?>
			</td>
		</tr>
		<?php if ($tree['layer']->noindent): ?>
			<tr colspan="2">
				<div class='sub_rels' <?php if (!$tree['layer']->expand): ?> style="display:none"<?php endif; ?> id='subrels_<?php echo $subid; ?>'>
					<?php if ($tree['layer']->expand): ?>
						<?php $view->TreeLayerOutputSet($tree['layer'], $tree['selected'] . $relnode->getId()); ?>
					<?php endif; ?>
				</div>
			</tr>
		<?php endif; ?>
	<?php endforeach; ?>
</table>
<?php endif; ?>

<?php if ($outputcount == 0 && $tree['layer']->nonefound): ?>
	<div class='none_found'><?php echo $tree['layer']->nonefound; ?></div>
<?php endif; ?>
