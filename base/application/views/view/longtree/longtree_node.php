<?php if (!$ajax) echo "<div id='longtree_$sectionid'>"; ?>

<?php if (count($longtree['nodes']) > 0):?>
<?php 
$p = new Pagination(); 
$p->template = "<a href='#' onclick=\"setOffset('".$longtree['sectionid']."',%%PAGE%%);return false;\">%%NAME%%</a>";
$p->prevnexttmpl = $p->template;
$pagination = $p->OutputPages(count($longtree['nodes']), $longtree['page'], $longtree['perpage']);
?>
	
	<?php echo $longtree['layer']->GetHeader(); ?>

<?php echo $pagination; ?>
<table class="relations" width="100%">
<?php $i = 0; ?>
	<?php foreach ($longtree['nodes'] as &$relnode): ?>
		<?php $i++; ?>
		<?php //echo "$i => {$relnode->getTitleDisp()} - " ?>
		<?php if ($i <= $longtree['offset']) { /*echo "SKIP<br>";*/ continue;} ?>
		<?php if ($i > $longtree['offset'] + $longtree['perpage']) { /*echo "SKIP<br>";*/ continue;} ?>
		<?php //echo "OK<br>"; ?>
		
		<?php $subid = mt_rand(); ?>
		<tr>
			<?php if (!$longtree['finallayer']) : ?>
				<td class='td_expand'>
					<a href="#" onclick="return ExpandNodeTree('<?php echo $longtree['sectionid'] ?>', '<?php echo $longtree['selected']; ?><?php echo $relnode->getId(); ?>', <?php echo $subid; ?>);">
						<img src='<?php echo asset_url(); ?>image/misc/add.png' id='image_<?php echo $subid; ?>'>
					</a>
				</td>
			<?php else: ?>
				<td class='td_expand'>
					<img src='<?php echo asset_url(); ?>image/misc/sphere.png' id='image_<?php echo $subid; ?>' width="16" height="16">
				</td>
			<?php endif; ?>
			<td>
				<?php if ($longtree['layer']->tools): ?>
					<?php echo ViewTools::NodeTools($relnode, $longtree['layer']->tools, $node); ?>
				<?php endif; ?>
				<div class="relations_title">
					<?php if ($longtree['layer']->nolink): ?>
						<?php echo $relnode->getTitleDisp($longtree['layer']->longtitle); ?>	
					<?php else: ?>
						<a href='<?php echo site_url('view/' . $relnode->getId()); ?>'>
							<?php echo $relnode->getTitleDisp($longtree['layer']->longtitle); ?>	
						</a>
					<?php endif; ?>
				</div>
				<div class='sub_rels' style="display:none" id='subrels_<?php echo $subid; ?>'></div>
			</td>
		</tr>
	<?php endforeach; ?>
</table>
<?php echo $pagination; ?>

<?php else: ?>
<!--	No nodes found -->
<?php endif; ?>


<?php if (!$ajax) echo "</div>"; ?>
