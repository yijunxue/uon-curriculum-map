<table class='niceround'>
<?php if ($select): ?>
<tr>
	<th><?php echo $select_title; ?></th>
	<td><?php echo $select_dropdown; ?></td>
</tr>
<?php endif; ?>
<tr>
	<th>Search:</th>
	<td><input name='search_<?php echo $longtree['sectionid']; ?>' id='search_<?php echo $longtree['sectionid']; ?>'></td>
	<td class='pagination'><a href='#' onclick='searchReset("<?php echo $longtree['sectionid']; ?>");return false;'>Reset</a></td>
</tr>	
</table>

<script>

$(document).ready( function () {

    $('#search_<?php echo $longtree['sectionid']; ?>').change(function () {
        searchUpdate("<?php echo $longtree['sectionid']; ?>");
    });

    $('#search_<?php echo $longtree['sectionid']; ?>').keyup(function () {
        searchUpdate("<?php echo $longtree['sectionid']; ?>");
    });
});

</script>