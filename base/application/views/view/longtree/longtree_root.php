<?php if ($longtree['url']): ?>
<div id='nodetree_url_<?php echo $longtree['sectionid']; ?>' style='display:none;'><?php echo $longtree['url']; ?></div>
<?php endif; ?>
<?php if (array_key_exists('murl', $longtree) && $longtree['murl']): ?>
<div id='nodetree_murl_<?php echo $longtree['sectionid']; ?>' style='display:none;'><?php echo $longtree['murl']; ?></div>
<?php endif; ?>
