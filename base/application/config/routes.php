<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

/*
$route['(:any)/view/(:num)'] = 'view/node/$2';
$route['(:any)/api/(:num)'] = 'api/node/$2';
$route['(:any)/api/(:num)/(:any)'] = 'api/node/$2/$3';
$route['(:any)/report/(:num)'] = 'report/index/$2';
$route['(:any)/report/(:num)/(:any)'] = 'report/index/$2/$3';
$route['(:any)/link/(:num)'] = 'link/index/$2';
$route['(:any)/link/(:num)/(:any)'] = 'link/index/$2/$3';
$route['(:any)/perms/(:num)'] = 'perms/index/$2';

$route['(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$2/$3/$4/$5/$6/$7/$8/$9';
$route['(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$2/$3/$4/$5/$6/$7/$8';
$route['(:any)/(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$2/$3/$4/$5/$6/$7';
$route['(:any)/(:any)/(:any)/(:any)/(:any)/(:any)'] = '$2/$3/$4/$5/$6';
$route['(:any)/(:any)/(:any)/(:any)/(:any)'] = '$2/$3/$4/$5';
$route['(:any)/(:any)/(:any)/(:any)'] = '$2/$3/$4';
$route['(:any)/(:any)/(:any)'] = '$2/$3';
$route['(:any)/(:any)'] = '$2';
$route['(:any)'] = 'main';
*/

$route['view/(:num)'] = 'view/node/$1';
$route['api/(:num)'] = 'api/node/$1';
$route['api/(:num)/(:any)'] = 'api/node/$1/$2';
$route['report/(:num)'] = 'report/index/$1';
$route['report/(:num)/(:any)'] = 'report/index/$1/$2';
$route['link/(:num)'] = 'link/index/$1';
$route['link/(:num)/(:any)'] = 'link/index/$1/$2';
$route['perms/(:num)'] = 'perms/index/$1';

$route['default_controller'] = 'main';
$route['404_override'] = 'gdb404';

/* End of file routes.php */
/* Location: ./application/config/routes.php */