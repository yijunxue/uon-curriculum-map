<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['protocol']    = 'ssl';
$config['smtp_host']    = '';
$config['smtp_port']    = '465';
$config['smtp_timeout'] = '7';
$config['smtp_user']    = '';
$config['smtp_pass']    = '';
$config['charset']    = 'utf-8';
$config['newline']    = "\r\n";
$config['mailtype'] = 'text'; // or html
$config['validation'] = TRUE;