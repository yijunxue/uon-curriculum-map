<?php

// uon cm config file

$config['UONCM_LDAP_Enable'] = true; // is LDAP enabled as an alternate login?
$config['UONCM_LDAP_UsernameFallback'] = false; // can an unknown username be looked up in the LDAP service. If ldap login is sucessfull, a user will be created
$config['UONCM_LDAP_PasswordFallback'] = true; // can an incorrect password be corrected with a valid ldap login?

$config['UONCM_LDAP_Host'] = 'iLDAP.nottingham.ac.uk';
$config['UONCM_LDAP_Port'] = NULL;
$config['UONCM_LDAP_BindRDN'] = 'LDAP_CUMapping';
$config['UONCM_LDAP_Password'] = 'LdAp_Mapp1ng';
$config['UONCM_LDAP_BaseDN'] = 'OU=University,DC=intdir,DC=nottingham,DC=ac,DC=uk';
$config['UONCM_LDAP_UserPrefix'] = 'sAMAccountName=';
$config['UONCM_LDAP_AuthRealm'] = 'My Realm';

$config['UONCM_InfoURL'] = 'http://cm.rji.ac.uk/studentinfo.php?username=$$';
$config['UONCM_TimetableURL'] = 'http://test.ws.nottingham.ac.uk/1.1/timetabling/';
$config['UONCM_TimetableAPIKey'] = '1f57ZLCf6mWTGQ4JGTv%2BQB1jNYajNq9jljFvSp7r9VPJceol2lvIFXDlQXi758SXuLXeHLRm0Kej2sJts7ta5Q%3D%3D';

$config['UONCM_CanResetPass'] = false; // should the reset password link be enabled?