<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

/**
 * This is class Graph_DB_Node
 *
 * Used modifiy a node, and retrieve and related information about the node.
 * If getting related items to a node, use this class instead of Graph_DB
 *
 * Graph database node class
 */

class Graph_DB_Node
{
	/**
	 * Reference to the graph db object
	 *
	 * @var mixed
	 *
	 */
	protected $graph_db;
	protected $id;
	protected $guid;
	protected $type;
	protected $title;
	protected $attribs = array();
	protected $relations = array();
	protected $history = array();

	protected $rel_loaded = array();
	protected $rel_all_loaded = false;

	var $attr_loaded = false;
	protected $hist_loaded = false;

	protected $queries = array();

	var $orig_author;
	var $last_author;
	var $created;
	var $modified;
	var $revised;
	var $id_replace;

	var $perms = array();
	var $perm_changed = false;

	var $to_delete = false;

	var $deleted = false;
	/**
	 * **********************
	 * Node constructor. Just used to link in the graph db
	 * **********************
	 **/

	/**
	 * This is method __construct
	 *
	 * @param mixed $graph_db You must pass in the graph database that is creating the class
	 *
	 */
	function __construct($graph_db = null)
	{
		//echo "<hr>".dumpStack();

		if (!$graph_db)
			return;

		$this->graph_db =& $graph_db;
		$this->CI =& get_instance();
		$this->gdb =& $this->graph_db->gdb;
		//echo "Node Construct: {$this->graph_db->gdb}";
	}

	/**********************************************************/
	/**
	* **********************
	* Node initialization functions
	* **********************
	**/

	/**
	 * Creates a new node. You need to create an instance of this class first
	 *
	 * @param string $title Title of the node
	 * @param int $type Node type ID
	 *
	 */
	function create($title, $type, $parentid, $guid = false)
	{
		if ($guid === false) {
			$guid = gen_uuid();
		}

		// set up data record for the node
		$data = array(
			'guid' => $guid,
			'type' => $type,
			'title' => $title,
			'revised' => 0,
			'orig_author' => $this->CI->dx_auth->get_user_id(),
			'last_author' => $this->CI->dx_auth->get_user_id(),
			'created' => mysql_datetime(),
			'modified' => mysql_datetime()
			);

		$this->type = $type;
    $this->title = $title;
    $this->guid = $guid;

		// hack for the id when creating a node
		// the id is set to an easily findable string so that once the node is
		// saved and has an id from the database, the remaining data inserts can use
		// the new id
		$this->id = "XX".rand(100,999)."XX";
		$this->id_replace = $this->id;

		// add record to the list of db stuff todo when the node is saved
		$this->queries[] = array('type' => 'insert', 'table' => 'graph_node', 'data' => $data, 'get_id' => 1);

		// add history of create
		$this->_addHistory(GDB_Hist_Action::Node_Create, $type, $title);

		// inherit permissions
		if ($parentid > 0)
		{
			$parnode = $this->graph_db->loadNode($parentid);

			$perms = $parnode->getPerms();
			//echo "Parent Perms: <br>";
			//print_p($perms);

			foreach ($perms as $perm)
			{
				$this->addPermSet($perm);
			}

			//echo "Node Perms: <Br>";
			//print_p($this->perms);

			// add parent relationship
			$this->addRelate($parentid, GDB_Rel::ChildOf);
		}


		// add creator permissions
		$perm = new Perm();
		$perm->groupid = $this->CI->dx_auth->get_my_node();
		if ($perm->groupid > 0)
		{
			$perm->AllowAll();
			$perm->perms = 0;
			$this->addPermSet($perm);
		}
	}

	/**
	 * Loads a node from the database
	 *
	 * @param int $nodeid unique ID of the node to load
	 *
	 */
	function loadNode($nodeid)
	{
		//echo "<h1>loadNode</h1>";
		//echo dumpStack();

		//echo "Trying to load $nodeid<br>";
		//$sql = "SELECT dest_id, type FROM graph_rel WHERE source_id = ?";
		//$query = $this->gdb->query($sql, array($this->id));
		$sql = "SELECT * FROM !!_graph_node WHERE id = ?";
		$query = $this->gdb->query($sql, array($nodeid));
		$loaded = false;
		foreach($query->result() as $row)
		{
			$loaded = true;
			$this->fromRow($row);
			break;
		}

		//if ($loaded)
		//	$this->_loadAttributes();

		return $loaded;
	}

	/**
	 * Loads a node from the database
	 *
	 * @param string $node_guid GUID of the node to load
         * @param bool $deleted search deleted nodes
	 *
	 */
	function loadNodeByGuid($node_guid, $deleted = true)
	{
            if ($deleted) {
                $where = "";
            } else {
                $where = "and deleted = 0";
            }
            $sql = "SELECT * FROM !!_graph_node WHERE guid = ? $where";
            $query = $this->gdb->query($sql, array($node_guid));
            $loaded = false;
            foreach($query->result() as $row)
            {
                    $loaded = true;
                    $this->fromRow($row);
                    break;
            }

            return $loaded;
	}

	/**
	 *Loads the nodes basic information from a database row. Only used in batch node loading
	 *
	 * @param object $row Database row from table graph_node
	 *
	 */
	function fromRow($row)
	{
		$this->guid = $row->guid;
		$this->type = $row->type;
		$this->title = $row->title;
		$this->id = $row->id;
		$this->orig_author = $row->orig_author;
		$this->last_author = $row->last_author;
		$this->revised = $row->revised;
		$this->created = $row->created;
		$this->modified = $row->modified;
		$this->deleted = $row->deleted;

		if ($row->perms)
		{
			$this->perms = unserialize($row->perms);
		}
	}

	function hasPerm($name)
	{
		//echo "Checking perm $name - ";

		$CI =& get_instance();
		$groups = $CI->dx_auth->get_group_ids();

		//print_p($groups);
		//print_p($this->perms);

		$allow = $this->perms[0]->$name;

		foreach ($groups as &$group)
		{
			if (trim($group) == "") continue;
			if (!array_key_exists($group,$this->perms)) continue;

			//echo "Checking group $group => {$this->perms[$group]->$name}<br>";
			if ($this->perms[$group]->$name != 0)
			{
				$allow = $this->perms[$group]->$name;
				//echo "New Allow : $allow<br>";
			}
		}

		//echo "$allow<br>";
		if ($allow > 0)
			return true;
		return false;
	}
	/**
	 * Adds an attribute to the node from a database row. Only used in batch node loading
	 *
	 * @param object $row Row from the graph_attr table
	 *
	 */
	function attributeFromRow($row)
	{
		// make sure any existing attributes are already loaded
		// not needed as this is only called when batch loading a node, and all attributes will be loaded there
		//$this->_loadAttributes();

		// load the attribute from the row
		$this->attribs[$row->attrib] = new Graph_DB_Attribute((int)$this->type,$row);
	}

	/**********************************************************/
	/**
	 * **********************
	 * Node edit functions
	 * **********************
	 **/

	/**
	 * Add a relationship to another node
	 *
	 * @param Graph_DB_Node $node The node the relationship is being created to
	 * @param GDB_Rel $reltype Type of relationship
	 *
	 */
	function addRelate($node, $reltype = GDB_Rel::Relation, $weight = 1)
	{
		// reset loaded relations as the data has changed
		$this->unloadRelate();

		if (gettype($node) != "integer")
		{
			if (gettype($node) == "string")
				$node = (int)$node;
			else
				$node = $node->getId();
		}

		$node->unloadRelate();

		$data = array(
			'source_id' => $node,
			'dest_id' => $this->id,
			'weight' => $weight,
			'type' => $reltype
			);

		//echo "Relate {$this->id} to {$node}<br>";

		$this->queries[] = array('type' => 'insert', 'table' => 'graph_rel', 'data' => $data);

		$this->_addHistory(GDB_Hist_Action::Relation_Add, $node, $reltype, $weight);

		$recip = GDB_Rel::RecipRel($reltype);

		if ($recip)
		{
			// add recip relationship
			$data = array(
				'source_id' => $this->id,
				'dest_id' => $node,
				'weight' => $weight,
				'type' => $recip,
				);
			//echo "Relate {$node} to {$this->id}<br>";

			if (array_key_exists('type', $data))
			{
				$this->queries[] = array('type' => 'insert', 'table' => 'graph_rel', 'data' => $data);
			}

			$this->_addHistoryById($node, GDB_Hist_Action::Relation_Add, $this->getId(), $recip, $weight);

		}

	}

	function editRelateWeight(&$node, $newwieght)
	{

		// need to build 2 sql update queries

		if ($this->isRelatedTo($node))
		{
			$qry = "UPDATE !!_graph_rel SET weight = ? WHERE source_id = ? AND dest_id = ?";

			$data = array($newwieght, $this->getId(), $node->getId());
			$this->queries[] = array('type' => 'sql', 'query' => $qry, 'data' => $data);

			$data = array($newwieght, $node->getId(), $this->getId());
			$this->queries[] = array('type' => 'sql', 'query' => $qry, 'data' => $data);
		}
	}
	/**
	 * Removes the relationship with $node
	 **/
	function removeRelate(&$node)
	{
		// only try to remove if the node is already related to the requested node
		if ($this->isRelatedTo($node))
		{
			$sql = "DELETE FROM !!_graph_rel WHERE source_id = ? AND dest_id = ?";

			// TODO: This needs to be type aware, and only remove the recip relationship if the rel type requires it

			// remove main entry in graph_rel
			$this->queries[] = array('type' => 'sql', 'query' => $sql, 'data' => array($this->getId(), $node->getId()));

			// remove recip entry
			$this->queries[] = array('type' => 'sql', 'query' => $sql, 'data' => array($node->getId(), $this->getId()));

			// add removal history
			$this->_addHistory(GDB_Hist_Action::Relation_Delete, $node->getId(),$this->getRelType($node));
			$node->_addHistoryByID($node->getId(),GDB_Hist_Action::Relation_Delete, $this->getId(),GDB_Rel::RecipRel($this->getRelType($node)));

		}
	}

	/**
	 * Sets the title of the current node
	 *
	 * @param string $newtitle The new title to be ste
	 *
	 */
	function setTitle($newtitle)
	{
		$sql = "UPDATE !!_graph_node SET title = ? WHERE id = ?";

		// add the set to the query batch for later save
		$this->queries[] = array('type' => 'sql', 'query' => $sql, 'data' => array($newtitle, $this->id));
		$this->_addHistory(GDB_Hist_Action::Node_ChangeTitle, $this->title, $newtitle);
		$this->title = $newtitle;
	}

	/**
	 * Set an attribute on the node
	 *
	 * if graph db batch mode is set, then creating new fields is added to the batch and written to the db when flusing data.
	 * Mainly for performance reasons, as lots of mysql inserts separatly are very slow, but batched together they are quick
	 *
	 * @param strings $name ID of the attribute to add
	 * @param mixed $value Value of the attribute. This must be an object that can be converted to a string and back
	 * @param GDB_Attrib_Type $type Type of the attribute to add
	 *
	 */
	function setAttribute($name, $value, $type = GDB_Attrib_Type::_String)
	{
		// make sure any existing attributes for the node are loaded
		$this->_loadAttributes();

		//echo "Type : $type<br>";
		$field = GDB_Attrib_Type::GetField($type);
		//echo "Field : $field<br>";
		// if the attribute already exists, then update it
		if (array_key_exists($name, $this->attribs))
		{

			// update attribute
			$old_attr = $this->attribs[$name];

			if ($old_attr->GetValue() == $value)
			{
				//echo "Skipping attribute $name - Same<br>";
				return;
			}

			$data = array(
				'node_id' => $this->id,
				'node_type' => $this->type,
				'attrib' => $name,
				'type' => $type,
				'value_str' => '',
				'value_text' => '',
				'value_int' => 0,
				'value_float' => 0,
				'value_time' => date('H:i:s'),
				'value_datetime' => date('Y-m-d H:i:s')
			);

			$data[$field] = $value;

			$this->queries[] = array('type' => 'replace', 'table' => 'graph_attr', 'data' => $data);

			$this->_addHistory(GDB_Hist_Action::Attribute_Change,
				$name,$value,$old_attr->getValue());

		} else {

			// no attribute exists, so set an empty
			if (is_null($value))
				$value = '';

			if ($value == "")
				return;

			// new attribute
			$data = array(
				'node_id' => $this->id,
				'node_type' => $this->type,
				'attrib' => $name,
				'type' => $type,
				'value_str' => '',
				'value_text' => '',
				'value_int' => 0,
				'value_float' => 0,
				'value_time' => date('H:i:s'),
				'value_datetime' => date('Y-m-d H:i:s')
				);

			$data[$field] = $value;

			$this->queries[] = array('type' => 'insert', 'table' => 'graph_attr', 'data' => $data);

			$this->_addHistory(GDB_Hist_Action::Attribute_Add,
				$name,$value);

		}

		// overwrite the current attribute store with a newly created attribute with the current value etc
		// this should be only really called if adding an attribute, and when editing the existing attribute should
		// be altered
		$this->attribs[$name] = new Graph_DB_Attribute((int)$this->type, $name, $type, $value);
	}

	/**********************************************************/
	/**
	 * **********************
	 * Node information functions
	 * **********************
	 **/

	/**
	 * Gets the id of the node
	 *
	 * @return int Return the nodes unique id
	 *
	 */
	function getId()	{
		return $this->id;
	}

	/**
	 * Get the GUID of the current node
	 * @return string The GUID of the node
	 */
	function getGuid() {
		return $this->guid;
	}

	/**
	 * Gets the current type of the node
	 *
	 * @return int ID of the node
	 *
	 */
	function getType()
	{
		return $this->type;
	}

	/**
	 * Gets the current type of the node (name version
	 *
	 * @return int ID of the node
	 *
	 */
	function getTypeName()
	{
		return $this->type_obj->name;
	}

	function getTypeID()
	{
		return $this->type_obj->id;
	}

	/**
	 * Gets the title of the current node
	 *
	 * @return string title of the node
	 *
	 */
	function getTitle()
	{
		return $this->title;
	}

	/**
	 * Gets a node attribute by name
	 *
	 * @param string $name This is a description
	 * @return mixed This is the return value description
	 *
	 */
	function getAttribute($name)
	{
		$this->_loadAttributes();

		if (!array_key_exists($name, $this->attribs))
		{
			return new Graph_DB_Attribute((int)$this->type, $name, GDB_Attrib_Type::_String, "");
		}

		return $this->attribs[$name];
	}

	function getAttributeValue($name)
	{
		$this->_loadAttributes();

		if (!array_key_exists($name, $this->attribs))
		{
			return "";
		}

		return $this->attribs[$name]->getValue();
	}

	/**
	 * Get an array of the nodes attributes
	 *
	 * NOTE: This returns all attributes with data set, and blank attributes will
	 * not be listed, you will need to use the Node_type class to get a complete
	 * list of attributes
	 **/
	function &getAttributes()
	{
		$this->_loadAttributes();

		return $this->attribs;
	}

	/**
	 * return an array containing the nodes history
	 **/
	function getHistory()
	{
		$this->_loadHistory();

		return $this->history;
	}

	/**
	 * Find if the current node is related to $node
	 *
	 * Return true is they are related otherwise return false
	 **/
	function isRelatedTo(&$node)
	{
		$this->_loadRelations();
		return array_key_exists($node->getId(),$this->relations);
	}

	/**
	 * Gets the relationship object if this node is related to $node
	 **/
	function getRel(&$node)
	{
		if (!$this->isRelatedTo($node))
			return false;

		return $this->relations[$node->getId()];
	}

	/**
	 * Return the type of the relationship of the current node to $node
	 *
	 * IF not related, then return false
	 **/
	function getRelType(&$node)
	{
		$rel = $this->getRel($node);
		if (!$rel)
			return false;

		return $rel->GetType();
	}

	/**********************************************************/
	/**
	 * **********************
	 * Node relationship listing functions
	 * **********************
	 **/

	/**
	 * Return the current nodes relationships grouped by the relation ship type (ie, parent, child etc)
	 *
	 * @return array Array of relation GDB_Rel, containing array of Graph_DB_Relation
	 *
	 **/
	function getRelations_GroupByRelType()
	{
		$this->_loadRelations();

		$result = array();
		foreach ($this->relations as &$rel)
		{
			$result[$rel->getType()][] =& $rel->getNode();
		}

		return $result;
	}

	/**
	 * Returns the current nodes relationships grouped by the type of the related node
	 *
	 * @return array Array of node types, containing array of Graph_DB_Relation
	 *
	 */
	function getRelations_GroupByNodeType()
	{
		$this->_loadRelations();

		$result = array();
		foreach ($this->relations as &$rel)
		{
			$node = $rel->getNode();
			if ($node)
			{
				$result[$rel->getNode()->getType()][] =& $rel->getNode();
			}
		}

		return $result;
	}

	/**
	 * Returns the current nodes relationships grouped by the type of the related node, and then by relationship type
	 *
	 * @return array Array of node types, containing array of GDB_Rel, containing array of Graph_DB_Relation
	 *
	 */
	function getRelations_GroupByRelType_GroupByNodeType()
	{
		$this->_loadRelations();

		$result = array();
		foreach ($this->relations as &$rel)
		{
			$result[$rel->getType()][$rel->getNode()->getType()][] =& $rel->getNode();
		}

		return $result;
	}

	function getRelations_GroupByNodeType_GroupByRelType()
	{
		$this->_loadRelations();

		$result = array();
		foreach ($this->relations as &$rel)
		{
			$result[$rel->getNode()->getType()][$rel->getType()][] =& $rel->getNode();
		}

		return $result;
	}

	function &getRelationsRaw()
	{
		$this->_loadRelations();
		return $this->relations;
	}

	function getRelations($nodetype = '', $rel_type = '')
	{
		$this->_loadRelations();
		$result = array();
		foreach ($this->relations as &$rel)
		{
			$dnode =& $rel->getNode();
			if ($nodetype && $dnode->getTypeId() != $nodetype) continue;
			if ($rel_type && $rel->getType() != $rel_type) continue;

			$result[] = $dnode;
		}

		return $result;
	}

	function getRelationTo(&$node)
	{
		$this->_loadRelations();

		if (!array_key_exists($node->getId(),$this->relations))
			return 0;

		return $this->relations[$node->getId()]->GetType();
	}

	function getWeightTo(&$node)
	{
		$this->_loadRelations();

		if (!array_key_exists($node->getId(),$this->relations))
			return 0;

		return $this->relations[$node->getId()]->GetWeight();
	}

	function getRelations_OfType($type)
	{
		$this->_loadRelations($type);
		$result = array();
		foreach ($this->relations as &$rel)
		{
			if ($rel->getType() != $type) continue;

			$result[] = $rel->getNode();
		}

		return $result;
	}

	/**********************************************************/
	/**
	 * **********************
	 * Node Extra data loading function
	 * **********************
	 **/

	/**
	 * Used to unload all the loaded relationships for the current node.
	 *
	 * Called when adding a relationship to clear out any esisting ones so
	 * when relationship information is next requested, its fully reloaded
	 **/
	function unloadRelate()
	{
		$this->relations = array();
		$this->rel_all_loaded = false;
		$this->rel_loaded = array();
	}

	/**
	 * Load in to the node class any history associated with the node
	 *
	 * Stored in $this->history;
	 *
	 */
	protected function _loadHistory()
	{
		if ($this->hist_loaded)
			return;

		$sql = "SELECT * FROM !!_graph_hist WHERE node_id = ? ORDER BY `when` DESC, id DESC";
		$query = $this->gdb->query($sql, array($this->id));
		$this->history =& $query->result();

		$this->hist_loaded = true;
	}

	/**
	 * Load any relations of the node into the node object.
	 * These are stored in $this->relations
	 *
	 * @param GDB_Rel $type This is a description
	 *
	 */
	protected function _loadRelations($type = null, $loadnodes = true)
	{
		if ($this->rel_all_loaded)
			return;

		$sql = "SELECT dest_id, type, weight FROM !!_graph_rel WHERE source_id = ?";
		$opts = array($this->id);

		// if we have a type specified, then limit the relations to a particular type
		if ($type)
		{
			// check we havent already loaded this nodes relationships of
			// the particular type
			if (array_key_exists($type, $this->rel_loaded))
			{
				return;
			}

			$this->rel_loaded[$type] = 1;
			$sql .= " AND type = ?";
			$opts[] = $type;
		} else {
			// no type specified, we are loadign all relationships, so flag the node as such
			$this->rel_all_loaded = true;
		}

		$query = $this->gdb->query($sql, $opts);

		foreach ($query->result() as $row)
		{
			$this->relations[$row->dest_id] = new Graph_DB_Relation($row->dest_id,$row->type, $row->weight);
		}

		//new dBug($this);

		// load the actual nodes in that we are related too using the main graph_db
		//if ($loadnodes)
		// we now allways load in the nodes due to permissions stuff
		$this->graph_db->loadNodesFromRelations($this->relations);

		foreach ($this->relations as $dest_id => $node)
		{
			if (!$node->GetNode() || !$node->GetNode()->can_read())
			{
				//echo "$dest_id => Not loaded<br>";
				unset($this->relations[$dest_id]);
			} else {
				//echo "$dest_id => OK<br>";
			}
		}
	}

	/**
	 * Load attributes for the current node from the database
	 **/
	protected function _loadAttributes()
	{
		if ($this->attr_loaded) return;

		//dumpStackHeader("Load Attributes for node {$this->id}");
		$sql = "SELECT * FROM !!_graph_attr WHERE node_id = ?";
		$query = $this->gdb->query($sql, array($this->id));
		foreach ($query->result() as $row)
		{
			$this->attribs[$row->attrib] = new Graph_DB_Attribute((int)$this->type,$row);
		}
		$this->attr_loaded = true;
	}

	/**********************************************************/
	/**
	 * **********************
	 * Internal node functions
	 * **********************
	 **/
	/**
	 * Add a history action to the node and store it in the database
	 * This must be called for EVERY change made to the node
	 *
	 * if graphdb
	 *
	 * See graph_db_const.php in GDB_Hist_Action for documentation
	 * on the action types
	 *
	 * @param GDB_Hist_Action $action Action type
	 * @param mixed $value1 Value 1 associated wit action
	 * @param mixed $value2 Value 2 associated wit action
	 * @param mixed $value3 Value 3 associated wit action
	 *
	 */
	public function _addHistory($action, $value1 = "", $value2 = "", $value3 = "")
	{
		$this->_addHistoryById($this->id, $action, $value1, $value2, $value3);
	}

	public function _clearHistory()
	{
		// save first!
		$this->save();

		$this->history = array();
		$this->hist_loaded = false;

		$qry = "DELETE FROM !!_graph_hist WHERE node_id = ?";
		$this->gdb->query($qry, array($this->id));
	}

	/**
	 * Only called directly when adding other half of a relationship histoy
	 *
	 **/
	protected function _addHistoryById($nodeid, $action, $value1 = "", $value2 = "", $value3 = "")
	{
		$this->hist_loaded = false;
		$this->history = array();

		$data = array(
			'node_id' => $nodeid,
			'action' => $action,
			'user' => $this->CI->dx_auth->get_user_id(),
			'value1' => $value1,
			'value2' => $value2,
			'value3' => $value3
			);

		$this->queries[] = array('type' => 'insert', 'table' => 'graph_hist', 'data' => $data);

		/*if ($this->graph_db->batchIsEnabled())
		{
			$this->graph_db->batchAdd('graph_hist', $data);
		} else {
			$this->gdb->insert('graph_hist', $data);
		}*/
		//$this->gdb->insert('graph_hist', $data);
	}

	function delete()
	{
		$this->to_delete = true;
		$this->_addHistory(GDB_Hist_Action::Node_Delete);
	}

	/**
	 * Saves any changes that have been made to the node to the database. Any title, attribute, relationship or other
	 * changes will not be stored until this is called
	 */
	function save()
	{
		$changed = false;

		if (count($this->queries) > 0)
		{
			$insert = array();

			//echo "Saving Node - " . count($this->queries) . " in queue<br>";
			//print_p($this->queries);
			foreach ($this->queries as &$query)
			{
				$type = $query['type'];

				foreach ($query['data'] as $var => &$value)
				{
					if ($value && $value == $this->id_replace) $value = $this->id;
				}
				if ($type == "insert")
				{
					if ($this->graph_db->batchIsEnabled() && ($query['table'] == "graph_hist" || $query['table'] == "graph_attr" || $query['table'] == "graph_rel"))
					{
						$this->graph_db->batchAdd($query['table'],$query['data']);
					} else {
						$this->gdb->insert("!!_".$query['table'], $query['data']);
					}
				} else if ($type == "replace")
				{
					if ($this->graph_db->batchIsEnabled() && ($query['table'] == "graph_hist" || $query['table'] == "graph_attr" || $query['table'] == "graph_rel"))
					{
						$this->graph_db->batchAdd($query['table'],$query['data']);
					} else {
						$this->gdb->replace("!!_".$query['table'], $query['data']);
					}
				} else if ($type == "sql")
				{
					$this->gdb->query($query['query'],$query['data']);
				}

				if (array_key_exists("get_id",$query))
				{
					// need to update the graph_db and reinsert the node into the node list with the new id
					$this->id = $this->gdb->insert_id();
					$this->graph_db->ReIDNode($this->id_replace, $this->id);
				}
			}

			$this->queries = array();
			$changed = true;
		}

		if ($this->perm_changed)
		{
			$perms = serialize($this->perms);
			$qry = "UPDATE !!_graph_node SET perms = ? WHERE id = ?";
			$this->gdb->query($qry, array($perms, $this->id));
			$changed = true;
		}

		// separate delete process
		if ($this->to_delete)
		{
			$qry = "UPDATE !!_graph_node SET deleted = 1 WHERE id = ?";
			$this->gdb->query($qry, array($this->id));
			$this->deleted = 1;
			$changed = true;

			// We COULD just delete all attributes based on the type and value but this gives
			// us an audit trail, although it could get heavy when deleting a large tree
			$this->CI->load->helper('node_types');
			if ($this->type == NTypes::KV_PAIR)
			{
				$types_to_check = array(NTypes::BODY_OUTCOME, NTypes::PROGRAMME_OUTCOME, NTypes::MODULE_OUTCOME, NTypes::SESSION_OUTCOME, NTypes::TOPIC_OUTCOME, NTypes::RESOURCE);

				foreach ($types_to_check as $ttc)
				{
					// If we're a group node romove any group attributes that point to us
					$nodes_in_group = $this->graph_db->findNodeFromAttr($ttc, 'group', $this->guid, true, 50000);

					if (!empty($nodes_in_group) and is_array($nodes_in_group))
					{
						foreach ($nodes_in_group as $nig)
						{
							$nig->setAttribute('group', '');
							$nig->save();
						}
					}
				}
			}
		}

		if ($changed)
		{
			// update change count and stuff
			$qry = "UPDATE !!_graph_node SET revised = revised + 1, last_author = ?, modified = ? WHERE id = ?";
			$this->gdb->query($qry, array($this->CI->dx_auth->get_user_id(), mysql_datetime(), $this->id));

			$this->revised ++;
			$this->modified = mysql_datetime();
			$this->last_author = 1;
		}

	}

	/**
        * Replace the target nodes permissions with the source nodes, fixing any referenced nodes
        * @param type $perms the source nodes permissions
        * @param type $nodeid the source node id
        * @param type $id the unique identifier of the user or group
        */
        function replace_and_update_perms($perms, $nodeid, $id) {
            if (array_key_exists($nodeid, $perms)) {
                $perms[$id] = $perms[$nodeid];
                $perms[$id]->groupid = $id;
                if ($nodeid != $id) {
                    unset($perms[$nodeid]);
                }
            }
            $this->ReplacePerms($perms);
        }
  
	function ReplacePerms($newPerms) {
            $this->perms = $newPerms;

            $perms = serialize($this->perms);
            $qry = "UPDATE !!_graph_node SET perms = ? WHERE id = ?";
            $this->gdb->query($qry, array($perms, $this->id));
	}

	/**********************************************************/
	/**
	 * **********************
	 * Node debug functions
	 * **********************
	 **/

	/**
	 * Dump the node, and its associated data as HTML
	 *
	 * @param bool $attr Should the attributes be listed
	 * @param bool $hist Should the history be listed
	 * @param bool $relations Should the relations of the node be listed
	 *
	 */
	function dump($attr = false, $hist = false, $relations = false, $loadifmissing = true)
	{
		echo "<table width='100%' class='node_dump'>";
		echo "<tr class='node'><td colspan='4'>({$this->id}) {$this->title}</td><td align='right' width='100'>{$this->type}</td></tr>";

		if ($attr)
		{
			if ($loadifmissing) $this->_loadAttributes();
			// dump attributes
			echo "<tr class='attribute'><th rowspan='".(count($this->attribs)+1)."' width='100'>Attributes</th><th width='100'>Name</th><th colspan='2'>Value</th><th width='100'>Type</th></tr>";
			$rn = 0;
			if (count($this->attribs))
			{
				foreach ($this->attribs as $attrib)
				{
					$rn = 1 - $rn;
					echo "<tr class='row_$rn'><td>{$attrib->getName()}</td><td colspan='2'>{$attrib->getValue()}</td><td>" . GDB_Attrib_Type::string($attrib->getType()) . "</td></tr>";
				}
			}
		}

		if ($relations)
		{
			if ($loadifmissing) $this->_loadRelations();
			echo "<tr class='relations'><th rowspan='".(count($this->relations)+1)."' width='100'>Relations</th><th width='100'>Type</th><th colspan='3'>Node</th></tr>";
			$rn = 0;
			if (count($this->relations))
			{
				foreach ($this->relations as &$rel)
				{
					$rn = 1 - $rn;
					$relnode = $rel->getNode();
					echo "<tr class='row_$rn'><td>".GDB_Rel::string($rel->getType())."</td><td colspan='3'>{$relnode->getType()} - ({$relnode->getId()}) {$relnode->getTitle()}</td></tr>";
				}
			}
			//print_r($rels);
		}
		if ($hist)
		{
			if ($loadifmissing) $this->_loadHistory();
			echo "<tr class='history'><th rowspan='".(count($this->history)+1)."' width='100'>History</th><th width='100'>Type</th><th colspan='3'>Node</th></tr>";
			$rn = 0;
			if (count($this->history))
			{
				foreach ($this->history as &$hist)
				{
					$rn = 1 - $rn;
					echo "<tr class='row_$rn'><td>".GDB_Hist_Action::string($hist->action)."</td><td colspan='3'>{$hist->user} = {$hist->value1}, {$hist->value2}, {$hist->value3}</td></tr>";
				}
			}
		}



		echo "</table>";
	}


	function getRelationsCount($type = null)
	{
		$count = 0;
		$this->_loadRelations($type, false);
		foreach ($this->relations as &$rel)
		{
			if ($type && $rel->GetType() == $type) $count++;
			if (!$type) $count++;
		}

		return $count;
	}
	/*
	function getRelationsCount_ByNodeType($type)
	{
		//$this->_loadRelations($type, false);
		$result = array();

		$qry = "SELECT * FROM !!_graph_rel WHERE id IN (" . implode(",", $ids) . ")";

		$ids = array();
		foreach ($this->relations as &$rel)
			$ids[] = $rel->GetDestId();

		$qry = "SELECT count(*) as cnt, type FROM !!_graph_node WHERE id IN (" . implode(",", $ids) . ")";
		echo $qry."<Br>";

		return $count;
	}*/

	function getRelationIDs($type = null)
	{
		$ids = array();
		foreach ($this->relations as &$rel)
		{
			if ($type && $rel->getType() != $type) continue;
			$ids[] = $rel->GetDestId();
		}

		return $ids;
	}

	function getParent()
	{
		$this->_loadRelations();
		foreach ($this->relations as &$rel)
		{
			//echo "Rel : {$rel->GetType()} - {$rel->getNode()->getTitle()}<br>";
			if ($rel->GetType() == GDB_Rel::ParentOf) return $rel->getNode();
		}
		return $this->graph_db->loadNode(1);
	}

	function ParseTitle($string)
	{
		while (strpos($string, "%%") !== false)
		{
			$pos1 = strpos($string, "%%") + 2;
			$pos2 = strpos($string, "%%", $pos1 + 1);

			$var = strtolower(substr($string, $pos1, $pos2 - $pos1));
			//echo "$pos1 -> $pos2 = $var<br>";

			$value = "";
			switch ($var)
			{
				case 'title':
					$value = $this->title;
					break;
				case 'base_url':
					$value = asset_url();
					break;

				default:
				{
					if (substr($var,0,1) == "!")
					{
						$var = substr($var,1);
						$attr = $this->getAttribute($var);
						$value = $attr->getValue();
					} else {
						$attr = $this->getAttribute($var);
						$value = $attr->format();
					}
				}
			}

			$string = substr($string, 0, $pos1-2) . $value . substr($string,$pos2+2);
		}
		$string = $this->removeEmptyTags($string);
		return $this->removeEmptyBraces($string);
	}

	private function removeEmptyTags($html_replace)
	{
		$pattern = "/<[^\/>]*>([\s]?)*<\/[^>]*>/";
		return preg_replace($pattern, '', $html_replace);
	}

	private function removeEmptyBraces($html_replace)
	{
		$pattern = '@(\(\)|\[\]|\{\})@';
		$ret = str_replace('  ', ' ', preg_replace($pattern, '', $html_replace));
		return $ret;
	}

	// permissions stuff

	function can_read()
	{

		if ($this->deleted)
		{
			$CI =& get_instance();
			if ($CI->dx_auth->get_current_role() == "admin")
				return 1;
			return 0;
		}

		if (Graph_DB_Node_Typed::$is_admin)
		{
			return 1;
		}

		if ($this->hasPerm("read"))
			return 1;

		return 0;
	}

	function can_edit()
	{
		if (Graph_DB_Node_Typed::$is_admin)
			return 1;

		if ($this->hasPerm("edit"))
			return 1;

		return 0;
	}

	function can_history()
	{
		if (Graph_DB_Node_Typed::$is_admin)
			return 1;

		if ($this->hasPerm("hist"))
			return 1;

		return 0;
	}

	function can_clone()
	{
		if (Graph_DB_Node_Typed::$is_admin)
			return 1;

		if ($this->hasPerm("clone"))
			return 1;

		return 0;
	}

	function can_link()
	{
		if (Graph_DB_Node_Typed::$is_admin)
			return 1;

		// if the node type cannot be linked, then return false so we dont get linking icons appearing for nodes that cannot be linked
		$CI =& get_instance();
		$to = $CI->node_types->GetType($this->type);
		if ($to->link_anywhere == 0 && count($to->link) == 0)
			return 0;

		if ($this->hasPerm("link"))
			return 1;

		return 0;
	}

	function can_create() // can cwe create children of the current node
	{
		if (Graph_DB_Node_Typed::$is_admin)
			return 1;

		if ($this->hasPerm("create"))
			return 1;

		return 0;
	}

	function can_perms() // can cwe create children of the current node
	{
		if (Graph_DB_Node_Typed::$is_admin)
			return 1;

		if ($this->hasPerm("perms"))
			return 1;

		return 0;
	}

	function can_delete()
	{
		if (Graph_DB_Node_Typed::$is_admin)
			return 1;

		if ($this->hasPerm("delete"))
			return 1;

		return 0;
	}

	function removePermSet($groupid)
	{
		if (array_key_exists($groupid,$this->perms))
		{
			// add history to node, storing old permissions
			$this->_addHistory(GDB_Hist_Action::Perms_Delete,
				$groupid, serialize($this->perms[$groupid]));

			// remove it
			unset($this->perms[$groupid]);

			$this->perm_changed = true;
		}
	}

	function addPermSet(&$set)
	{
		$key = $set->getkey();

		if (array_key_exists($key, $this->perms))
		{
			$old = serialize($this->perms[$key]);

			$pl = new PermList();

			foreach ($pl->perms as $name => $data)
			{
				if ($set->$name != '') $this->perms[$key]->$name = $set->$name;
			}

			$this->perm_changed = true;

			$this->_addHistory(GDB_Hist_Action::Perms_Update,
				$key, serialize($this->perms[$key]), $old);
		} else {
			$this->perms[$key] = $set;
			$this->perm_changed = true;

			$this->_addHistory(GDB_Hist_Action::Perms_Add,
				$key, serialize($set));
		}
	}

	function getPerms()
	{
		// load in all node permissions
		return $this->perms;
	}

	function getBasePerms()
	{
		return $this->perms[0];
	}

	function canLink(&$node)
	{
		//$desttype = $node->getType();
		$type_obj = $this->GetTypeObj();
		$dest_type = $node->GetTypeObj();

		$oklinks = $type_obj->getPossibleLinks();

		//print_p($oklinks);
		//echo "Can link : {$type_obj->id} to {$dest_type->id} - ";

		if (!array_key_exists('direct',$oklinks))
			return false;

		if (!in_array($dest_type->id, $oklinks['direct']))
		{
			//echo "NO<br>";
			return false;
		}

		//echo "YES<br>";
		return true;
	}

	function nodesAlongPath($path)
	{
		//echo "<h1>Node : " . $this->getTitle() . "</h1>";
		//echo "<h2>Path : " . $path . "</h2>";

		$path = explode("/", $path);

		$nodelist = array();
		$nodelist[] = &$this;

		foreach ($path as $pathitem)
		{
			//echo "<h4>Path : $pathitem</h4>";

			$result = array();

			foreach ($nodelist as &$node)
			{
				//echo "Check Node : " . $node->getTitle() . "<br>";
				//echo "Looking for type " . $pathitem . "<br>";

				$foundnodes = $node->getRelations($pathitem);

				foreach ($foundnodes as &$foundnode)
				{
					//echo "Rel Node : " . $foundnode->getTypeObj()->name . " - " . $foundnode->getTitle() . "<br>";
					$result[] = $foundnode;
				}
			}

			$nodelist = $result;
		}

		return $nodelist;
	}

	function getCloneInfo()
	{
		$this->_loadHistory();

		$cloneinfo = array();

		foreach ($this->history as &$hist)
		{
			if ($hist->action != GDB_Hist_Action::Node_CloneFrom && $hist->action != GDB_Hist_Action::Node_CloneTo) continue;

			$ci = new stdClass();
			$ci->type = $hist->action;
			$ci->node = $hist->value1;
			$ci->dataset = $hist->value2;
			$ci->user = $hist->user;
			$ci->when = $hist->when;

			$cloneinfo[] = $ci;
		}

		return $cloneinfo;
	}
}