<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

class Tabs 
{
	function __construct()
	{
		static $loaded = false;

		if (!$loaded) {
			$this->_loadBehavior();
			$loaded = true;
		}
		
		$this->opened = '';
	}
	
	function _loadBehavior()
	{
		$this->CI =& get_instance();
		$this->CI->head->AddJS('js/lib/persist.js');
		$this->CI->head->AddJS('js/lib/tabs.js');
		$this->CI->head->AddCSS('css/lib/tabs.css');
	}
	
	function startPane( $id )
	{
		$this->opened = '';
		return '<div class="fsjtabcont"><dl class="fsjtabs" id="'.$id.'">';
	}
	
	function addPanel( $text, $id, $classextra = "" )
	{
		if ($this->opened)
		{
			return '<dt id="'.$id.'" style="cursor: pointer; " class="closed top_round '.$classextra.'"><span>'.$text.'</span></dt>';
		} else {
			$this->opened = $id;
			return '<dt id="'.$id.'" style="cursor: pointer; " class="open top_round '.$classextra.'"><span>'.$text.'</span></dt>';
		}
	}
	
	function endHead()
	{
		return "</dl>";
	}

	function endPane()
	{
		return "</div>";
	}
	
	function startPanel( $id )
	{
		if ($this->opened == $id)
			return "<div class='current' id='tab_$id'>";
		return "<div class='current' id='tab_$id' style='display:none'>";
	}

	function endPanel()
	{
		return "</div>";
	}

}