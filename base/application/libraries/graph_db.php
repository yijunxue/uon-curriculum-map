<?php

/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

/**
 * This is class Graph_DB
 *
 * Generic graph database class used to retrieve nodes from the graph database
 * This class is only used when we are not starting from a node
 *
 * If you already have a node, then use the functions within Graph_DB_Node class
 */
class Graph_DB
{
	/**
	 * Node cache, stores an array of loaded nodes by id
	 *
	 * @var array
	 *
	 */
	var $nodes = array();

	/**
	 * Should batch queries be used or not
	 *
	 * Take care as this can cause problems with ids etc, and change attributes.
	 * Only reccomented to use to speed up the import for data
	 *
	 * @var bool
	 *
	 */
	var $batch_queries = false;

	/**
	 * Reference to the current active dataset object
	 *
	 * @var mixed
	 *
	 */
	var $active_dataset;

	function __construct()
	{
		$this->CI =& get_instance();


		// check for the dataset we are accessing being ok
		$this->CI->load->library('session');
		// need to switch instance here

		$this->loadDatasets();

		$this->selectDataset();


		// old code that stored the dataset in the session
		/*$this->CI->config->load("datasets", true);
		$this->dataset = $this->CI->session->userdata('dataset');
		if (!$this->dataset) $this->dataset = "main";

		//echo "Using dataset : {$this->dataset}<br>";

		$this->datasets = $this->CI->config->item('datasets','datasets');
		$this->active_dataset = $this->datasets[$this->dataset];*/

		//echo "USing DB Prefix : {$this->CI->gdb->dbprefix}<br>";

		$this->CI->load->library('graph_db_const');
		$this->CI->load->library('graph_db_node');
		$this->CI->load->library('graph_db_node_typed');
		$this->CI->load->library('graph_db_attribute');
		$this->CI->load->library('graph_db_relation');
		$this->CI->load->library('graph_db_batch');
		$this->CI->load->library("node_types");

		$this->checkForTables();

		$this->graph_db_batch =& $this->CI->graph_db_batch;
	}

	/**
   * Load a list of datasets from the database
   */
  function loadDatasets() {
    $qry = "SELECT * FROM datasets";

    $this->datasets = array();

    $result = $this->CI->db->query($qry);
    foreach ($result->result() as $row)
    {
      if (empty($row->nodes)) {
        continue;
      }
      $row->nodes = unserialize($row->nodes);
      $this->datasets[$row->table] = $row;
    }
  }

  /**********************************************************/
	/**
	 * **********************
	 * Node creation
	 * **********************
	 **/

	/**
	 * Creates a new node
	 * @param  string  $title  Title of the new node
	 * @param  mixed   $type   Type of the new node. Can either be the gdbid of the node type, or its name
	 * @param  integer $parent ID of the parent node
	 * @return object          The new Graph_DB_Node object. The node is not saved to the database until save() is called on the resulting node class
	 */
	function &createNode($title, $type, $parent, $guid = false)
	{
		if (is_object($parent))
			$parent = $parent->getId();

		if ($parent == 0 && $type != 0)
		{
			echo "Trying to create a node without a parent!<br>";
			echo dumpStack();
			exit;
		}

		$node = new Graph_DB_Node_Typed($this);
		$node->create($title, $type, $parent, $guid);

		$this->nodes[$node->getId()] =& $node;

		return $node;
	}

	/**********************************************************/
	/**
	 * **********************
	 * Node loading
	 * **********************
	 **/

	/**
	 * Load a single node based on its unique ID
	 * @param  integer $nodeid id of the Graph_DB_Node to load
	 * @return object          The node object loaded
	 */
	function &loadNode($nodeid)
	{
		//echo "Graph_DB::loadNode($nodeid)<br>";

		$this->load_error = 0;
		if (array_key_exists($nodeid,$this->nodes))
			return $this->nodes[$nodeid];
		//dumpStackHeader("Graph_DB::loadNode($nodeid)");

		$node = new Graph_DB_Node_Typed($this);
		if (!$node->loadNode($nodeid))
		{
			$this->load_error = 1;
			$false = false;
			return $false;
		}
		$this->nodes[$nodeid] =& $node;

		if (!$node->can_read())
		{
			$this->load_error = 2;
			$null = null;
			return $null;
		}
		return $node;
	}

	/**
	 * Load a single node based on its GUID
	 * @param  string $node_guid GUID of the node to load
         * @param bool $deleted search deleted nodes
	 * @return object|null|boolean The node object loaded, null if does not have read permission, and false if it does not exist.
	 */
	function &loadNodeByGuid($node_guid, $deleted = true)
	{
		// Relatively rare operation so skip the caching done by the main loadNode method

		$this->load_error = 0;

		$node = new Graph_DB_Node_Typed($this);
		if (!$node->loadNodeByGuid($node_guid, $deleted))
		{
			$this->load_error = 1;
			$false = false;
			return $false;
		}

		if (!$node->can_read())
		{
			$this->load_error = 2;
			$null = null;
			return $null;
		}
		return $node;
	}

	/**
	 * Load multiple nodes from id array
	 * @param  	array		$nodelist				Array containing ids of the nodes to load
	 * @param  	boolean	$get_attributes	Should the attribute be loaded for the nodes. If not loading them, they will be loaded automatically when each nodes attributes are accessed
	 * @return 	array 	Graph_DB_Node objects loaded
	 */
	function &loadNodes(array $nodelist, $get_attributes = false)
	{
		// load the nodes using a single SQL batch
		$this->batchLoadNodes($nodelist, $get_attributes);

		// all nodes are now in node cache, so fetch them into the result
		$result = array();
		foreach ($nodelist as $nodeid)
		{
			if (!array_key_exists($nodeid,$this->nodes)) continue;
			$result[$nodeid] =& $this->nodes[$nodeid];
		}

		return $result;
	}

	/**
	 * Called to load in the nodes relations. This should only
	 * ever be called from within Graph_DB_Node::_loadRelations()
	 *
	 * @param array $relations Array of relation objects to load
	 *
	 */
	function loadNodesFromRelations(array &$relations)
	{
		// batch load any missing relation nodes (speeds up large nodes)
		// eg, node with 15 related nodes, without this takes 2-3ms to load,
		// with this, takes 0.2ms to load
		$toload = array();
		foreach ($relations as $rel)
		{
			if (!array_key_exists($rel->GetDestId(),$this->nodes))
			{
				$toload[] = $rel->GetDestId();
			}
		}
		$this->batchLoadNodes($toload);

		// foreach
		foreach ($relations as $rel)
		{
			if (array_key_exists($rel->GetDestId(),$this->nodes))
			{
				$rel->SetNode($this->nodes[$rel->GetDestId()]);
			} else {
				// this should never be called as nodes are already loaded

				/*$node = new Graph_DB_Node_Typed($this);	// old and complex way, can just call loadNode node
				$node->loadNode($rel->GetDestId());
				$this->nodes[$node->getId()] = $node;*/
				$node = $this->loadNode($rel->GetDestId());
				if ($node)
				{
					$rel->SetNode($node);
				}
			}
		}
	}

	/**
	 * Check if the given GUID exists in the active dataset
	 * @param  strin $node_guid GUID to search for
	 * @return boolean          True if the GUID exists in the dataset, otherwise false
	 */
	function doesGuidExist($node_guid)
	{
		$sql = "SELECT * FROM !!_graph_node WHERE guid = ? AND deleted = 0";
		$query = $this->gdb->query($sql, array($node_guid));
		return $query->num_rows > 0;
	}

	/**
	 * Batch load a set of nodes into the node cache. Generally only used
	 * internally
	 *
	 * @param array $nodelist This is a description
	 * @return mixed This is the return value description
	 *
	 */
	function batchLoadNodes(array &$nodelist, $get_attributes = true)
	{
		// this needs splitting into batches if there are more then 1000 nodes requested
		// probably can be ignored, as why the hell would you wnat to load 1000 nodes???
		if (count($nodelist) == 0)
			return;

		$result = array();

		$this->gdb->from('graph_node');
		$this->gdb->where_in('id', $nodelist);
		$query = $this->gdb->get();
		foreach ($query->result() as $row)
		{
			$node = new Graph_DB_Node_Typed($this);
			$node->fromRow($row);
			$this->nodes[$node->getId()] = $node;

			if (!$node->can_read())
			{
				//echo "Skipping {$node->getId()}<br>";
				continue;
			}

			$result[] =& $this->nodes[$node->getId()];
		}
		//$sql = "SELECT * FROM graph_node WHERE id IN(?)";

		if ($get_attributes)
		{
			$this->gdb->from('graph_attr');
			$this->gdb->where_in('node_id', $nodelist);
			$query = $this->gdb->get();
			foreach ($query->result() as $row)
			{
				$node_id = $row->node_id;
				$node = $this->loadNode($node_id);
				if ($node)
				{
					$node->attributeFromRow($row);
					$node->attr_loaded = true;
				}
			}
		}

		return $result;
	}

	/**********************************************************/
	/**
	 * **********************
	 * Batch data base import functions
	 * These are currently not working!
	 * **********************
	 **/

	/**
	 * Sets if batch importing is enabled or not
	 *
	 * @param bool $enabled This is a description
	 * @return mixed This is the return value description
	 *
	 */
	function batchSetEnabled($enabled)
	{
		$this->batch_queries = $enabled;
	}

	/**
	 * Sets if batch processing of queries is enabled or not
	 *
	 * @return mixed This is the return value description
	 *
	 */
	function batchIsEnabled() { return $this->batch_queries; }

	/**
	 * Adds a batch action
	 **/
	function batchAdd($table, &$row)
	{
		$this->graph_db_batch->addBatch($table, $row);
	}

	/**
	 * Stores the current batch queue into the database
	 *
	 * @return mixed This is the return value description
	 *
	 */
	function batchFlush()
	{
		$this->graph_db_batch->flush();
	}


	/**********************************************************/
	/**
	 * **********************
	 * Admin function
	 * **********************
	 **/

	/**
	 * Clears the loaded node cache. This is mainly for testing
	 * things
	 **/
	function flushLoadedNodes()
	{
		$this->nodes = array();
	}

	/**
	 * Dumps the list of nodes in the node cache into html
	 **/
	function dump_loaded_nodes($type = null, $attr = false, $hist = false, $relations = false, $loadifmissing = true)
	{
		if ($type)
		{
			echo "<h3>Dumping Loaded Nodes of type $type</h3>";
		}

		foreach ($this->nodes as $nid => &$node)
		{
			if (!$node)
			{
				echo "Trying to dump $nid<br>";
			}
			if ($type && $node->getType() != $type) continue;
			$node->dump($attr, $hist, $relations, $loadifmissing);
		}
	}


	/**********************************************************/
	/**
	 * Node find functions
	 * */

	/**
	 * Returns an array of nodes that have no other nodes related to them. This is an old debug functions
	 * and it is not reccomended to use any more
	 */
	function findOrphanNodes()
	{
		$sql = "SELECT id FROM `!!_graph_node` WHERE id NOT IN (SELECT source_id FROM !!_graph_rel)";
		$nodeids = array();

		$query = $this->gdb->query($sql);
		foreach ($query->result() as $row)
		{
			$nodeids[] = $row->id;
		}
		if (count($nodeids) == 0)
			return array();

		$this->batchLoadNodes($nodeids);

		$result = array();
		foreach ($nodeids as $id)
		{
			if (array_key_exists($id, $this->nodes))
				$result[] = $this->nodes[$id];
		}

		return $result;
	}

	/**********************************************************/
	/**
	 * Data information functions
	 **/

	/**
	 * This is method CountLoadedNodes
	 *
	 * @return mixed This is the return value description
	 *
	 */
	function CountLoadedNodes()
	{
		return count($this->nodes);
	}

	/**********************************************************/
	/**
	 * Misc functions
	 **/

	/**
	 * This is method ReIDNode. Used when a node is created and saved for the first time.
	 *
	 * Only used internalyl
	 *
	 * @param mixed $oldid This is a description
	 * @param mixed $newid This is a description
	 * @return mixed This is the return value description
	 *
	 */
	function ReIDNode($oldid, $newid)
	{
		$this->nodes[$newid] =& $this->nodes[$oldid];
		unset($this->nodes[$oldid]);
	}

  /**
   * Truncate all tables for target year dataset
   *
   * @param int $target dataset year number
   */
  function truncatedataset($target)
  {
    $tables = $this->get_year_tables($target);
    foreach ($tables as $table)
    {
      $this->CI->db->truncate($table);
    }
  }

  /**
   * Get all tables for a year dataset
   *
   * @param integer $year
   * @return array tables for the year
   *
   */
  function get_year_tables($year)
  {
    $sql = "SHOW TABLES LIKE '%_" . $year . "_%'";
    $tablelist = array();
    $query = $this->gdb->query($sql);
    $databasename = $this->gdb->database;
    $name = "Tables_in_$databasename (%_". $year . "_%)";
    foreach ($query->result() as $row)
    {
      $tablelist[] = $row->{$name};
    }
    return $tablelist;
  }



  /**
	 * Check to make sure that the tables for the current dataset
	 * exist. If they dont they will be created.
	 *
     * @param string $source_table source dataset if we are cloning
	 * @return mixed This is the return value description
	 *
	 */
	function checkForTables($source_table = '')
	{
		$tables = $this->CI->db->list_tables();
		$this->tablelist = array();
		foreach ($tables as $table)
		{
			$this->tablelist[$table] = 1;
		}

		$table_check = "gdb_" . $this->active_dataset->table . "_graph_node";

		if (!array_key_exists($table_check, $this->tablelist))
		{
			$this->createTables($source_table);
		}
		//
	}

	/**
	 * Creates the table for the current dataset
	 *
	 * Will not create them if they are alreadt loaded.
	 *
	 * This should only be called from within checkForTables as it uses
	 * variables set up there
     *
     * @param string $source_table source dataset if we are cloning
	 */
	function createTables($source_table = '')
	{
		$newcreated = false;
		if (!array_key_exists("gdb_" . $this->active_dataset->table . '_graph_attr',$this->tablelist))
		{
			$sql = "CREATE TABLE IF NOT EXISTS `gdb_{$this->active_dataset->table}_graph_attr` (
					  `node_id` int(11) NOT NULL,
					  `node_type` int(11) NOT NULL,
					  `attrib` varchar(50) NOT NULL,
					  `type` int(11) NOT NULL,
					  `value_str` varchar(250) DEFAULT NULL,
					  `value_text` text,
					  `value_int` int(11) DEFAULT NULL,
					  `value_float` double DEFAULT NULL,
					  `value_time` time DEFAULT NULL,
					  `value_datetime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
					  PRIMARY KEY (`node_id`,`attrib`)
					) ENGINE=MyISAM";
			$this->CI->db->query($sql);

			$newcreated = true;
		}

		if (!array_key_exists("gdb_" . $this->active_dataset->table . '_graph_hist',$this->tablelist))
		{
			$sql = "CREATE TABLE IF NOT EXISTS `gdb_{$this->active_dataset->table}_graph_hist` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `node_id` int(11) NOT NULL,
					  `when` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
					  `action` int(11) NOT NULL,
					  `user` int(11) NOT NULL,
					  `value1` text,
					  `value2` text,
					  `value3` text,
					  PRIMARY KEY (`id`),
					  KEY `node_id` (`node_id`)
					) ENGINE=MyISAM";
			$this->CI->db->query($sql);

			$newcreated = true;
		}

		if (!array_key_exists("gdb_" . $this->active_dataset->table . '_graph_node',$this->tablelist))
		{
			$sql = "CREATE TABLE IF NOT EXISTS `gdb_{$this->active_dataset->table}_graph_node` (
					  `id` int(11) NOT NULL AUTO_INCREMENT,
					  `type` int(11) NOT NULL,
					  `title` varchar(250) NOT NULL,
					  `revised` int(11) NOT NULL,
					  `orig_author` int(11) NOT NULL,
					  `last_author` int(11) NOT NULL,
					  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
					  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
					  `deleted` TINYINT NOT NULL DEFAULT  '0',
					  `perms` TEXT NULL,
  					`guid` char(36) NOT NULL,
					  PRIMARY KEY (`id`),
					  KEY `type` (`type`),
					  FULLTEXT KEY `title` (`title`)
					) ENGINE=MyISAM";
			$this->CI->db->query($sql);

			$newcreated = true;
		}

		if (!array_key_exists("gdb_" . $this->active_dataset->table . '_graph_rel',$this->tablelist))
		{
			$sql = "CREATE TABLE IF NOT EXISTS `gdb_{$this->active_dataset->table}_graph_rel` (
					  `source_id` int(11) NOT NULL,
					  `dest_id` int(11) NOT NULL,
					  `type` int(11) NOT NULL,
					  `weight` float NOT NULL,
					  PRIMARY KEY (`source_id`,`dest_id`),
					  KEY `source_id` (`source_id`,`type`)
					) ENGINE=MyISAM";
			$this->CI->db->query($sql);

			$newcreated = true;
		}

		if ($newcreated) {
			$this->wipe_db($source_table);
		}
	}

	/**
	 * Wipes all graph database elements
	 *
	 * USE WITH CAUTION!
     *
     * @param string $source_table source dataset if we are cloning
	 */
	function wipe_db($source_table)
	{
		//echo "Wipe DB!<br>";
		//dumpStack();


		// clear data for testing
		$this->CI->db->query("TRUNCATE gdb_{$this->active_dataset->table}_graph_node");
		$this->CI->db->query("TRUNCATE gdb_{$this->active_dataset->table}_graph_rel");
		$this->CI->db->query("TRUNCATE gdb_{$this->active_dataset->table}_graph_hist");
		$this->CI->db->query("TRUNCATE gdb_{$this->active_dataset->table}_graph_attr");

		// set current user id as 0 while creating data
		$user_id = $this->CI->dx_auth->get_user_id();
		$this->CI->session->set_userdata('DX_user_id', 0);
		$this->CI->dx_auth->set_my_node(0);

		$node = $this->createNode("Root Node",0,0);
		$ps = new Perm();
		$ps->init_for_root_node();
		$node->addPermSet($ps);
		$node->save();

		// delete user_node and role_node stuff
		$this->CI->db->query("DELETE FROM user_node WHERE dataset = ?", array($this->active_dataset->id));
		$this->CI->db->query("DELETE FROM role_node WHERE dataset = ?", array($this->active_dataset->id));

		// clear cached nodes
		$this->CI->db->query("UPDATE datasets SET nodes = '' WHERE id = ?", array($this->active_dataset->id));

		// check if the current dataset has an init class
		$file = "nodedef/{$this->active_dataset->nodedef}/init_{$this->active_dataset->nodedef}.php";
		$class = "Init_{$this->active_dataset->nodedef}";
		if (file_exists($file))
		{
			require_once($file);
			$obj = new $class();
			$obj->initset($this, $source_table);
		}

		unset($this->active_dataset->nodedef->nodes);

		$this->clearLoadedNodes();

		// restore user id
		$this->CI->session->set_userdata('DX_user_id', $user_id);
	}

	/**
	 * Find a node based on a string. This will search the nodes title only.
	 * @param  string  $string       Text to search for in the node.
	 * @param  string  $type         gdbid of the type of nodes to find. If this is empty then all node types are searched.
	 *															 You can also pass list of gdbids separated by | to search multiple types.
	 *															 I.e., 2|3|4 to search node types 2, 3 and 4.
	 * @param  integer $maxcount     The maximum number of nodes to return.
	 * @param  boolean $excludeusers Exclude the nodes that have been specified as nosearch within their xml definitions.
     * @param  string  $search_func  Search operation to use.
	 * @return array                 The Graph_DB_Node objects found
	 */
	function findNodes($string, $type, $maxcount = 100, $excludeusers = true, $search_func = "Mysql_Like")
	{
		if (is_string($type))
		{
			$type = urldecode($type);
			if (strpos($type,"|") > 0) $type = explode("|", $type);
		}

		$function = "_search_" . $search_func;
		return $this->$function($string, $type, $maxcount, $excludeusers);
	}

	/**
	 * Search for a set of nodes based on an attributes value. You can only search 1 node type and
	 * 1 attribute type at once. It is recommended to use the findNodeFromAttr instead of this one.
	 * @param  string 	$string Text to search for
	 * @param  integer  $type   gdbid of the type of nodes to find
	 * @param  string 	$attr   id of the attribute to search
	 * @return array          	The Graph_DB_Node objects found
	 */
	function findAttribute($string, $type, $attr)
	{
		// can only search for 1 attribute and 1 node type at a time!!!

		// need to search the attributes table for attribute
		$sql = "SELECT node_id FROM !!_graph_attr WHERE ";

		$where[] = "node_type = ?";
		$data[] = $type;

		$where[] = "attrib = ?";
		$data[] = $attr;

		$node_type = $this->CI->node_types->GetType($type);
		$attrib = $node_type->GetAttr($attr);

		$field = GDB_Attrib_Type::GetField($attrib->GetType());

		$tooshort = false;
		$search = $this->StringToSearch($string,$tooshort);

		if (count($search) == 0)
			return array();

		foreach ($search as $word)
		{
			$where[] = "$field LIKE ?";
			$data[] = $word;
		}

		$sql .= implode(" AND ", $where);

		//echo $sql."<br>";
		//$start = microtime(true);
		$query = $this->gdb->query($sql, $data);

		$result = array();

		foreach ($query->result() as $row)
		{
			$nodeid = $row->node_id;
			//echo "Node : $nodeid<br>";
			$node = $this->loadNode($nodeid);
			$result[] = $node;
		}
		//$end = microtime(true);
		//echo "Mysql Like took " . round($end-$start,2) . "ms<br>";
		return $result;
	}


	/**
	 * List the first x nodes of a specific type
	 * @param  int     $count     The number of nodes to load
	 * @param  integer $type      gdbid of the type of nodes to load
	 * @param  boolean $loadattrs Auto load nodes attributes. If not loaded, they will be loaded node by node when first accessed
	 * @return array             	Graph_DB_Node objects loaded
	 */
	function firstNodes($count, $type = 0, $loadattrs = false)
	{
		$sql = "SELECT * FROM !!_graph_node";
		$data = array();

		if ($type > 0)
		{
			$sql .= " WHERE type = ? ";
			$data[] = $type;
		}

		$sql .= " LIMIT ? ";
		$data[] = $count;

		$query = $this->gdb->query($sql, $data);

		$result = array();

		foreach ($query->result() as $row)
		{
			$nodeid = $row->id;
			if (!array_key_exists($nodeid, $this->nodes))
			{
				$node = new Graph_DB_Node_Typed($this);
				$node->fromRow($row);
				$this->nodes[$node->getId()] = $node;
			}

			$result[] =& $this->nodes[$nodeid];
		}

		return $result;
	}

	/**
	 * Parses a text string into an array containing %word% elements
	 * for creating a mysql search
	 */
	function StringToSearch($string, &$tooshort)
	{
		$string = urldecode($string);
		while (strpos($string,"  ") > 0)
		$string = str_replace("  "," ",$string);
		$strings = explode(" ",$string);

		$search = array();
		foreach ($strings as $string)
		{
			if (strlen($string) < 2)
			{
				$tooshort = true;
				continue;
			}

			$search[] = "%$string%";
		}

		return $search;
	}

	/**
	 * Mysql LIKE based search function
	 */
	private function _search_Mysql_Like($string, $type, $maxcount, $excludeusers)
	{
		$tooshort = false;
		$search = $this->StringToSearch($string, $tooshort);

		if (count($search) == 0)
		{
			if ($tooshort)
			{
				return array('error' => "Please enter a longer search");
			} else {
				return array('error' => "No search terms");
			}
		}

		$sql = "SELECT * FROM !!_graph_node WHERE ";

		$where = array();
		$data = array();
		foreach ($search as $txt)
		{
			$where[] = "title LIKE ?";
			$data[] = $txt;
		}

		if (is_array($type) && count($type) > 0)
		{
			$where[] = "type IN (" . implode(", ",$type) . ")";
		} else if ((is_int($type) or ctype_digit($type)) && $type > 0)
		{
			$where[] = "type = ?";
			$data[] = $type;
		}

		if (count($this->CI->node_types->nosearch) > 0 && $excludeusers)
		{
			$where[] = "type NOT IN (".implode(", ",$this->CI->node_types->nosearch).")";
		}

		$sql .= implode(" AND ", $where);

		$sql .= " LIMIT $maxcount";

		//echo $sql."<br>";

		$start = microtime(true);
		$query = $this->gdb->query($sql, $data);

		$result = array();

		foreach ($query->result() as $row)
		{
			$nodeid = $row->id;
			if (!array_key_exists($nodeid, $this->nodes))
			{
				$node = new Graph_DB_Node_Typed($this);
				$node->fromRow($row);
				if (!$node->can_read()) continue;
				$this->nodes[$node->getId()] = $node;
			}

			$result[] =& $this->nodes[$nodeid];
		}
		$end = microtime(true);
		//echo "Mysql Like took " . round($end-$start,2) . "ms<br>";
		return $result;
	}

	private function &_search_Mysql_Fulltext($string, $type, $maxcount)
	{
		$sql = "SELECT * FROM !!_graph_node WHERE MATCH(title) AGAINST (?)";
		$data = array();
		$data[] = $string;

		if ($type > 0)
		{
			$sql .= " AND type = ?";
			$data[] = $type;
		}

		$sql .= " LIMIT $maxcount";
		$query = $this->gdb->query($sql, $data);
		//$this->gdb->from('graph_node');
		//$this->gdb->where_in('id', $nodelist);

		$result = array();

		foreach ($query->result() as $row)
		{
			$nodeid = $row->id;
			if (!array_key_exists($nodeid, $this->nodes))
			{
				$node = new Graph_DB_Node_Typed($this);
				$node->fromRow($row);
				$this->nodes[$node->getId()] = $node;
			}

			$result[] =& $this->nodes[$nodeid];
		}

		return $result;
	}

        /**
         * Find a node based on an id
         * @param array $string the target node id
         * @param type $type target node type
         * @param int $maxcount count to limit results to
         * @return array the discovered node
         */
        private function &_search_Mysql_Nodeid($string, $type, $maxcount)
	{
		$sql = "SELECT * FROM !!_graph_node WHERE id = ?";
		$data = array();
		$data[] = $string;

		if ($type > 0)
		{
			$sql .= " AND type = ?";
			$data[] = $type;
		}

		$sql .= " LIMIT $maxcount";
		$query = $this->gdb->query($sql, $data);

		$result = array();

		foreach ($query->result() as $row)
		{
			$nodeid = $row->id;
			if (!array_key_exists($nodeid, $this->nodes))
			{
				$node = new Graph_DB_Node_Typed($this);
				$node->fromRow($row);
				$this->nodes[$node->getId()] = $node;
			}

			$result[] =& $this->nodes[$nodeid];
		}

		return $result;
	}
        
	/*private function _search_Sphinx($string, $type, $maxcount)
	{
		$this->CI->load->helper("sphinxapi");
		$cl = new SphinxClient();
		$cl->SetServer ( $this->active_dataset['search']['host'], $this->active_dataset['search']['port'] );
		$cl->SetConnectTimeout ( 1 );
		$cl->SetArrayResult ( true );
		$cl->SetMatchMode ( SPH_MATCH_ALL );
		$cl->SetLimits(0,10000,$maxcount);

		if ($type > 0)
		{
			$cl->SetFilter("type",array($type));
		}

		$string = urldecode($string);
		while (strpos($string,"  ") > 0)
			$string = str_replace("  "," ",$string);
		$strings = explode(" ",$string);

		$search = "";
		foreach ($strings as $string)
		{
			if (strlen($string) < 2)
				continue;

			$search .= "$string* ";
		}

		if ($search == "")
		{
			echo "No search term<br>";
			return array();
		}

		echo "Searching for $search in index {$this->active_dataset['search']['index']}<Br>";
		$res = $cl->Query ( $search, $this->active_dataset['search']['index'] );

		if ( $res===false )
		{
			print "Query failed: " . $cl->GetLastError() . ".\n";
		} else {
			$nodes = array();
			if ($res['total'] == 0)
				return array();
			echo "Found {$res['total_found']} matches, displaying the first {$res['total']}<br>";
			if (!array_key_exists('matches', $res) || count($res['matches']) == 0)
				return array();

			foreach ($res['matches'] as &$match)
			{
				$nodes[] = $match['id'];
			}
		}

		return $this->batchLoadNodes($nodes);
	} */

	/**
	 * Groups a provided array of node object into an array grouped by the
	 * node type
	 */
	function &groupNodes_Type(&$nodes)
	{
		$result = array();

		if (!is_array($nodes))
			return $result;

		if (array_key_exists("error",$nodes))
		{
			$result[] = $nodes;
			return $result;
		}

		//new dBug($nodes);

		foreach ($nodes as &$node)
		{
			//print_p($node);
			$result[$node->getType()][] = $node;
		}

		return $result;
	}

	/**
	 * Clears the current list of loaded nodes from memory. This is useful if you want to process a
	 * large amount of nodes. Any nodes that you are still using and referencing elsewhere will
	 * remain loaded
	 */
	function clearLoadedNodes()
	{
		$this->nodes = array();
	}

	/**
	 * Returns some statistics about the current dataset, and what its usage is
	 **/
	function getStats($set = "")
	{
		$temp = $this->active_dataset->table;
		if ($set)
		{
			$this->setDataset($set);
		}

		$prefix = $this->active_dataset->table;

		$this->checkForTables();
		$result = array();

		$result['Totals']['rows'] = 0;
		$result['Totals']['index'] = 0;
		$result['Totals']['data'] = 0;
		$result['Totals']['total'] = 0;

		$query = $this->CI->db->query("SELECT * FROM information_schema.TABLES where TABLE_SCHEMA = ?",array($this->CI->db->database));
		foreach ($query->result() as $row)
		{
			$tnsplit = explode("_",$row->TABLE_NAME);
			if (count($tnsplit) != 4) continue;
			if ($tnsplit[0] != "gdb") continue;
			if ($tnsplit[2] != "graph") continue;

			if ($tnsplit[1] != $prefix) continue;

			$tablename = $tnsplit[2] . "_" . $tnsplit[3];

			$result[$tablename]['rows'] = $row->TABLE_ROWS;
			$result[$tablename]['index'] = $row->INDEX_LENGTH;
			$result[$tablename]['data'] = $row->DATA_LENGTH;
			$result[$tablename]['total'] = $row->INDEX_LENGTH + $row->DATA_LENGTH;

			$result['Totals']['rows'] += $result[$tablename]['rows'];
			$result['Totals']['index'] += $result[$tablename]['index'];
			$result['Totals']['data'] += $result[$tablename]['data'];
			$result['Totals']['total'] += $result[$tablename]['total'];
			//$result['history'] = $row->cnt;
		}

		$this->setDataset($temp);
		return $result;
	}

	/**
	 * Returns the counts of each node type in the current dataset
	 */
	function getNodeStats()
	{
		$qry = "SELECT type, count(*) as cnt FROM !!_graph_node GROUP BY type ORDER BY type";
		$result = $this->gdb->query($qry);
		foreach ($result->result() as $row)
		{
			$out[$row->type] = $row->cnt;
		}
		return $out;
	}

	/**
	 * Set up the current dataset based on what has been provided in the datasets index.php
	 * file.
	 */
	function selectDataset()
	{
		// see if we have a session dataset, and use it if it exists
		//$sessionset = (string)$this->CI->session->userdata('dataset');

		global $current_data_set_folder;

		// try to get dataset from the url!
		//$router = $this->CI->router;
		//$sessionset = $router->uri->segments[1];
		$sessionset = $current_data_set_folder;
		//echo "Using :$sessionset<br>";

		if (array_key_exists($sessionset, $this->datasets))
		{
			return $this->setDataset($sessionset);
		}

		// no session dataset, so try for the default
		foreach ($this->datasets as &$set)
		{
			if ($set->isdefault)
			{
				return $this->setDataset($set->table);
			}
		}

		// no default dataset, so use "main"
		if (array_key_exists("main", $this->datasets))
		{
			return $this->setDataset("main");
		}
		
		show_error("No datasets");

		/*$this->CI->config->load("datasets", true);
		$this->dataset = $this->CI->session->userdata('dataset');
		if (!$this->dataset) $this->dataset = "main";

		//echo "Using dataset : {$this->dataset}<br>";

		$this->datasets = $this->CI->config->item('datasets','datasets');
		$this->active_dataset = $this->datasets[$this->dataset];*/
	}

	/**
	 * Changes the current dataset to $name.
	 *
	 */
	function setDataset($name, $store_in_session = true)
	{
		if (!array_key_exists($name, $this->datasets))
			return false;

		$this->active_dataset =& $this->datasets[$name];

		$this->CI->node_def = $this->active_dataset->nodedef;
		$this->CI->load->database();

		$this->gdb = $this->CI->load->database('graph', TRUE);
		$this->gdb->dbprefix = "gdb_" . $this->active_dataset->table . "_";
		return true;
	}

/**
 * Gets the count of the relationships available for an array of nodes. This is a quick way to
 * find is a list of nodes have relations instead of having to load all the related nodes
 * @param 	array 	$ids  	An array of node ids to lookup
 * @param 	string 	$type 	GDB_Rel relation type constant. If left null, all relationship types
 * are listed
 * @return 	array   A nested array containing the node id, the number of each related node type,
 * a list of the related node ids, and the total relation count
 */
	function GetRelCounts(&$ids, $type = null)
	{
		if (count($ids) == 0)
			return array();

		$rels = array();
		$nodeids = array();

		$qry = "SELECT * FROM !!_graph_rel WHERE source_id IN (" . implode(", ", $ids) . ")";
		$data = array();

		if ($type)
		{
			$qry .= " AND type = ?";
			$data[] = $type;
		}
		$query = $this->gdb->query($qry, $data);
		foreach ($query->result() as $row)
		{
			$rels[] = $row;
			$nodeids[] = $row->dest_id;
		}

		if (count($rels) == 0)
				return array();

		$nodes = array();
		$qry = "SELECT id, type FROM !!_graph_node WHERE id IN (" . implode(", ", $nodeids) . ")";
		$query = $this->gdb->query($qry);
		foreach ($query->result() as $row)
		{
			$nodes[$row->id] = $row;
		}

		//print_p($nodes);

		$result = array();

		foreach ($rels as &$rel)
		{
			//print_p($rel);
			$node = $nodes[$rel->dest_id];
			if (!array_key_exists($rel->source_id, $result)) $result[$rel->source_id] = array();
			if (!array_key_exists($node->type, $result[$rel->source_id])) $result[$rel->source_id][$node->type] = 0;
			if (!array_key_exists("total", $result[$rel->source_id])) $result[$rel->source_id]["total"] = 0;

			$result[$rel->source_id][$node->type]++;
			$result[$rel->source_id]['ids'][$node->type][] = $rel->dest_id;
			$result[$rel->source_id]["total"]++;
		}

		//print_p($result);

		return $result;
	}

	/**
	 * Sort internal comparison function
	 */
	static function _sort_cmp(&$node1, &$node2)
	{
		global $sort_order;

		foreach ($sort_order as $field => $dir)
		{
			$val1 = "";
			$val2 = "";
			if ($field == "title")
			{
				$val1 = $node1->getTitle();
				$val2 = $node2->getTitle();
			} else {
				$val1 = $node1->getAttributeValue($field);
				$val2 = $node2->getAttributeValue($field);
			}

			$cmp = strcasecmp($val1, $val2);

			if ($cmp != 0)
			{
				return $dir * $cmp;
			}
		}
		return 0;
	}

	/**
	 * Sorts an array of nodes based on a string. If the string passed is empty the nodes will
	 * be sorted by their title
	 * @param  array 	$nodearr An array of Graph_DB_Node objects to be sorted
	 * @param  string $sort    String defining how the nodes should be sorted. The string is a | separated list of node attribute names
	 */
	function sort_nodes(&$nodearr,$sort = "")
	{
		//echo "Sort Nodes : $sort<br>";
		global $sort_order;

		if (strlen($sort) > 0)
		{
			$sort_order = array();
			$bits = explode("|",$sort);
			//print_p($bits);
			foreach ($bits as $bit)
			{
				$segs = explode(",",$bit,2);
				//print_p($segs);
				if (count($segs) == 2)
				{
					$sort_order[$segs[0]] = $segs[1];
				} else {
					$sort_order[$segs[0]] = 1;
				}
			}
		} else {
			$sort_order = array();
			$sort_order["title"] = 1;
		}

		//print_p($sort_order);

		usort($nodearr,array("Graph_DB", "_sort_cmp"));
	}

	/**
	 * Saves the list of prefefined nodes into the datasets table
	 */
	function update_active_dataset_nodes()
	{
		$qry = "UPDATE datasets SET nodes = ? WHERE id = ?";
		$this->gdb->query($qry, array(serialize($this->active_dataset->nodes), $this->active_dataset->id));
	}

	/**
	 * Find a node based on an attribute value. This is very similar to the findAttribute function,
	 * just has more options
	 * @param  integer  $node_type gdbid of the type of nodes to find
	 * @param  string   $attr_name id of the attribute to search
	 * @param  string   $value     Text to search for
	 * @param  boolean  $exact     Should exact matching be used (without this it will use LIKE %string%)
	 * @param  integer  $limit     Maximum number of nodes to return
	 * @return array             	 Graph_DB_Node objects found
	 */
	function findNodeFromAttr($node_type, $attr_name, $value, $exact = true, $limit = 50)
	{
		$nodetype = $this->CI->node_types->GetType($node_type);
		//echo "Node Type : {$nodetype->name}, {$nodetype->id}, {$nodetype->gdbid}<br>";

		$attr = $nodetype->GetAttr($attr_name);

		$field = GDB_Attrib_Type::GetField($attr->type);
		//echo "Attrib : $attr_name, Field : $field<br>";

		$qry = "SELECT node_id FROM !!_graph_attr WHERE node_type = ? AND attrib = ? AND ";
		if ($exact)
		{
			$qry .= " $field = ? ";
		} else {
			$qry .= " $field LIKE ? ";
			$value = "%" . $value . "%";
		}

		$qry .= " LIMIT $limit";

		$params = array($nodetype->gdbid, $attr_name, $value);
		$query = $this->gdb->query($qry,$params);

		$ids = array();
		foreach ($query->result() as $row)
		{
			$ids[] = $row->node_id;
		}

		//print_p($ids);

		return $this->batchLoadNodes($ids);
	}
}
