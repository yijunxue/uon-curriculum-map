<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

/**
 * Helper library for the top menus of the site
 **/
class Menu
{
	private $mainmenu = array();
	private $submenu = array();
	private $nodemenu = array();
	
	function AddMenuItem($title, $link = '', $image = '')
	{
		$item = array();
		$item['title'] = $title;
		$item['link'] = $link;
		$item['image'] = $image;
		
		$this->mainmenu[] = $item;
	}
	
	function AddSubMenuItem($title, $link = '', $image = '')
	{
		$item = array();
		$item['title'] = $title;
		$item['link'] = $link;
		$item['image'] = $image;
		
		$this->submenu[] = $item;
	}
	
	function AddNodeMenuItem($title, $link = '', $image = '', $class = '', $extra = '', $pad = 0)
	{
		$item = array();
		$item['title'] = $title;
		$item['link'] = $link;
		$item['image'] = $image;
		$item['class'] = $class;
		$item['extra'] = $extra;
		$item['pad'] = $pad;
		
		$this->nodemenu[] = $item;
	}

	function OutputMainMenu()
	{
		$output = "";
		foreach ($this->mainmenu as &$item)
		{
			if ($item['image'])
			{
				$output .= "<img />";
			}
			if ($item['link'])
			{
				$output .= "<div class='main_menu_item'>" . anchor($item['link'], $item['title']) . "</div>";
			} else {
				$output .= "<div class='main_menu_item'>" . $item['title'] . "</div>";
			}	
		}

		$output .= "<div class='clear'></div>";
		
		return $output;
	}
	
	function OutputSubMenu()
	{
		$output = "";
		foreach ($this->submenu as &$item)
		{
			if ($item['image'])
			{
				$output .= "<img />";
			}
			if ($item['link'])
			{
				$output .= "<div class='main_submenu_item'>" . anchor($item['link'], $item['title']) . "</div>";
			} else {
				$output .= "<div class='main_submenu_item'>" . $item['title'] . "</div>";
			}	
		}

		$output .= "<div class='clear'></div>";
		
		return $output;
	}		
	
	function OutputNodeMenu()
	{
		$output = "";
		foreach ($this->nodemenu as &$item)
		{
			$image = "";
			if ($item['image'])
				$image = "<img src='" . asset_url() . "image/" . $item['image'] . "'>";

			$class = $item['class'];
			$extra = $item['extra'];
			
			$padding = str_repeat("&nbsp;",$item['pad']);

			if ($item['link'])
			{
				$output .= "<div class='main_nodemenu_item'>" . anchor($item['link'], $padding . $image . $item['title'], "class='" . $class . " round' " . $extra) . "</div>";
			} else {
				$output .= "<div class='main_nodemenu_item'>$padding$image" . $item['title'] . "</div>";
			}	
		}

		$output .= "<div class='clear'></div>";
		
		return $output;
	}	
	
	function HasSubMenu()
	{
		return count($this->submenu) > 0;	
	}	
	
	function HasNodeMenu()
	{
		return count($this->nodemenu) > 0;	
	}
}
