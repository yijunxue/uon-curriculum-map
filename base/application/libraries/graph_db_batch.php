<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

/**
 * Graph db batch insert
 * 
 * Currently no longer working
 * */
class Graph_DB_Batch 
{
	var $queue = array();
	
	function __construct()
	{
		$this->CI =& get_instance();
	}
	function addBatch($table, &$data)
	{
		$this->queue[$table][] = $data;	
	}
	
	function flush()
	{
		//print_r($this->queue);
		//echo "Flushing batch queries - " . count($this->queue).  " tables<br>";	
		
		foreach ($this->queue as $table => &$rows)
		{
			//echo "Flushing batch queries - table : $table, " . count($rows).  " rows<br>";	
			$this->gdb->insert_batch("!!_".$table, $rows);
		}
		
		$this->queue = array();
	}
}
	