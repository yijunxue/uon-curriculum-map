<?php

/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

/**
 * This is class Graph_DB_Relation
 * 
 * Class to hold a related node. Only used from within Graph_DB_Node.
 * Stores the destination node, and the relationship type
 *
 */
class Graph_DB_Relation
{
	/**
	 * ID of the destination node
	 *
	 * @var int 
	 *
	 */
	private $dest_id;
	
	/**
	 * Type of the relation
	 *
	 * @var GDB_Rel 
	 *
	 */
	private $type;

	/**
	 * Destination node that the relation is to
	 *
	 * @var Graph_DB_Node
	 *
	 */
	var $dest_node;


	/**
	 * Weight of the relation. Defaults to 1

	 */
	private $weight;
	

	/**
	 * Create a new object
	 *
	 * @param int $dest_id ID of the destination node
	 * @param int $type Type of the node
	 *
	 */
	
	
	function __construct($dest_id = null, $type = null, $weight = 1)
	{
		$this->dest_id = $dest_id;
		$this->type = $type;
		$this->weight = $weight;
	}	
	
	/**
	 * Set the node assoicated with the relationship
	 *
	 * @param Graph_DB_Node $node This is a description
	 * @return mixed This is the return value description
	 *
	 */
	function SetNode(Graph_DB_Node &$node)
	{
		$this->dest_node = $node;	
	}
	
	function &GetNode()
	{
		return $this->dest_node;	
	}
	
	function GetType() { return $this->type; }
	function GetDestId() { return $this->dest_id; }
	function GetWeight() { return $this->weight; }
}