<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

/**
 * This is class Graph_DB_Attribute
 *
 * Attribute storage class. Used to store and diplay attributes for a node
 */
class Graph_DB_Attribute
{
	/**
	 * Name of the attribute
	 *
	 * @var string 
	 *
	 */
	private $name;

	/**
	 * Type of the attribute.
	 *
	 * @var GDB_Attrib_Type 
	 *
	 */
	private $type;

	/**
	 * Value of the attribute. Mixed type
	 *
	 * @var mixed 
	 *
	 */
	private $value;
	
	/**
	 * Stores the attribute type object (
	 *
	 * @var Node_Type_Attr 
	 *
	 */
	var $type_obj;
	
	/**
	 * Initialize a new attribute.
	 * 
	 * Can be called with no parameters to create a blank attribute
	 *
	 * @param string $name Name of the attribute
	 * @param GDB_Attrib_Type $type Attribute type
	 * @param mixed $value Value of the attribute
	 *
	 */
	public function __construct($nodetype = null, $name = null, $type = null, $value = null)
	{
		if (empty($nodetype) || empty($name))
			return;
			
		// we have been called as a create from row, so call alternate constructor
		if (gettype($name) == "object")
		{
			$this->__construct_from_row($nodetype, $name);
			return;
		}
		
		// normal constructor, when creating a new attribute
		$this->name = $name;
		$this->type = $type;
		$this->value = $value;

		// get a reference to the attribute type object
		$this->LoadTypeInformation($nodetype);
	}	
	
	/**
	 * Convert the attribute to a string
	 * This allows the attribute to be echoed without additional conversion
	 *
	 * @return string Current value of the node attribute
	 *
	 */
	function __toString()
	{
		return $this->value;	
	}

	/**
	 * Construct an attribute from a row retrieved from the graph_attr table
	 *
	 * @param int $nodetype The type of the current node
	 * @param mixed $row database row from graph_attr
	 *
	 */
	public function __construct_from_row($nodetype, $row)
	{
		$this->name = $row->attrib;
		$this->type = $row->type;
		
		$field = GDB_Attrib_Type::GetField($row->type);
		$this->value = $row->$field;	
		
		// get a reference to the attribute type object
		$this->LoadTypeInformation($nodetype);
	}
	
	/**
	 * Loads the attribute type object from the Node_Type class
	 *
	 * @param int $nodetype Type of the node this attribute is attached to
	 *
	 */
	function LoadTypeInformation($nodetype)
	{
		// attach the node attribute type information to the object
		//$nodetype
		$CI =& get_instance();
		
		// find the node type object
		$node_type =& $CI->node_types->GetType($nodetype);
		//$this->node_type = $nodetype;
		
		// find the attribute type object
		$this->type_obj =& $node_type->GetAttr($this->name);
	}
	
	/**
	 * Return the value of the current attribute
	 *
	 * @return string Returns the value of the attribute
	 *
	 */
	public function getValue() { return $this->value; }

	/**
	 * Return the current id name of the attribute
	 *
	 * @return string Returns the name of the attribute
	 *
	 */
	public function getName() { return $this->name; }

	/**
	 * Return the type id of the current attribute
	 *
	 * @return GDB_Attrib_Type Returns the type of the attribute
	 *
	 */
	public function getType() { return $this->type; }
		
	/**
	 * Gets the description of the attribute from the type object
	 *
	 * @return string The text based name of the attribute
	 *
	 */
	public function getDescription() { return $this->type_obj->title; }
	
	function format($format = "")
	{
		if ($format == "raw")
			return $this->getValue();
		
		if (!$this->type_obj)
			echo dumpStack();
		
		switch ($this->type_obj->type)
		{
			case GDB_Attrib_Type::_Date:
				if (!$format) $format = "F jS, Y";
				$ti = strtotime($this->getValue());
				return date($format, $ti);	
				
			case GDB_Attrib_Type::_DateTime:
				if (!$format) $format = "F jS, Y, g:ia";
				$ti = strtotime($this->getValue());
				return date($format, $ti);	
				
			case GDB_Attrib_Type::_Time:
				if (!$format) $format = "g:ia";
				$ti = strtotime($this->getValue());
				return date($format, $ti);	
				
			case GDB_Attrib_Type::_Float:
				if (!$format && strpos($format,",") < 1)
				{
					$format_1 = "%f";
					$format_2 = 3;
				} else if ($format != "") {
					list($format_1, $format_2) = explode(",",$format,2);	
				} else {
					return $this->getValue();	
				}
				return number_format(sprintf($format_1, $this->getValue()),$format_2);	
				
			case GDB_Attrib_Type::_Int:
				return number_format((int)$this->getValue());	
				
			case GDB_Attrib_Type::_Bool:
				if ($format == "tf")
					return $this->getValue() ? "True" : "False";	
				if ($format == "10")
					return $this->getValue() ? "1" : "0";	
				
				return $this->getValue() ? "Yes" : "No";	
			
			case GDB_Attrib_Type::_SelectInt:
			case GDB_Attrib_Type::_SelectStr:
				$val = $this->getValue();
				if (array_key_exists($val, $this->type_obj->values))
					return $this->type_obj->values[$val];
				return $val;	
				
			case GDB_Attrib_Type::_KVPair:
				$val = $this->getValue();
				$bits = explode("~",$val,2);
				if (count($bits) == 1)
				{
					if ($bits[0] == "")
					{
						$output = "<div class='kvpair_title'>No Group</div>";
					} else {
						$output = "<div class='kvpair_title'>{$bits[0]}</div>";
					}
				} else {
					$output = "<div class='kvpair_title'>{$bits[0]}</div><div class='kvpair_body'>{$bits[1]}</div>";
				}
				return $output;		
		}
		
		return $this->__toString();	
	}
	
	function AsXML()
	{
		if (!$this->type_obj)
			echo dumpStack();
		
		switch ($this->type_obj->type)
		{
			case GDB_Attrib_Type::_KVPair:
				$val = $this->getValue();
				$bits = explode("~",$val,2);
				if (count($bits) == 1)
				{
					if ($bits[0] == "")
					{
						$output = "<title>No Group</title>";
					} else {
						$output = "<title>".cdata($bits[0])."</title>";
					}
				} else {
					$output = "<title>".cdata($bits[0])."</title>";
					if ($bits[1]) $output .= "<description>".cdata($bits[1])."</description>";
				}
				return $output;		
				break;
			
			default:
				return $this->format();
		}			
	}
}

function cdata($string)
{
	if ($string == "")
		return "";
	if (preg_match('/^[a-zA-Z0-9_\ ]+$/', $string))
	{
		return $string;	
	} else {
		return "<![CDATA[$string]]>";	
	}
}

