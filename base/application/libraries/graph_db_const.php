<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

/**
 * A set of classes used to denote constants used through the graph db classes
 **/

/**
 * Base class with string getting function
 **/
class Graph_db_const
{
	/**
	 * Outputs the text name of the constant
	 *
	 * @param int $id Constant to output
	 * @return string Name of the constant
	 *
	 */
	static function string($id, $classname)
	{
		static $constants_by_id;

		// if this is the first time this is called for the particular class, then
		// create an array of id to name mappings
		if (empty($constants_by_id) || !array_key_exists($classname, $constants_by_id))
		{
			$constants_by_id[$classname] = array();
			//$classname = get_called_class();
			$class = new ReflectionClass($classname);
			$constants = $class->getConstants();
			foreach ($constants as $name => $constid)
			{
				$constants_by_id[$classname][$constid] = $name;
			}
		}

		// return the name of the constant
		return $constants_by_id[$classname][$id];
	}

	/**
	 * Outputs the text name of the constant, and replaces _ with :
	 *
	 * @param int $id Constant to output
	 * @return string Name of the constant
	 *
	 */
	static function string_nice($id, $classname)
	{
		// Was the following code:
		//$classname = get_called_class();
		//$name = $classname::string($id);
		//return str_replace("_",": ",$name);

		// but editor is having a hissy fit about the $classname::string part
		// instead of calling the ::stirng function, we are including it directly

		/* static function string($id)
		{	*/
		static $constants_by_id;

		// if this is the first time this is called for the particular class, then
		// create an array of id to name mappings
		if (empty($constants_by_id))
		{
			$constants_by_id = array();
			//$classname = get_called_class();
			$class = new ReflectionClass($classname);
			$constants = $class->getConstants();
			foreach ($constants as $name => $constid)
			{
				$constants_by_id[$constid] = $name;
			}
		}

		// return the name of the constant
		/*return $constants_by_id[$id];
		}*/

		return str_replace("_",": ",$constants_by_id[$id]);
	}
}

/**
 * List of relation ship types
 *
 */
class GDB_Rel extends Graph_db_const
{
	const Unrelated = 0;
	/**
	 * Parent of - the source_id node is a parent node of the dest_id node. a node should only ever have 1 parent node!
	 **/

	const ParentOf = 1;

	/**
	 * Parent of - the source_id node is a child node of the dest_id node, a node should be created as a child node of its parent
	 **/
	const ChildOf = 2;

	/**
	 * Relation - generic relation state
	 **/
	const Relation = 3;

	static function string($id, $notused = null) { return parent::string($id, "GDB_Rel"); }
	static function string_nice($id, $notused = null) { return parent::string_nice($id, "GDB_Rel"); }

	/**
	 * Static array of ID to display name mappings (reveresed names)
	 */
	static $reverse_names = array (
		GDB_Rel::ParentOf => 'Children',
		GDB_Rel::ChildOf => 'Parents',
		GDB_Rel::Relation => 'Relations',
		);

	/**
	 * Revers name mappings, single version
	 **/
	static $reverse_names_single = array (
		GDB_Rel::ParentOf => 'Child',
		GDB_Rel::ChildOf => 'Parent',
		GDB_Rel::Relation => 'Relation',
		);

	static $names_single = array (
		GDB_Rel::ParentOf => 'Parent',
		GDB_Rel::ChildOf => 'Child',
		GDB_Rel::Relation => 'Relation',
		);

	/**
	 * Recip relations ships of the node
	 *
	 * If this mapping exists for a relationship type, then when creating
	 * a relationship, the reciporical relationship should be created
	 **/
	static $opposite = array(
		GDB_Rel::ParentOf => GDB_Rel::ChildOf,
		GDB_Rel::ChildOf => GDB_Rel::ParentOf,
		GDB_Rel::Relation => GDB_Rel::Relation,
		);

	/**
	 * Static function to get the field used to store a particular
	 * field type
	 *
	 * @param GDB_Attrib_Type $type The type of field to get
	 * @return string sql field name
	 *
	 */
	static function GetReverseName($type)
	{
		if (!array_key_exists($type, GDB_Rel::$reverse_names))
		{
			show_error("Missing constant in GDB_Rel::GetReverseName(".GDB_Rel::string($type).")");
		}
		return GDB_Rel::$reverse_names[$type];
	}

	/**
	 * Static function to get the field used to store a particular
	 * field type, single version
	 *
	 * @param GDB_Attrib_Type $type The type of field to get
	 * @return string sql field name
	 *
	 */
	static function GetReverseNameSingle($type)
	{
		if (!array_key_exists($type, GDB_Rel::$reverse_names_single))
		{
			show_error("Missing constant in GDB_Rel::GetReverseNameSingle(".GDB_Rel::string($type).")");
		}
		return GDB_Rel::$reverse_names_single[$type];
	}

	static function GetNameSingle($type)
	{
		if (!array_key_exists($type, GDB_Rel::$names_single))
		{
			show_error("Missing constant in GDB_Rel::GetNameSingle(".GDB_Rel::string($type).")");
		}
		return GDB_Rel::$names_single[$type];
	}

	/**
	 * Static function to get the reciporical of a relationship type
	 *
	 * @param GDB_Attrib_Type $type Type of relationship we want the recip for
	 * @return GDB_Attrib_Type recip for the  relationship. If doesnt exist, the -1 is returned
	 *
	 */
	static function RecipRel($type)
	{
		if (!array_key_exists($type, GDB_Rel::$reverse_names_single))
		{
			return -1;
		}
		return GDB_Rel::$opposite[$type];
	}
}

/**
 * List of history action id types
 *
 */
class GDB_Hist_Action extends Graph_db_const
{
	/**
	 * Node_Create
	 *
	 * Node has been created.
	 * No values needed
	 **/
	const Node_Create = 1;

	/**
	 * Node_Delete
	 *
	 * Node deleted.
	 *
	 * When deleting a node, you must also delete any relations
	 * and attributed for the node, to create the history items for them
	 *
	 * NOT YET USED!
	 **/
	const Node_Delete = 2;


	const Node_CloneTo = 4;

	const Node_CloneFrom = 5;

  const Node_CloneCreateDataset = 6;

	/**
	 * Node_ChangeTitle
	 *
	 * The title of the node has been changed
	 *
	 **/
	const Node_ChangeTitle = 3;

	/**
	 * Relation_Add
	 *
	 * A link between 2 nodes created
	 * Value1 is id of linked node
	 **/
	const Relation_Add = 10;

	/**
	 * Relation_Delete
	 *
	 * Relationship between nodes deleted
	 **/
	const Relation_Delete = 11;

	/**
	 * Attribute_Add
	 *
	 * New attribute added to a node
	 *
	 * value1 : attribute name
	 * value2 : attribute value
	 **/
	const Attribute_Add = 20;

	/**
	 * Attribute_Change
	 *
	 * attribute value changed on a node
	 *
	 * value1 : attribute name
	 * value2 : attribute value
	 * value3 : old attribute value
	 **/
	const Attribute_Change = 21;

	/**
	 * Attribute_Delete
	 *
	 * Attributed deleted on a node
	 *
	 * value1 : attribute name
	 * value2 : attribute value
	 **/
	const Attribute_Delete = 22;


	const Perms_Add = 30;

	const Perms_Update = 31;

	const Perms_Delete = 32;


	static function string($id, $notused = null) { return parent::string($id, "GDB_Hist_Action"); }

	static function string_nice($id, $notused = null)
	{
		switch ($id)
		{
			case GDB_Hist_Action::Node_Create:
				return "Create Node";
			case GDB_Hist_Action::Node_Delete:
				return "Delete Node";
			case GDB_Hist_Action::Node_CloneTo:
				return "Clone Node To";
			case GDB_Hist_Action::Node_CloneFrom:
				return "Node Cloned From";
			case GDB_Hist_Action::Node_ChangeTitle:
				return "Change Title";
			case GDB_Hist_Action::Relation_Add:
				return "Add Relation";
			case GDB_Hist_Action::Relation_Delete:
				return "Delete Relation";
			case GDB_Hist_Action::Attribute_Add:
				return "Add Attribute";
			case GDB_Hist_Action::Attribute_Change:
				return "Create Attribute";
			case GDB_Hist_Action::Attribute_Delete:
				return "Delete Attribute";
			case GDB_Hist_Action::Perms_Add:
				return "Add Permissions";
			case GDB_Hist_Action::Perms_Update:
				return "Update Permissions";
			case GDB_Hist_Action::Perms_Delete:
				return "Remove Permissions";
      case GDB_Hist_Action::Node_CloneCreateDataset:
        return 'Clone user node on dataset creation';
		}

		return parent::string_nice($id);
	}
		/**
	 * Method to output a string describing the history item
	 *
	 * @param mixed $row Database row from the graph_hist table
	 * @param bool $show_time Should the date, time, and user be displayed
	 * @param string $separator How the different entries of data should be separated (default to <br />)
	 * @return string Contains text description of the node history item
	 *
	 */
	function Describe($row, $show_time = true, $separator = "<br />")
	{
		$output = array();

		// add time, and user details to the output if requested
		if ($show_time)
		{
			$output[] = "Time : " . $row->when;
			$output[] = "User : " . $row->user;
		}
		$this->CI =& get_instance();

		// load node and type information
		$node = $this->CI->graph_db->loadNode($row->node_id);
		$node_type = $node->GetTypeObj();

		// output each action differently depending on what the values are needed for
		switch($row->action)
		{
			case GDB_Hist_Action::Node_Create:
				{
					$output[] = "Node Name : " . $row->value2;
					$node_type = $this->CI->node_types->GetType($row->value1);
					$output[] = "Type : " .  $this->CI->node_types->GetType($row->value1)->GetName();
					break;
				}
			case GDB_Hist_Action::Node_Delete:
				{
					/*$output[] = " : " . $row->value1;
					$output[] = " : " . $row->value2;
					$output[] = " : " . $row->value3;*/
					break;
				}
			case GDB_Hist_Action::Node_ChangeTitle:
				{
					$output[] = "Old Title : " . $row->value1;
					$output[] = "New Title : " . $row->value2;
					break;
				}

			/** Relation_Add
			 *  Relation_Delete
			 *
			 * Both use the same format so can be grouped together
			 **/
			case GDB_Hist_Action::Relation_Add:
			case GDB_Hist_Action::Relation_Delete:
				{
					$dest_node = $this->CI->graph_db->loadNode($row->value1);
					$str = "Destination Node : ";
					$title = $dest_node->getTitle() . " (" . $dest_node->GetTypeName() . ", " . $dest_node->GetId(). ")";
					$str .= anchor("node/{$dest_node->getId()}", $title);

					$output[] = $str;
					$output[] = "Relation : " . GDB_Rel::GetReverseNameSingle($row->value2);
					break;
				}

			case GDB_Hist_Action::Attribute_Add:
				{
					$attr = $node_type->GetAttr($row->value1);
					$output[] = "Attribute : " . $attr->title;
					$output[] = "Value : " . $row->value2;
					break;
				}

			case GDB_Hist_Action::Attribute_Change:
				{
					$attr = $node_type->GetAttr($row->value1);
					$output[] = "Attribute : " . $attr->title;
					$output[] = "New Value : " . $row->value2;
					$output[] = "Old Value : " . $row->value3;
					break;
				}

			case GDB_Hist_Action::Attribute_Delete:
				{
					/*$output[] = " : " . $row->value1;
					$output[] = " : " . $row->value2;
					$output[] = " : " . $row->value3;*/
					break;
				}
		}
		return implode($separator,$output);
	}

	function AsXML($row)
	{
		$output = array();

		$this->CI =& get_instance();

		// load node and type information
		$node = $this->CI->graph_db->loadNode($row->node_id);
		$node_type = $node->GetTypeObj();

		// output each action differently depending on what the values are needed for
		switch($row->action)
		{
			case GDB_Hist_Action::Node_Create:
			{
				$output[] = "<title><![CDATA[" . $row->value2 . "]]></title>";
				$node_type = $this->CI->node_types->GetType($row->value1);
				$output[] = "<type id='" . $this->CI->node_types->GetType($row->value1)->id . "'>" .  $this->CI->node_types->GetType($row->value1)->GetName() . "</type>";
				break;
			}
			case GDB_Hist_Action::Node_Delete:
			{
				/*$output[] = " : " . $row->value1;
				$output[] = " : " . $row->value2;
				$output[] = " : " . $row->value3;*/
				break;
			}
			case GDB_Hist_Action::Node_ChangeTitle:
			{
				$output[] = "<old_title><![CDATA[" . $row->value1 . "]]></old_title>";
				$output[] = "<new_title><![CDATA[" . $row->value2 . "]]></new_title>";
				break;
			}

			/** Relation_Add
			 *  Relation_Delete
			 *
			 * Both use the same format so can be grouped together
			 **/
			case GDB_Hist_Action::Relation_Add:
			case GDB_Hist_Action::Relation_Delete:
			{
				$dest_node = $this->CI->graph_db->loadNode($row->value1);

				if (!$dest_node)
				{
					$output[] = "<destination id='{$row->value1}' cant_load='1' />";
				} else {
					$output[] = "<destination id='{$dest_node->getId()}' type='{$dest_node->getTypeObj()->id}'>" . $dest_node->getTitle() . "</destination>";
					$output[] = "<relation>" . GDB_Rel::GetReverseNameSingle($row->value2) . "</relation>";
				}
				break;
			}

			case GDB_Hist_Action::Attribute_Add:
			{
				$attr = $node_type->GetAttr($row->value1);
				$output[] = "<attribute id='{$attr->id}'>" . $attr->title . "</attribute>";
				$output[] = "<value><![CDATA[" . $row->value2 . "]]></value>";
				break;
			}

			case GDB_Hist_Action::Attribute_Change:
			{
				$attr = $node_type->GetAttr($row->value1);
				$output[] = "<attribute id='{$attr->id}'>" . $attr->title . "</attribute>";
				$output[] = "<old_value><![CDATA[" . $row->value2 . "]]></old_value>";
				$output[] = "<new_value><![CDATA[" . $row->value3 . "]]></new_value>";
				break;
			}

			case GDB_Hist_Action::Attribute_Delete:
			{
				/*$output[] = " : " . $row->value1;
				$output[] = " : " . $row->value2;
				$output[] = " : " . $row->value3;*/
				break;
			}
			case GDB_Hist_Action::Perms_Add:
			case GDB_Hist_Action::Perms_Delete:
			{
				$groupid = $row->value1;
				$perms = unserialize($row->value2);
				if ($groupid > 0)
				{
					$groupnode = $this->CI->graph_db->loadNode($groupid);
					if ($groupnode)
					{
						$output[] = "<user type='".str_replace('perm_','',$groupnode->getTypeObj()->id)."' id='$groupid'>". $groupnode->getTitle(). "</user>";
					} else {
						$output[] = "<user unknown='1'>$groupid</user>";
					}
				} else {
					$output[] = "<user base='1'>base</user>";
				}

				if ($perms)
				{
					$output[] = "<perms>" . implode("\n",$perms->AsXML()) . "</perms>";
				} else {
					$output[] = "<perms invalid='1' />";
				}
				break;
			}
			case GDB_Hist_Action::Perms_Update:
			{
				$groupid = $row->value1;
				if ($groupid > 0)
				{
					$groupnode = $this->CI->graph_db->loadNode($groupid);
					if (!$groupnode)
					{
						$output[] = "<user unknown='1'>$groupid</user>";
					} else {
						$output[] = "<user type='{$groupnode->getTypeObj()->id}' id='$groupid'>". $groupnode->getTitle(). "</user>";
					}
				} else {
					$output[] = "<user base='1'>base</user>";
				}

				$perms = unserialize($row->value2);
				if ($perms)
				{
					$output[] = "<new_perms>" . implode("\n",$perms->AsXML()) . "</new_perms>";
				} else {
					$output[] = "<new_perms invalid='1' />";
				}

				$perms = unserialize($row->value3);
				if ($perms)
				{
					$output[] = "<old_perms>" . implode("\n",$perms->AsXML()) . "</old_perms>";
				} else {
					$output[] = "<old_perms invalid='1' />";
				}
				break;
			}
		}
		return implode("\n",$output);
	}

	function DescribeShort($row, $show_time = true, $separator = "<br />")
	{
		$output = array();

		// add time, and user details to the output if requested
		if ($show_time)
		{
			$output[] = "Time : " . $row->when;
			$output[] = "User : " . $row->user;
		}
		$this->CI =& get_instance();

		// load node and type information
		$node = $this->CI->graph_db->loadNode($row->node_id);
		$node_type = $node->GetTypeObj();

		// output each action differently depending on what the values are needed for
		switch($row->action)
		{
			case GDB_Hist_Action::Node_Create:
				{
					$output[] = "<b>Title</b>: " . $row->value2 . "<br />";
					$output[] = "<b>Type</b>: " . $this->CI->node_types->GetType($row->value1)->GetName();
					break;
				}
			case GDB_Hist_Action::Node_Delete:
				{
					$output[] = "Node Deleted";
					break;
				}
			case GDB_Hist_Action::Node_CloneFrom:
			case GDB_Hist_Action::Node_CloneTo:
			{
				$dest_node_id = $row->value1;
				$dest_dataset = $row->value2;

				if ($dest_dataset == $this->CI->graph_db->active_dataset->table)
				{
					$this->graph_dest =& $this->CI->graph_db;
				} else if ($dest_dataset != "") {
					$dest_title = $dest_dataset;
					if (array_key_exists($dest_dataset, $this->CI->graph_db->datasets))
						$dest_title = $this->CI->graph_db->datasets[$dest_dataset]->name;

					$output[] = "Dataset : <a href='" . root_url() . $dest_dataset . "' target='_blank'>$dest_title</a><br>";
					//echo "Using Dataset : $dest_dataset<br>";
					$this->graph_dest = new Graph_DB();
					$this->graph_dest->setDataset($dest_dataset, false);
				}

				if ($dest_dataset == "")
					$dest_dataset = $this->CI->graph_db->active_dataset->table;

				$dest_node = $this->graph_dest->loadNode($row->value1);
				if ($dest_node)
				{
					$title = $dest_node->getTitle() . " (" . $dest_node->GetTypeName() . ", " . $dest_node->GetId(). ")";
					$str = "Node : " . anchor(root_url() . $dest_dataset . "/view/" . $dest_node->getID() , $title);
					$output[] = $str;
				}
				break;
			}
			case GDB_Hist_Action::Node_ChangeTitle:
				{
					$output[] =  "<b>Title</b>: " . $row->value2;
					break;
				}

			/** Relation_Add
			 *  Relation_Delete
			 *
			 * Both use the same format so can be grouped together
			 **/
			case GDB_Hist_Action::Relation_Add:
			case GDB_Hist_Action::Relation_Delete:
				{
					$dest_node = $this->CI->graph_db->loadNode($row->value1);
					if ($dest_node !== false) { // Check we actually found the node record.
						$title = $dest_node->getTitle() . " (" . $dest_node->GetTypeName() . ", " . $dest_node->GetId(). ")";
					} else {
						$title = '';
					}
					$str = anchor("view/node/{$dest_node->getId()}", $title);

					$output[] = $str;
					break;
				}

			case GDB_Hist_Action::Attribute_Add:
				{
					$attr = $node_type->GetAttr($row->value1);
					$output[] = "<b>".$attr->title."</b>: ";
					$output[] = $row->value2;
					break;
				}

			case GDB_Hist_Action::Attribute_Change:
				{
					$attr = $node_type->GetAttr($row->value1);
					$output[] = "<b>" . $attr->title."</b>: ";
					$output[] = $row->value2;
					break;
				}

			case GDB_Hist_Action::Attribute_Delete:
				{
					/*$output[] = " : " . $row->value1;
					$output[] = " : " . $row->value2;
					$output[] = " : " . $row->value3;*/
					break;
				}
			case GDB_Hist_Action::Perms_Add:
			case GDB_Hist_Action::Perms_Delete:
				{
					$groupid = $row->value1;
					$perms = unserialize($row->value2);
					if ($groupid > 0)
					{
						$groupnode = $this->CI->graph_db->loadNode($groupid);
						if ($groupnode)
						{
							$output[] = $groupnode->getTypeObj()->name . " : " . $groupnode->getTitleDisp()."<br>";
						} else {
							$output[] = "Unknown User or Group ($groupid)<br>";
						}
					} else {
						$output[] = "Base Node Permissions<br>";
					}

					if ($perms)
					{
						$output[] = "Permissions : " . implode(", ",$perms->describe()) . "<br>";
					} else {
						$output[] = "Invalid Permission Data";
					}
					break;
				}
			case GDB_Hist_Action::Perms_Update:
				{
					$groupid = $row->value1;
					if ($groupid > 0)
					{
						$groupnode = $this->CI->graph_db->loadNode($groupid);
						if (!$groupnode)
						{
							$output[] = "Unknown User or Group ($groupid)<br>";
						} else {
							$output[] = $groupnode->getTypeObj()->name . " : " . $groupnode->getTitleDisp()."<br>";
						}
					} else {
						$output[] = "Base Node Permissions<br>";
					}

					$perms = unserialize($row->value2);
					if ($perms)
					{
						$output[] = "New Permissions : " . implode(", ",$perms->describe()) . "<br>";
					} else {
						$output[] = "Invalid New Permission Data";
					}

					$perms = unserialize($row->value3);
					if ($perms)
					{
						$output[] = "Old Permissions : " . implode(", ",$perms->describe()) . "<br>";
					} else {
						$output[] = "Invalid Old Permission Data";
					}
					break;
				}
		}
		return implode("",$output);
	}
}

/**
 * Type of attribute on a node
 *
 * Different types of attribute are stored in different fields in the database
 *
 **/
class GDB_Attrib_Type extends Graph_db_const
{
	const _String = 1;
	const _Text = 2;
	const _Int = 3;
	const _Float = 4;
	const _Time = 5;
	const _DateTime = 6;
	const _Bool = 7;
	const _SelectInt = 8;
	const _SelectStr = 9;
	const _Date = 10;
	const _File = 11;
	const _Link = 12;
	const _Image = 13;
	const _KVPair = 20;

	/**
	 * Static array of ID to field mappings
	 */
	static $fields = array (
		GDB_Attrib_Type::_String => 'value_str',
		GDB_Attrib_Type::_Text => 'value_text',
		GDB_Attrib_Type::_Int => 'value_int',
		GDB_Attrib_Type::_Float => 'value_float',
		GDB_Attrib_Type::_Time => 'value_time',
		GDB_Attrib_Type::_DateTime => 'value_datetime',
		GDB_Attrib_Type::_Bool => 'value_int',
		GDB_Attrib_Type::_SelectInt => 'value_int',
		GDB_Attrib_Type::_SelectStr => 'value_str',
		GDB_Attrib_Type::_Date => 'value_datetime',
		GDB_Attrib_Type::_File => 'value_str',
		GDB_Attrib_Type::_Link => 'value_str',
		GDB_Attrib_Type::_Image => 'value_str',
		GDB_Attrib_Type::_KVPair => 'value_text',
		);

	/**
	 * XML Strings lookup to field types - when loading the
	 * node definition from an xml file, these type attributes
	 * are mapped to the attribute type constants
	 */
	static $xml_lookup = array (
		'string' => GDB_Attrib_Type::_String,
		'text' => GDB_Attrib_Type::_Text,
		'integer' => GDB_Attrib_Type::_Int,
		'int' => GDB_Attrib_Type::_Int,
		'float' => GDB_Attrib_Type::_Float,
		'time' => GDB_Attrib_Type::_Time,
		'datetime' => GDB_Attrib_Type::_DateTime,
		'bool' => GDB_Attrib_Type::_Bool,
		'yesno' => GDB_Attrib_Type::_Bool,
		'select' => GDB_Attrib_Type::_SelectInt,
		'select_int' => GDB_Attrib_Type::_SelectInt,
		'select_str' => GDB_Attrib_Type::_SelectStr,
		'selectint' => GDB_Attrib_Type::_SelectInt,
		'selectstr' => GDB_Attrib_Type::_SelectStr,
		'date' => GDB_Attrib_Type::_Date,
		'file' => GDB_Attrib_Type::_File,
		'link' => GDB_Attrib_Type::_Link,
		'image' => GDB_Attrib_Type::_Image,
		'kvpair' => GDB_Attrib_Type::_KVPair,
		);

	static function string($id, $notused = null) { return parent::string($id, "GDB_Attrib_Type"); }
	static function string_nice($id, $notused = null) { return parent::string_nice($id, "GDB_Attrib_Type"); }

	/**
	 * Static function to get the field used to store a particular
	 * field type
	 *
	 * @param GDB_Attrib_Type $type The type of field to get
	 * @return string sql field name
	 *
	 */
	static function GetField($type)
	{
		if (!array_key_exists($type, GDB_Attrib_Type::$fields))
		{
			show_error("Missing constant field in GDB_Attrib_Type, $type");
		}
		return GDB_Attrib_Type::$fields[$type];
	}

	/**
	 * Used when loading a node definitions fields from an xml file,
	 * will return the type associated with the type attribute
	 **/
	static function GetTypeFromXMLString($string)
	{
		if (!array_key_exists($string, GDB_Attrib_Type::$xml_lookup))
		{
			show_error("Missing XML field type constant field in GDB_Attrib_Type, $string");
		}
		return GDB_Attrib_Type::$xml_lookup[$string];
	}
}
