<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

/**
 * Node type information class.
 *
 * On being constructed, will load the node types from the basedir, and create a Node_Type class for each different type
 *
 * This will need some sort of cacheing as we dont want to be loading multiple XML files for every page
 *
 */
class Node_Types
{
	var $node_types;
	var $create_anywhere = array();

	var $name_to_id = array();
	var $createfrom = array();

	var $nosearch = array();

	function __construct()
	{
		$this->CI =& get_instance();
		$this->CI->load->helper('file');

		//$basedir = "cm_layout";
		$basedir = $this->CI->node_def;

		$this->node_types[0] = new Node_Type(null, $this);
		$this->name_to_id["root"] = 0;

		$filenames = get_filenames("nodedef/$basedir/nodetypes");
		//echo "Loading Node type def : $basedir<br>";
		if (is_array($filenames) && count($filenames) > 0)
		{
			foreach ($filenames as $filename)
			{
				$path = "nodedef/$basedir/nodetypes/$filename";
				$data = read_file($path);
				$xml = simplexml_load_string($data);
				$node_type = new Node_Type($xml, $this);
				$this->node_types[$node_type->GetGDBId()] = $node_type;

				$this->name_to_id[$node_type->GetId()] = $node_type->GetGDBId();

				if ($node_type->nosearch) $this->nosearch[] = $node_type->GetGDBId();
			}
		}

		foreach ($this->createfrom as $type => $create)
		{
			$node_type_id = $this->name_to_id[$type];
			$node_type =& $this->node_types[$node_type_id];

			foreach ($create as $ctype)
			{
				$node_type->create[] = $ctype;
			}
		}
		//print_p($this->createfrom);
		//print_p($this->node_types);

		// build an array containing what node types can be created from where
		foreach ($this->node_types as &$type)
		{
			foreach ($type->create as $create)
			{
				$this->can_be_created_from[$create][$type->gdbid] = $type->name;
			}
		}
	}

	function createBlankNodeType($type)
	{
		$this->node_types[$type] = new Node_Type(null, $this);
		$this->node_types[$type]->id = "type".$type;
		$this->node_types[$type]->gdbid = $type;
		$this->node_types[$type]->name = "type".$type;
		$this->name_to_id["type".$type] = $type;
	}

	function &GetType($type) // pass in integer type
	{
		//echo "&GetType($type)<br>";
		// if we have passed in a string node name, then convert it to a type id
		if (gettype($type) == "string")
		{
			// if we have passed a number as a string, then convert it to an int
			$ti = (string)((int)$type);
			if ($ti == $type)
			{
				$type = $ti;
			} else {
				$type = $this->NameToId($type);
			}
		}

		if (array_key_exists($type, $this->node_types))
		{
			return $this->node_types[$type];
		} else {
			//$this->createBlankNodeType($type);
			//return $this->node_types[$type];
			show_error("Node_Types::GetType - $type not found<Br>");
		}
	}

	function NameToId($name)
	{
		//echo "NameToId($name) - ";
		//print_p($this->name_to_id);
		if (array_key_exists($name, $this->name_to_id))
		{
			//echo $this->name_to_id[$name]."<br>";
			return $this->name_to_id[$name];
		} else {
			//echo "NameToId : $name - no type<br>";
			//echo dumpStack();
			//exit;
			//echo "-1<br>";
			return -1;
		}
	}

	function GetTypesForForm($forsearch = true)
	{
		$output = array();
		$alts = array();

		if ($forsearch)
			$output[0] = "-- All Types --";

		foreach ($this->node_types as $name => $type)
		{
			if ($name == 0) continue;
			if ($type->nosearch > 0) continue;
			$output[$name] = $type->GetName();

			if ($forsearch && $type->search_alt != "")
				$alts[$type->search_alt][] = $name;
		}
		foreach ($alts as $disp => &$vals)
		{
			$name =	implode("|",$vals);
			$output[$name] = $disp;
		}
		//print_p($alts);
		//print_p($output);

		asort($output);
		//print_p($output);
		return $output;
	}

	/**
	 * Returns an array of gdbid to id mappings for all nodes
	 *
	 * @return mixed This is the return value description
	 *
	 */
	function getAllTypes()
	{
		$result = array();
		foreach ($this->node_types as &$node_type)
			$result[$node_type->getGDBId()] = $node_type->getID();

		return $result;
	}

	function getCount()
	{
		return count($this->node_types) - 1;
	}

	function addCreateAnywhere($node)
	{
		$this->create_anywhere[$node->GetGDBId()] = $node->GetId();
	}

	function getCreateAnywhere()
	{
		return $this->create_anywhere;
	}
}

/**
 * Class containg the details of a particular node type
 *
 * Includes naming informations, what attributes the node is allowed
 *
 * Will also contain information about what nodes can be linked to this node and other
 * similar information
 **/
class Node_Type
{
	var $id;
	var $gdbid;
	var $name;
	var $attrs = array(); // indexed array of attributes

	var $node_types; // reference to the node types class

	var $attr_order = array(); // array storing the order the attributes are listed in

	// node creating options
	var $create = array(); // list of node types that can be created from this node type
	var $create_anywhere = 0; // can this node type be created anywhere in the system

	// node linking options
	var $link_anywhere = 0; // can this node type link to anything
	var $link = array(); // list of possible node types that this node can link to directly
	var $link_path = array(); // list of possible nodes that can be linked to from this node type in path form

	var $title;
	var $longtitle;

	var $hasAPI = 0;
	var $hasHooks = 0;

	var $hide_create = 0;
	var $no_title_req = 0;

	function __construct($xml = null, &$node_types)
	{
		$this->node_types =& $node_types;

		// no xml passed in, so create a root node
		if (!$xml)
		{
			$this->id = "root";
			$this->gdbid = 0;
			$this->name = "Root Node";
			$this->nosearch = 1;
			return;
		}

		$this->id = (string)$xml->id->attributes()->id;
		$this->gdbid = (int)$xml->id->attributes()->gdbid;
		$this->name = (string)$xml->id->attributes()->name;
		$this->shortname = (string)$xml->id->attributes()->name;
		if ($xml->id->attributes()->shortname)
			$this->shortname = (string)$xml->id->attributes()->shortname;
		$this->nosearch = (int)$xml->id->attributes()->nosearch;
		$this->search_alt = (string)$xml->id->attributes()->search_alt;
		$this->hide_create = (int)$xml->hide_create;
		$this->no_title_req = (int)$xml->id->attributes()->no_title_req;
		// load attributes

    if ($xml->api)
      $this->hasAPI = 1;

    // If the node has hooks register them under their type
    if ($xml->hooks) {
      $this->hasHooks = 1;
      $this->hooks = array();

      foreach ($xml->hooks->hook as $hook) {
        $tmp_hook = array();
        $h_type = (string)$hook->attributes()->type;
        foreach ($hook->attributes() as $h_attr_k => $h_attr_v) {
          if ($h_attr_k != 'type') {
            $tmp_hook[$h_attr_k] = (string) $h_attr_v;
          }
        }
        if (isset($this->hooks[$h_type])) {
          $this->hooks[$h_type][] = $tmp_hook;
        } else {
          $this->hooks[$h_type] = array($tmp_hook);
        }
      }
    }


    if ($xml->fields->field)
		{
			foreach ($xml->fields->field as $field)
			{
				// work out the node attribute type
				$type = (string)$field->attributes()->type;
				$type = GDB_Attrib_Type::GetTypeFromXMLString($type);

				// check to see if a derived class exists for that node attribute type
				$classname = "Node_Type_Attr".GDB_Attrib_Type::string($type);
				if (!class_exists($classname)) $classname = "Node_Type_Attr";

				// create a new object based on the attribute type
				$attr = new $classname($field);


				$this->attrs[$attr->id] = $attr;
				// set a reference in the order array so they can be displayed in order
				$this->attr_order[] =& $this->attrs[$attr->id];
			}
		}

		//echo "Node Type : {$this->id}<br>";

		if ($xml->create)
		{
			if ((int)($xml->create->attributes()->anywhere) == 1)
			{
				$node_types->addCreateAnywhere($this);
				$this->create_anywhere = 1;
			}
			if ($xml->create->type)
			{
				foreach ($xml->create->type as $type)
				{
					//echo "Can create $type<br>";
					$this->create[] = (string)$type;
				}
			}
		}

		if ($xml->createfrom)
		{
			// load create from spec, and add it to main class
			// main class will then add this info to the node_types needed
			if ($xml->createfrom->type)
			{
				foreach ($xml->createfrom->type as $type)
				{
					//echo "Can create $type<br>";
					$this->node_types->createfrom[(string)$type][] = $this->id;
				}
			}
		}

		$this->single_dir_link = 0;

		if ($xml->link)
		{
			if ((int)($xml->link->attributes()->anywhere) == 1)
			{
				$this->link_anywhere = 1;
			}
			$this->single_dir_link = (int)$xml->link->attributes()->single_dir;


			if ($xml->link->type)
				foreach ($xml->link->type as $type)
					$this->link[] = (string)$type;

			if ($xml->link->path)
				foreach ($xml->link->path as $path)
					$this->link_path[] = (string)$path;
		}

		if ($xml->title)
		{
			$this->title = (string)$xml->title;
			$this->longtitle = $this->title;
		}

		if ($xml->longtitle)
		{
			$this->longtitle = (string)$xml->longtitle;
		}

		$this->tabparams = new stdClass();

		if ($xml->tabparams)
		{
			foreach ($xml->tabparams->attributes() as $a => $b)
			{
				$this->tabparams->$a = $b;
			}
		}
	}

	function GetGDBId()
	{
		//echo "Getting GetGDBId : {$this->gdbid}<br>";
		return $this->gdbid;
	}
	function GetId()
	{
		//echo "Getting GetGDBId : {$this->gdbid}<br>";
		return $this->id;
	}
	function GetName()
	{
		//echo "Getting GetGDBId : {$this->gdbid}<br>";
		return $this->name;
	}

	function GetAttr($id)
	{
		if (array_key_exists($id, $this->attrs))
		{
			return $this->attrs[$id];
		} else {
			//echo "Node_Type::GetAttr - TYPE : {$this->name}, Attribute: $id - MISSING<br>";
			return new Node_Type_Attr(null);
		}
	}

	function &GetAttributes()
	{
		return $this->attr_order;
	}

	function getPossibleCreates()
	{
		$anywhere = $this->node_types->getCreateAnywhere();

		$result = array();
		foreach ($this->create as $create)
		{
			//echo "Getting node type $create<Br>";
			$node_type = $this->node_types->GetType($create);
			$result[$node_type->getGDBId()] = $node_type->getID();
			//echo "Got {$node_type->getID()}<br>";
		}

		foreach ($anywhere as $id => $name)
			$result[$id] = $name;

		//print_p($result);

		return $result;
	}

	function getPossibleLinks()
	{
		$result = array();
		if ($this->link_anywhere)
		{
			$result['direct'] = $this->node_types->getAllTypes();
		} else {
			$result['direct'] = $this->link;
			$result['path'] = $this->link_path;
		}
		return $result;
	}
}

/**
 * Contains details about a node types attribute, including some functions to aid in outputting the data
 * and setting up forms for editing the data
 *
 * Different attribute types can be subclassed (derived) from this class to alter the display and form behaviour
 */
class Node_Type_Attr
{
	var $id;
	var $title;
	var $type;
	var $validation = array();
	var $default = '';
	var $attrs = array();
	var $search = 0;
	var $autocomplete = 0;

	function __construct($xml)
	{
		if (!$xml)
			return;

		$this->id = (string)$xml->attributes()->id;
		$this->title = (string)$xml->attributes()->title;
		$type = (string)$xml->attributes()->type;
		$this->type = GDB_Attrib_Type::GetTypeFromXMLString($type);
		$this->default = (string)$xml->attributes()->default;
		$this->noedit = (string)$xml->attributes()->noedit;
		$this->nocreate = (string)$xml->attributes()->nocreate;
		$this->hidden = (string)$xml->attributes()->hidden;
		$this->search = (int)$xml->attributes()->search;
		$this->autocomplete = (int)$xml->attributes()->autocomplete;

		$this->validation['type'] = '';

		if ($xml->validation)
		{
			foreach ($xml->validation->attributes() as $name => $attr)
			{
				$this->validation[$name] = (string)$attr;
			}
		}

		if ($xml->attrs)
		{
			foreach ($xml->attrs->children() as $child)
			{
				$name = $child->getName();
				$this->attrs[$name] = (string)$child;
			}
		}
	}

	function SetValidation($fv)
	{
		if ($this->validation['type'])
		{
			$fv->set_rules($this->id, $this->title, $this->validation['type']);
		} else {
			$fv->set_rules($this->id, $this->title);
		}
	}

	function OutputHeader()
	{
		$text = $this->title . ($this->validation['type'] ? '<span class="req_field"> *</span>' : '');
		return form_label($text, $this->id);
	}

	function OutputField($cur_val, &$node = null)
	{
		$field = array('name' => $this->id, 'id' => $this->id, 'value' => $cur_val);
		$field = array_merge($field, $this->attrs);
		$output = form_input($field);

		if ($this->autocomplete && $node)
		{
			$url = site_url("node/lookups/" . $node->getID() . "/" . $this->id);
			$output .= "
<script>
$( '#{$this->id}' ).autocomplete({
			source: '$url',
			minLength: 1,
			delay: 100
		});
</script>";
			//print_p($url);
		}

		return $output;
	}

	function GetDefault()
	{
		return $this->default;
	}

	function GetTitle()
	{
		return $this->title;
	}

	function GetPostValue(&$node)
	{
		return set_value($this->id);
	}

	function OutputJS($cur_val)
	{
		return "";
	}

	function GetType()
	{
		return $this->type;
	}
}

/**
 * Different node attribute type classes that have specific implementations
 * for the different functions
 **/

// bool attribute type
class Node_Type_Attr_Bool extends Node_Type_Attr
{
	var $confirm = 0;

	function __construct($xml) {
		parent::__construct($xml);

		if (!$xml)
			return;

		$this->confirm = (int)$xml->attributes()->confirm;
	}

	function OutputField($cur_val, &$node = null)
	{
		$data = array('name' => $this->id);
		if ($this->confirm) {
			$data['class'] = 'confirm';
		}
		return form_checkbox($data, 1, $cur_val);
	}

	function GetPostValue(&$node)
	{
		if (set_value($this->id))
			return 1;
		return 0;
	}
}

// text attribute type
class Node_Type_Attr_Text extends Node_Type_Attr
{
	function OutputField($cur_val, &$node = null)
	{
		return form_textarea(array('name' => $this->id, 'id' => $this->id, 'value' => $cur_val, 'rows' => 20, 'cols' => 76));
	}

	function OutputJS($cur_val)
	{
		$script[] = "$('#".$this->id."').tinymce({";
		// Location of TinyMCE script
		$script[] = "	script_url : '" .  asset_url() . "js/tiny_mce/tiny_mce.js',";

		// General options
		$script[] = "	theme : 'advanced',";
		$script[] = "	plugins : 'autolink,lists,style,paste',";

		// Theme options
		$script[] = "	theme_advanced_buttons1 : 'cut,copy,paste,pastetext,pasteword,|,formatselect,|,bold,italic,bullist,numlist,outdent,indent,|,undo,redo,removeformat,|,link,unlink,|,code,help',";
		$script[] = "	theme_advanced_buttons2 : '',";
		$script[] = "	theme_advanced_toolbar_location : 'top',";
		$script[] = "	theme_advanced_toolbar_align : 'left',";
		$script[] = "	theme_advanced_statusbar_location : 'bottom',";
		$script[] = "	theme_advanced_resizing : true,";

		// Example content CSS (should be your site CSS)
		//$script[] = "	content_css : 'css/content.css',";

		// Drop lists for link/image/media/template dialogs
		$script[] = "	template_external_list_url : 'lists/template_list.js',";
		$script[] = "	external_link_list_url : 'lists/link_list.js',";
		$script[] = "	external_image_list_url : 'lists/image_list.js',";
		$script[] = "	media_external_list_url : 'lists/media_list.js'";

		$script[] = "});	";
		return "\t".implode("\n\t",$script) . "\n";
	}
}

// text attribute type
class Node_Type_Attr_SelectInt extends Node_Type_Attr
{
	var $display = "dropdown";
	var $values = array();

	function __construct($xml)
	{
		parent::__construct($xml);

		// load in select values, and options for select display
		if ($xml->select)
		{
			if ((string)$xml->select->attributes()->display == "radio")
				$this->display = "radio";

			if ($xml->select->value)
			{
				foreach ($xml->select->value as $value)
				{
					// get attribute as a string do we can derive the selectstr class directly from this one
					$id = (string)$value->attributes()->id;
					$value = (string)$value;
					$this->values[$id] = $value;
				}
			}
		}
	}

	function OutputField($cur_val, &$node = null)
	{
		if ($this->display == "dropdown")
		{
			return form_dropdown($this->id, $this->values, $cur_val);
		} else {
			$output = array();
			foreach ($this->values as $id => $val)
			{
				$output[] = $val . ": " . form_radio($this->id, $id, $cur_val == $id);
			}
			return implode(", ",$output);
		}
	}
}

// derive directly from the SelectInt, with no changes, as behaves exactly the same
class Node_Type_Attr_SelectStr extends Node_Type_Attr_SelectInt
{
}

class Node_Type_Attr_Date extends Node_Type_Attr
{
	function OutputJS($cur_val)
	{
		$script[] = "cal_{$this->id} = new dhtmlXCalendarObject(['{$this->id}']);";
		$script[] = "cal_{$this->id}.setSkin('omega');";
		$script[] = "cal_{$this->id}.setDateFormat('%Y-%m-%d');";
		$script[] = "cal_{$this->id}.setPosition('right');";
		$script[] = "cal_{$this->id}.hideTime();";
		return "\t".implode("\n\t",$script) . "\n";
	}

	function OutputField($cur_val, &$node = null)
	{
		$time = strtotime($cur_val);
		return form_input(array('name' => $this->id, 'id' => $this->id, 'value' => date("Y-m-d", $time))) . "<img style='position: relative; top:2px;' src='" . asset_url() . "image/misc/datepopup.png'>";
	}

	function GetDefault()
	{
		return date("Y-m-d");
	}

}

// derive directly from the SelectInt, with no changes, as behaves exactly the same
class Node_Type_Attr_DateTime extends Node_Type_Attr
{
	function OutputJS($cur_val)
	{
		$script[] = "cal_{$this->id} = new dhtmlXCalendarObject(['{$this->id}']);";
		$script[] = "cal_{$this->id}.setSkin('omega');";
		$script[] = "cal_{$this->id}.setDateFormat('%Y-%m-%d %H:%i');";
		$script[] = "cal_{$this->id}.setPosition('right');";
		return "\t".implode("\n\t",$script) . "\n";
	}

	function OutputField($cur_val, &$node = null)
	{
		$time = strtotime($cur_val);
		return form_input(array('name' => $this->id, 'id' => $this->id, 'value' => date("Y-m-d H:i", $time))) . "<img style='position: relative; top:2px;' src='" . asset_url() . "image/misc/datepopup.png'>";
	}

	function GetDefault()
	{
		return date("Y-m-d H:i");
	}
}


// derive directly from the SelectInt, with no changes, as behaves exactly the same
class Node_Type_Attr_Time extends Node_Type_Attr
{
	function OutputJS($cur_val)
	{
		/*$script[] = "cal_{$this->id} = new dhtmlXCalendarObject(['{$this->id}']);";
		$script[] = "cal_{$this->id}.setSkin('omega');";
		$script[] = "cal_{$this->id}.setDateFormat('%H:%i');";
		return "\t".implode("\n\t",$script) . "\n";	*/
	}

	function OutputField($cur_val, &$node = null)
	{
		$time = strtotime($cur_val);
		return form_input(array('name' => $this->id, 'id' => $this->id, 'value' => date("H:i", $time)));
	}

	function GetDefault()
	{
		return date("H:i");
	}
}

// derive directly from the SelectInt, with no changes, as behaves exactly the same
class Node_Type_Attr_KVPair extends Node_Type_Attr
{
	var $node = '';
	var $extpath = array();
	var $kvtype = '';

	function __construct($xml)
	{
		parent::__construct($xml);

		if (!$xml) return;

		$this->xml = $xml;

		$this->node = (string)$xml->attributes()->node;
		$this->kvtype = (string)$xml->attributes()->kvtype;

		if ($xml->extpath)
		{
			foreach ($xml->extpath as $extpath)
			{
				$this->extpath[] = trim((string)$extpath);
			}
		}
	}


	function OutputJS($cur_val)
	{
		$script[] = "$('#".$this->id."_pair').tinymce({";
		// Location of TinyMCE script
		$script[] = "	script_url : '" .  asset_url() . "js/tiny_mce/tiny_mce.js',";

		// General options
		$script[] = "	theme : 'advanced',";
		$script[] = "	plugins : 'autolink,lists,style,paste',";

		// Theme options
		$script[] = "	theme_advanced_buttons1 : 'cut,copy,paste,pastetext,pasteword,|,formatselect,|,bold,italic,bullist,numlist,outdent,indent,|,undo,redo,removeformat,|,link,unlink,|,code,help',";
		$script[] = "	theme_advanced_buttons2 : '',";
		$script[] = "	theme_advanced_toolbar_location : 'top',";
		$script[] = "	theme_advanced_toolbar_align : 'left',";
		$script[] = "	theme_advanced_statusbar_location : 'bottom',";
		$script[] = "	theme_advanced_resizing : true,";

		// Example content CSS (should be your site CSS)
		//$script[] = "	content_css : 'css/content.css',";

		// Drop lists for link/image/media/template dialogs
		$script[] = "	template_external_list_url : 'lists/template_list.js',";
		$script[] = "	external_link_list_url : 'lists/link_list.js',";
		$script[] = "	external_image_list_url : 'lists/image_list.js',";
		$script[] = "	media_external_list_url : 'lists/media_list.js'";

		$script[] = "});	";
		return "\t".implode("\n\t",$script) . "\n";
	}

	function OutputField($cur_val, &$node = null, $nodeisparent = false)
	{
		//print_p($this);

		if (!$node)
		{
			echo dumpStack();
			return "You must pass in a node";
		}

		$sel_node = $node;
		if ($this->node == "parent" && !$nodeisparent)
		{
			//echo "Using Parent<br>";
			$sel_node = $node->getParent();
		}

		$nodelist = array();
		$nodelist[] = $sel_node;

		$selopts = array();
		$selopts[''] = " - New Group - ";

		$lookup = array();

		// get currently selected item
		$bits = explode("~",$cur_val);
		$cur_val = $bits[0];

		foreach ($this->extpath as $extpath)
		{
			//echo "Ext Path : $extpath<br>";
			// need to find node along the path
			$extnodes = $node->nodesAlongPath($extpath);

			foreach ($extnodes as &$subnode)
			{
				//echo "Adding Node " . $subnode->getTitle() . "<br>";
				$nodelist[] = $subnode;
			}
		}

		foreach ($nodelist as &$sel_node)
		{
			//echo "KV Pairs from " . $sel_node->getTitle() . "<br>";
			$kv_pairs = $sel_node->getRelations("kv_pair");
			foreach ($kv_pairs as &$kvnode)
			{
				$text = $kvnode->getTitle();
				$var = 	$kvnode->getGuid();

				// need to check for type being passes, and make sure nodes in the list match the path specified

				$type = $kvnode->getAttributeValue("type");
				if ($type == "") $type = "outcome";
				//echo "Node : $text, type : $type<br>";

				if ($type != $this->kvtype) continue;

				$selopts[$sel_node->getTypeObj()->name . " - " . $sel_node->getTitle()][$var] = $text;
				$lookup[$var] = $kvnode->getAttributeValue("desc");
			}
		}

		$output = "";

		$output .= "<script>\n";
		$output .= "if (typeof kvpairs=='undefined') kvpairs = new Object();\n";
		$output .= "kvpairs['{$this->id}'] = new Object();\n";
		foreach ($lookup as $key => $value)
			$output .= "kvpairs['{$this->id}']['$key'] = '" . str_replace("+"," ",urlencode($value)) . "'\n";
		$output .= "</script>";

		// set up field parameters
		$field = array('name' => $this->id, 'id' => $this->id, 'value' => $cur_val, 'style' => 'width: 300px;', 'class' => 'kv_pair_select');
		$field = attrs_to_text($field);

		// dropdown
		$output .= "<div style='margin-bottom:2px;'><div class='round_block kv_title'>Group :</div>". form_dropdown($this->id, $selopts, urlencode($cur_val),$field) . "</div><div class='clear'></div>";

		// new group html
		$output .= "<div id='{$this->id}_newgroup'>";

		$field = array('name' => $this->id . "_key", 'id' => $this->id . "_key", 'value' => '', 'style' => 'width: 300px;');
		$output .= "<div><div class='round_block kv_title'>New Title <span class='req_field'>*</span> :</div>". form_input($field) . "</div><div class='clear'></div>";


		$field = array('name' => $this->id . "_value", 'id' => $this->id . "_pair", 'value' => '', 'rows' => 8, 'cols' => 50);
		$output .= "<div><div style='margin-bottom: 4px;' class='round_block kv_title'>Description :</div><div class='clear'></div>".form_textarea($field)."</div>";

		$output .= "</div>";

		// existing group html
		$output .= "<div id='{$this->id}_exgroup'>";
		$output .= "<div class='round_block kv_title'>Description :</div><div id='{$this->id}_description' style='padding: 3px;'></div>";
		$output .= "</div>";

		return $output;
	}

	function GetPostValue(&$node)
	{
		$target_node = $node;
		if ($this->node == "parent")
			$target_node = $node->getParent();

		$CI = get_instance();
		$value = urldecode($CI->input->post($this->id));

    if ($value != '')
    {
      return set_value($this->id);
    }

		$kv_pairs = $target_node->getRelations("kv_pair");

		$value = urldecode($CI->input->post($this->id . "_key"));
		$desc = urldecode($CI->input->post($this->id . "_value"));

		if (!$value)
			return "";

		foreach ($kv_pairs as &$kvnode)
		{
			// check we aren't creating a node with the same title as one that exists
			if ($kvnode->getTitle() == $value)
			{
				return $kvnode->getGuid();
			}
		}

		$newnode = $CI->graph_db->createNode($value, 100, $target_node);
		$newnode->setAttribute("desc", $desc);
		$attr = $node->getAttribute($this->id);
		$newnode->setAttribute("type", $attr->type_obj->kvtype, GDB_Attrib_Type::_SelectStr);
		$newnode->save();

		return $newnode->getGuid();
	}
}

function attrs_to_text(&$ar)
{
	$out = "";
	foreach ($ar as $attr => $value)
	{
		$out .= "$attr='$value' ";
	}

	return $out;
}