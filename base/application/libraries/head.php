<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

/**
 * Helper library that makes outputting js and css files in the header of the page easier
 *
 */
class Head
{
  var $js = array();
  var $js_ie = array();
  var $css = array();
	var $raw = "";
	var $footer = "";
	var $trailing = "";
	var $leading = "";
	
	function __construct()
	{
		$this->AddJS("js/jquery/jquery-1.7.1.min.js");
		$this->AddJS("js/jquery/json.js");
		$this->AddJS("js/lib/debug.js");
		$this->AddJS("js/lib/popup.js");
		$this->AddJS("js/lib/tooltip.js");
		$this->AddJS("js/lib/base64.js");
    $this->AddJS("js/main.js");
    $this->AddJSIE("js/selectivizr-min.js", 'lte IE 8');

    $this->AddCSS("css/main.css");
		$this->AddCSS("css/lib/debug.css");
		$this->AddCSS("css/lib/popup.css");
		$this->AddCSS("css/lib/tooltip.css");
	}

  // add a js file to the output
  function AddJS($file)
  {
    $this->js[] = $file;
  }
  // add a js file to the output
  function AddJSIE($file, $version)
  {
    $this->js_ie[$version] = $file;
  }


  // add a css file to the output
	function AddCSS($file)
	{
		$this->css[] = $file;
	}	
	
	// add raw js code to the output
	function AddRawScript($script)
	{
		$this->raw .= $script . "\n";	
	}
	
	// output the html for the requested js and css
	function Output()
	{
		$CI =& get_instance();
    foreach ($this->js as &$js)
    {
      echo $CI->javascript->_open_script($CI->config->slash_item('asset_url').$js);
      echo $CI->javascript->_close_script();
    }

    foreach ($this->js_ie as $version => &$js)
    {
      echo "<!--[if $version]>\n";
      echo $CI->javascript->_open_script($CI->config->slash_item('asset_url').$js);
      echo $CI->javascript->_close_script();
      echo "<![endif]-->\n";
    }


    foreach ($this->css as &$css)
		{
			echo "<link rel='stylesheet' type='text/css' href='" . asset_url() . $css . "' />\n";
		}
		
		$CI->javascript->compile();
		
		$script_foot = $CI->load->get_var('script_foot');
		echo $script_foot;
		
		if ($this->raw)
		{
			echo $CI->javascript->_open_script(). "\n";
			echo $this->raw;
			echo $CI->javascript->_close_script();
		}
	}
	
	function AddFooter($string)
	{
		$this->footer .= $string;	
	}
	
	function HasFooter()
	{
		return $this->footer != "";
	}	
	
	function Footer()
	{
		return $this->footer;	
	}
	
	function AddMessage($msg)
	{
		$CI =& get_instance();
		$messages = $CI->session->userdata('messages');
		if (empty($messages)) $messages = array();
		$messages[] = $msg;
		$CI->session->set_userdata('messages', $messages);
	}
	
	function AddError($msg)
	{
		$CI =& get_instance();
		$errors = $CI->session->userdata('errors');
		if (empty($errors)) $errors = array();
		$errors[] = $msg;
		$CI->session->set_userdata('errors', $errors);
	}
	
	function MessageText($message)
	{
		return "<div class='message round'><img src='".asset_url()."image/misc/message.png'>$message</div>";	
	}
	
	function ErrorText($error)
	{
		return "<div class='error round'><img src='".asset_url()."image/misc/error.png'>$error</div>";		
	}
	
	function ShowMessages()
	{
		$CI =& get_instance();
		$messages = $CI->session->userdata('messages');
				
		if (is_array($messages) && count($messages) > 0)
		{
			foreach ($messages as $message)
			{
				echo $this->MessageText($message);	
			}
		}
		
		$errors = $CI->session->userdata('errors');
				
		if (is_array($errors) && count($errors) > 0)
		{
			foreach ($errors as $error)
			{
				$this->ErrorText($error);
			}
		}

		$CI->session->unset_userdata('messages');
		$CI->session->unset_userdata('errors');		
		
		//exit;
	}

	function AddTrailing($string)
	{
		$this->trailing .= $string;	
	}
	
	function HasTrailing()
	{
		return $this->trailing != "";
	}	
	
	function Trailing()
	{
		return $this->trailing;	
	}
	function AddLeading($string)
	{
		$this->leading .= $string;	
	}
	
	function HasLeading()
	{
		return $this->leading != "";
	}	
	
	function Leading()
	{
		return $this->leading;	
	}
	
	function ParentRefresh($url = "")
	{
		if ($url)
		{
?>
<script>
window.parent.location = '<?php echo site_url($url); ?>';
</script>
<?php		} else {
?>
<script>
window.parent.location.reload(true);
</script>
<?php
		}
		
		exit;
	}
	
	function ValidationErrors()
	{
		if (FALSE === ($OBJ =& _get_validation_object()))
		{
			return '';
		}
		
		$errors = $OBJ->error_string("~","~");
		$errors = trim($errors,"~ \r\n");
		$errors = str_replace("\n","",$errors);
		$errors = str_replace("\r","",$errors);
		$errors = str_replace("~~","~",$errors);
		$errors = str_replace("~ ~","~",$errors);
		if ($errors)
		{
			$parts = explode("~",$errors);
			foreach ($parts as $part)
			{
				$part = trim($part);
				if (strlen($part) == 0) continue;
				
				echo "<div class='error round'><img src='".asset_url()."image/misc/error.png'>$part</div>";	
			}			
		}		
	}
}