<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

/**
 * Class derived from the basic node class that is aware of the nodes type
 *
 * Its very liklety that this class will end up merged with the graph_db_node class, as all nodes
 * should be type aware
 */
class Graph_DB_Node_Typed extends Graph_DB_Node
{
	var $type_object;

	static $dx_auth;
	static $is_admin;

	function __construct($graph_db = null)
	{
		if (!$graph_db) return;

		if (empty(Graph_DB_Node_Typed::$dx_auth))
		{
			//echo "Setting dx_auth for node<br>";
			$CI =& get_instance();
			Graph_DB_Node_Typed::$dx_auth =& $CI->dx_auth;

			Graph_DB_Node_Typed::$is_admin = 0;
			if ($CI->dx_auth->is_role("admin"))
			{
				Graph_DB_Node_Typed::$is_admin = 1;
			}
		}

		parent::__construct($graph_db);
	}
	/**********************************************************/
	/**
	* **********************
	* Overloaded basic node functions with extra type loading
	* **********************
	**/

	/**
	 * Overridden create function that also loads in the type object
	 *
	 * @param string $title Nodes title
	 * @param int $type Node type
	 *
	 */
	function create($title, $type, $parent, $guid = false)
	{
		$result = parent::create($title, $type, $parent, $guid);
		$this->type_object = $this->CI->node_types->GetType($type);
		return $result;
	}

	/**
	 * Overridden loadNode function that also loads in the type object
	 *
	 * @param mixed $nodeid ID of the node to be loaded
	 *
	 */
	function loadNode($nodeid)
	{
		//dumpStackHeader("Graph_DB_Node_Typed::loadNode($nodeid)");
		$result = parent::loadNode($nodeid);
		if ($result && !$this->type_object)
		{
			$this->type_object = $this->CI->node_types->GetType($this->type);
		}
		return $result;
	}

	/**
	 * Overridden loadNodeByGuid function that also loads in the type object
	 *
	 * @param string $node_guid GUID of the node to load
         * @param bool $deleted search deleted nodes
	 *
	 */
	function loadNodeByGuid($node_guid, $deleted = true)
	{
		//dumpStackHeader("Graph_DB_Node_Typed::loadNode($nodeid)");
		$result = parent::loadNodeByGuid($node_guid, $deleted);
		if ($result && !$this->type_object)
		{
			$this->type_object = $this->CI->node_types->GetType($this->type);
		}
		return $result;
	}

	/**
	 * Overridden fromRow function that also loads in the type object
	 *
	 * @param object $row Database row from table graph_node
	 *
	 */
	function fromRow($row)
	{
		parent::fromRow($row);
		$this->type_object = $this->CI->node_types->GetType($this->type);
	}

	function addRelate($node, $reltype = GDB_Rel::Relation, $weight = 1)
	{
		// reset loaded relations as the data has changed
		$this->unloadRelate();

		if (gettype($node) != "integer")
		{
			if (gettype($node) == "string")
				$node = (int)$node;
			else
				$node = $node->getId();
		}

		//$node->unloadRelate();

		$recip = GDB_Rel::RecipRel($reltype);

			$data = array(
			'source_id' => $this->id,
			'dest_id' => $node,
			'weight' => $weight,
			'type' => $recip
			);

		//echo "Relate {$this->id} to {$node}<br>";

		$this->queries[] = array('type' => 'replace', 'table' => 'graph_rel', 'data' => $data);

		$this->_addHistory(GDB_Hist_Action::Relation_Add, $node, $reltype, $weight);

		$node_type = $this->GetTypeObj();


		//echo "Single Link : {$node_type->single_dir_link} -> {$node_type->name}<br>";

		$recipok = true;
		if ($node_type && $node_type->single_dir_link)
			$recipok = false;

		if ($recip && $recipok) // only relink back to node if the rel type has a recip type, and the node type doesnt have single_dir_link set
		{
			// add recip relationship
			$data = array(
				'source_id' => $node,
				'dest_id' => $this->id,
				'weight' => $weight,
				'type' => $reltype,
				);

			//echo "Relate {$node} to {$this->id}<br>";

			if (array_key_exists('type', $data))
			{
				$this->queries[] = array('type' => 'replace', 'table' => 'graph_rel', 'data' => $data);
			}

			$this->_addHistoryById($node, GDB_Hist_Action::Relation_Add, $this->getId(), $recip, $weight);
		}
	}

	/**********************************************************/
	/**
	* **********************
	* Typed node info functions
	* **********************
	**/

	/**
	 * Gets the types database id (not really usefull)
	 *
	 * @return int Type objects database id
	 *
	 */
	function GetTypeGDBId()
	{
		return $this->type_object->GetGDBId();
	}

	/**
	 * Gets the types id
	 *
	 * @return int Type objects database id
	 *
	 */
	function GetTypeId()
	{
		return $this->type_object->GetId();
	}

	/**
	 * Return the name of the type of node (string like outcome, or module)
	 *
	 * @return mixed This is the return value description
	 *
	 */
	function GetTypeName()
	{
		if ($this->type_object)
			return $this->type_object->GetName();
		return "Untyped";
	}

	/**
	 * Gets a reference to the node type information object
	 *
	 * @return mixed This is the return value description
	 *
	 */
	function &GetTypeObj()
	{
		return $this->type_object;
	}

	function setAttribute($name, $value, $type = GDB_Attrib_Type::_String)
	{
		$attrobj = $this->type_object->GetAttr($name);
		if (!$attrobj)
		{
			show_error("Trying to setAttribute names '$name', type " .GDB_Attrib_Type::string($type) . ", node type {$this->type_object->id}, attribute doesnt exist");
		}
		$typeid = $attrobj->type;
		//echo "Setting attribute $name, type: " . GDB_Attrib_Type::string($typeid). "<br>";
		return parent::setAttribute($name, $value, $typeid);
	}

	/**
	 * Dump the node, and its associated data as HTML
	 *
	 * @param bool $attr Should the attributes be listed
	 * @param bool $hist Should the history be listed
	 * @param bool $relations Should the relations of the node be listed
	 *
	 */
	function dump($attr = false, $hist = false, $relations = false, $loadifmissing = true)
	{
?>
<style>

</style>
<?php
		echo "<table width='98%' class='node_dump'>";
		echo "<tr class='node' onclick='Debug_Show_Node({$this->id})'><td colspan='4'>({$this->id}) {$this->title}</td><td align='right' width='300' nowrap>{$this->GetTypeName()}</td></tr>";

		if ($attr)
		{
			// dump attributes
			if ($loadifmissing) $this->_loadAttributes();

			if (!$this->attr_loaded)
			{
				echo "<tr class='attribute node_dump_row node_dump_row_{$this->id}' onclick='Debug_Show_Attributes({$this->id})'><th width='100'>Attributes</th><th colspan='4'>Attributes NOT loaded</th></tr>";
			} else {
				echo "<tr class='attribute node_dump_row node_dump_row_{$this->id} node_dump_attribh_{$this->id}' onclick='Debug_Show_Attributes({$this->id})'><th width='100'>Attributes</th><th colspan='4'>" . count($this->attribs) . " attributes loaded</th></tr>";
				echo "<tr class='attribute node_dump_row node_dump_attrib_{$this->id}' onclick='Debug_Hide_Attributes({$this->id})'><th width='100'>Attributes</th><th width='100'>Name</th><th colspan='2'>Value</th><th width='100'>Type</th></tr>";
				$rn = 0;
				foreach ($this->attribs as $attrib)
				{
					$rn = 1 - $rn;
					echo "<tr class='row_$rn node_dump_attrib node_dump_attrib_{$this->id}'><td class='attribute'></td><td>{$attrib->getName()}</td><td colspan='2'>{$attrib->getValue()}</td><td>" . GDB_Attrib_Type::string($attrib->getType()) . "</td></tr>";
				}
			}
		}

		if ($relations)
		{
			if ($loadifmissing) $this->_loadRelations();
			if (!$this->rel_all_loaded)
			{
				echo "<tr class='relations node_dump_row node_dump_row_{$this->id}' onclick='Debug_Show_Attributes({$this->id})'><th width='100'>Relations</th><th colspan='4'>Relations NOT loaded</th></tr>";
			} else {
				echo "<tr class='relations node_dump_row node_dump_row_{$this->id} node_dump_relh_{$this->id}' onclick='Debug_Show_Relations({$this->id})'><th width='100'>Relations</th><th colspan='4'>" . count($this->relations) . " relations loaded</th></tr>";
				echo "<tr class='relations node_dump_rel node_dump_rel_{$this->id}' onclick='Debug_Hide_Relations({$this->id})'><th width='100'>Relations</th><th width='100'>Type</th><th colspan='3'>Node</th></tr>";
				$rn = 0;
				foreach ($this->relations as &$rel)
				{
					$rn = 1 - $rn;
					$relnode = $rel->getNode();
					echo "<tr class='row_$rn node_dump_rel node_dump_rel_{$this->id}'><td class='relations'></td><td>".GDB_Rel::GetReverseName($rel->getType())."</td><td colspan='3'>";
					$title = "{$relnode->getTypeName()} - {$relnode->getTitle()} ({$relnode->getId()})";
					echo anchor("node/{$relnode->getId()}", $title);
					echo "</td></tr>";
				}
			}
			//print_r($rels);
		}
		if ($hist)
		{
			if ($loadifmissing) $this->_loadHistory();
			if (!$this->hist_loaded)
			{
				echo "<tr class='history node_dump_row node_dump_row_{$this->id}' onclick='Debug_Show_Attributes({$this->id})'><th width='100'>History</th><th colspan='4'>History NOT loaded</th></tr>";
			} else {
				echo "<tr class='history node_dump_row node_dump_row_{$this->id} node_dump_histh_{$this->id}' onclick='Debug_Show_Hist({$this->id})'><th width='100'>History</th><th colspan='4'>" . count($this->history) . " history records loaded</th></tr>";
				echo "<tr class='history node_dump_rel node_dump_hist_{$this->id}' onclick='Debug_Hide_Hist({$this->id})'><th width='100'>History</th><th width='100'>Type</th><th colspan='3'>Details</th></tr>";
				$rn = 0;
				foreach ($this->history as &$hist)
				{
					$rn = 1 - $rn;
					echo "<tr class='row_$rn node_dump_hist node_dump_hist_{$this->id}'><td class='history'></td><td>".GDB_Hist_Action::string_nice($hist->action)."</td><td colspan='3'>".GDB_Hist_Action::Describe($hist)."</td></tr>";
				}
			}
		}



		echo "</table>";
	}

	function getCreateTypesForDropdown()
	{
		$result = $this->type_object->getPossibleCreates();
		foreach ($result as $id => $display)
		{
			$nt =& $this->CI->node_types->GetType($id);
			$result[$id] = $nt->getName();
		}

		return $result;
	}


	function getTitleDisp($long = false)
	{
		$prefix = "";

		if ($this->deleted)
			$prefix = "<b>DELETED</b> ";

		if ($long && $this->type_object->longtitle)
		{
			return $prefix . $this->ParseTitle($this->type_object->longtitle);
		}

		if ($this->type_object->title)
		{
			return $prefix . $this->ParseTitle($this->type_object->title);
		}

		if ($this->title)
			return $prefix . $this->title;

		// no title available, so try to use the description instead
		// will only happend if no title or long title attrib is specified in the xml for the node definition
		// Not sure if this is a good idea or not though
		return $prefix . elipsis(strip_tags($this->getAttributeValue("desc")),150);
	}
}
