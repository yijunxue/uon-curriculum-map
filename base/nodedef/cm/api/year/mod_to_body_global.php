<?php

class API_mod_to_body_global extends APIBase
{
  function GetGroupText(&$node)
  {
    $guid = $node->getAttributeValue('group');
    $group_node = $this->graph_db->loadNodeByGuid($guid);

    if ($group_node)
    {
      $group = $group_node->getTitle();
    } else {
      $group = "No Group";
    }

    return $group;
  }

  function Process()
  {
    $this->ci =& get_instance();
    $this->graph_db = $this->ci->graph_db;

    $mode = 'csv';

    $show_title = true;
    $use_perc_weights = (isset($_GET['use_percs']) and $_GET['use_percs'] == 1);
    $render_mode = (isset($_GET['render'])) ? $_GET['render'] : 'count';
    $sum_weights = (!isset($_GET['count_cells']) or $_GET['count_cells'] == 0);

    // $heading = $this->node->getTitleDisp() . ' - Module to Accrediting Body Map';
    // echo ($mode != 'csv') ? '<h1>' . $heading . '</h1>' : "\"$heading\"\n";

    $this->ci->load->helper('study_year');

    $dom = new DOMDocument();
    $courses_el = $dom->createElement('courses');
    $dom->appendChild($courses_el);


    $courses = $this->node->getRelations("course");
    foreach ($courses as $course) {
      $all_modules = array();
      $mods_by_year = get_modules_by_studyyear($course, 'code', $all_modules);

      $bodys = $course->getRelations("body");

      $maxweight = 0;
      $weights = array();

      $course_el = $dom->createElement('course');
      $course_el->setAttribute('id', $course->getID());

      // $title_el = $dom->createElement('title');
      // $text = $dom->createCDATASection($course->getTitle());
      // $title_el->appendChild($text);
      // $course_el->appendChild($title_el);

      // $code_el = $dom->createElement('code', $course->getAttribute('code'));
      // $course_el->appendChild($code_el);

      $courses_el->appendChild($course_el);

	  $mappings = array();
	  $is_mapped = array();

	  // Build an array of body outcomes.
      foreach ($bodys as &$body)
      {
        $title = $body->getTitleDisp();

        $body_outcomes = $body->getRelations("outcome_body");

        if (count($body_outcomes) > 0) {
          // echo ($mode != 'csv') ? "<h2>$title</h2>" : "\"$title\"\n\n";

          $this->graph_db->sort_nodes($body_outcomes, "order|group|title");

          $outcome_count = array_fill(0, count($body_outcomes), 0);
          $outcome_sum = array_fill(0, count($body_outcomes), 0);

          // Need to build data in advance so that we can skip un-mapped body outcomes
          foreach ($all_modules as &$module)
          {
            $is_mod_mapped = array();

            $module_outcomes = $module->getRelations("outcome_module");
            $num_outcomes = count($module_outcomes);

            // Store sum data
            $outcome_idx = 0;

            foreach ($body_outcomes as &$body_out)
            {
              //$mappings[$body_out->getID()] = array();

              $mod_out_weight = 0;
              $mod_out_normalised = 0;

              foreach ($module_outcomes as &$module_out)
              {
                if ($body_out->isRelatedTo($module_out))
                {
                  $is_mapped[$body_out->getID()] = true;
                  $is_mod_mapped[$body_out->getID()] = true;

                  $rel = $body_out->getRel($module_out);
                  $mod_out_weight += $rel->getWeight();

                  if ($sum_weights and $rel->getWeight() > 0)
                  {
                    $outcome_count[$outcome_idx]++;
                  }
                }
              }

              if ($num_outcomes > 0) {
                $mod_out_normalised= round($mod_out_weight / $num_outcomes, 2);
              }

              if (!$sum_weights and isset($is_mod_mapped[$body_out->getID()]) and $is_mod_mapped[$body_out->getID()])
              {
                $outcome_count[$outcome_idx]++;
              }

              $mappings[$module->getID()][$body_out->getID()] = array($mod_out_weight, $mod_out_normalised);
              $outcome_sum[$outcome_idx++] += $mod_out_weight;
            }
          }
        }
	  }

        $curr_year = '------------';

        // Display
        foreach ($mods_by_year as $study_year)
        {
          $study_year_el = $dom->createElement('studyyear');
          $study_year_el->setAttribute('id', $study_year['id']);
          $course_el->appendChild($study_year_el);

          if (count($mods_by_year) > 1 or (count($mods_by_year) == 1 and $study_year['title'] != 'none')) {
            if ($study_year['title'] != $curr_year) {
              $title = ($study_year['title'] == 'none') ? 'No Study Year' : $study_year['title'];
              // echo $title . "\n";
              $curr_year = $study_year['title'];
            }
          }

          foreach ($study_year['modules'] as &$module)
          {
			$moduleid = $module->getID();
            $module_el = $dom->createElement('module');
            $module_el->setAttribute('id', $moduleid);
            $study_year_el->appendChild($module_el);
			if (isset($mappings[$moduleid])) {
				foreach ($mappings[$moduleid] as $bodyid => &$body_out)
				{
				  $mod_out_weight = $body_out[0];
				  $mod_out_normalised = $body_out[1];

				  if ($mod_out_weight > 0)
				  {
					$weight_el = $dom->createElement('mapping');
					$weight_el->setAttribute('outcome_id', $bodyid);
					$weight_el->setAttribute('weight', $mod_out_weight);
					$weight_el->setAttribute('normalised', $mod_out_normalised);
					$module_el->appendChild($weight_el);
				  }
				}
			}
          }
        }
    }

    echo $dom->saveXML($courses_el);
  }
}