<?php

// TODO: needs tidying

class API_module_body_outcome extends APIBase
{
  function GetGroupText(&$node, &$group_node = null)
  {
    $guid = $node->getAttributeValue('group');
    $group_node = $this->graph_db->loadNodeByGuid($guid);

    if ($group_node)
    {
      $group = $group_node->getTitle();
    } else {
      $group = '';
    }

    return $group;
  }

  function Process()
  {
    $this->ci =& get_instance();
    $this->graph_db = $this->ci->graph_db;

    $mode = 'csv';

    $show_title = true;
    $use_perc_weights = (isset($_GET['use_percs']) and $_GET['use_percs'] == 1);
    $render_mode = (isset($_GET['render'])) ? $_GET['render'] : 'count';
    $sum_weights = (!isset($_GET['count_cells']) or $_GET['count_cells'] == 0);

    // $heading = $this->node->getTitleDisp() . ' - Module to Accrediting Body Map';
    // echo ($mode != 'csv') ? '<h1>' . $heading . '</h1>' : "\"$heading\"\n";

    $dom = new DOMDocument();
    $module_el = $dom->createElement('module');
    $module_el->setAttribute('id', $this->node->getID());
    $dom->appendChild($module_el);

    $title_el = $dom->createElement('title');
    $text = $dom->createCDATASection($this->node->getTitle());
    $title_el->appendChild($text);
    $module_el->appendChild($title_el);

    $code_el = $dom->createElement('code', $this->node->getAttributeValue('code'));
    $module_el->appendChild($code_el);

    $desc_el = $dom->createElement('desc');
    $text = $dom->createCDATASection($this->node->getAttributeValue('desc'));
    $desc_el->appendChild($text);
    $module_el->appendChild($desc_el);

    $bodys = array();

    $courses = $this->node->getRelations("course");
    foreach ($courses as $course)
    {
      $tmp_bodies = $course->getRelations("body");
      foreach ($tmp_bodies as $body)
      {
        $bodys[$body->getID()] = $body;
      }
    }

    foreach ($bodys as $bid => $body)
    {
      $body_el = $dom->createElement('body');
      $body_el->setAttribute('id', $body->getID());
      $module_el->appendChild($body_el);

      $title_el = $dom->createElement('title');
      $text = $dom->createCDATASection($body->getTitle());
      $title_el->appendChild($text);
      $body_el->appendChild($title_el);

      $desc_el = $dom->createElement('desc');
      $text = $dom->createCDATASection($body->getAttributeValue('desc'));
      $desc_el->appendChild($text);
      $body_el->appendChild($desc_el);

      $body_outcomes = $body->getRelations("outcome_body");

      if (count($body_outcomes) > 0)
      {
        $curr_group = '--------------------';

        $body_outcomes_el = $dom->createElement('outcomes_body');
        $body_el->appendChild($body_outcomes_el);

        foreach ($body_outcomes as $body_out)
        {
          $group_node = null;
          $group = $this->GetGroupText($body_out, $group_node);
          if ($group != $curr_group)
          {
            $group_el = $dom->createElement('group');
            if ($group == '')
            {
              $group_id = 'none';
              $group_desc = '';
              $guid = '';
            } else {
              $group_id = $group_node->getID();
              $group_desc = $group_node->getAttributeValue('desc');
              $guid = $group_node->getGuid();
            }
            $group_el->setAttribute('id', $group_id);
            $group_el->setAttribute('guid', $guid);
            $body_outcomes_el->appendChild($group_el);

            $g_title_el = $dom->createElement('group_title');
            $text = $dom->createCDATASection($group);
            $g_title_el->appendChild($text);
            $g_title_el->setAttribute('status', 'deprecated');
            $group_el->appendChild($g_title_el);

            $title_el = $dom->createElement('title');
            $text = $dom->createCDATASection($group);
            $title_el->appendChild($text);
            $group_el->appendChild($title_el);

            $g_desc_el = $dom->createElement('group_desc');
            $text = $dom->createCDATASection($group_desc);
            $g_desc_el->appendChild($text);
            $g_desc_el->setAttribute('status', 'deprecated');
            $group_el->appendChild($g_desc_el);

            $desc_el = $dom->createElement('desc');
            $text = $dom->createCDATASection($group_desc);
            $desc_el->appendChild($text);
            $group_el->appendChild($desc_el);

            $curr_group = $group;
          }

          $outcome_el = $dom->createElement('outcome_body');
          $outcome_el->setAttribute('id', $body_out->getID());
          $group_el->appendChild($outcome_el);

          $title_el = $dom->createElement('title');
          $text = $dom->createCDATASection($body_out->getTitle());
          $title_el->appendChild($text);
          $outcome_el->appendChild($title_el);

          $desc_el = $dom->createElement('desc');
          $text = $dom->createCDATASection($body_out->getAttributeValue('desc'));
          $desc_el->appendChild($text);
          $outcome_el->appendChild($desc_el);
        }
      }
    }

    $mod_outs = $this->node->getRelations("outcome_module");

    if (count($mod_outs) > 0)
    {
      $mod_outcomes_el = $dom->createElement('outcomes_module');
      $module_el->appendChild($mod_outcomes_el);

      $this->graph_db->sort_nodes($mod_outs, "order|group|title");

      $curr_group = '--------------------';

      foreach ($mod_outs as $mod_out)
      {
        $group_node = null;
        $group = $this->GetGroupText($mod_out, $group_node);
        if ($group != $curr_group)
        {
          $group_el = $dom->createElement('group');
          if ($group == '')
          {
            $group_id = 'none';
            $group_desc = '';
            $guid = '';
          } else {
            $group_id = $group_node->getID();
            $group_desc = $group_node->getAttributeValue('desc');
            $guid = $group_node->getGuid();
          }
          $group_el->setAttribute('id', $group_id);
          $group_el->setAttribute('guid', $guid);
          $mod_outcomes_el->appendChild($group_el);

          $g_title_el = $dom->createElement('group_title');
          $text = $dom->createCDATASection($group);
          $g_title_el->appendChild($text);
          $g_desc_el->setAttribute('status', 'deprecated');
          $group_el->appendChild($g_title_el);

          $title_el = $dom->createElement('title');
          $text = $dom->createCDATASection($group);
          $title_el->appendChild($text);
          $group_el->appendChild($title_el);

          $g_desc_el = $dom->createElement('group_desc');
          $text = $dom->createCDATASection($group_desc);
          $g_desc_el->appendChild($text);
          $g_desc_el->setAttribute('status', 'deprecated');
          $group_el->appendChild($g_desc_el);

          $desc_el = $dom->createElement('desc');
          $text = $dom->createCDATASection($group_desc);
          $desc_el->appendChild($text);
          $group_el->appendChild($desc_el);

          $curr_group = $group;
        }

        $outcome_el = $dom->createElement('outcome_module');
        $outcome_el->setAttribute('id', $mod_out->getID());
        $group_el->appendChild($outcome_el);

        $title_el = $dom->createElement('title');
        $text = $dom->createCDATASection($mod_out->getTitle());
        $title_el->appendChild($text);
        $outcome_el->appendChild($title_el);

        $desc_el = $dom->createElement('desc');
        $text = $dom->createCDATASection($mod_out->getAttributeValue('desc'));
        $desc_el->appendChild($text);
        $outcome_el->appendChild($desc_el);

        $body_rels = $mod_out->getRelations("outcome_body");

        if (count($body_rels) > 0)
        {
          $outcomes_el = $dom->createElement('outcomes_body');
          $outcome_el->appendChild($outcomes_el);

          foreach ($body_rels as $body_out)
          {
            $weight = $body_out->getWeightTo($mod_out)  * 100;
            $body_outcome_el = $dom->createElement('outcome_body');
            $body_outcome_el->setAttribute('id', $body_out->getID());
            $body_outcome_el->setAttribute('weight', $weight);
            $body_outcome_el->setAttribute('weight_dest', $mod_out->getID());
            $outcomes_el->appendChild($body_outcome_el);
          }
        }
      }
    }

    echo $dom->saveXML($module_el);
  }
}