<?php

class Report_body_act_timings extends ReportBase
{
	function GetGroupText(&$node)
	{
		$guid = $node->getAttributeValue('group');
		$group_node = $this->graph_db->loadNodeByGuid($guid);

		if ($group_node)
		{
			$group = $group_node->getTitle();
		} else {
			$group = "No Group";
		}

		return $group;
	}

	function Process()
	{
		$this->ci =& get_instance();
		$this->graph_db = $this->ci->graph_db;

		$mode = $this->ci->uri->segment(2);
		$body_id = $this->ci->uri->segment(5);

		$show_unmapped = (isset($_GET['only_mapped'])) ? $_GET['only_mapped'] : 'show';
		$show_title = (!isset($_GET['only_code']) or $_GET['only_code'] == 0);
		if ($mode != 'csv') {
?>
<style>
.heatmap, .heatmap td {
	border: 1px solid #aaaaaa;
	border-collapse: collapse;
}
th.unmapped {
	background-color: #f55;
}
.ui-tooltip, .qtip {
	max-width: 260px;
}
.ui-tooltip-content {
	color: #fff !important;
	background-color: #333 !important;
	padding: 5px;
}
</style>
<?php
		}

		$this->ci->load->helper('node_heading');

		$heading = 'Study Time by Accrediting Body Outcome (mins.)';
		echo ($mode != 'csv') ? '<h1>' . $heading . '</h1>' : "\"$heading\"\n";

		$this->ci->load->helper('study_year');

		$all_modules = array();
		$mods_by_year = get_modules_by_studyyear($this->node, 'code', $all_modules);

		if ($body_id === false) {
			$bodys = $this->node->getRelations("body");
		} else {
			$bodys = array($this->ci->graph_db->loadNode($body_id));
		}

		$maxtime = 0;
		$weights = array();

		// Need to build data in advance so that we can skip un-mapped body outcomes
		$act_type_times = array();

		foreach ($bodys as &$body)
		{
			$title = $body->getTitleDisp();

			$body_outcomes = $body->getRelations("outcome_body");

			if (count($body_outcomes) > 0)
			{
				echo ($mode != 'csv') ? "<h2>$title</h2>" : "\"$title\"\n\n";

				$this->graph_db->sort_nodes($body_outcomes, "order|group|title");

				$seen_acts = array() ;

				foreach ($all_modules as &$module)
				{
					$learning_acts = $module->getRelations("learning_act");

					foreach ($learning_acts as $learning_act)
					{
						$act_id = $learning_act->getID();

						if (!in_array($act_id, $seen_acts))
						{
							$seen_acts[] = $act_id;

							$act_type = $learning_act->getAttributeValue('act_type');
							if ($act_type == '') $act_type = 'Undefined';
							if (!isset($act_type_times[$act_type])) $act_type_times[$act_type] = array();

							$act_duration = $learning_act->getAttributeValue('duration');
							if ($act_duration != '')
							{
								$act_outcomes = $learning_act->getRelations("outcome_learning_act");
								$act_count = count($act_outcomes);

								foreach ($body_outcomes as &$body_out)
								{
									$bo_id = $body_out->getID();

									foreach ($act_outcomes as &$act_out)
									{
										$ao_id = $act_out->getID();


										if ($body_out->isRelatedTo($act_out))
										{
											if (isset($map_counts[$ao_id]))
											{
												$map_counts[$ao_id]++;
											} else {
												$map_counts[$ao_id] = 1;
											}
										}
									}
								}

								foreach ($act_outcomes as &$act_out)
								{
									$ao_id = $act_out->getID();

									foreach ($body_outcomes as &$body_out)
									{
										$bo_id = $body_out->getID();

										if ($body_out->isRelatedTo($act_out))
										{
											$is_mapped[$bo_id] = true;
											$is_act_mapped[$bo_id] = true;

											if (isset($map_counts[$ao_id]))
											{
												if (isset($act_type_times[$act_type][$bo_id]))
												{
													$act_type_times[$act_type][$bo_id] += $act_duration / $act_count / $map_counts[$ao_id];
												} else {
													$act_type_times[$act_type][$bo_id] = $act_duration / $act_count / $map_counts[$ao_id];
												}
											}
										}
									}
								}
							}
						}
					}
				}

				$curgroup = '------------';
				if ($mode != 'csv')
				{
					echo '<table class="acc-body niceround grid" style="background-color: #fff">' . "\n";
					echo '<tr><td class="nogrid"></td>';

					// output initial group heading
					$span = 1;

					foreach ($body_outcomes as &$body_out)
					{
						if ($show_unmapped != 'hide' or isset($is_mapped[$body_out->getID()]))
						{
							$group = $this->GetGroupText($body_out);
							if ($group != $curgroup)
							{
								if ($curgroup == '------------') {
									$curgroup = $group;
								} else {
									echo "<td colspan=\"$span\" class=\"nogrid\"><b>" . $curgroup . "</b></td>";
								}
								$span = 1;
							} else {
								$span ++;
							}

							$curgroup = $group;
						}
					}
					echo "<td colspan=\"$span\" class=\"nogrid\"><b>" . $curgroup . "</b></td>";
					echo "</tr>\n";

					echo "<tr><td class=\"nogrid\">&nbsp;</td>";
					foreach ($body_outcomes as &$body_out)
					{
						if ($show_unmapped != 'hide' or isset($is_mapped[$body_out->getID()]))
						{
							$classes = array();
							if ($show_unmapped == 'highlight' and !isset($is_mapped[$body_out->getID()]))
							{
								$classes[] = 'unmapped';
							}

							$desc = $body_out->getAttribute('desc');
							$desc_mod = '';
							if ($desc != '')
							{
								$desc_mod = ' title="' . $desc . '"';
								$classes[] = 'tooltip';
							}

							$class_mod = '';
							if (count($classes) > 0)
							{
								$class_mod = ' class="' . implode(' ', $classes) . '"';
							}
							echo "<th{$class_mod}{$desc_mod}>" . GetNodeHeadingText($body_out) . "</th>";
						}
					}
					echo "</tr>\n";

					// Display
					$full_row_span = count($body_outcomes) + 1;
					foreach ($act_type_times as $title => $times)
					{
						echo "<tr><th>{$title}</th>";

						foreach ($body_outcomes as &$body_out)
						{
							if ($show_unmapped != 'hide' or isset($is_mapped[$body_out->getID()])) {
								$time = (isset($times[$body_out->getID()])) ? $times[$body_out->getID()] : 0;
								$time = round($time);

								if ($time > 0)
								{
									if ($time > $maxtime)
										$maxtime = $time;

									$weights["$time"] = true;

									$class_mod = ' class="heatmap_' . str_replace('.', '_', $time) . '"';

									echo "<td{$class_mod} align=\"center\">";
									echo $time;
									echo "</td>";
								} else {
									echo '<td>&nbsp;</td>';
								}
							}
						}
						echo "</tr>\n";
					}

					echo "<tr><td colspan=\"$full_row_span\" class=\"nogrid\">&nbsp;</td></tr>\n";
					echo "</table>";

				} else {
					// Generate CSV output

					// output initial group heading
					$span = 1;

					echo ',';
					foreach ($body_outcomes as &$body_out)
					{
						if ($show_unmapped != 'hide' or isset($is_mapped[$body_out->getID()]))
						{
							$group = $this->GetGroupText($body_out);
							if ($group != $curgroup)
							{
								if ($curgroup == "------------") {
									$curgroup = $group;
								} else {
									echo "\"$curgroup\"" . str_repeat(',', $span);
								}
								$span = 1;
							} else {
								$span ++;
							}

							$curgroup = $group;
						}
					}
					echo "\"$curgroup\"\n";

					echo ',';
					foreach ($body_outcomes as &$body_out)
					{
						if ($show_unmapped != 'hide' or isset($is_mapped[$body_out->getID()])) {
							echo "\"" . GetNodeHeadingText($body_out) . "\",";
						}
					}
					echo "\n";

					// Display
					$full_row_span = count($body_outcomes) + 1;
					foreach ($act_type_times as $title => $times)
					{
						echo "\"{$title}\",";

						foreach ($body_outcomes as &$body_out)
						{
							if ($show_unmapped != 'hide' or isset($is_mapped[$body_out->getID()])) {
								$time = (isset($times[$body_out->getID()])) ? $times[$body_out->getID()] : 0;
								$time = round($time);

								if ($time > 0)
								{
									if ($time > $maxtime)
										$maxtime = $time;

									$weights["$time"] = true;


									echo $time;
									echo ",";
								} else {
									echo ',';
								}
							}
						}
						echo "\n";
					}
					echo "\n";
				}
			}
		}

		if ($mode != 'csv') {
			// output additional style information to highlight the required colours
			if ($maxtime > 0) {
				echo "<style type=\"text/css\">";
				$step = 1 / $maxtime;
				foreach (array_keys($weights) as $i)
				{
					$red = 247;
					$green = floor(210 - ($step * 100 * $i));
					$blue = floor(180 - ($step * 180 * $i));

					$class_mod = str_replace('.', '_', $i);
					echo ".heatmap_$class_mod { background-color: rgb($red,$green,$blue) }\n";
				}
			}

			echo "</style>";
?>
<script>
function showPopup(url)
{
	TINY.box.show({ iframe: url, width: 400, height: 250 });
}
$('.tooltip').qtip({
	position: {
		viewport: true,
		my: 'bottom center',
		at: 'top center'
	},
	show: {
		event: 'click'
	}
});
</script>

<?php
		}
	}
}