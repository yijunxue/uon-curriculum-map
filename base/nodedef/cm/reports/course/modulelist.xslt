<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:output method="xml" indent="yes"/>
  <xsl:template match="/cmapi/course">
    <div>
      <h1>
        <xsl:value-of select="/cmapi/course/title"/>
      </h1>
      <ul>
        <xsl:for-each select="/cmapi/course/module">
          <li>
            <xsl:value-of select="code"/>: <xsl:value-of select="title"/>
          </li>
        </xsl:for-each>
      </ul>
    </div>
  </xsl:template>
</xsl:stylesheet>