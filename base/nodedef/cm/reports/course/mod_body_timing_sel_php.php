<?php

class Report_mod_body_timing_sel_php extends ReportBase
{
	function Process()
	{
    $ci = $this->node->CI;
    $ci->load->helper('url');

    $dataset = $ci->graph_db->active_dataset->table;
    $base_uri = $ci->config->item('root_url') . $dataset . '/' . uri_string();

    if (isset($_REQUEST['dest_body'])) {
			// We have a post. Process the form and redirect to the report
			$url_parts = explode('/', $base_uri);
			array_pop($url_parts);
			$url = implode('/', $url_parts) . '/' . 'mod_body_timings';

			$have_query_string = false;

			if (isset($_REQUEST['dest_body']) and $_REQUEST['dest_body'] != '') {
				$url .= '/' . $_REQUEST['dest_body'];
			}

			header('Location: ' . $url);
		} else {
			echo "<h1>".$this->node->getTitleDisp()." - Select bodies to map</h1>";

			$data['bodies'] = $this->node->getRelations("body");
      		$data['form_url'] = $base_uri;

			$ci->load->view('reports/course/mod_body_timing_sel', $data);
		}
	}
}