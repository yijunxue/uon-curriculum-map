<?php

class Report_mod_body_map_popup extends ReportBase
{
	function Process()
	{
		$this->ci =& get_instance();
		$mod_id = $this->ci->uri->segment(5);
		$body_out_id = $this->ci->uri->segment(6);
		if ($mod_id < 1 || $body_out_id < 1)
		{
			echo "This report cannot be run directly";
			return;
		}

		$data['title'] = "Mapped Objectives";
		$this->ci->load->view('templates/popup/header', $data);
		$this->ci->load->helper('view');

		$module = $this->ci->graph_db->loadNode($mod_id);
		$body_out = $this->ci->graph_db->loadNode($body_out_id);

		$module_outcomes = $module->getRelations("outcome_module");
		$this->ci->graph_db->sort_nodes($module_outcomes, "order|title");

		echo '<ul class="has_weight">';

		// TODO: add weight
		foreach ($module_outcomes as &$module_out)
		{
			if ($module_out->isRelatedTo($body_out)) {
				$rel = $module_out->getRel($body_out);
				echo "<li>";
				$wi = round($rel->getWeight() * 100,0) ."% to " . strip_tags($body_out->getTitleDisp(true));
				echo '<span class="hasTooltip" title="'. htmlentities(strip_tags($wi)) .'">' . WeightImage($rel->getWeight()) . '</span>';
				echo "<a href='" . site_url('view/' . $module->getId())."' target='_blank'>";
				echo $module_out->getTitleDisp(true);

				echo "</a>";
				echo "</li>";
			}
		}

		echo "</ul>";

?>
<style type="text/css">
h2 {
  font-size: 100%;
  display: inline;
}
li p {
  display: inline;
}
</style>
<?php

		$this->ci->load->view('templates/popup/footer', $data);
	}
}
