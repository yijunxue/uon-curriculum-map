<?php

class Report_mod_to_course extends ReportBase
{
	function GetGroupText(&$node)
	{
		$guid = $node->getAttributeValue('group');
		$group_node = $this->graph_db->loadNodeByGuid($guid);

		if ($group_node)
		{
			$group = $group_node->getTitle();
		} else {
			$group = "No Group";
		}

		return $group;
	}

	function Process()
	{
		$this->ci =& get_instance();
		$this->graph_db = $this->ci->graph_db;

		$mode = $this->ci->uri->segment(2);

		$show_unmapped = (isset($_GET['only_mapped'])) ? $_GET['only_mapped'] : 'show';
		$show_title = (!isset($_GET['only_code']) or $_GET['only_code'] == 0);
		$use_perc_weights = (isset($_GET['use_percs']) and $_GET['use_percs'] == 1);
		$render_mode = (isset($_GET['render'])) ? $_GET['render'] : 'count';
		$sum_weights = (!isset($_GET['count_cells']) or $_GET['count_cells'] == 0);
		if ($mode != 'csv') {
?>
<style>
.heatmap, .heatmap td {
	border: 1px solid #aaaaaa;
	border-collapse: collapse;
}
th.unmapped {
	background-color: #f55;
}
.ui-tooltip, .qtip {
	max-width: 260px;
}
.ui-tooltip-content {
	color: #fff !important;
	background-color: #333 !important;
	padding: 5px;
}
</style>
<?php
		}

		$this->ci->load->helper('node_heading');

		$heading = $this->node->getTitleDisp() . ' - Module to Programme Map';
		echo ($mode != 'csv') ? '<h1>' . $heading . '</h1>' : "\"$heading\"\n";

		$this->ci->load->helper('study_year');

		$all_modules = array();
		$mods_by_year = get_modules_by_studyyear($this->node, 'code', $all_modules);

		$maxweight = 0;
		$weights = array();

		$mappings = array();
		$is_mapped = array();
		$is_course_mapped = false;

		$title = $this->node->getTitleDisp();

		$course_outcomes = $this->node->getRelations("outcome_course");

		echo ($mode != 'csv') ? "<h2>$title</h2>" : "\"$title\"\n\n";

		$this->graph_db->sort_nodes($course_outcomes, "order|group|title");

		if (count($course_outcomes) > 0) {
			$outcome_count = array_fill(0, count($course_outcomes), 0);
			$outcome_sum = array_fill(0, count($course_outcomes), 0);

			// Need to build data in advance so that we can skip un-mapped course outcomes
			foreach ($all_modules as &$module)
			{
	      $is_mod_mapped = array();

				$module_outcomes = $module->getRelations("outcome_module");
				$num_outcomes = count($module_outcomes);

				$module_weight = $module->getAttributeValue('credits');
	      $module_weight = ($module_weight == '') ? 1 : $module_weight/10;

				// Store sum data
				$outcome_idx = 0;

				foreach ($course_outcomes as &$course_out)
				{
					$mappings[$course_out->getID()] = array();

					$mod_out_weight = 0;

					foreach ($module_outcomes as &$module_out)
					{
						if ($course_out->isRelatedTo($module_out))
						{
							$is_mapped[$course_out->getID()] = true;
	            $is_mod_mapped[$course_out->getID()] = true;
							$is_course_mapped = true;

							$rel = $course_out->getRel($module_out);
							$mod_out_weight += $module_weight * $rel->getWeight();

	            if ($sum_weights and $rel->getWeight() > 0)
	          	{
	          		$outcome_count[$outcome_idx]++;
	          	}
						}
					}

					if ($use_perc_weights) {
						if (count($module_outcomes) > 0) {
							$mod_out_weight = round($mod_out_weight / $num_outcomes, 2);
						} else {
							$mod_out_weight = 0;
						}
					}

					if (!$sum_weights and isset($is_mod_mapped[$course_out->getID()]) and $is_mod_mapped[$course_out->getID()])
					{
						$outcome_count[$outcome_idx]++;
					}

					$mappings[$module->getID()][$course_out->getID()] = $mod_out_weight;
					$outcome_sum[$outcome_idx++] += $mod_out_weight;
				}
			}

			$curr_year = '------------';
			$curgroup = '------------';
			$outcome_idx = 0;
			if ($is_course_mapped)
			{
				if ($mode != 'csv') {
					echo '<table class="acc-body niceround grid" style="background-color: #fff">' . "\n";
					echo '<tr><td class="nogrid"></td>';

					// output initial group heading
					$span = 1;

					foreach ($course_outcomes as &$course_out)
					{
						if ($show_unmapped != 'hide' or isset($is_mapped[$course_out->getID()]))
						{
							$group = $this->GetGroupText($course_out);
							if ($group != $curgroup)
							{
								if ($curgroup == '------------') {
									$curgroup = $group;
								} else {
									echo "<td colspan=\"$span\" class=\"nogrid\"><b>" . $curgroup . "</b></td>";
								}
								$span = 1;
							} else {
								$span ++;
							}

							$curgroup = $group;
						} else {
							// Remove the sum data
							unset($outcome_count[$outcome_idx], $outcome_sum[$outcome_idx]);
						}
						$outcome_idx++;
					}
					echo "<td colspan=\"$span\" class=\"nogrid\"><b>" . $curgroup . "</b></td>";
					echo "</tr>\n";

					echo "<tr><td class=\"nogrid\">&nbsp;</td>";
					foreach ($course_outcomes as &$course_out)
					{
						if ($show_unmapped != 'hide' or isset($is_mapped[$course_out->getID()]))
						{
							$classes = array();
							if ($show_unmapped == 'highlight' and !isset($is_mapped[$course_out->getID()]))
							{
								$classes[] = 'unmapped';
							}

							$desc = $course_out->getAttribute('desc');
							$desc_mod = '';
							if ($desc != '')
							{
								$desc_mod = ' title="' . $desc . '"';
								$classes[] = 'tooltip';
							}

		          $class_mod = '';
							if (count($classes) > 0)
							{
								$class_mod = ' class="' . implode(' ', $classes) . '"';
							}
							echo "<th{$class_mod}{$desc_mod}>" . GetNodeHeadingText($course_out) . "</th>";
						}
					}
					echo "</tr>\n";

					// Display
					$full_row_span = count($course_outcomes) + 1;
					foreach ($mods_by_year as $study_year)
					{
						if (count($mods_by_year) > 1 or (count($mods_by_year) == 1 and $study_year['title'] != 'none')) {
							if ($study_year['title'] != $curr_year)
							{
								// TODO: should this use GetNodeHeadingText?
								$title = ($study_year['title'] == 'none') ? 'No Study Year' : $study_year['title'];
								echo "<tr><td colspan=\"$full_row_span\" class=\"nogrid\"><b>" . $title . "</b></td></tr>\n";
		            $curr_year = $study_year['title'];
							}
						}

						foreach ($study_year['modules'] as &$module)
						{
							echo "<tr><th>" . GetNodeHeadingText($module, $show_title) . "</th>";

							foreach ($course_outcomes as &$course_out)
							{
		            if ($show_unmapped != 'hide' or isset($is_mapped[$course_out->getID()])) {
									$mod_out_weight = $mappings[$module->getID()][$course_out->getID()];

									if ($mod_out_weight > 0)
									{
										$weights["$mod_out_weight"] = true;

										if ($mod_out_weight > $maxweight)
											$maxweight = $mod_out_weight;

										$class_mod = ($render_mode == 'count') ? ' class="heatmap_' . str_replace('.', '_', $mod_out_weight) . '"': '';

										$url = site_url("report/raw/" . $this->node->getID() . "/mod_body_map_popup/" . $module->getId() . "/" . $course_out->getID());

										echo "<td{$class_mod} align=\"center\" onclick=\"showPopup('$url');\" style=\"cursor: pointer\">";
										if ($render_mode == 'count')
										{
											echo $mod_out_weight;
										} else {
											echo '<img src="' . asset_url() . 'image/misc/tick.png" alt="Tick" width="16" height="16" />';
										}
										echo "</td>";
									} else {
										echo '<td>&nbsp;</td>';
									}
								}
							}
							echo "</tr>\n";
						}
					}

					echo "<tr><td colspan=\"$full_row_span\" class=\"nogrid\">&nbsp;</td></tr>\n";
					echo "<tr><td colspan=\"$full_row_span\" class=\"nogrid\"><b>Totals</b></td></tr>\n";

					echo '<tr><td>Count</td>';
					foreach ($outcome_count as $count)
					{
						echo "<td>";
						echo ($count > 0) ? $count : '&nbsp;';
						echo "</td>";
					}
					echo "</tr>\n";

					echo '<tr><td>Sum</td>';
					foreach ($outcome_sum as $sum)
					{
						echo "<td>";
						echo ($sum > 0) ? $sum : '&nbsp;';
						echo "</td>";
					}
					echo "</tr>\n";

					echo "</table>";

				} else {
					// Generate CSV output

					// output initial group heading
					$span = 1;

					echo ',';
					foreach ($course_outcomes as &$course_out)
					{
		        if ($show_unmapped != 'hide' or isset($is_mapped[$course_out->getID()]))
		        {
							$group = $this->GetGroupText($course_out);
							if ($group != $curgroup)
							{
								if ($curgroup == "------------") {
									$curgroup = $group;
								} else {
									echo "\"$curgroup\"" . str_repeat(',', $span);
								}
								$span = 1;
							} else {
								$span ++;
							}

							$curgroup = $group;
						} else {
							// Remove the sum data
							unset($outcome_count[$outcome_idx], $outcome_sum[$outcome_idx]);
						}
						$outcome_idx++;
					}
					echo "\"$curgroup\"\n";

					echo ',';
					foreach ($course_outcomes as &$course_out)
					{
		        if ($show_unmapped != 'hide' or isset($is_mapped[$course_out->getID()])) {
							echo "\"" . GetNodeHeadingText($course_out) . "\",";
						}
					}
					echo "\n";

					// Display
					foreach ($mods_by_year as $study_year) {
						if (count($mods_by_year) > 1 or (count($mods_by_year) == 1 and $study_year['title'] != 'none')) {
							if ($study_year['title'] != $curr_year) {
								// TODO: should this use GetNodeHeadingText?
								$title = ($study_year['title'] == 'none') ? 'No Study Year' : $study_year['title'];
								echo $title . "\n";
		            $curr_year = $study_year['title'];
							}
						}

						foreach ($study_year['modules'] as &$module)
						{
							echo "\"" . GetNodeHeadingText($module, $show_title) . "\",";

							foreach ($course_outcomes as &$course_out)
							{
		            if ($show_unmapped != 'hide' or isset($is_mapped[$course_out->getID()])) {
									$mod_out_weight = $mappings[$module->getID()][$course_out->getID()];

									if ($mod_out_weight > 0)
									{
										$weights["$mod_out_weight"] = true;

										if ($mod_out_weight > $maxweight)
											$maxweight = $mod_out_weight;

										if ($render_mode == 'count')
										{
											echo "$mod_out_weight,";
										} else {
											echo '1,';
										}
									} else {
										echo ',';
									}
								}
							}
							echo "\n";
						}
					}
					echo "\nTotals\n";

					echo 'Count';
					foreach ($outcome_count as $count)
					{
						echo ",";
						if ($count > 0) echo $count;
					}
					echo "\n";

					echo 'Sum';
					foreach ($outcome_sum as $sum)
					{
						echo ",";
						if ($sum > 0) echo $sum;
					}
					echo "\n";
				}
			} else {
				if ($mode != 'csv')
				{
					echo "<p>No module to course mappings found</p>";
				} else {
					echo "No module to course mappings found";
				}
			}
		} else {
			echo "No Programme outcomes defined\n";
		}

		if ($mode != 'csv') {
			// output additional style information to highlight the required colours
			if ($maxweight > 0) {
				echo "<style type=\"text/css\">";
				$step = 1 / $maxweight;
				foreach (array_keys($weights) as $i)
				{
					$red = 247;
					$green = floor(210 - ($step * 100 * $i));
					$blue = floor(180 - ($step * 180 * $i));

					$class_mod = str_replace('.', '_', $i);
					echo ".heatmap_$class_mod { background-color: rgb($red,$green,$blue) }\n";
				}
			}

			echo "</style>";
?>
<script>
function showPopup(url)
{
	TINY.box.show({ iframe: url, width: 400, height: 250 });
}
$('.tooltip').qtip({
	position: {
		viewport: true,
		my: 'bottom center',
		at: 'top center'
	},
	show: {
		event: 'click'
	}
});
</script>

<?php
		}
	}
}
