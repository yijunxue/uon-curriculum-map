<?php

class Report_body_map_php extends ReportBase
{
	function Process()	
	{
		echo "<h1>".$this->node->getTitleDisp()." - Body Outcome Map</h1>";
		
		$course_outcomes = $this->node->getRelations("outcome_course");
		
		$bodys = $this->node->getRelations("body");
		foreach ($bodys as &$body)
		{
			echo "<h2>". $body->getTitleDisp() . "</h2>";
			
			$body_outcomes = $body->getRelations("outcome_body");
			
			echo "<table>";
			echo "<tr><td></td>";
			foreach ($body_outcomes as &$body_out)
			{
				echo "<td>" . $body_out->getTitle() . "</td>";
			}
			echo "</tr>";
			
			foreach ($course_outcomes as &$course_out)
			{
				echo "<tr><td>" . $course_out->getTitle() . "</td>";
				foreach ($body_outcomes as &$body_out)
				{
					echo "<td>";
					if ($body_out->isRelatedTo($course_out))
					{
						echo "<img src='".asset_url()."image/clone/tick.png' />";
					} else {
						echo "<img src='".asset_url()."image/clone/cross.png' />";
					}
					echo "</td>";
				}	
				echo "</tr>";
			}
			echo "</table>";
			
		}
		
	}
}