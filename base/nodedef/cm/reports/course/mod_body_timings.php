<?php

class Report_mod_body_timings extends ReportBase
{
	// Hardcoding alert - thus SHOULD stay consistent between academic years if cloned properly
	const EAEVE_GUID = 'de130092-02ef-47a0-9677-7a8a1cf25f7a';

	function GetGroupText(&$node)
	{
		$guid = $node->getAttributeValue('group');
		$group_node = $this->graph_db->loadNodeByGuid($guid);

		if ($group_node)
		{
			$group = $group_node->getTitle();
		} else {
			$group = "No Group";
		}

		return $group;
	}

	function Process()
	{
		$this->ci =& get_instance();
		$this->graph_db = $this->ci->graph_db;

		$mode = $this->ci->uri->segment(2);
		$body_id = $this->ci->uri->segment(5);

		if ($mode != 'csv') {
?>
<style>
.heatmap, .heatmap td {
	border: 1px solid #aaaaaa;
	border-collapse: collapse;
}
th.unmapped {
	background-color: #f55;
}
.ui-tooltip, .qtip {
	max-width: 260px;
}
.ui-tooltip-content {
	color: #fff !important;
	background-color: #333 !important;
	padding: 5px;
}
</style>
<?php
		}

		$this->ci->load->helper('node_heading');

		$heading = 'Study Time by Module and Accrediting Body Outcomes (mins.)';
		echo ($mode != 'csv') ? '<h1>' . $heading . '</h1>' : "\"$heading\"\n";

		$this->ci->load->helper('study_year');

		$all_modules = $this->node->getRelations('module');
		$this->graph_db->sort_nodes($all_modules, 'code');

		if ($body_id === false) {
			$body = $this->ci->graph_db->loadNodeByGuid(self::EAEVE_GUID);
		} else {
			$body = $this->ci->graph_db->loadNode($body_id);
		}

		$maxtime = 0;
		$weights = array();

		// Need to build data in advance so that we can skip un-mapped body outcomes
		$act_type_times = array();

		$title = $body->getTitleDisp();

		$body_outcomes = $body->getRelations("outcome_body");

		$act_types = array();
		$mod_titles = array();
		$all_outcomes = array();

		if (count($body_outcomes) > 0)
		{
			echo ($mode != 'csv') ? "<h2>$title</h2>" : "\"$title\"\n\n";

			$this->graph_db->sort_nodes($body_outcomes, "order|title");

			$seen_acts = array() ;

			foreach ($all_modules as &$module)
			{
				$mod_id = $module->getID();
				$mod_titles[$mod_id] = $module->getAttribute('code');

				foreach ($body_outcomes as &$body_out)
				{
					$bo_id = $body_out->getID();
					$act_type_times[$mod_id][$bo_id] = array();
				}

				$learning_acts = $module->getRelations("learning_act");
				$this->graph_db->sort_nodes($learning_acts, "order|title");

				foreach ($learning_acts as $learning_act)
				{
					$act_id = $learning_act->getID();

					if (!in_array($act_id, $seen_acts))
					{
						$seen_acts[] = $act_id;

						$act_type = $learning_act->getAttributeValue('act_type');
						if ($act_type == '') $act_type = 'Undefined';

						$act_types[$act_type] = true;

						$act_duration = $learning_act->getAttributeValue('duration');
						if ($act_duration != '')
						{
							$act_outcomes = $learning_act->getRelations("outcome_learning_act");
							$act_count = count($act_outcomes);

							$map_counts = array();

							foreach ($body_outcomes as &$body_out)
							{
								$bo_id = $body_out->getID();

								foreach ($act_outcomes as &$act_out)
								{
									$ao_id = $act_out->getID();

									if ($body_out->isRelatedTo($act_out))
									{
										if (isset($map_counts[$ao_id]))
										{
											$map_counts[$ao_id]++;
										} else {
											$map_counts[$ao_id] = 1;
										}
									}
								}
							}

							foreach ($body_outcomes as &$body_out)
							{
								$bo_id = $body_out->getID();

								foreach ($act_outcomes as &$act_out)
								{
									$ao_id = $act_out->getID();

									if ($body_out->isRelatedTo($act_out))
									{
										$all_outcomes[$bo_id] = $body_out;

										if (isset($map_counts[$ao_id]))
										{
											if (isset($act_type_times[$mod_id][$bo_id][$act_type]))
											{
												$act_type_times[$mod_id][$bo_id][$act_type] += $act_duration / $act_count / $map_counts[$ao_id];
											} else {
												$act_type_times[$mod_id][$bo_id][$act_type] = $act_duration / $act_count / $map_counts[$ao_id];
											}
										}
									}
								}
							}
						}
					}
				}

				if (empty($act_type_times[$mod_id]))
				{
					unset($act_type_times[$mod_id]);
				}
			}

			$act_types = array_keys($act_types);

			// print_r($act_type_times); exit;

			if ($mode != 'csv')
			{
				$full_row_span = count($act_types) +2;

				echo '<table class="acc-body niceround grid" style="background-color: #fff">' . "\n";
				echo '<tr><td class="nogrid"></td><td class="nogrid"></td>';

				foreach ($act_types as $act_type)
				{
					echo '<td class="nogrid">' . $act_type . '</td>';
				}
				echo '</tr>';

				// Display
				foreach ($act_type_times as $module => $eaeve)
				{
					foreach ($eaeve as $bo_id => $times)
					{
						if (!empty($times) and isset($all_outcomes[$bo_id]))
						{
							echo "<tr><th>{$mod_titles[$module]}</th>";

							$title = strip_tags(GetNodeHeadingText($all_outcomes[$bo_id], true, true));
							echo "<th>{$title}</th>";

							foreach ($act_types as $type)
							{
								if (isset($times[$type]) and $times[$type] > 0){
									$time = round($times[$type]);

									if ($time > $maxtime)
										$maxtime = $time;

									$weights["$time"] = true;

									$class_mod = ' class="heatmap_' . str_replace('.', '_', $time) . '"';

									echo "<td{$class_mod} align=\"center\">";
									echo $time;
									echo "</td>";
								} else {
									echo "<td>&nbsp;</td>";
								}
							}
							echo "</tr>\n";
						}
					}
				}

				echo "<tr><td colspan=\"$full_row_span\" class=\"nogrid\">&nbsp;</td></tr>\n";
				echo "</table>";

			} else {
				// Generate CSV output

				// output initial group heading
				echo "\n";
				echo ',';
				foreach ($act_types as $act_type)
				{
					echo ',"' . $act_type . '"';
				}
				echo "\n";

				// Display
				foreach ($act_type_times as $module => $eaeve)
				{
					foreach ($eaeve as $bo_id => $times)
					{
						if (!empty($times) and isset($all_outcomes[$bo_id]))
						{
							echo '"' . $mod_titles[$module] . '",';

							$title = strip_tags(GetNodeHeadingText($all_outcomes[$bo_id], true, true));
							echo '"' . $title . '"';

							foreach ($act_types as $type)
							{
								if (isset($times[$type]) and $times[$type] > 0){
									$time = round($times[$type]);

									echo ',' . $time;
								} else {
									echo ',';
								}
							}
							echo "\n";
						}
					}
				}
			}
		}

		if ($mode != 'csv') {
			// output additional style information to highlight the required colours
			if ($maxtime > 0) {
				echo "<style type=\"text/css\">";
				$step = 1 / $maxtime;
				foreach (array_keys($weights) as $i)
				{
					$red = 247;
					$green = floor(210 - ($step * 100 * $i));
					$blue = floor(180 - ($step * 180 * $i));

					$class_mod = str_replace('.', '_', $i);
					echo ".heatmap_$class_mod { background-color: rgb($red,$green,$blue) }\n";
				}
			}

			echo "</style>";
?>
<script>
function showPopup(url)
{
	TINY.box.show({ iframe: url, width: 400, height: 250 });
}
$('.tooltip').qtip({
	position: {
		viewport: true,
		my: 'bottom center',
		at: 'top center'
	},
	show: {
		event: 'click'
	}
});
</script>

<?php
		}
	}
}