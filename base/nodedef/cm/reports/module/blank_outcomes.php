<?php

class Report_blank_outcomes extends ReportBase
{
	protected $child_types = 	array(
				'topic',
				'session',
				'learning_act'
		);

	function Process()
	{
		$this->ci =& get_instance();
		$this->graph_db = $this->ci->graph_db;

		echo '<h1>' . $this->node->getTitleDisp() . ' - Blank Learning Outcomes</h1>';

		$total_count = 0;

		$total_count += $this->output_blank_outcomes($this->node, 'outcome_module', 'Module');

		$total_count += $this->process_relationship($this->node, 'topic', 'Theme');
		$total_count += $this->process_relationship($this->node, 'session', 'Session');
		$total_count += $this->process_relationship($this->node, 'learning_act', 'Learning Activity');
		
		if ($total_count == 0)
		{
			echo "<p>No blank outcomes found</p>\n";
		} else {
			echo "<p>$total_count blank outcomes found</p>\n";
		}
		
	}

	private function output_blank_outcomes($parent, $outcome_type, $level_name)
	{
		$outcomes = $parent->getRelations($outcome_type);

		$blank = array();

		foreach ($outcomes as $outcome)
		{
			if ($outcome->getTitle() == '' && $outcome->getAttributeValue('desc') == '')
			{
				$blank[] = $outcome->getId();
			}
		}

		if (count($blank) > 0)
		{
			echo '<h3><a href="' . site_url('view/' . $parent->getId()) . '" target="_blank">' . $level_name  . ' &ndash; ' . $parent->getTitleDisp() . ' (' . count($blank) . ")</a></h3>\n";
		}

		return count($blank);
	}

	private function process_relationship($parent, $relationship, $level_name)
	{
		$count = 0;

		$nodes = $parent->getRelations($relationship);

		foreach ($nodes as $node)
		{
			$count += $this->output_blank_outcomes($node, 'outcome_' . $relationship, $level_name);
		}

		return $count;
	}
}