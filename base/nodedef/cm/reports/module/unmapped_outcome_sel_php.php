<?php

class Report_unmapped_outcome_sel_php extends ReportBase
{
	function Process()
	{
    $ci = $this->node->CI;
    $ci->load->helper('url');

    $dataset = $ci->graph_db->active_dataset->table;
    $base_uri = $ci->config->item('root_url') . $dataset . '/' . uri_string();

    if (isset($_REQUEST['dest_body']) and $_REQUEST['dest_body'] != '') {
    	// We have a post. Process the form and redirect to the report
    	$url_parts = explode('/', $base_uri);
    	array_pop($url_parts);
    	$url = implode('/', $url_parts) . '/' . 'unmapped_outcomes';

    	$have_query_string = false;

    	$url .= '/' . $_REQUEST['dest_body'];

    	header('Location: ' . $url);
    } else {
			echo "<h1>".$this->node->getTitleDisp()." - Select bodies to check</h1>";

			$course = $this->node->getRelations("course");
			$course = $course[0];

			$data['bodies'] = $course->getRelations("body");
      $data['form_url'] = $base_uri;

			$ci->load->view('reports/module/unmapped_outcomes_sel', $data);
		}
	}
}