<?php

class Report_acts_no_outcomes extends ReportBase
{
	function Process()
	{
		$this->ci =& get_instance();

		echo "<h1>" . $this->node->getTitleDisp() . " - Learning Activities Without Outcomes</h1>";
		
		$activities = $this->node->getRelations("learning_act");
		$this->ci->graph_db->sort_nodes($activities, "order|title");

		$outcomeless = array();

		foreach ($activities as $activity)
		{
			$outcomes = $activity->getRelations('outcome_learning_act');
			if (count($outcomes) == 0)
			{
				$outcomeless[] = $activity;
			}
		}

		if (count($outcomeless) > 0) 
		{
			echo '<ul>';

			foreach ($outcomeless as $act)
			{
				echo '<li><a href="' . site_url('view/' . $act->getId()) . '" target="_blank">' . $act->getTitleDisp() . '</a></li>';
			}

			echo "</ul>\n";
		} else {
			echo "<p>No Learning Activities found.</p>\n";
		}
	}
}