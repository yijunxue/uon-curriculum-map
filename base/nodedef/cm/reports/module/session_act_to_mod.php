<?php

class Report_session_act_to_mod extends ReportBase
{
	function GetGroupText(&$node)
	{
		$guid = $node->getAttributeValue('group');
		$group_node = $this->graph_db->loadNodeByGuid($guid);

		if ($group_node)
		{
			$group = $group_node->getTitle();
		} else {
			$group = "No Group";
		}

		return $group;
	}

	function Process()
	{
		$this->ci =& get_instance();
		$this->graph_db = $this->ci->graph_db;

		$mode = $this->ci->uri->segment(2);
		$show_unmapped = (isset($_GET['only_mapped'])) ? $_GET['only_mapped'] : 'show';
		$render_mode = (isset($_GET['render'])) ? $_GET['render'] : 'count';
		if ($mode != 'csv') {
?>
<style>
.heatmap, .heatmap td {
	border: 1px solid #aaaaaa;
	border-collapse: collapse;
}
th.unmapped {
	background-color: #f55;
}
.ui-tooltip, .qtip {
	max-width: 260px;
}
.ui-tooltip-content {
	color: #fff !important;
	background-color: #333 !important;
	padding: 5px;
}
th h2 {
	display: inline;
	font-size: 100%;
}
</style>
<?php
		}

		$this->ci->load->helper('node_heading');

		$heading = $this->node->getTitleDisp() . ' - Session / Learning Activity to Module Map';
		echo ($mode != 'csv') ? '<h1>' . $heading . '</h1>' : "\"$heading\"\n";

		$this->ci->load->helper('study_year');

		$sessions = $this->node->getRelations("session");
		$this->graph_db->sort_nodes($sessions, "start");

		$activities = $this->node->getRelations("learning_act");
		$this->graph_db->sort_nodes($activities, "order|title");


		$maxweight = 0;
		$weights = array();

		$mappings = array();
		$is_mapped = array();

		$module_outcomes = $this->node->getRelations("outcome_module");

		$this->graph_db->sort_nodes($module_outcomes, "order|group|title");

		$outcome_count = array_fill(0, count($module_outcomes), 0);
		$outcome_sum = array_fill(0, count($module_outcomes), 0);

		// Need to build data in advance so that we can skip un-mapped body outcomes
		foreach ($sessions as &$session)
		{
      $is_sess_mapped = array();

			$session_outcomes = $session->getRelations("outcome_session");
			$num_outcomes = count($session_outcomes);

			// Store sum data
			$outcome_idx = 0;

			foreach ($module_outcomes as &$module_out)
			{
				$mappings[$module_out->getID()] = array();

				$mod_out_weight = 0;

				foreach ($session_outcomes as &$session_out)
				{
					if ($module_out->isRelatedTo($session_out))
					{
						$is_mapped[$module_out->getID()] = true;
            $is_sess_mapped[$module_out->getID()] = true;

						$rel = $module_out->getRel($session_out);
						$mod_out_weight += $rel->getWeight();
					}
				}

				$mappings[$session->getID()][$module_out->getID()] = $mod_out_weight;
			}
		}

		foreach ($activities as &$activity)
		{
      $is_sess_mapped = array();

			$activity_outcomes = $activity->getRelations("outcome_learning_act");
			$num_outcomes = count($activity_outcomes);

			// Store sum data
			$outcome_idx = 0;

			foreach ($module_outcomes as &$module_out)
			{
				$mappings[$module_out->getID()] = array();

				$mod_out_weight = 0;

				foreach ($activity_outcomes as &$activity_out)
				{
					if ($module_out->isRelatedTo($activity_out))
					{
						$is_mapped[$module_out->getID()] = true;
            $is_sess_mapped[$module_out->getID()] = true;

						$rel = $module_out->getRel($activity_out);
						$mod_out_weight += $rel->getWeight();
					}
				}

				$mappings[$activity->getID()][$module_out->getID()] = $mod_out_weight;
			}
		}

		$curr_year = '------------';
		$curgroup = '------------';
		$outcome_idx = 0;
		if ($mode != 'csv') {
			echo '<table class="acc-body niceround grid" style="background-color: #fff">' . "\n";
			echo '<tr><td class="nogrid"></td>';

			// output initial group heading
			$span = 1;

			foreach ($module_outcomes as &$module_out)
			{
				if ($show_unmapped != 'hide' or isset($is_mapped[$module_out->getID()]))
				{
					$group = $this->GetGroupText($module_out);
					if ($group != $curgroup)
					{
						if ($curgroup == '------------') {
							$curgroup = $group;
						} else {
							echo "<td colspan=\"$span\" class=\"nogrid\"><b>" . $curgroup . "</b></td>";
						}
						$span = 1;
					} else {
						$span ++;
					}

					$curgroup = $group;
				} else {
					// Remove the sum data
					unset($outcome_count[$outcome_idx], $outcome_sum[$outcome_idx]);
				}
				$outcome_idx++;
			}
			echo "<td colspan=\"$span\" class=\"nogrid\"><b>" . $curgroup . "</b></td>";
			echo "</tr>\n";

			echo "<tr><td class=\"nogrid\">&nbsp;</td>";
			foreach ($module_outcomes as &$module_out)
			{
				if ($show_unmapped != 'hide' or isset($is_mapped[$module_out->getID()]))
				{
					$classes = array();
					if ($show_unmapped == 'highlight' and !isset($is_mapped[$module_out->getID()]))
					{
						$classes[] = 'unmapped';
					}

					$desc = $module_out->getAttribute('desc');
					$desc_mod = '';
					if ($desc != '')
					{
						$desc_mod = ' title="' . $desc . '"';
						$classes[] = 'tooltip';
					}

          $class_mod = '';
					if (count($classes) > 0)
					{
						$class_mod = ' class="' . implode(' ', $classes) . '"';
					}
					echo "<th{$class_mod}{$desc_mod}>" . GetNodeHeadingText($module_out, true, true) . "</th>";
				}
			}
			echo "</tr>\n";

			// Display
			$full_row_span = count($module_outcomes) + 1;
			if (count($sessions) > 0) {
				echo "<tr><td colspan=\"$full_row_span\" class=\"nogrid\"><b>Sessions</b></td></tr>\n";
			}

			foreach ($sessions as &$session)
			{
				$classes = array();
				$desc = $session->getTitle();
				$desc_mod = '';
				if ($desc != '')
				{
					$desc_mod = ' title="' . $desc . '"';
					$classes[] = 'tooltip-l';
				}

        $class_mod = '';
				if (count($classes) > 0)
				{
					$class_mod = ' class="' . implode(' ', $classes) . '"';
				}
				echo "<tr><th{$class_mod}{$desc_mod}>" . GetNodeHeadingText($session, true, true) . "</th>";

				foreach ($module_outcomes as &$module_out)
				{
          if ($show_unmapped != 'hide' or isset($is_mapped[$module_out->getID()])) {
						$mod_out_weight = $mappings[$session->getID()][$module_out->getID()];

						if ($mod_out_weight > 0)
						{
							$weights["$mod_out_weight"] = true;

							if ($mod_out_weight > $maxweight)
								$maxweight = $mod_out_weight;

							$class_mod = ($render_mode == 'count') ? ' class="heatmap_' . str_replace('.', '_', $mod_out_weight) . '"': '';

							$url = site_url("report/raw/" . $this->node->getID() . "/session_act_to_mod_popup/" . $session->getId() . "/" . $module_out->getID());

							echo "<td{$class_mod} align=\"center\" onclick=\"showPopup('$url');\" style=\"cursor: pointer\">";
							if ($render_mode == 'count')
							{
								echo $mod_out_weight;
							} else {
								echo '<img src="' . asset_url() . 'image/misc/tick.png" alt="Tick" width="16" height="16" />';
							}
							echo "</td>";
						} else {
							echo '<td>&nbsp;</td>';
						}
					}
				}
				echo "</tr>\n";
			}

			if (count($activities) > 0) {
				echo "<tr><td colspan=\"$full_row_span\" class=\"nogrid\"><b>Learning Activities</b></td></tr>\n";
			}

			foreach ($activities as &$activity)
			{
				$classes = array();
				$desc = $activity->getTitle();
				$desc_mod = '';
				if ($desc != '')
				{
					$desc_mod = ' title="' . $desc . '"';
					$classes[] = 'tooltip-l';
				}

        $class_mod = '';
				if (count($classes) > 0)
				{
					$class_mod = ' class="' . implode(' ', $classes) . '"';
				}
				echo "<tr><th{$class_mod}{$desc_mod}>" . GetNodeHeadingText($activity, true, true) . "</th>";

				foreach ($module_outcomes as &$module_out)
				{
          if ($show_unmapped != 'hide' or isset($is_mapped[$module_out->getID()])) {
						$mod_out_weight = $mappings[$activity->getID()][$module_out->getID()];

						if ($mod_out_weight > 0)
						{
							$weights["$mod_out_weight"] = true;

							if ($mod_out_weight > $maxweight)
								$maxweight = $mod_out_weight;

							$class_mod = ($render_mode == 'count') ? ' class="heatmap_' . str_replace('.', '_', $mod_out_weight) . '"': '';

							$url = site_url("report/raw/" . $this->node->getID() . "/session_act_to_mod_popup/" . $activity->getId() . "/" . $module_out->getID());

							echo "<td{$class_mod} align=\"center\" onclick=\"showPopup('$url');\" style=\"cursor: pointer\">";
							if ($render_mode == 'count')
							{
								echo $mod_out_weight;
							} else {
								echo '<img src="' . asset_url() . 'image/misc/tick.png" alt="Tick" width="16" height="16" />';
							}
							echo "</td>";
						} else {
							echo '<td>&nbsp;</td>';
						}
					}
				}
				echo "</tr>\n";
			}

			echo "</table>";

		} else {
			// Generate CSV output

			// output initial group heading
			$span = 1;
			$curgroup = '------------';

			foreach ($module_outcomes as &$module_out)
			{
				if ($show_unmapped != 'hide' or isset($is_mapped[$module_out->getID()]))
				{
					$group = $this->GetGroupText($module_out);
					if ($group != $curgroup)
					{
						if ($curgroup == '------------') {
							$curgroup = $group;
						} else {
							echo "\"$curgroup\"" . str_repeat(',', $span);
						}
						$span = 1;
					} else {
						$span ++;
					}

					$curgroup = $group;
				}
			}
			echo "\"$curgroup\"\n";

			echo ',';
			foreach ($module_outcomes as &$module_out)
			{
        if ($show_unmapped != 'hide' or isset($is_mapped[$module_out->getID()])) {
					echo "\"" . GetNodeHeadingText($module_out) . "\",";
				}
			}
			echo "\n";

			// Display
			$full_row_span = count($module_outcomes) + 1;
			if (count($sessions) > 0) {
				echo "Sessions\n";
			}

			foreach ($sessions as &$session)
			{
				echo '"' . GetNodeHeadingText($session, true) . '",';

				foreach ($module_outcomes as &$module_out)
				{
          if ($show_unmapped != 'hide' or isset($is_mapped[$module_out->getID()])) {
						$mod_out_weight = $mappings[$session->getID()][$module_out->getID()];

						if ($mod_out_weight > 0)
						{
							$weights["$mod_out_weight"] = true;

							if ($mod_out_weight > $maxweight)
								$maxweight = $mod_out_weight;

							if ($render_mode == 'count')
							{
								echo $mod_out_weight;
							} else {
								echo 'X';
							}
						}
						echo ',';
					}
				}
				echo "\n";
			}

			if (count($activities) > 0) {
				echo "Learning Activities\n";
			}

			foreach ($activities as &$activity)
			{
				echo '"' . GetNodeHeadingText($activity, true) . '",';

				foreach ($module_outcomes as &$module_out)
				{
          if ($show_unmapped != 'hide' or isset($is_mapped[$module_out->getID()])) {
						$mod_out_weight = $mappings[$activity->getID()][$module_out->getID()];

						if ($mod_out_weight > 0)
						{
							$weights["$mod_out_weight"] = true;

							if ($mod_out_weight > $maxweight)
								$maxweight = $mod_out_weight;

							if ($render_mode == 'count')
							{
								echo $mod_out_weight;
							} else {
								echo 'X';
							}
						}
						echo ',';
					}
				}
				echo "\n";
			}
		}

		if ($mode != 'csv') {
			// output additional style information to highlight the required colours
			if ($maxweight > 0) {
				echo "<style type=\"text/css\">";
				$step = 1 / $maxweight;
				foreach (array_keys($weights) as $i)
				{
					$red = 247;
					$green = floor(210 - ($step * 100 * $i));
					$blue = floor(180 - ($step * 180 * $i));

					$class_mod = str_replace('.', '_', $i);
					echo ".heatmap_$class_mod { background-color: rgb($red,$green,$blue) }\n";
				}
			}

			echo "</style>";
?>
<script>
function showPopup(url)
{
	TINY.box.show({ iframe: url, width: 400, height: 250 });
}
$('.tooltip').qtip({
	position: {
		viewport: true,
		my: 'bottom center',
		at: 'top center'
	},
	show: {
		event: 'click'
	}
});
$('.tooltip-l').qtip({
	position: {
		viewport: true,
		my: 'bottom left',
		at: 'top left'
	},
	show: {
		event: 'click'
	}
});
</script>

<?php
		}
	}
}