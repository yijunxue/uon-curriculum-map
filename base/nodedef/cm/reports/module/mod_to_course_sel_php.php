<?php

class Report_mod_to_course_sel_php extends ReportBase
{
	function Process()
	{
    $ci = $this->node->CI;
    $ci->load->helper('url');

    $dataset = $ci->graph_db->active_dataset->table;
    $base_uri = $ci->config->item('root_url') . $dataset . '/' . uri_string();

    if (isset($_REQUEST['via_course'])) {
			// We have a post. Process the form and redirect to the report
			$url_parts = explode('/', $base_uri);
			array_pop($url_parts);
			$url = implode('/', $url_parts) . '/' . 'mod_to_course';

			$have_query_string = false;

			if ($_REQUEST['via_course'] != '') {
				$url .= '/' . $_REQUEST['via_course'];
			}

			if(isset($_REQUEST['only_mapped'])) {
				$url .= '/?only_mapped=' . $_REQUEST['only_mapped'];
				$have_query_string = true;
			}

			if(isset($_REQUEST['render'])) {
				$url .= ($have_query_string) ? '&' : '/?';
				$url .= 'render=tick';
			}

			header('Location: ' . $url);
		} else {
			echo "<h1>".$this->node->getTitleDisp()." - Select programmes to map</h1>";

			$data['courses'] = $this->node->getRelations("course");
      $data['form_url'] = $base_uri;

			$ci->load->view('reports/module/mod_to_course_sel', $data);
		}
	}
}