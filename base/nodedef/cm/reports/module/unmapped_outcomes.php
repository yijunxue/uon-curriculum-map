<?php

class Report_unmapped_outcomes extends ReportBase
{
	protected $child_types = 	array(
				'topic',
				'session',
				'learning_act'
		);

	function Process()
	{
		$this->ci =& get_instance();
		$this->graph_db = $this->ci->graph_db;

		$body_id = $this->ci->uri->segment(5);

		echo '<h1>' . $this->node->getTitleDisp() . ' - Unmapped Learning Outcomes</h1>';

		if ($body_id === false) {
			$course = $this->node->getRelations("course");
			$course = $course[0];

			$bodies = $course->getRelations("body");
		} else {
			$bodies = array($this->ci->graph_db->loadNode($body_id));
		}

		$total_count = 0;

		foreach ($bodies as $body) {
			$title = $body->getTitleDisp();

			echo "<h2>$title</h2>\n";

			$body_outcomes = $body->getRelations('outcome_body');
			$total_count += $this->output_unmapped_outcomes($this->node, $body_outcomes, 'outcome_module', 'Module');

			$total_count += $this->process_relationship($this->node, $body_outcomes, 'topic', 'Topic');
			$total_count += $this->process_relationship($this->node, $body_outcomes, 'session', 'Session');
			$total_count += $this->process_relationship($this->node, $body_outcomes, 'learning_act', 'Learning Activity');
		}


		if ($total_count == 0)
		{
			echo "<p>No unmapped outcomes found</p>\n";
		} else {
			echo "<p>$total_count unmapped outcomes found</p>\n";
		}

	}

	private function output_unmapped_outcomes($parent, &$body_outcomes, $outcome_type, $level_name)
	{
		$outcomes = $parent->getRelations($outcome_type);

		foreach ($body_outcomes as $body_outcome)
		{
			foreach ($outcomes as $ix => $outcome)
			{
				if ($body_outcome->isRelatedTo($outcome))
				{
					unset($outcomes[$ix]);
				}
			}
		}

		if (count($outcomes) > 0)
		{
			echo '<h3><a href="' . site_url('view/' . $parent->getId()) . '" target="_blank">' . $level_name  . ' &ndash; ' . $parent->getTitleDisp() . ' (' . count($outcomes) . ")</a></h3>\n";

			echo "<ul>\n";
			foreach ($outcomes as $unm)
			{
				echo '<li><a href="' . site_url('view/' . $unm->getId()) . '" target="_blank">' . $unm->getTitleDisp() . "</a></li>\n";
			}
			echo "</ul>\n";
		}

		return count($outcomes);
	}

	private function process_relationship($parent, &$body_outcomes, $relationship, $level_name)
	{
		$count = 0;

		$nodes = $parent->getRelations($relationship);

		foreach ($nodes as $node)
		{
			$count += $this->output_unmapped_outcomes($node, $body_outcomes, 'outcome_' . $relationship, $level_name);
		}

		return $count;
	}
}
