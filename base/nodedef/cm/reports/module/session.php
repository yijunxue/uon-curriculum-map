<?php

class Report_session extends ReportBase
{
	function Process()	
	{
		//echo "<pre>".htmlentities($this->data_xml->asXML())."</pre>";
		
		echo "<h1>" . $this->data_xml->module->title . ": Session List</h1>";
		
		foreach ($this->data_xml->module->session as $session)
		{
			$attr = $session->attributes();
			echo '<h2><a href="' . site_url('view/' . $attr['id']) . '" target="_blank">' . $session->title . "</a></h2>";	
			echo "Start : " . $session->start . "<br>";
			echo "End : " . $session->end . "<br>";
			echo "Description : " . $session->desc . "<br>";
		}
	}
}