<?php

class Report_session_act_to_mod_popup extends ReportBase
{
	function Process()
	{
		$this->ci =& get_instance();
		$sess_id = $this->ci->uri->segment(5);
		$mod_out_id = $this->ci->uri->segment(6);
		if ($sess_id < 1 || $mod_out_id < 1)
		{
			echo "This report cannot be run directly";
			return;
		}

		$data['title'] = "Mapped Outcomes";
		$this->ci->load->view('templates/popup/header', $data);

    $session = $this->ci->graph_db->loadNode($sess_id);
    $mod_out = $this->ci->graph_db->loadNode($mod_out_id);

    $mapped_outcomes = array();

    $session_outcomes = $session->getRelations("outcome_session");
    $learning_act_outcomes = $session->getRelations("outcome_learning_act");

    $all_outcomes = array_merge($session_outcomes, $learning_act_outcomes);

    foreach ($all_outcomes as &$session_out)
    {
      if ($session_out->isRelatedTo($mod_out))
      {
        $mapped_outcomes[] = $session_out;
      }
    }

    echo "<ul>";

		foreach ($mapped_outcomes as &$outcome)
		{
			echo "<li>";

			echo $outcome->getTitleDisp(true);

			echo "</li>";
		}
		echo "</ul>";
?>
<style type="text/css">
h2 {
  font-size: 100%;
  display: inline;
}
li p {
  display: inline;
}
</style>
<?php

		$this->ci->load->view('templates/popup/footer', $data);
	}
}
