<?php

class Report_all_outcomes extends ReportBase
{
	protected $child_types = 	array(
				'topic',
				'session',
				'learning_act'
		);

	function Process()
	{
?>
<style type="text/css">
li {
	padding: 2px 0;
}
li a h2 {
	font-size: 100%;
	display: inline;
}
li a p {
	display: inline;
}
</style>
<?php
		$this->ci =& get_instance();
		$this->graph_db = $this->ci->graph_db;

		echo '<h1>' . $this->node->getTitleDisp() . ' - All Learning Outcomes</h1>';

?>
<table class="niceround">
<tr>
	<th><label for="search_outcomes">Search:</label></th>
	<td><input name='search_outcomes' id='search_outcomes'></td>
	<td><button type="button" id="search">Filter</button></td>
	<td><button type="button" id="reset">Reset</button></td>
</tr>	
</table>

<script type="text/javascript">
var AllOutcomes = {
	setup: function() {
    $('#search').click(function () {
        AllOutcomes.searchUpdate($('#search_outcomes').val());
    });

    $('#reset').click(function () {
    		$('#search_outcomes').val('');
        AllOutcomes.searchUpdate($('#search_outcomes').val());
    });
	},

	searchUpdate: function(text) {
		$('.node').hide();
		$('.node > ul > li:not(:contains("' + text + '"))').hide();
		$('.node > ul > li:contains("' + text + '")').show().closest('.node').show();
	}
}

$(document).ready(function() {
	AllOutcomes.setup();
});

</script>
<?php

		$total_count = 0;

		$total_count += $this->output_outcomes($this->node, 'outcome_module', 'Module');

		$total_count += $this->process_relationship($this->node, 'topic', 'Theme');
		$total_count += $this->process_relationship($this->node, 'session', 'Session');
		$total_count += $this->process_relationship($this->node, 'learning_act', 'Learning Activity');
	}

	private function output_outcomes($parent, $outcome_type, $level_name)
	{
		$outcomes = $parent->getRelations($outcome_type);

		if (count($outcomes) > 0)
		{
			echo '<div class="node">';
			echo '<h3><a href="' . site_url('view/' . $parent->getId()) . '" target="_blank">' . $level_name  . ' &ndash; ' . $parent->getTitleDisp() . "</a></h3>\n";
			echo '<ul>';

			foreach ($outcomes as $outcome)
			{
				echo '<li><a href="' . site_url('view/' . $outcome->getId()) . '" target="_blank">' . $outcome->getTitleDisp(true) . "</a></li>\n";
			}

			echo "</ul>\n";
			echo "</div>\n";
		}

		return count($outcomes);
	}

	private function process_relationship($parent, $relationship, $level_name)
	{
		$count = 0;

		$nodes = $parent->getRelations($relationship);

		foreach ($nodes as $node)
		{
			$count += $this->output_outcomes($node, 'outcome_' . $relationship, $level_name);
		}

		return $count;
	}
}