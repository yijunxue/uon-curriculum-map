<?php

class Report_acts_no_type extends ReportBase
{
	function Process()
	{
		$this->ci =& get_instance();

		echo "<h1>" . $this->node->getTitleDisp() . " - Learning Activities Without Type or Duration</h1>";
		
		$activities = $this->node->getRelations("learning_act");
		$this->ci->graph_db->sort_nodes($activities, "order|title");

		$typeless = array();
		$durationless = array();

		foreach ($activities as $activity)
		{
			if ($activity->getAttribute('act_type')->getValue() === '')
			{
				$typeless[] = $activity;
			}

			if ($activity->getAttribute('duration')->getValue() === '')
			{
				$durationless[] = $activity;
			}
		}

		if (count($typeless) > 0) 
		{
			echo "<h2>Learning Activities without Type</h2>";
			echo '<ul>';

			foreach ($typeless as $act)
			{
				echo '<li><a href="' . site_url('view/' . $act->getId()) . '" target="_blank">' . $act->getTitleDisp() . '</a></li>';
			}

			echo "</ul>\n";
		}

		if (count($durationless) > 0) 
		{
			echo "<h2>Learning Activities without Duration</h2>";
			echo '<ul>';

			foreach ($durationless as $act)
			{
				echo '<li><a href="' . site_url('view/' . $act->getId()) . '" target="_blank">' . $act->getTitleDisp() . '</a></li>';
			}

			echo "</ul>\n";
		}

		if (count($typeless) == 0 && count($durationless) == 0)
		{
			echo "<p>No Learning Activities found.</p>\n";
		}
	}
}