<?php

class Report_session_act_to_mod_opts_php extends ReportBase
{
	function Process()
	{
    $ci = $this->node->CI;
    $ci->load->helper('url');

    $dataset = $ci->graph_db->active_dataset->table;
    $base_uri = $ci->config->item('root_url') . $dataset . '/' . uri_string();

    if (isset($_REQUEST['only_mapped'])) {
			// We have a post. Process the form and redirect to the report
			$url_parts = explode('/', $base_uri);
			array_pop($url_parts);
			$url = implode('/', $url_parts) . '/' . 'session_act_to_mod';

			$have_query_string = false;

			if(isset($_REQUEST['only_mapped'])) {
				$url .= '/?only_mapped=' . $_REQUEST['only_mapped'];
				$have_query_string = true;
			}

			if(isset($_REQUEST['render'])) {
				$url .= ($have_query_string) ? '&' : '/?';
				$url .= 'render=tick';
			}

			header('Location: ' . $url);
		} else {
			echo "<h1>".$this->node->getTitleDisp()." - Select Session / Learning Activities to Module Options</h1>";

      $data['form_url'] = $base_uri;

			$ci->load->view('reports/module/session_act_to_mod', $data);
		}
	}
}