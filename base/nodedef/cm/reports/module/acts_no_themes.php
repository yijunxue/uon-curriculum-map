<?php

class Report_acts_no_themes extends ReportBase
{
	function Process()
	{
		$this->ci =& get_instance();

		echo "<h1>" . $this->node->getTitleDisp() . " - Learning Activities not belonging to a Theme</h1>";
		
		$activities = $this->node->getRelations("learning_act");
		$this->ci->graph_db->sort_nodes($activities, "order|title");

		$themeless = array();

		foreach ($activities as $activity)
		{
			$outcomes = $activity->getRelations('topic');
			if (count($outcomes) == 0)
			{
				$themeless[] = $activity;
			}
		}

		if (count($themeless) > 0) 
		{
			echo '<ul>';

			foreach ($themeless as $act)
			{
				echo '<li><a href="' . site_url('view/' . $act->getId()) . '" target="_blank">' . $act->getTitleDisp() . '</a></li>';
			}

			echo "</ul>\n";
		} else {
			echo "<p>No Learning Activities found.</p>\n";
		}
	}
}