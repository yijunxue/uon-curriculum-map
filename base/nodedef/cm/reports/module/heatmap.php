<?php

class Report_heatmap extends ReportBase
{
	function GetGroupText(&$node)
	{
		$guid = $node->getAttributeValue('group');
		$group_node = $this->graph_db->loadNodeByGuid($guid);

		if ($group_node)
		{
			$group = $group_node->getTitle();
		} else {
			$group = "No Group";
		}

		return $group;
	}

	function Process()
	{
?>
<style>
.heatmap, .heatmap td {
	border: 1px solid #aaaaaa;
	border: collapse;
}
</style>
<?php
		$this->ci =& get_instance();
		$this->graph_db = $this->ci->graph_db;

		$this->ci->load->helper('node_heading');

		echo "<h1>".$this->node->getTitleDisp()." - Body Outcome Map</h1>";

		$module_outcomes = $this->node->getRelations("outcome_module");

		$this->graph_db->sort_nodes($module_outcomes, "group|order|title");
		$course = $this->node->getRelations("course");
		$course = $course[0];

    $maxcount = 0;

		$bodys = $course->getRelations("body");
		foreach ($bodys as &$body)
		{

			echo "<h2>". $body->getTitleDisp() . "</h2>";

			$body_outcomes = $body->getRelations("outcome_body");
			$this->graph_db->sort_nodes($body_outcomes, "group|order|title");
			echo "<table class='niceround grid'>";
			echo "<tr><td class=\"nogrid\"></td>";

			// output initial group heading
			$curgroup = "------------";
			$span = 1;

			foreach ($body_outcomes as &$body_out)
			{
				$group = $this->GetGroupText($body_out);
				if ($group != $curgroup)
				{
					if ($curgroup == "------------") {
						$curgroup = $group;
					} else {
						echo "<td colspan=\"$span\" class=\"nogrid\"><b>" . $curgroup . "</b></td>";
					}
					$span = 1;
				} else {
					$span ++;
				}

				$curgroup = $group;
			}
			echo "<td colspan=\"$span\" class=\"nogrid\"><b>" . $curgroup . "</b></td>";
			echo "</tr>";

			echo "<tr><td class=\"nogrid\"></td>";
			foreach ($body_outcomes as &$body_out)
			{
				echo "<th>" . GetNodeHeadingText($body_out) . "</th>";
			}
			echo "</tr>";

			$curgroup = "------------";
			foreach ($module_outcomes as &$module_out)
			{
				// output another group heading if required
				$group = $this->GetGroupText($module_out);

				if ($group != $curgroup)
				{
					echo "<tr><td class=\"nogrid\"><b>" . $group . "</b></td></tr>";
					$curgroup = $group;
				}

				echo "<tr><th>" . GetNodeHeadingText($module_out) . "</th>";
				foreach ($body_outcomes as &$body_out)
				{

					if ($body_out->isRelatedTo($module_out))
					{
						// the module outcome we are dealing with is linked to the body outcome we are dealing with, so need to count up all the sessions that are linked to
						// the current module outcome.

						$session_outcomes = $module_out->getRelations("outcome_session");

						$sessions = array();

						foreach ($session_outcomes as &$session_out)
						{
							$session_temp = $session_out->getRelations("session");

							foreach ($session_temp as &$session)
							{
								$sessions[$session->getID()] = $session;
							}
						}

						$learning_act_outcomes = $module_out->getRelations("outcome_learning_act");

						$learning_acts = array();

						foreach ($learning_act_outcomes as &$learning_act_out)
						{
							$learning_act_temp = $learning_act_out->getRelations("learning_act");

							foreach ($learning_act_temp as &$learning_act)
							{
								$sessions[$learning_act->getID()] = $learning_act;
							}
						}

						$count = count($sessions);

						if ($count > 0)
						{
							if ($count > $maxcount)
								$maxcount = $count;

							$url = site_url("report/raw/" . $this->node->getID() . "/heatmap_popup/" . $module_out->getId() . "/" . $body_out->getID());

							echo "<td class='heatmap_{$count}' align='center' onclick='showPopup(\"$url\");' style='cursor: pointer'>";
							//echo "<a class='a_popup' href='$url'>";
							echo "$count";
							//echo "</a>";
							echo "</td>";
						}
					} else {
						echo "<td></td>";
					}

				}
				echo "</tr>";
			}
			echo "</table>";

		}


		// output additional style information to highlight the required colours
		echo "<style>";

		if ($maxcount !== 0) {
			$step = 1 / $maxcount;
		} else {
		    $step = 0;
		}

		for ($i = 1 ; $i <= $maxcount ; $i++)
		{
			$red = 247;
			$green = floor(210 - ($step * 100 * $i));
			$blue = floor(180 - ($step * 180 * $i));

			echo ".heatmap_$i { background-color: rgb($red,$green,$blue) }\n";
		}

		echo "</style>";
?>
<script>
function showPopup(url)
{
	TINY.box.show({ iframe: url, width: 400, height: 250 });
}
</script>

<?php
	}
}