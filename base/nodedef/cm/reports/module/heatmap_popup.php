<?php

class Report_heatmap_popup extends ReportBase
{
	function Process()	
	{
		$this->ci =& get_instance();
		$mod_out_id = $this->ci->uri->segment(5);
		$body_out_id = $this->ci->uri->segment(6);
		if ($mod_out_id < 1 || $body_out_id < 1)	
		{
			echo "This report cannot be run directly";
			return;	
		}
		
		$data['title'] = "Mapped Sessions/Activities";
		$this->ci->load->view('templates/popup/header', $data);
		
		$module_out = $this->ci->graph_db->loadNode($mod_out_id);

    $sessions = array();

    $session_outcomes = $module_out->getRelations("outcome_session");

    foreach ($session_outcomes as &$session_out)
    {
      $session_temp = $session_out->getRelations("session");

      foreach ($session_temp as &$session)
      {
        $sessions[$session->getID()] = $session;
      }
    }

    $learning_act_outcomes = $module_out->getRelations("outcome_learning_act");

    foreach ($learning_act_outcomes as &$learning_act_out)
    {
      $learning_act_temp = $learning_act_out->getRelations("learning_act");

      foreach ($learning_act_temp as &$learning_act)
      {
        $sessions[$learning_act->getID()] = $learning_act;
      }
    }


    echo "<ul>";
		
		foreach ($sessions as &$session)
		{
			echo "<li>";
			
			echo "<a href='" . site_url('view/' . $session->getId())."' target='_blank'>";
			echo $session->getTitleDisp();
			
			echo "</a></li>";	
		}
		echo "</ul>";
		$this->ci->load->view('templates/popup/footer', $data);
	}
}