<?php

class Report_mod_to_body extends ReportBase
{
	function GetGroupText(&$node)
	{
		$guid = $node->getAttributeValue('group');
		$group_node = $this->graph_db->loadNodeByGuid($guid);

		if ($group_node)
		{
			$group = $group_node->getTitle();
		} else {
			$group = "No Group";
		}

		return $group;
	}

	function Process()
	{
		$this->ci =& get_instance();
		$this->graph_db = $this->ci->graph_db;

		$mode = $this->ci->uri->segment(2);
		$body_id = $this->ci->uri->segment(5);
		$ext_body_id = $this->ci->uri->segment(6);

		$show_unmapped = (isset($_GET['only_mapped'])) ? $_GET['only_mapped'] : 'show';
		$show_title = (!isset($_GET['only_code']) or $_GET['only_code'] == 0);
		$render_mode = (isset($_GET['render'])) ? $_GET['render'] : 'count';
		if ($mode != 'csv') {
?>
<style>
.heatmap, .heatmap td {
	border: 1px solid #aaaaaa;
	border-collapse: collapse;
}
th.unmapped {
	background-color: #f55;
}
.ui-tooltip, .qtip {
	max-width: 260px;
}
.ui-tooltip-content {
	color: #fff !important;
	background-color: #333 !important;
	padding: 5px;
}
th h2 {
	display: inline;
	font-size: 100%;
}
</style>
<?php
		}

		$this->ci->load->helper('node_heading');

		$heading = $this->node->getTitleDisp() . ' - Module Outcome to Accrediting Body Map';
		echo ($mode != 'csv') ? '<h1>' . $heading . '</h1>' : "\"$heading\"\n";

		$module_outcomes = $this->node->getRelations("outcome_module");

		$this->graph_db->sort_nodes($module_outcomes, "group|order|title");


		if ($body_id === false) {
			$course = $this->node->getRelations("course");
			$course = $course[0];

			$bodys = $course->getRelations("body");
		} else {
			$bodys = array($this->ci->graph_db->loadNode($body_id));
		}

    $maxcount = 0;

		$maxweight = 0;
		$weights = array();

		foreach ($bodys as &$body)
		{
			$mappings = array();
			$is_mapped = array();
			$is_body_mapped[$body->getId()] = false;

			$title = $body->getTitleDisp();

			$body_outcomes = $body->getRelations("outcome_body");

			// TODO: Is there a better way todo this without requiring the 3-deep nested loops?
			if ($ext_body_id !== false and $ext_body_id != $body_id) {
				// Check for mapped nodes
				// WARNING: This may not give the expected results if a single accrediting body outcome is mapped to multiple outcomes
				// in the intermediate body. Will also completely override any relations to the destination body if there is a mapping
				// to the intermediate, which maybe isn't ideal

				$ext_body = $this->ci->graph_db->loadNode($ext_body_id);

				if ($ext_body) {
					$title = $ext_body->getTitleDisp() . ' via ' . $title;

					$tmp_body_outcomes = $body_outcomes;
					// $ext_body_outcomes = $ext_body->getRelations("outcome_body");

					$body_outcomes = array();

					foreach ($tmp_body_outcomes as &$body_outcome) {
						$body_rels = $body_outcome->getRelations("outcome_body");

						foreach ($body_rels as $body_rel) {
							if ($body_rel->isRelatedTo($ext_body)) {
								$body_outcome->setTitle($body_rel->getTitle());
								$body_outcomes[] = $body_outcome;
								break;
							}
						}
					}
				}
			}

			echo ($mode != 'csv') ? "<h2>$title</h2>" : "\"$title\"\n\n";

			if (count($body_outcomes) > 0)
			{
				$this->graph_db->sort_nodes($body_outcomes, "order|group|title");

				$outcome_count = array_fill(0, count($body_outcomes), 0);
				$outcome_sum = array_fill(0, count($body_outcomes), 0);

				// Need to build data in advance so that we can skip un-mapped body outcomes
				foreach ($module_outcomes as &$module_out)
				{
					// Store sum data
					$outcome_idx = 0;

					foreach ($body_outcomes as &$body_out)
					{
						$mappings[$body_out->getID()] = array();

						$mod_out_weight = 0;

						if ($body_out->isRelatedTo($module_out))
						{
							$is_mapped[$body_out->getID()] = true;
							$is_body_mapped[$body->getId()] = true;

							$rel = $body_out->getRel($module_out);
							$mod_out_weight = $rel->getWeight();
						}

						$mappings[$module_out->getID()][$body_out->getID()] = $mod_out_weight;
					}
				}

				$curr_year = '------------';
				$curgroup = '------------';
				$outcome_idx = 0;

				if ($is_body_mapped[$body->getId()])
				{
					if ($mode != 'csv') {
						echo '<table class="acc-body niceround grid" style="background-color: #fff">' . "\n";
						echo '<tr><td class="nogrid"></td>';

						// output initial group heading
						$span = 1;

						foreach ($body_outcomes as &$body_out)
						{
							if ($show_unmapped != 'hide' or isset($is_mapped[$body_out->getID()]))
							{
								$group = $this->GetGroupText($body_out);
								if ($group != $curgroup)
								{
									if ($curgroup == '------------') {
										$curgroup = $group;
									} else {
										echo "<td colspan=\"$span\" class=\"nogrid\"><b>" . $curgroup . "</b></td>";
									}
									$span = 1;
								} else {
									$span ++;
								}

								$curgroup = $group;
							} else {
								// Remove the sum data
								unset($outcome_count[$outcome_idx], $outcome_sum[$outcome_idx]);
							}
							$outcome_idx++;
						}
						echo "<td colspan=\"$span\" class=\"nogrid\"><b>" . $curgroup . "</b></td>";
						echo "</tr>\n";

						echo "<tr><td class=\"nogrid\">&nbsp;</td>";
						foreach ($body_outcomes as &$body_out)
						{
							if ($show_unmapped != 'hide' or isset($is_mapped[$body_out->getID()]))
							{
								$classes = array();
								if ($show_unmapped == 'highlight' and !isset($is_mapped[$body_out->getID()]))
								{
									$classes[] = 'unmapped';
								}

								$desc = $body_out->getAttribute('desc');
								$desc_mod = '';
								if ($desc != '')
								{
									$desc_mod = ' title="' . $desc . '"';
									$classes[] = 'tooltip';
								}

		            $class_mod = '';
								if (count($classes) > 0)
								{
									$class_mod = ' class="' . implode(' ', $classes) . '"';
								}
								echo "<th{$class_mod}{$desc_mod}>" . GetNodeHeadingText($body_out) . "</th>";
							}
						}
						echo "</tr>\n";

						// Display
						$curgroup = "------------";

						$full_row_span = count($body_outcomes) + 1;
						foreach ($module_outcomes as &$module_out)
						{
							$group = $this->GetGroupText($module_out);

							if ($group != $curgroup)
							{
								// TODO: should this use GetNodeHeadingText?
								$title = ($group == 'none') ? 'No Group' : $group;
								echo "<tr><td colspan=\"$full_row_span\" class=\"nogrid\"><b>" . $title . "</b></td></tr>\n";
		            $curgroup = $group;
							}

							$classes = array();
							$desc = $module_out->getAttribute('desc');
							$desc_mod = '';
							if ($desc != '')
							{
								$desc_mod = ' title="' . $desc . '"';
								$classes[] = 'tooltip-l';
							}

	            $class_mod = '';
							if (count($classes) > 0)
							{
								$class_mod = ' class="' . implode(' ', $classes) . '"';
							}
							echo "<tr><th{$class_mod}{$desc_mod}>" . GetNodeHeadingText($module_out, true, true) . "</th>";

							foreach ($body_outcomes as &$body_out)
							{
		            if ($show_unmapped != 'hide' or isset($is_mapped[$body_out->getID()])) {
									$mod_out_weight = $mappings[$module_out->getID()][$body_out->getID()];

									if ($mod_out_weight > 0)
									{
										$weights["$mod_out_weight"] = true;

										if ($mod_out_weight > $maxweight)
											$maxweight = $mod_out_weight;

										$class_mod = ($render_mode == 'count') ? ' class="heatmap_' . str_replace('.', '_', $mod_out_weight) . '"': '';

										echo "<td{$class_mod} align=\"center\">";
										if ($render_mode == 'count')
										{
											echo $mod_out_weight;
										} else {
											echo '<img src="' . asset_url() . 'image/misc/tick.png" alt="Tick" width="16" height="16" />';
										}
										echo "</td>";
									} else {
										echo '<td>&nbsp;</td>';
									}
								}
							}
							echo "</tr>\n";
						}

						echo "</table>";

					} else {
						// Generate CSV output

						// output initial group heading
						$span = 1;
						$curgroup = "------------";

						echo ',';
						foreach ($body_outcomes as &$body_out)
						{
		          if ($show_unmapped != 'hide' or isset($is_mapped[$body_out->getID()]))
		          {
								$group = $this->GetGroupText($body_out);
								if ($group != $curgroup)
								{
									if ($curgroup == "------------") {
										$curgroup = $group;
									} else {
										echo "\"$curgroup\"" . str_repeat(',', $span);
									}
									$span = 1;
								} else {
									$span ++;
								}

								$curgroup = $group;
							}
						}
						echo "\"$curgroup\"\n";

						echo ',';
						foreach ($body_outcomes as &$body_out)
						{
		          if ($show_unmapped != 'hide' or isset($is_mapped[$body_out->getID()])) {
								echo "\"" . GetNodeHeadingText($body_out) . "\",";
							}
						}
						echo "\n";

						// Display
						$curgroup = "------------";

						foreach ($module_outcomes as &$module_out)
						{
							$group = $this->GetGroupText($module_out);

							if ($group != $curgroup)
							{
								// TODO: should this use GetNodeHeadingText?
								$title = ($group == 'none') ? 'No Group' : $group;
								echo '"' . $title . "\"\n";
		            $curgroup = $group;
							}

							echo '"' . GetNodeHeadingText($module_out, $show_title) . '",';

							foreach ($body_outcomes as &$body_out)
							{
		            if ($show_unmapped != 'hide' or isset($is_mapped[$body_out->getID()])) {
									$mod_out_weight = $mappings[$module_out->getID()][$body_out->getID()];

									if ($mod_out_weight > 0)
									{
										$weights["$mod_out_weight"] = true;


										if ($render_mode == 'count')
										{
											echo $mod_out_weight;
										} else {
											echo 'X';
										}
									}

									echo ",";
								}
							}
							echo "\n";
						}
					}
				} else {
					if ($mode != 'csv')
					{
						echo "<p>No module to body mappings found</p>";
					} else {
						echo "No module to body mappings found";
					}
				}
			} else {
				echo "No Body outcomes defined\n";
			}
		}

		if ($mode != 'csv') {
			// output additional style information to highlight the required colours
			if ($maxweight > 0) {
				echo "<style type=\"text/css\">";
				$step = 1 / $maxweight;
				foreach (array_keys($weights) as $i)
				{
					$red = 247;
					$green = floor(210 - ($step * 100 * $i));
					$blue = floor(180 - ($step * 180 * $i));

					$class_mod = str_replace('.', '_', $i);
					echo ".heatmap_$class_mod { background-color: rgb($red,$green,$blue) }\n";
				}
			}

			echo "</style>";
?>
<script>
$('.tooltip').qtip({
	position: {
		viewport: true,
		my: 'bottom center',
		at: 'top center'
	},
	show: {
		event: 'click'
	}
});
$('.tooltip-l').qtip({
	position: {
		viewport: true,
		my: 'bottom left',
		at: 'top left'
	},
	show: {
		event: 'click'
	}
});
</script>

<?php
		}
	}
}
