<?php

class Report_learning_activity extends ReportBase
{
	function Process()	
	{
		//echo "<pre>".htmlentities($this->data_xml->asXML())."</pre>";
		
		echo "<h1>" . $this->data_xml->module->title . ": Learning Activity List</h1>";
		
		foreach ($this->data_xml->module->learning_act as $learning_act)
		{
			$attr = $learning_act->attributes();
			echo '<h2><a href="' . site_url('view/' . $attr['id']) . '" target="_blank">' . $learning_act->title . "</a></h2>\n";	
			echo "Description : " . $learning_act->desc . "<br>";
			echo "Type : " . $learning_act->type . "<br>";
			echo "Duration : " . $learning_act->duration . "<br>";
		}
	}
}