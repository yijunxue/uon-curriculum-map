<?php
/* NEEDS CLEANUP */
/* NEEDS COMMENTS */

class Init_CM
{
	public function initset(&$graph_db, $source_table = '')
	{
		$this->ci = get_instance();
		
		$this->graph_db = $graph_db;
		$this->gdb = $this->graph_db->gdb;
		$this->dx_auth = $this->ci->dx_auth;
		
		//print_p($this->graph_db->active_dataset);
		if (empty($this->graph_db->active_dataset->nodes) || !is_object($this->graph_db->active_dataset->nodes))
		{
			$this->graph_db->active_dataset->nodes = new stdClass();	
		}
		
		//load role data
		$this->roledata = array();
		$this->roleids = array();
		$qry = "SELECT * FROM roles";
		$query = $this->gdb->query($qry);
		foreach ($query->result() as $row)
		{
			$this->roledata[$row->id] = $row;
			$this->roleids[] = $row->id;
		}	

		
		// create year node if doesnt exist (base on dataset title)
		$year = $this->createYearNode();
		//$year->dump();
		
		// create Users and Groups node if doesnt exist
		$ung = $this->createUsersAndGroups();
		//$ung->dump();
		
		// create All Users node
		$allusers = $this->createAllUsers($ung, $source_table);
		//$allusers->dump();
			
		// create Role Groups node
		$rolegroup = $this->createRoleBaseNode($ung, $source_table);
		//$rolegroup->dump();
		
		// create all roles nodes	
		$rolenodes = $this->createRoleNodes($rolegroup, $source_table);
		/*foreach ($rolenodes as $node)
			$node->dump();*/
		
		//print_p($this->graph_db->active_dataset);
	}
	
	private function createYearNode()
	{
		$yearnodes = $this->graph_db->firstNodes(1, 1);
		if (count($yearnodes) == 0)
		{
			//print_p($this->graph_db->active_dataset);
			$yearnode = $this->graph_db->createNode($this->graph_db->active_dataset->name, 1, 1);
			//$yearnode->addRelate(1,GDB_Rel::ChildOf);
			$yearnode->save();
			
			$this->graph_db->active_dataset->nodes->year = $yearnode->getId();
			$this->graph_db->update_active_dataset_nodes();
			
			return $yearnode;
		} else {
			return $yearnodes[0];
		}
	}	
	
	private function createUsersAndGroups()
	{
		$ungs = $this->graph_db->firstNodes(1, 500);
		if (count($ungs) == 0)
		{
			//print_p($this->graph_db->active_dataset);
			$ung = $this->graph_db->createNode("Users And Groups", 500, 1);
			//$ung->addRelate(1,GDB_Rel::ChildOf);
			$ung->save();
			
			$this->graph_db->active_dataset->nodes->users_and_groups = $ung->getId();
			$this->graph_db->update_active_dataset_nodes();
			
			return $ung;
		} else {
			return $ungs[0];
		}
	}
	
	function findRelNode(&$rels, $nodetype, $nodetitle)
	{
		//echo "Finding $nodetype -> $nodetitle<br>";
		if (!array_key_exists($nodetype, $rels))
			return null;
		
		foreach ($rels[$nodetype] as &$node)
		{
			if ($node->getTitle() == $nodetitle)
			{
				//echo "Found!<br>";
				return $node;	
			}
		}	
		
		return null;
	}
	
	private function createAllUsers($ung, $source_table)
	{
		$rels = $ung->getRelations_GroupByNodeType();
		
		$name = "All Users";
		$node = $this->findRelNode($rels, 501, $name);
		
		if (!$node)
		{
            if ($source_table != '') {
                $guid = $this->get_role_group_nodes($source_table, 501, $name);
                $allusers = $this->graph_db->createNode($name, 501, $ung, $guid);
            } else {
                //print_p($this->graph_db->active_dataset);
                $allusers = $this->graph_db->createNode($name, 501, $ung);
                //$allusers->addRelate($ung,GDB_Rel::ChildOf);
            }
            $allusers->save();

            $this->graph_db->active_dataset->nodes->all_users = $allusers->getId();
            $this->graph_db->update_active_dataset_nodes();

            return $allusers;
		} else {
			return $node;
		}
	}
	
	private function createRoleBaseNode($ung, $source_table)
	{
		$rels = $ung->getRelations_GroupByNodeType();
		$name = "Role Groups";
		$node = $this->findRelNode($rels, 501, $name);
		
		if (!$node)
		{
			//print_p($this->graph_db->active_dataset);
            if ($source_table != '') {
                $guid = $this->get_role_group_nodes($source_table, 501, $name);
                $rolegroups = $this->graph_db->createNode($name, 501, $ung, $guid);
            } else {
                $rolegroups = $this->graph_db->createNode($name, 501, $ung);
            }
			//$rolegroups->addRelate($ung,GDB_Rel::ChildOf);
			$rolegroups->save();
			
			$this->graph_db->active_dataset->nodes->role_groups = $rolegroups->getId();
			$this->graph_db->update_active_dataset_nodes();
			
			return $rolegroups;
		} else {
			return $node;
		}
	}

	private function createRoleNodes($ung, $source_table)
	{
		$datasetid = $this->graph_db->active_dataset->id;
		
		if (empty($this->graph_db->active_dataset->nodes->role_group))
			$this->graph_db->active_dataset->nodes->role_group = new stdClass();
		
		$role_nodes = array();
		
		$qry = "SELECT * FROM role_node WHERE role IN (" . implode(", ", $this->roleids). ") AND dataset = ?";
		//echo $qry."<br>";
		$query = $this->gdb->query($qry, array($datasetid));
		foreach ($query->result() as $row)
		{
			$role_nodes[$row->role] = $row;
		}
		
		//print_p($role_nodes);
		
		$basenode = null;
		
		$roledata = array();
		
		$rolenodes = array();
				
		foreach ($this->roleids as $roleid)
		{
			if (!array_key_exists($roleid, $role_nodes))
			{
				//echo "Need to create node for role $roleid<br>";	
				
				// if we dont have em loaded, load all nodes (saves a bunch of queries as opposed to loading em 1 at a time, cos chances
				// are, if 1 mis missing then the rest will also be
				$role = $this->roledata[$roleid];
				
                if ($source_table != '') {
                    $guid = $this->get_role_group_nodes($source_table, 501, $role->description);
                    $rolenode = $this->graph_db->createNode($role->description, 501, $ung, $guid);
                } else {
                    $rolenode = $this->graph_db->createNode($role->description, 501, $ung);
                }
				//$rolenode->addRelate($ung, GDB_Rel::ChildOf);
				$rolenode->save();
				
				$qry = "REPLACE INTO role_node (role, dataset, nodeid) VALUES (?, ?, ?)";
				$this->gdb->query($qry, array($roleid, $datasetid, $rolenode->getId()));
				
				$this->graph_db->active_dataset->nodes->role_group->$roleid = $rolenode->getId();
				$rolenodes[] = $rolenode;
			} else {
				$rolenodes[] = $this->graph_db->loadNode($role_nodes[$roleid]->nodeid);	
			}
		}
		$this->graph_db->update_active_dataset_nodes();
		
		return $rolenodes;
		//print_p($roledata);
		//print_p($this->dx_auth->ci->session);
	}

        /**
         * Get graph_node records for the given dataset
         * @param  integer $source_table Database ID of the source dataset
         * @param integer $type type of node
         * @param string $title title of node
         * @return array              Array of user_node records
        */
        private function get_role_group_nodes($source_table, $type, $title) {
            $this->ci->db->select('guid');
            $entries = $this->ci->db->get_where('gdb_' . $source_table . '_graph_node', array('type' => $type, 'title' => $title));
            $row = $entries->row();
            return $row->guid;
        }
}