<?php
/**
 * User: nazrji
 */
abstract class CM_Hook
{
	protected $_ci;                 // CodeIgniter instance

	function __construct()
	{
		$this->_ci = & get_instance();
	}

	abstract public function execute($node);
}
