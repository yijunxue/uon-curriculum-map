<?php
/**
 * User: nazrji
 */

require_once 'cm_hook.php';

class UpdateTimetableSessions extends CM_Hook
{
	public function execute($node) {
		$use_tt = $node->getAttributeValue('ttsync');
		$already_synced = $node->getAttributeValue('ttsynced');

		if ($use_tt and !$already_synced) {
			$this->_ci->load->library('curl');
			$this->_ci->load->helper('curl');
			$this->_ci->load->helper('timetable');

			$tt_sessions_created = 0;

			$moduleid = $node->getAttributeValue('code');

			import_tt_sessions($this, $this->_ci, $node->getID(), $moduleid, $this->_ci->graph_db->active_dataset->table, $tt_sessions_created);

			if ($tt_sessions_created > 0) {
				$node->setAttribute('ttsynced', '1');
				$node->save();
			} else {
				$this->_ci->head->AddMessage("No timetable sessions found");
			}
		}
	}
}
