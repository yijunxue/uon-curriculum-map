<?php
$pageURL = $_SERVER["REQUEST_URI"];

if (strpos($pageURL, "index.php") > 0)
{
	$pageURL = substr($pageURL, 0, strpos($pageURL, "index.php"));
}

if (substr($pageURL, strlen($pageURL) - 1, 1) == "/") {
	$pageURL = substr($pageURL, 0, strlen($pageURL) - 1);
}

$db_config_path = 'base/application/config/database.php';
$config_path = 'base/application/config/config.php';
$ldap_path = 'base/application/config/_uoncm.php';

$missingconfigfiles = 0;

if (!file_exists($db_config_path)) {
	$missingconfigfiles++;
}

if (!file_exists($config_path)) {
	$missingconfigfiles++;
}

if (!file_exists($ldap_path)) {
	$missingconfigfiles++;
}

if ($missingconfigfiles > 0) {
	header("Location: install/index.php");
	die();
}

header("Location: $pageURL/_default/");
die();
