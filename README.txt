University of Nottingham Curriculum Mapping System
--------------------------------------------------

Version: 5.3.0

This is the curriculum mapping system of The University of Nottingham.

The system allows staff to represent the structure of a curriculum in the form of a hierarchical graph. Learning Outcomes may be defined at various levels within the system and mapped to each other in order to represent the way in which higher level outcomes are delivered through the course structure. The reporting sub-system allows reports to be generated inorder to contribute towards, amongst other things, accreditation assessments by accrediting bodies.

Node types include:

* Academic year
* Accrediting body
* Programme
* Module
* Session
* Learning Activity
* Learning Outcome
* Collection (a flexible grouping of nodes)
