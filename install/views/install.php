<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>Install the Curriculum map application</title>
		<link rel="stylesheet" type="text/css" href="css/styles.css" />
	</head>
	<body>
    <center><h1>Curriculum map installation</h1></center>
		<?php if (is_writable($db_config_path)){ ?>
			<?php
			if (isset($message)) {
				echo '<p class="error">' . $message . '</div>';
			}
			?>
			<p>
				<form class="user-form" id="install_form" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
					<fieldset>
						<legend>Settings</legend>
						<legend>Site details</legend>
						<div class="field">
							<label for="hostname">Root URL <br/>(e.g. http://localhost/uon/)</label>
							<input type="text" id="root_url" class="input_text" name="root_url" />
						</div>
						<div class="field">
							<label for="UDX_website_name">Site name</label>
							<input type="text" id="DX_website_name" class="input_text" name="DX_website_name" />
						</div>
                                                <legend>Database settings</legend>
						<div class="field">
							<label for="hostname">Hostname</label>
							<input type="text" id="hostname" value="localhost" class="input_text" name="hostname" />
						</div>
						<div class="field">
							<label for="username">Database Username</label>
							<input type="text" id="username" class="input_text" name="username" />
						</div>
						<div class="field">
							<label for="password">Database Password</label>
							<input type="password" id="password" class="input_text" name="password" />
						</div>
						<div class="field">
							<label for="database">Database Name</label>
							<input type="text" id="database" class="input_text" name="database" />
						</div>
						<legend>Base data</legend>
						<div class="field">
							<label for="datasetyear">Dataset year (e.g. 2014)</label>
							<input type="number" id="datasetyear"  min="2000" max="2100" class="input_text" name="datasetyear" />
						</div>
						<div class="field">
							<label for="datasetstart">Dataset start date<br/>(dd/mm/yyyy)</label>
							<input type="date" id="datasetstart"  min="2000" max="2100" class="input_text" name="datasetstart" />
						</div>
						<div class="field">
							<label for="datasetend">Dataset end date<br/>(dd/mm/yyyy)</label>
							<input type="date" id="datasetend"  min="2000" max="2100" class="input_text" name="datasetend" />
						</div>
						<div class="field">
							<label for="datasetname">Dataset name</label>
							<input type="text" id="datasetname" class="input_text" name="datasetname" />
						</div>
						<div class="field">
							<label for="datasetdescription">Dataset description</label>
							<input type="text" id="datasetdescription" class="input_text" name="datasetdescription" />
						</div>
						<div class="field">
							<label for="datasetcolour">Dataset colour</label>
							<input type="color" id="datasetcolour" value="#ffffff" class="input_text" name="datasetcolour" />
						</div>
						<legend>Administration account</legend>
						<div class="field">
							<label for="adminusername">Admin username</label>
							<input type="text" id="adminusername" class="input_text" name="adminusername" />
						</div>
						<div class="field">
							<label for="adminpassword">Admin password</label>
							<input type="password" id="adminpassword" class="input_text" name="adminpassword" />
						</div>
						<div class="field">
							<label for="DX_webmaster_email">Site administrator's email</label>
							<input type="text" id="DX_webmaster_email" class="input_text" name="DX_webmaster_email" />
						</div>
						<legend>Authentication</legend>
						<div class="field">
							<label for="UONCM_CanResetPass">Enable reset password link?
								<div class="info">Should the reset password link be enabled?</div>
							</label>
							<input type="checkbox" id="UONCM_CanResetPassk" class="input_checkbox" name="UONCM_CanResetPass" value="1"/>
						</div>						
						<div class="field">
							<label for="UONCM_LDAP_Enable">Enable LDAP
								<div class="info">Should LDAP be enabled as an alternate login?</div>
							</label>
							<input type="checkbox" id="enableldap" class="input_checkbox" name="UONCM_LDAP_Enable" value="1"/>
						</div>
						<div class="field">
							<label for="UONCM_LDAP_UsernameFallback">Enable LDAP Username fallback
								<div class="info">Should an unknown username be looked up in the LDAP service. If ldap login is sucessfull, a user will be created</div>
							</label>
							<input type="checkbox" id="UONCM_LDAP_UsernameFallback" class="input_checkbox" name="UONCM_LDAP_UsernameFallback" value="1"/>
						</div>
						<div class="field">
							<label for="UONCM_LDAP_PasswordFallback">Enable LDAP Password fallback
								<div class="info">Should an incorrect password be corrected with a valid ldap login?</div>
							</label>
							<input type="checkbox" id="UONCM_LDAP_PasswordFallback" class="input_checkbox" name="UONCM_LDAP_PasswordFallback" value="1"/>
						</div>
						<div class="field">
							<label for="UONCM_LDAP_Host">LDAP Hostname</label>
							<input type="text" id="UONCM_LDAP_Host" class="input_text" name="UONCM_LDAP_Host" />
						</div>
						<div class="field">
							<label for="UONCM_LDAP_Port">LDAP Port</label>
							<input type="text" id="UONCM_LDAP_Port"  class="input_text" name="UONCM_LDAP_Port" />
						</div>
						<div class="field">
							<label for="UONCM_LDAP_BindRDN">LDAP Bind RDN</label>
							<input type="text" id="UONCM_LDAP_BindRDN"  class="input_text" name="UONCM_LDAP_BindRDN" />
						</div>
						<div class="field">
							<label for="UONCM_LDAP_Password">LDAP Password</label>
							<input type="password" id="UONCM_LDAP_Password" class="input_text" name="UONCM_LDAP_Password" />
						</div>
						<div class="field">
							<label for="UONCM_LDAP_BaseDN">LDAP Base DN</label>
							<input type="text" id="UONCM_LDAP_BaseDN"  class="input_text" name="UONCM_LDAP_BaseDN" />
						</div>
						<div class="field">
							<label for="UONCM_LDAP_UserPrefix">LDAP User prefix</label>
							<input type="text" id="UONCM_LDAP_UserPrefix"  class="input_text" name="UONCM_LDAP_UserPrefix" />
						</div>
						<div class="field">
							<label for="UONCM_LDAP_AuthRealm">LDAP Auth realm</label>
							<input type="text" id="UONCM_LDAP_AuthRealm"  class="input_text" name="UONCM_LDAP_AuthRealm" />
						</div>
						<div class="field">
							<label for="UONCM_InfoURL">LDAP Info URL</label>
							<input type="text" id="UONCM_InfoURL" class="input_text" name="UONCM_InfoURL" />
						</div>
						<legend>Timetable</legend>
						<div class="field">
							<label for="UONCM_TimetableURL">Timetable URL</label>
							<input type="text" id="UONCM_TimetableURL" class="input_text" name="UONCM_TimetableURL" />
						</div>
						<div class="field">
							<label for="UONCM_TimetableAPIKey">Timetable API key</label>
							<input type="text" id="UONCM_TimetableAPIKey" class="input_text" name="UONCM_TimetableAPIKey" />
						</div>
						<legend>SMTP settings</legend>
						<div class="field">
							<label for="smtp_protocol">Protocol</label>
							<select id="smtp_protocol" class="input_text" name="smtp_protocol">
								<option value="ssl">SSL</option>
								<option value="tls">TLS</option>
								<option value="null">Basic</option>
							</select>
						</div>
                                                <div class="field">
							<label for="smtp_host">Host</label>
							<input type="text" id="smtp_host" class="input_text" name="smtp_host" />
						</div>
                                                <div class="field">
							<label for="smtp_port">Port</label>
							<input type="text" id="smtp_port" class="input_text" name="smtp_port" />
						</div>
                                                <div class="field">
							<label for="smtp_timeout">Timeout</label>
							<input type="text" id="smtp_timeout" class="input_text" name="smtp_timeout" />
						</div>
                                                <div class="field">
							<label for="smtp_user">Username</label>
							<input type="text" id="smtp_user" class="input_text" name="smtp_user" />
						</div>
                                                <div class="field">
							<label for="smtp_pass">Password</label>
							<input type="text" id="smtp_pass" class="input_text" name="smtp_pass" />
						</div>
						<div class="field">
							<div class="submitbutton">
								<input type="submit" value="Install" id="submit" />
							</div>
						</div>
					</fieldset>
				</form>
			</p>

		<?php } else { ?>
			<div class="error">Please make the application/config/database.php file writable. <strong>Example</strong>:<br /><br /><code>chmod 775 application/config/database.php</code></div>
			<?php } ?>

	</body>
</html>
