<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

$config['protocol']    = '%emailprotocol%';
$config['smtp_host']    = '%smtphost%';
$config['smtp_port']    = '%smtpport%';
$config['smtp_timeout'] = '%smtptimeout%';
$config['smtp_user']    = '%smtpusername%';
$config['smtp_pass']    = '%smtppassword%';
$config['charset']    = 'utf-8';
$config['newline']    = "\r\n";
$config['mailtype'] = 'text'; // or html
$config['validation'] = TRUE;   