<?php

// uon cm config file
$config['UONCM_LDAP_Enable'] = '%%LDAP_Enable%%'; // is LDAP enabled as an alternate login?
$config['UONCM_LDAP_UsernameFallback'] = '%%UsernameFallback%%'; // can an unknown username be looked up in the LDAP service. If ldap login is sucessfull, a user will be created
$config['UONCM_LDAP_PasswordFallback'] = '%%PasswordFallback%%'; // can an incorrect password be corrected with a valid ldap login?
$config['UONCM_LDAP_Host'] = '%HOSTNAME%';
$config['UONCM_LDAP_Port'] = '%PORT%';
$config['UONCM_LDAP_BindRDN'] = '%BindRDN%';
$config['UONCM_LDAP_Password'] = '%LDAP_Password%';
$config['UONCM_LDAP_BaseDN'] = '%BaseDN%';
$config['UONCM_LDAP_UserPrefix'] = '%UserPrefix%';
$config['UONCM_LDAP_AuthRealm'] = '%AUTHREALM%';
$config['UONCM_InfoURL'] = '%InfoURL%';
$config['UONCM_TimetableURL'] = '%TimetableURL%';
$config['UONCM_CanResetPass'] = '%CanResetPass%'; // should the reset password link be enabled?
