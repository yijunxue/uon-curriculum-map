<?php

// Determine the current dataset to use.
$folder = dirname(__FILE__);
$folder = str_replace("/", "\\", $folder);
$bits = explode("\\", $folder);

// Set the dataset.
global $current_data_set_folder;
$current_data_set_folder = $bits[count($bits) - 1];

// Use the main app from the base dir.
chdir("../base");
require "index.php";
