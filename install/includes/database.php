<?php

/**
 * Initial db connection class for building PDO connection
 * @copyright (c) 2014, University of Nottingham
 * @author Barry Oosthuizen <barry.oosthuizen@nottingham.ac.uk>
 */
class db_connect {

	/**
	 * DB host name
	 * @var string
	 */
	public $host;
	/**
	 * Database name
	 * @var string
	 */
	public $dbname;
	/**
	 * Database user
	 * @var string
	 */
	public $dbuser;
	/**
	 * Database password
	 * @var string
	 */
	public $dbpass;
	/**
	 * Database type
	 * @var string
	 */
	public $dbtype;
	/**
	 * PDO connection options
	 * @var array
	 */
	public $options = array();

	/**
	 * PDO connection object
	 * @var object
	 */
	public $db;

	/**
	 * Class constructor
	 * @param string $host
	 * @param string $dbuser
	 * @param string $dbpass
	 * @param string $dbname
	 * @param array $options
	 */
	public function __construct($host, $dbuser, $dbpass, $dbname = '', $options = array()) {
		$this->host = $host;
		$this->dbname = $dbname;
		$this->dbuser = $dbuser;
		$this->dbpass = $dbpass;
		$this->dsn = "mysql:dbname=$dbname;host=$host";
		$this->options = $options;
		$this->connect();
	}

	/**
	 * DB connection method
	 * @return void
	 */
	private function connect() {
		try {
			$db = new PDO($this->dsn, $this->dbuser, $this->dbpass, $this->options);
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
			$this->db = $db;
		} catch (PDOException $e) {
			echo '<br/>Connection failed: ' . $e->getMessage();
			echo '<br/>' . $e->getTraceAsString();
		}
	}

	/**
	 * Close the PDO connection
	 * @return void
	 */
	public function close() {
		$this->db = null;
	}
}
