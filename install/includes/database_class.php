<?php

require_once('database.php');

/**
 * Class for creating database with initial data
 */
class Database {

	/**
	 * Submitted $_POST data
	 * @var array
	 */
	private $data;

	/**
	 * PDO connection object
	 * @var object
	 */
	private $pdo;

	/**
	 * Class constructor
	 * @param array $data
	 */
	public function __construct($data) {
		$this->data = $data;
		$this->pdo = new db_connect($this->data['hostname'], $this->data['username'], $this->data['password'], '',
			array(PDO::MYSQL_ATTR_LOCAL_INFILE => true, PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true));
	}

	/**
	 * Create the database
	 * @return boolean
	 */
	public function create_database() {
		try {
			$this->pdo->db->exec("CREATE DATABASE `" . $this->data['database'] . "`")
				or die(print_r($this->pdo->errorInfo(), true));
			$this->pdo->db->exec("use `" . $this->data['database'] . "`");
		} catch (PDOException $e) {
			die("DB ERROR: " . $e->getMessage() . $e->getTraceAsString());
		}
		return true;
	}

	/**
	 * Insert initial data required for a new installation
	 * @param string $salt
	 * @return boolean
	 * @throws exception
	 */
	public function insert_data($salt) {
		$year = (int) $this->data['datasetyear'];
		$nextyear = substr($year, -2);
		$today = date('Y-m-d H:i:s');

		if ($this->pdo->db) {
			try {
				$database = "assets/install.sql";
				// Create database tables.
				$this->execute_sql_from_file($database, "####", $this->data['datasetyear']);
				// Create initial dataset.
				$this->create_dataset();
				// Data import SQL files.
				$nodes = "assets/graph_node.sql";
				$nodes_attr = "assets/graph_attr.sql";
				$nodes_rel = "assets/graph_rel.sql";
				$lookups = "assets/lookups.sql";
				$permissions = "assets/permissions.sql";
				$role_node = "assets/role_node.sql";
				$roles = "assets/roles.sql";
				$user_node = "assets/user_node.sql";
				$user_profile = "assets/user_profile.sql";
				$users = "assets/users.sql";

				$this->execute_sql_from_file($nodes, '[yyyy]', $year, '[Year yyyy/yy]', "[Year $year/$nextyear]", '[yyyy-mm-dd] [hh:mm:ss]', $today);
				$this->execute_sql_from_file($nodes_attr, '[yyyy]', $year);
				$this->execute_sql_from_file($nodes_rel, '[yyyy]', $year);
				$this->execute_sql_from_file($lookups);
				$this->execute_sql_from_file($permissions);
				$this->execute_sql_from_file($role_node);
				$this->execute_sql_from_file($roles);
				$this->execute_sql_from_file($user_node);
				$this->execute_sql_from_file($user_profile);
				$this->execute_sql_from_file($users, '[yyyy-mm-dd] [hh:mm:ss]', $today);
				$this->create_initial_migration();
				$this->set_initial_dataset_year($year);
				$this->set_initial_dataset_timeperiod($year);
				$this->set_admin_password($salt);
				$this->pdo->close();
				return true;
			} catch (exception $e) {
				$this->pdo->close();
				echo $e->getMessage();
				return false;
			}
		} else {
			throw new exception("Couldn't connect to database....");
		}
	}

	/**
	 * Insert data into the database based on SQL in a specific file and replace certain values (optional)
	 * @param string $file Path to the SQL file
	 * @param string $replace1
	 * @param string $newvalue1
	 * @param string $replace2
	 * @param string $newvalue2
	 * @param string $replace3
	 * @param string $newvalue3
	 * @throws \Exception
	 */
	private function execute_sql_from_file($file, $replace1 = '', $newvalue1 = '', $replace2 = '', $newvalue2 = '', $replace3 = '', $newvalue3 = '') {
		$sql = file_get_contents($file);

		if (!empty($replace1)) {
			$sql = str_replace($replace1, $newvalue1, $sql);
		}

		if (!empty($replace2)) {
			$sql = str_replace($replace2, $newvalue2, $sql);
		}

		if (!empty($replace3)) {
			$sql = str_replace($replace3, $newvalue3, $sql);
		}

		$this->pdo->db->setAttribute(PDO::ATTR_EMULATE_PREPARES, 0);
		$this->pdo->db->exec($sql);
		if ($this->pdo->db->errorCode() !== '00000') {
			$error = $this->pdo->db->errorInfo();
			throw new Exception($error[0] . ': ' . $error[1] . ' - ' . $error[2]);
		}
	}

	/**
	 * Insert data to define the initial dataset
	 */
	private function create_dataset() {
		$sql = "INSERT INTO datasets
						(`table`, `name`, `description`, `color`, `adminonly`, `nodedef`, `isdefault`, `nodes`)
							VALUES
						(:datasettable, :datasetname, :datasetdescription, :datasetcolour, '0', 'cm', '1',
						'O:8:\"stdClass\":5:{s:4:\"year\";i:2;s:16:\"users_and_groups\";i:3;s:9:\"all_users\";i:4;s:11:\"role_groups\";i:5;s:10:\"role_group\";O:8:\"stdClass\":4:{s:1:\"1\";i:6;s:1:\"2\";i:7;s:1:\"3\";i:8;s:1:\"4\";i:9;}}')
						";
		$sth = $this->pdo->db->prepare($sql);

		$sth->bindParam(':datasettable', $this->data['datasetyear'], PDO::PARAM_INT);
		$sth->bindParam(':datasetname', $this->data['datasetname'], PDO::PARAM_STR);
		$sth->bindParam(':datasetdescription', $this->data['datasetdescription'], PDO::PARAM_STR);
		$sth->bindParam(':datasetcolour', $this->data['datasetcolour'], PDO::PARAM_STR);
		$sth->execute();
		unset($sth);
	}

	/**
	 * Set the intial migration version
	 */
	private function create_initial_migration() {
		$sql = "INSERT INTO migrations (`version`) VALUES	(5)";
		$sth = $this->pdo->db->prepare($sql);
		$sth->execute();
	}

	/**
	 * Set intitial dataset's title
	 * @param int $year
	 */
	private function set_initial_dataset_year($year) {
		$yearstring = $year . '/' . substr($year + 1, 3);
		$sql = "UPDATE gdb_" . $year . "_graph_node SET `title`='Year " . $yearstring . "' WHERE `id`='2'";
		$sth = $this->pdo->db->prepare($sql);
		$sth->execute();
	}

	/**
	 * Set the start and end date for the initial dataset
	 * @param int $year
	 */
	private function set_initial_dataset_timeperiod($year) {
		$sql = "UPDATE gdb_" . $year . "_graph_attr "
			. "SET `value_datetime`='" . $this->data['datasetstart'] . "' "
			. "WHERE `node_type`='1' AND `attrib` = 'start'  AND `type` = 10 AND `node_id` = 2";
		$sth = $this->pdo->db->prepare($sql);
		$sth->execute();

		$sql = "UPDATE gdb_" . $year . "_graph_attr "
			. "SET `value_datetime`='" . $this->data['datasetend'] . "' "
			. "WHERE `node_type`='1' AND `attrib` = 'end' AND `type` = 10 AND `node_id` = 2";
		$sth = $this->pdo->db->prepare($sql);
		$sth->execute();
	}

	/**
	 * Set the admin password
	 * @param string $salt
	 */
	private function set_admin_password($salt) {
		$password = $this->encode($this->data['adminpassword'], $salt);
		$username = $this->data['adminusername'];
		$sql = "UPDATE users SET `password` = '" . crypt($password) . "', `username` = '" . $username . "' WHERE `id` = 1";
		$sth = $this->pdo->db->prepare($sql);
		$sth->execute();
	}

	/**
	 * Encode a password using a salt
	 * Function: _encode
	 * Modified for DX_Auth
	 * Original Author: FreakAuth_light 1.1
	 *
	 * @param string $password
	 * @param string $salt
	 * @return string
	 */
	private function encode($password, $salt) {
		$_pass = str_split($password);
		// encrypts every single letter of the password
		foreach ($_pass as $_hashpass)
		{
			$salt .= md5($_hashpass);
		}
		// encrypts the string combinations of every single encrypted letter
		// and finally returns the encrypted password
		return md5($salt);
	}
}
