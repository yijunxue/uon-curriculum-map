<?php

/**
 * Initial installation class
 */
class Core {

  /**
   * Holds submitted $_POST data
   * @var array 
   */
  private $data;

  /**
   * Pool of characters for producing random salt from
   * @var string 
   */
  private $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

  /**
   * Class constructor
   * @param array $data $_POST data
   */
  public function __construct($data) {
    $this->data = $data;
  }

  /**
   * Function to validate the post data
   * @return bool
   */
  public function validate_post() {
    /* Validating the hostname, the database name and the username. The password is optional. */
    return !empty($this->data['hostname']) && !empty($this->data['username']) && !empty($this->data['database']) && !empty($this->data['datasetyear']) && !empty($this->data['datasetname']) && !empty($this->data['adminusername']) && !empty($this->data['adminpassword']);
  }

  /**
   * Function to show an error
   * @param string $type
   * @param string $message
   * @return string
   */
  public function show_message($type, $message) {
    return $message;
  }

  /**
   * Write the contents of a config file
   * @param string $output_path Path to the new config file
   * @param resource $handle
   * @param string $new Config file content
   * @return boolean
   */
  public function save_new_config_content($output_path, $new) {
    // Write the new database.php file
    $handle = fopen($output_path, 'w+');
    // Chmod the file, in case the user forgot
    @chmod($output_path, 0775);
    // Verify file permissions.
    if (is_writable($output_path)) {
      // Write the file.
      if (fwrite($handle, $new)) {
        return true;
      } else {
        return false;
      }
    } else {
      return false;
    }
  }

  /**
   * Function to write the DB config file
   * @return boolean
   */
  public function write_db_config() {
    // Config path.
    $template_path = 'config/database.php';
    $output_path = '../base/application/config/database.php';
    // Open the file.
    $database_file = file_get_contents($template_path);
    $new = str_replace("%HOSTNAME%", $this->data['hostname'], $database_file);
    $new = str_replace("%USERNAME%", $this->data['username'], $new);
    $new = str_replace("%PASSWORD%", $this->data['password'], $new);
    $new = str_replace("%DATABASE%", $this->data['database'], $new);
    return $this->save_new_config_content($output_path, $new);
  }

  /**
   * Write the email configuration file.
   * @return boolean
   */
  public function write_email_config() {
    // Config path.
    $template_path = 'config/site/email.php';
    $output_path = '../base/application/config/email.php';
    // Open the file.
    $email_file = file_get_contents($template_path);
    $new = str_replace("%emailprotocol%", $this->data['protocol'], $email_file);
    $new = str_replace("%smtphost%", $this->data['smtp_host'], $new);
    $new = str_replace("%smtpport%", $this->data['smtp_port'], $new);
    $new = str_replace("%smtptimeout%", $this->data['smtp_timeout'], $new);
    $new = str_replace("%smtpusername%", $this->data['smtp_user'], $new);
    $new = str_replace("%smtppassword%", $this->data['smtp_pass'], $new);
    return $this->save_new_config_content($output_path, $new);
  }

  /**
   * Write the LDAP config file
   * @return boolean
   */
  public function write_ldap_config() {
    // Config path
    $template_path = 'config/site/_uoncm.php';
    $output_path = '../base/application/config/_uoncm.php';
    // Open the file
    $file = file_get_contents($template_path);

    if (array_key_exists('UONCM_LDAP_Enable', $this->data)) {
      $enableldap = 'true';
    } else {
      $enableldap = 'false';
    }
    $new = str_replace("'%%LDAP_Enable%%'", $enableldap, $file);
    if (array_key_exists('UONCM_LDAP_UsernameFallback', $this->data)) {
      $usernamefallback = 'true';
    } else {
      $usernamefallback = 'false';
    }
    $new = str_replace("'%%UsernameFallback%%'", $usernamefallback, $new);
    if (array_key_exists('UONCM_LDAP_passwordFallback', $this->data)) {
      $passwordfallback = 'true';
    } else {
      $passwordfallback = 'false';
    }
    $new = str_replace("'%%PasswordFallback%%'", $passwordfallback, $new);
    $new = str_replace("%HOSTNAME%", $this->data['UONCM_LDAP_Host'], $new);
    $new = str_replace("%PORT%", $this->data['UONCM_LDAP_Port'], $new);
    if (empty($this->data['UONCM_LDAP_Port'])) {
      $this->data['UONCM_LDAP_Port'] = null;
    }
    $new = str_replace("%BindRDN%", $this->data['UONCM_LDAP_BindRDN'], $new);
    $new = str_replace("%LDAP_Password%", $this->data['UONCM_LDAP_Password'], $new);
    $new = str_replace("%BaseDN%", $this->data['UONCM_LDAP_BaseDN'], $new);
    $new = str_replace("%UserPrefix%", $this->data['UONCM_LDAP_UserPrefix'], $new);
    $new = str_replace("%AUTHREALM%", $this->data['UONCM_LDAP_AuthRealm'], $new);
    $new = str_replace("%InfoURL%", $this->data['UONCM_InfoURL'], $new);
    $new = str_replace("%TimetableURL%", $this->data['UONCM_TimetableURL'], $new);
    $new = str_replace("%TimetableAPIKey%", $this->data['UONCM_TimetableAPIKey'], $new);
    if (array_key_exists('UONCM_CanResetPass', $this->data)) {
      $canresetpassword = 'true';
    } else {
      $canresetpassword = 'false';
    }
    $new = str_replace("'%CanResetPass%'", $canresetpassword, $new);
    return $this->save_new_config_content($output_path, $new);
  }

  /**
   * Write the main config file
   * @return bool
   */
  public function write_config() {
    // Config path
    $template_path = 'config/site/config.php';
    $output_path = '../base/application/config/config.php';
    // Open the file
    $database_file = file_get_contents($template_path);
    $new = str_replace("%%ROOT_URL%%", $this->data['root_url'], $database_file);
    return $this->save_new_config_content($output_path, $new);
  }

  /**
   * Write the dataset index file
   * @return boolean
   */
  public function write_dataset_index() {
    // Config path
    $template_path = 'config/index.php';
    $year = (int) $this->data['datasetyear'];
    $output_path = "../$year";
    // Open the file
    $indexfile = file_get_contents($template_path);
    mkdir($output_path);
    return $this->save_new_config_content($output_path . '/index.php', $indexfile);
  }

  /**
   * Write the dx_auth config file (contains password salt, site name and admin email)
   * @return boolean||string False if the salt could not be written to file otherwise return the salt string.
   */
  public function write_dx_auth_config() {
    // Config path
    $template_path = 'config/site/dx_auth.php';
    $output_path = '../base/application/config/dx_auth.php';
    // Open the file
    $database_file = file_get_contents($template_path);
    $salt = $this->generateRandomString(64);
    $new = str_replace("%%SALT%%", $salt, $database_file);
    $new = str_replace("%%WEBSITENAME%%", $this->data['DX_website_name'], $new);
    $new = str_replace("%%ADMINEMAIL%%", $this->data['DX_webmaster_email'], $new);
    if ($this->save_new_config_content($output_path, $new)) {
      return $salt;
    }
    return false;
  }

  /**
   * Used as part of building random string
   * @param int $num
   * @return string
   */
  private function getBase62Char($num) {
    return $this->chars[$num];
  }

  /**
   * Generate a long random string
   * @param int $nbLetters
   * @return string
   */
  public function generateRandomString($nbLetters) {
    $randString = "";
    for ($i = 0; $i < $nbLetters; $i++)
    {
      $randChar = $this->getBase62Char(mt_rand(0, 61));
      $randString .= $randChar;
    }
    return $randString;
  }

}
