/*
-- Query: SELECT * FROM uoncm.users LIMIT 0, 2
-- Date: 2014-11-26 11:14
*/
INSERT INTO `users` (`id`,`username`,`password`,`email`,`banned`,`ban_reason`,`newpass`,`newpass_key`,`newpass_time`,`last_ip`,`last_login`,`created`,`modified`,`role_id`,`api_key`) VALUES (0,'System','','',0,NULL,NULL,NULL,NULL,'','[yyyy-mm-dd] [hh:mm:ss]','[yyyy-mm-dd] [hh:mm:ss]','[yyyy-mm-dd] [hh:mm:ss]',0,NULL);
INSERT INTO `users` (`id`,`username`,`password`,`email`,`banned`,`ban_reason`,`newpass`,`newpass_key`,`newpass_time`,`last_ip`,`last_login`,`created`,`modified`,`role_id`,`api_key`) VALUES (1,'admin','$1$2u5.hN2.$zxv2Vh4tASl9ZRGjdNnPm1','cm.administrator@nottingham.ac.uk',0,NULL,NULL,NULL,NULL,'127.0.0.1','[yyyy-mm-dd] [hh:mm:ss]','[yyyy-mm-dd] [hh:mm:ss]','[yyyy-mm-dd] [hh:mm:ss]',1,'c7bb45c91441');
