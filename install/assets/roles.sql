/*
-- Query: SELECT * FROM uoncm.roles
LIMIT 0, 1000

-- Date: 2014-11-26 10:37
*/
INSERT INTO `roles` (`id`,`parent_id`,`name`,`description`) VALUES (1,2,'admin','Global Administrator');
INSERT INTO `roles` (`id`,`parent_id`,`name`,`description`) VALUES (2,3,'courseadmin','Course Admin');
INSERT INTO `roles` (`id`,`parent_id`,`name`,`description`) VALUES (3,4,'lecturer','Lecturer');
INSERT INTO `roles` (`id`,`parent_id`,`name`,`description`) VALUES (4,0,'student','Student');
