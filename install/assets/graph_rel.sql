/*
-- Query: SELECT * FROM uoncm.gdb_2012_graph_rel WHERE source_id < 11 AND dest_id < 11
LIMIT 0, 1000

-- Date: 2014-11-25 13:59
*/
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (1,2,2,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (1,3,2,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (2,1,1,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (3,1,1,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (3,4,2,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (3,5,2,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (4,3,1,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (4,10,2,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (5,3,1,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (5,6,2,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (5,7,2,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (5,8,2,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (5,9,2,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (6,5,1,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (6,10,3,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (7,5,1,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (7,10,3,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (8,5,1,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (8,10,3,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (9,5,1,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (9,10,3,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (10,4,1,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (10,6,3,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (10,7,3,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (10,8,3,1);
INSERT INTO `gdb_[yyyy]_graph_rel` (`source_id`,`dest_id`,`type`,`weight`) VALUES (10,9,3,1);
