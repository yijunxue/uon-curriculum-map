<?php

error_reporting(E_ALL);
$db_config_path = '../base/application/config/database.php';
$config_path = '../base/application/config/config.php';
$ldap_path = '../base/application/config/_uoncm.php';
$dx_auth_path = '../base/application/config/dx_auth.php';
$email_path = '../base/application/config/email.php';
if (!$_POST && file_exists($db_config_path) && file_exists($config_path) && file_exists($ldap_path) && file_exists('../index.php')
        && file_exists($email_path)) {
	header('Location: ../index.php');
	die();
}

fopen($db_config_path, "w");
fopen($config_path, "w");
fopen($ldap_path, "w");
fopen($dx_auth_path, "w");
fopen($email_path, "w");

// Only load the classes in case the user submitted the form.
if ($_POST) {

	// Load the classes and create the new objects
	require_once('includes/core_class.php');
	require_once('includes/database_class.php');

	$core = new Core($_POST);
	$database = new Database($_POST);

	// Validate the post data
	if ($core->validate_post() == true) {

		// First create the database, then create tables, then write config file.
		$database->create_database();

		if ($core->write_db_config() == false) {
			$message = $core->show_message('error', "The database configuration file could not be written, please chmod application/config/database.php file to 775");
		}

		if ($core->write_config() == false) {
			$message = $core->show_message('error', "The main configuration file could not be written, please chmod application/config/config.php file to 775");
		}

		if ($core->write_ldap_config() == false) {
			$message = $core->show_message('error', "The LDAP config file could not be written");
		}

		if ($core->write_dataset_index() == false) {
			$message = $core->show_message('error', "The index file in the new dataset directory could not be written");
		}

		if ($core->write_email_config() == false) {
			$message = $core->show_message('error', "The email configuration file could not be written");
		}

		try {
			$salt = $core->write_dx_auth_config();
		} catch (Exception $e) {
			$message = $core->show_message('error', "The dx auth config file could not be written");
		}

		sleep(15);

		$database->insert_data($salt);

		// If no errors, redirect to registration page
		if (!isset($message)) {
			$redir = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
			$redir .= "://" . $_SERVER['HTTP_HOST'];
			$redir .= str_replace(basename($_SERVER['SCRIPT_NAME']), "", $_SERVER['SCRIPT_NAME']);
			$redir = str_replace('install/', '', $redir);
			header('Location: ../index.php');
		}
	} else {
		$message = $core->show_message('error', 'Not all fields have been filled in correctly. The host, username, password, and database name are required.');
	}
}

include 'views/install.php';
